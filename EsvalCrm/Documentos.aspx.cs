﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using Microsoft.Ajax.Utilities;

namespace EsvalCrm
{
    public partial class Documentos : System.Web.UI.Page
    {
        Datos dal = new Datos();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.IsPostBack)
                {
                    BuscarCategoria();
                    Buscar();
                }
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        void Buscar()
        {
            DataTable dt = dal.getBuscarDocumento(0, TxtNombre.Text.Trim(), Convert.ToInt32(DdlCategoria.SelectedValue), TxtPalabraClave.Text.Trim()).Tables[0];
            Session["sessionDtDocumentos"] = dt;
            GrvDocumentos.DataSource = dt;
            GrvDocumentos.DataBind();
        }

        void BuscarCategoria()
        {
            DataTable dt = new DataTable();
            dt = dal.getBuscarCategoriaDocumento(0).Tables[0];
            DdlCategoria.DataSource = dt;
            DdlCategoria.DataTextField = "NOM_CATEGORIA_DOCUMENTO";
            DdlCategoria.DataValueField = "ID_CATEGORIA_DOCUMENTO";
            DdlCategoria.DataBind();
        }

        protected void DdlCategoria_DataBound(object sender, EventArgs e)
        {
            DdlCategoria.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Todos", "0"));
        }

        protected void btnBuscarTicket_Click(object sender, EventArgs e)
        {
            try
            {
                Buscar();
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void GrvDocumentos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label _LblBanner = (Label)e.Row.FindControl("LblBanner");
                CheckBox _ChkBanner = (CheckBox)e.Row.FindControl("ChkBanner");

                if (_LblBanner.Text == "True")
                {
                    _ChkBanner.Checked = true;
                }
                else
                {
                    _ChkBanner.Checked = false;
                }
            }
        }

        protected void LbtnIdDocumento_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                GridViewRow row = (GridViewRow)btn.NamingContainer;
                Label _lblId = (Label)GrvDocumentos.Rows[row.RowIndex].FindControl("LblIdDocumento");
                Response.Redirect("DocumentoIn.aspx?Id=" + _lblId.Text);
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void imgFirst_Click(object sender, EventArgs e)
        {
            //buscar();
            if (Session["sessionDtDocumentos"] != null)
            {
                GrvDocumentos.DataSource = Session["sessionDtDocumentos"];
                GrvDocumentos.DataBind();
            }
            else
            {
                //buscar();
            }
            GrvDocumentos.PageIndex = 0;
            GrvDocumentos.DataBind();
        }

        protected void imgPrev_Click(object sender, EventArgs e)
        {
            //buscar();
            if (Session["sessionDtDocumentos"] != null)
            {
                GrvDocumentos.DataSource = Session["sessionDtDocumentos"];
                GrvDocumentos.DataBind();
            }
            else
            {
                //buscar();
            }
            if (GrvDocumentos.PageIndex != 0)
                GrvDocumentos.PageIndex--;
            GrvDocumentos.DataBind();
        }

        protected void imgNext_Click(object sender, EventArgs e)
        {
            //buscar();
            if (Session["sessionDtDocumentos"] != null)
            {
                GrvDocumentos.DataSource = Session["sessionDtDocumentos"];
                GrvDocumentos.DataBind();
            }
            else
            {
                //buscar();
            }

            if (GrvDocumentos.PageIndex != (GrvDocumentos.PageCount - 1))
                GrvDocumentos.PageIndex++;
            GrvDocumentos.DataBind();
        }

        protected void imgLast_Click(object sender, EventArgs e)
        {
            //buscar();
            if (Session["sessionDtDocumentos"] != null)
            {
                GrvDocumentos.DataSource = Session["sessionDtDocumentos"];
                GrvDocumentos.DataBind();
            }
            else
            {
                //buscar();
            }

            GrvDocumentos.PageIndex = GrvDocumentos.PageCount - 1;
            GrvDocumentos.DataBind();
        }

        protected void btnArchivo_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                GridViewRow row = (GridViewRow)btn.NamingContainer;
                Label _lblArchivo = (Label)GrvDocumentos.Rows[row.RowIndex].FindControl("lblArchivo");
                ScriptManager.RegisterStartupScript(this, this.GetType(), UniqueID, "window.open('" + _lblArchivo.Text + "','_blank');", true);
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void ChkBanner_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                CheckBox btn = (CheckBox)sender;
                GridViewRow row = (GridViewRow)btn.NamingContainer;
                Label _LblIdDocumento = (Label)GrvDocumentos.Rows[row.RowIndex].FindControl("LblIdDocumento");
                Label _LblNombre = (Label)GrvDocumentos.Rows[row.RowIndex].FindControl("LblNombre");
                Label _LblDescripcion = (Label)GrvDocumentos.Rows[row.RowIndex].FindControl("LblDescripcion");
                Label _LblIdCategoria = (Label)GrvDocumentos.Rows[row.RowIndex].FindControl("LblIdCategoria");
                Label _lblArchivo = (Label)GrvDocumentos.Rows[row.RowIndex].FindControl("lblArchivo");
                Label _LblIdUsuarioIngreso = (Label)GrvDocumentos.Rows[row.RowIndex].FindControl("LblIdUsuarioIngreso");
                Label _LblPalabraClave = (Label)GrvDocumentos.Rows[row.RowIndex].FindControl("LblPalabraClave");
                CheckBox _Chk = (CheckBox)GrvDocumentos.Rows[row.RowIndex].FindControl("ChkBanner");
                if (_Chk.Checked)
                {
                    dal.SetEditarDocumento(Convert.ToInt32(_LblIdDocumento.Text), _LblNombre.Text, _LblDescripcion.Text, Convert.ToInt32(_LblIdCategoria.Text), _lblArchivo.Text, Convert.ToInt32(_LblIdUsuarioIngreso.Text), true, 1, _LblPalabraClave.Text);
                }
                else
                {
                    dal.SetEditarDocumento(Convert.ToInt32(_LblIdDocumento.Text), _LblNombre.Text, _LblDescripcion.Text, Convert.ToInt32(_LblIdCategoria.Text), _lblArchivo.Text, Convert.ToInt32(_LblIdUsuarioIngreso.Text), false, 1, _LblPalabraClave.Text);
                }

            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void GrvDocumentos_PreRender(object sender, EventArgs e)
        {
            if (GrvDocumentos.Rows.Count > 0)
            {
                GrvDocumentos.UseAccessibleHeader = true;
                GrvDocumentos.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void BtnNuevo_Click(object sender, EventArgs e)
        {
            Response.Redirect("DocumentoIn.aspx?Id=0");
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                GridViewRow row = (GridViewRow)btn.NamingContainer;

                Label _LblIdDocumento = (Label)GrvDocumentos.Rows[row.RowIndex].FindControl("LblIdDocumento");
                dal.SetEliminarDocumento(Convert.ToInt32(_LblIdDocumento.Text));

                Buscar();
                lblInfo.Text = "Usuario Eliminado Correctamente";
                divAlerta.Attributes["class"] = "alert alert-success";
                divAlerta.Visible = true;
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }
    }
}