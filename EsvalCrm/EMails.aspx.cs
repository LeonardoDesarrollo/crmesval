﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;

namespace EsvalCrm
{
    
    public partial class EMails : System.Web.UI.Page
    {
        Datos dal = new Datos();

        protected void Page_Load(object sender, EventArgs e)
        {
            lblInfo.Text = "";
            divAlerta.Visible = false;

            try
            {
                if (!this.Page.IsPostBack)
                {
                    Session["Emails_Data"] = null;



                    _div_filtros_ddl_tipificado.SelectedValue = "-1";
                    _div_filtros_ddl_spam.SelectedValue = "-1";
                    //_div_filtros_fecha_desde.Text = DateTime.Now.Subtract(new TimeSpan(7, 0, 0, 0)).ToShortDateString();
                    //_div_filtros_fecha_hasta.Text = DateTime.Now.ToShortDateString();
                    DdlCuenta.DataSource = dal.getBuscarEmails(0).Tables[0];
                    DdlCuenta.DataTextField = "EMAIL_UPCOM";
                    DdlCuenta.DataValueField = "EMAIL_UPCOM";
                    DdlCuenta.DataBind();


                    if (Session["EMails_Tipificado"] != null)
                    {
                        _div_filtros_ddl_tipificado.SelectedValue = Session["EMails_Tipificado"].ToString();

                        if (Session["EMails_EMail"] != null && Session["EMails_EMail"].ToString().Length > 5)
                            DdlCuenta.SelectedValue = Session["EMails_EMail"].ToString();

                        Session["EMails_Tipificado"] = null;
                    }

                    if (Session["variableVerTodos"].ToString() == "0")
                    {
                        DdlCuenta.Enabled = false;
                        try
                        {
                            DdlCuenta.SelectedValue = Session["variableEMail"].ToString();
                        }
                        catch { }
                    }

                    string tip = Convert.ToString(Request.QueryString["Tip"]);
                    if (!string.IsNullOrEmpty(tip))
                    {
                        _div_filtros_ddl_tipificado.SelectedValue = tip;
                    }

                    _div_filtros_buscar_Click(sender, e);

                }
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Visible = true;
            }
        }

        protected void _div_filtros_ddl_ejecutivo_DataBound(object sender, EventArgs e)
        {
            ((DropDownList)sender).Items.Insert(0, new ListItem("Todos", ""));
        }

        protected void _div_filtros_buscar_Click(object sender, EventArgs e)
        {
            DataTable dt = dal.OpF_EMail(Convert.ToInt32(_div_filtros_ddl_tipificado.SelectedValue), Convert.ToInt32(_div_filtros_ddl_spam.SelectedValue), _div_filtros_fecha_desde.Text, _div_filtros_fecha_hasta.Text, null, DdlCuenta.SelectedValue);
            Session["Emails_Data"] = dt.Copy();
            _div_grid_emails.DataSource = dt;
            _div_grid_emails.DataBind();

        }

        protected void imgFirst_Click(object sender, EventArgs e)
        {
            if (Session["Emails_Data"] != null)
            {
                _div_grid_emails.DataSource = Session["Emails_Data"];
                _div_grid_emails.DataBind();
                _div_grid_emails.PageIndex = 0;
                _div_grid_emails.DataBind();
            }
        }

        protected void imgPrev_Click(object sender, EventArgs e)
        {
            if (Session["Emails_Data"] != null)
            {
                _div_grid_emails.DataSource = Session["Emails_Data"];
                _div_grid_emails.DataBind();

                if (_div_grid_emails.PageIndex != 0)
                    _div_grid_emails.PageIndex--;
                _div_grid_emails.DataBind();
            }


        }

        protected void imgNext_Click(object sender, EventArgs e)
        {
            if (Session["Emails_Data"] != null)
            {
                _div_grid_emails.DataSource = Session["Emails_Data"];
                _div_grid_emails.DataBind();

                if (_div_grid_emails.PageIndex != (_div_grid_emails.PageCount - 1))
                    _div_grid_emails.PageIndex++;
                _div_grid_emails.DataBind();
            }
        }

        protected void imgLast_Click(object sender, EventArgs e)
        {
            if (Session["Emails_Data"] != null)
            {
                _div_grid_emails.DataSource = Session["Emails_Data"];
                _div_grid_emails.DataBind();
                _div_grid_emails.PageIndex = _div_grid_emails.PageCount - 1;
                _div_grid_emails.DataBind();
            }
        }

        protected void _div_grid_emails_spam_btn_Click(object sender, EventArgs e)
        {
            ((Label)((GridViewRow)((LinkButton)sender).NamingContainer).FindControl("_div_grid_emails_spam")).Text = "1";
            ((LinkButton)((GridViewRow)((LinkButton)sender).NamingContainer).FindControl("_div_grid_emails_uspam_btn")).Visible = true;
            ((LinkButton)sender).Visible = false;
            string Id_Email = ((Label)((GridViewRow)((LinkButton)sender).NamingContainer).FindControl("_div_grid_emails_id")).Text;
            dal.Op_Email(Datos.enum_Op_Email.OnlySPAM, 1, Convert.ToInt32(Id_Email), null, null, null, null, null, null);
        }

        protected void _div_grid_emails_tipificar_btn_Click(object sender, EventArgs e)
        {
            string Id_Email = ((Label)((GridViewRow)((LinkButton)sender).NamingContainer).FindControl("_div_grid_emails_id")).Text;
            //string De_Email = ((Label)((GridViewRow)((LinkButton)sender).NamingContainer).FindControl("_div_grid_emails_from")).Text;
            //string Asunto_EMail = ((Label)((GridViewRow)((LinkButton)sender).NamingContainer).FindControl("_div_grid_emails_subject")).Text;
            //string strhtmlTostr = HtmlToPlainText(((Label)((GridViewRow)((LinkButton)sender).NamingContainer).FindControl("_div_grid_emails_body")).Text);

            //strhtmlTostr = strhtmlTostr.Replace(" ", "nnn");
            //strhtmlTostr = Regex.Replace(strhtmlTostr, @"[^\w\.@-]", "", RegexOptions.None, TimeSpan.FromSeconds(1));
            //strhtmlTostr = strhtmlTostr.Replace("nnn", " ");

            //Session["Email_vId_Observacion"] = strhtmlTostr;
            //Session["Email_vId_Email"] = Id_Email;
            //Session["Email_vId_Email_Asunto"] = Asunto_EMail;
            //Session["Email_vId_Email_De"] = De_Email;
            Response.Redirect("IngresarTicket.aspx?IdEmail=" + Id_Email);
        }

        private string HtmlToPlainText(string html)
        {
            const string tagWhiteSpace = @"(>|$)(\W|\n|\r)+<";//matches one or more (white space or line breaks) between '>' and '<'
            const string stripFormatting = @"<[^>]*(>|$)";//match any character between '<' and '>', even when end tag is missing
            const string lineBreak = @"<(br|BR)\s{0,1}\/{0,1}>";//matches: <br>,<br/>,<br />,<BR>,<BR/>,<BR />
            var lineBreakRegex = new Regex(lineBreak, RegexOptions.Multiline);
            var stripFormattingRegex = new Regex(stripFormatting, RegexOptions.Multiline);
            var tagWhiteSpaceRegex = new Regex(tagWhiteSpace, RegexOptions.Multiline);

            var text = html;
            //Decode html specific characters
            text = System.Net.WebUtility.HtmlDecode(text);
            //Remove tag whitespace/line breaks
            text = tagWhiteSpaceRegex.Replace(text, "><");
            //Replace <br /> with line breaks
            text = lineBreakRegex.Replace(text, Environment.NewLine);
            //Strip formatting
            text = stripFormattingRegex.Replace(text, string.Empty);

            return text;
        }

        protected void _div_grid_emails_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label _lbl_SPAM = (Label)e.Row.FindControl("_div_grid_emails_spam");
                Label _lbl_Tipificar = (Label)e.Row.FindControl("_div_grid_emails_tipificar");

                LinkButton _btn_Spam = (LinkButton)e.Row.FindControl("_div_grid_emails_spam_btn");
                LinkButton _btn_uSpam = (LinkButton)e.Row.FindControl("_div_grid_emails_uspam_btn");
                LinkButton _btn_Tipificar = (LinkButton)e.Row.FindControl("_div_grid_emails_tipificar_btn");
                Label _div_grid_emails_body = (Label)e.Row.FindControl("_div_grid_emails_body");


                if (string.IsNullOrEmpty(_lbl_SPAM.Text) == false && _lbl_SPAM.Text == "1")
                {
                    _btn_Spam.Visible = false;
                    _btn_uSpam.Visible = true;
                }
                else
                {
                    _btn_uSpam.Visible = false;
                    _btn_Spam.Visible = true;
                }

                if (string.IsNullOrEmpty(_lbl_Tipificar.Text) == false && _lbl_Tipificar.Text == "1")
                    _btn_Tipificar.Visible = false;
                else
                    _btn_Tipificar.Visible = true;

            }

            if (e.Row.RowType == DataControlRowType.Pager)
            {
                Label _lblPagina = (Label)e.Row.FindControl("lblPagina");
                Label _lblTotal = (Label)e.Row.FindControl("lblTotal");
                Label _lblTotalRegistros = (Label)e.Row.FindControl("lblTotalRegistros");
                _lblPagina.Text = Convert.ToString(_div_grid_emails.PageIndex + 1);
                _lblTotal.Text = Convert.ToString(_div_grid_emails.PageCount);
                DataTable dt = new DataTable();
                dt = Session["Emails_Data"] as DataTable;
                _lblTotalRegistros.Text = dt.Rows.Count.ToString();

            }
        }

        protected void _div_grid_emails_uspam_btn_Click(object sender, EventArgs e)
        {

            ((Label)((GridViewRow)((LinkButton)sender).NamingContainer).FindControl("_div_grid_emails_spam")).Text = "0";
            ((LinkButton)((GridViewRow)((LinkButton)sender).NamingContainer).FindControl("_div_grid_emails_spam_btn")).Visible = true;
            ((LinkButton)sender).Visible = false;
            string Id_Email = ((Label)((GridViewRow)((LinkButton)sender).NamingContainer).FindControl("_div_grid_emails_id")).Text;
            dal.Op_Email(Datos.enum_Op_Email.OnlySPAM, 0, Convert.ToInt32(Id_Email), null, null, null, null, null, null);

        }

        protected void _div_grid_emails_subjectbtn_Click(object sender, EventArgs e)
        {
            _iw_modal_pEmailv_txt.Text = ((Label)((LinkButton)sender).NamingContainer.FindControl("_div_grid_emails_body")).Text;

            ScriptManager.RegisterStartupScript(_iw_modal_pEmailv, _iw_modal_pEmailv.GetType(), "show", "$(function () { $('#" + _iw_modal_pEmail.ClientID + "').modal('show'); });", true);
            _iw_modal_pEmailv.Update();

        }

        protected void _div_grid_emails_PreRender(object sender, EventArgs e)
        {
            if (_div_grid_emails.Rows.Count > 0)
            {
                _div_grid_emails.UseAccessibleHeader = true;
                _div_grid_emails.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void DdlCuenta_DataBound(object sender, EventArgs e)
        {
            ((DropDownList)sender).Items.Insert(0, new ListItem("Todos", ""));
        }
    }
}