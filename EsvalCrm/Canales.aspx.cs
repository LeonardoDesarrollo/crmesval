﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;

namespace EsvalCrm
{
   
    public partial class Canales : System.Web.UI.Page
    {
        Datos dal = new Datos();
        protected void Page_Load(object sender, EventArgs e)
        {
            lblInfo.Text = "";
            divAlerta.Visible = false;

            try
            {
                if (!Page.IsPostBack)
                {
                    buscar();
                }
            }
            catch (Exception ex)
            {

                lblInfo.Text = ex.Message;
                divAlerta.Visible = true;
            }
        }


        protected void btnEditar_Click(object sender, EventArgs e)
        {

            try
            {
                LinkButton btn = (LinkButton)sender;
                GridViewRow row = (GridViewRow)btn.NamingContainer;
                Label _lblIdCanal = (Label)grvCanales.Rows[row.RowIndex].FindControl("lblIdCanal");
                Label _lblCanal = (Label)grvCanales.Rows[row.RowIndex].FindControl("lblCanal");

                Label _txtAdicional1 = (Label)grvCanales.Rows[row.RowIndex].FindControl("lblAdicional1");
                Label _txtAdicional2 = (Label)grvCanales.Rows[row.RowIndex].FindControl("lblAdicional2");
                Label _txtAdicional3 = (Label)grvCanales.Rows[row.RowIndex].FindControl("lblAdicional3");
                Label _txtAdicional4 = (Label)grvCanales.Rows[row.RowIndex].FindControl("lblAdicional4");

                CheckBox _chkAdicional1 = (CheckBox)grvCanales.Rows[row.RowIndex].FindControl("chkAdicional1");
                CheckBox _chkAdicional2 = (CheckBox)grvCanales.Rows[row.RowIndex].FindControl("chkAdicional2");
                CheckBox _chkAdicional3 = (CheckBox)grvCanales.Rows[row.RowIndex].FindControl("chkAdicional3");
                CheckBox _chkAdicional4 = (CheckBox)grvCanales.Rows[row.RowIndex].FindControl("chkAdicional4");

                Label _lblActivo = (Label)grvCanales.Rows[row.RowIndex].FindControl("lblActivo");

                hfIdCanal.Value = _lblIdCanal.Text;

                txtCanal.Text = _lblCanal.Text;
                txtAdicional1.Text = _txtAdicional1.Text;
                txtAdicional2.Text = _txtAdicional2.Text;
                txtAdicional3.Text = _txtAdicional3.Text;
                txtAdicional4.Text = _txtAdicional4.Text;


                chkAdicional1.Checked = _chkAdicional1.Checked;
                chkAdicional2.Checked = _chkAdicional2.Checked;
                chkAdicional3.Checked = _chkAdicional3.Checked;
                chkAdicional4.Checked = _chkAdicional4.Checked;


                if (_lblActivo.Text != string.Empty)
                {
                    if (_lblActivo.Text == "False")
                        ddlActivo.SelectedValue = "0";
                    else
                        ddlActivo.SelectedValue = "1";
                }
                divCanales.Visible = false;
                divAddEditCanales.Visible = true;
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }

        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                GridViewRow row = (GridViewRow)btn.NamingContainer;

                Label _lblIdCanal = (Label)grvCanales.Rows[row.RowIndex].FindControl("lblIdCanal");
                dal.setEliminarCanal(Convert.ToInt32(_lblIdCanal.Text));

                lblInfo.Text = "Canal Eliminado Correctamente";
                divAlerta.Attributes["class"] = "alert alert-success";
                divAlerta.Visible = true;
                buscar();

            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }
        public void buscar()
        {
            DataTable dt = new DataTable();
            grvCanales.DataSource = dal.getBuscarCanal(null);
            grvCanales.DataBind();
            grvCanales.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            try
            {
                divCanales.Visible = false;
                limpiar();
                divAddEditCanales.Visible = true;
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }
        public void limpiar()
        {
            txtCanal.Text = "";
            txtAdicional1.Text = "";
            txtAdicional2.Text = "";
            txtAdicional3.Text = "";
            txtAdicional4.Text = "";

            chkAdicional1.Checked = false;
            chkAdicional2.Checked = false;
            chkAdicional3.Checked = false;
            chkAdicional4.Checked = false;

            hfIdCanal.Value = string.Empty;
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            string mensaje;
            try
            {

                mensaje = string.Empty;
                if (hfIdCanal.Value != string.Empty)
                {
                    dal.setEditarCanal(Convert.ToInt32(hfIdCanal.Value), txtCanal.Text, txtAdicional1.Text, txtAdicional2.Text,
                    txtAdicional3.Text, txtAdicional4.Text, ddlActivo.SelectedValue,
                    chkAdicional1.Checked, chkAdicional2.Checked, chkAdicional3.Checked, chkAdicional4.Checked);
                    mensaje = "Canal Editado Correctamente";
                }
                else
                {
                    dal.setIngresarCanal(txtCanal.Text, txtAdicional1.Text, txtAdicional2.Text, txtAdicional3.Text,
                   txtAdicional4.Text, ddlActivo.SelectedValue, chkAdicional1.Checked, chkAdicional2.Checked, chkAdicional3.Checked
                   , chkAdicional4.Checked);
                    mensaje = "Canal Agregado Correctamente";
                }

                buscar();
                divCanales.Visible = true;
                divAddEditCanales.Visible = false;

                lblInfo.Text = mensaje;
                divAlerta.Attributes["class"] = "alert alert-success";
                divAlerta.Visible = true;
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            divCanales.Visible = true;
            limpiar();
            divAddEditCanales.Visible = false;
        }
    }
}