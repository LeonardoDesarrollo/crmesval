﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdmTelefonosIn.aspx.cs" Inherits="EsvalCrm.AdmTelefonosIn" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Teléfono</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                             <li class="breadcrumb-item"><a href="#">Administración</a></li>
                        <li class="breadcrumb-item"><a href="#">Mantenedores</a></li>
                        <li class="breadcrumb-item active">Teléfonos</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <div class="content">
        <div class="container-fluid">

            <!-- Alertas -->
            <div id="divAlerta" runat="server" visible="false" class="alert alert-danger">
                <strong>Atención!: </strong>
                <asp:Label Text="" ID="lblInfo" runat="server" />
            </div>
            <asp:HiddenField ID="HdId" runat="server" />
            <div class="card card-secondary">
                <div class="card-header">
                    Teléfono
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <label class="input-group-text">ID:</label>
                                </div>
                                <asp:TextBox ID="TxtId" runat="server" CssClass="form-control" Enabled="false" placeholder="ID"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <label class="input-group-text">Nombre:</label>
                                </div>
                                <asp:TextBox ID="TxtNombre" runat="server" CssClass="form-control" placeholder="Nombre"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <label class="input-group-text">Teléfono:</label>
                                </div>
                                <asp:TextBox ID="TxtTelefono" runat="server" MaxLength="9" CssClass="form-control" placeholder="Teléfono"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <asp:Button ID="BtnGrabar" runat="server" Text="Guardar" CssClass="btn btn-primary" OnClick="BtnGrabar_Click" />
                                <asp:Button ID="BtnVolver" runat="server" Text="Ir a Teléfonos" CssClass="btn btn-default" OnClick="BtnVolver_Click" />
                </div>
            </div>

        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="server">
</asp:Content>
