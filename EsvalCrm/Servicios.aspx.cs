﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;

namespace EsvalCrm
{
    public partial class Servicios : System.Web.UI.Page
    {
        Datos dal = new Datos();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.Page.IsPostBack)
                {
                    Buscar();
                }
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Visible = true;
            }
        }

        void Buscar()
        {
            GrvServicios.DataSource = dal.GetBuscarServicio(0, null);
            GrvServicios.DataBind();
        }
        protected void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("ServicioIn.aspx?Id=0");
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-warning";
                divAlerta.Visible = true;

            }
        }

        protected void btnEditar_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                GridViewRow row = (GridViewRow)btn.NamingContainer;

                Label _LblIdServicio = (Label)GrvServicios.Rows[row.RowIndex].FindControl("LblIdServicio");
                Response.Redirect("ServicioIn.aspx?Id=" + _LblIdServicio.Text);
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-warning";
                divAlerta.Visible = true;

            }
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                GridViewRow row = (GridViewRow)btn.NamingContainer;

                Label _LblIdServicio = (Label)GrvServicios.Rows[row.RowIndex].FindControl("LblIdServicio");
                dal.setEliminarServicio(Convert.ToInt32(_LblIdServicio.Text));

                Buscar();

                lblInfo.Text = "Servicio Eliminado Correctamente";
                divAlerta.Attributes["class"] = "alert alert-success";
                divAlerta.Visible = true;

            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-warning";
                divAlerta.Visible = true;

            }
        }

        protected void GrvServicios_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label _lblActivo = (Label)e.Row.FindControl("lblActivo");
                if (_lblActivo.Text == "1")
                {
                    _lblActivo.Text = "SI";
                }
                else if (_lblActivo.Text == "0")
                {
                    _lblActivo.Text = "NO";
                }
                else
                {
                    _lblActivo.Text = "NO";
                }

            }
        }
    }
}