﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Respuestas.aspx.cs" Inherits="EsvalCrm.Respuestas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Respuestas</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                             <li class="breadcrumb-item"><a href="#">Administración</a></li>
                        <li class="breadcrumb-item"><a href="#">Mantenedores</a></li>
                        <li class="breadcrumb-item active">Respuestas</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <!-- Alertas -->
                    <div id="divAlerta" runat="server" visible="false" class="alert alert-danger">
                        <strong>Atención!: </strong>
                        <asp:Label Text="" ID="lblInfo" runat="server" />
                    </div>

                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <asp:LinkButton ID="LbtnNuevoRespuesta" runat="server" OnClick="LbtnNuevoRespuesta_Click" ForeColor="White"  Text="Nueva Respuesta" CssClass="btn btn-primary btn-sm"></asp:LinkButton>
                            <asp:ImageButton ID="ibtnExportarExcel" ImageUrl="~/img/file_extension_xls.png" runat="server" OnClick="ibtnExportarExcel_Click" />
                        </div>
                        <div class="card-body">
                            <asp:GridView ID="Grv" runat="server" CssClass="table table-bordered table-sm text-sm datatable" EmptyDataText="No hay registros"  AutoGenerateColumns="false" BorderColor="Transparent" OnPreRender="Grv_PreRender">
                                <Columns>
                                    <asp:TemplateField HeaderText="Id">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LbtnId" runat="server" data-toggle="tooltip" data-placement="top" ToolTip='Ir al detalle' Text='<%# Bind("ID") %>' ForeColor="White" CssClass="btn btn-xs btn-success" OnClick="LbtnId_Click"></asp:LinkButton>
                                            <asp:Label ID="LblId" runat="server" Visible="false" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Tipología">
                                        <ItemTemplate>
                                            <asp:Label ID="LblTipologia" runat="server" Visible="true" Text='<%# Bind("Tipologia") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Tema">
                                        <ItemTemplate>
                                            <asp:Label ID="LblTema" runat="server" Visible="true" Text='<%# Bind("Tema") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SubTema">
                                        <ItemTemplate>
                                            <asp:Label ID="LblSubTema" runat="server" Visible="true" Text='<%# Bind("SubTema") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Palabras Claves">
                                        <ItemTemplate>
                                            <asp:Label ID="LblPalabrasClaves" runat="server" Visible="true" Text='<%# Bind("PALABRAS_CLAVES") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Pregunta">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LbtnPregunta" runat="server" ForeColor="White" data-toggle="tooltip" data-placement="top" ToolTip='<%# Bind("Pregunta") %>' CssClass="btn btn-warning btn-xs" Text="Pregunta" OnClick="LbtnPregunta_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Respuesta">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LbtnRespuesta" runat="server" ForeColor="White" data-toggle="tooltip" data-placement="top" ToolTip='<%# Bind("Respuesta") %>' CssClass="btn btn-primary btn-xs" Text="Respuesta" OnClick="LbtnRespuesta_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-Width="7%">
                                        <ItemTemplate>
                                            <%--<asp:LinkButton ID="btnEditar" OnClick="btnEditar_Click" runat="server" CssClass="btn btn-xs btn-primary"><span class="glyphicon glyphicon-edit"></span></asp:LinkButton>--%>
                                            <asp:LinkButton ID="btnEliminar" OnClick="btnEliminar_Click" ForeColor="White" OnClientClick="return confirm('¿Desea eliminar el registro?');" ToolTip="Eliminar" runat="server" CssClass="btn btn-xs btn-danger"><span class="fa fa-eraser"></span></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>

                            </asp:GridView>
                        </div>
                    </div>

                    <!-- Modal bounceIn animated-->
                    <div class="modal" id="myModalFormulario" tabindex="-1" role="dialog" aria-labelledby="myModalLabelFormulario" aria-hidden="true">
                        <div class="modal-dialog modal-md ">
                            <div class="modal-content ">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="myModalLabelFormulario"></h4>
                                </div>
                                <div class="modal-body">
                                    <asp:TextBox ID="TxtTexto" runat="server" TextMode="MultiLine" Height="200px" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                </div>
                            </div>
                            <!-- modal-content -->
                        </div>
                        <!-- modal-dialog -->
                    </div>
                    <!-- modal -->
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="server">
      <script>
      

        function MostrarModal() {
            $('#myModalFormulario').modal('show');
        }
    </script>
</asp:Content>
