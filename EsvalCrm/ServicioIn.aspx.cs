﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;

namespace EsvalCrm
{
    public partial class ServicioIn : System.Web.UI.Page
    {
        Datos dal = new Datos();
        Comun com = new Comun();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lblInfo.Text = "";
                divAlerta.Visible = false;
                if (!this.Page.IsPostBack)
                {

                    int Id = Convert.ToInt32(this.Request.QueryString["Id"]);
                    HfId.Value = Id.ToString();
                    Script();
                    if (Id != 0)
                    {
                        DivNotificaciones.Visible = true;
                        Buscar(Id);
                    }
                    else
                    {
                        DivNotificaciones.Visible = false;
                        lblIdServicio.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Visible = true;
            }
        }

        void Buscar(int Id)
        {
            DataTable dt = new DataTable();
            dt = dal.GetBuscarServicio(Id).Tables[0];
            foreach (DataRow item in dt.Rows)
            {
                HfId.Value = item["ID_SERVICIO"].ToString();
                lblIdServicio.Visible = true;
                lblIdServicio.Text = "ID Servicio: " + HfId.Value;

                TxtServicio.Text = item["SERVICIO"].ToString();

                if (!string.IsNullOrEmpty(item["ID_SCRIPT"].ToString()))
                {
                    DdlScript.SelectedValue = item["ID_SCRIPT"].ToString();
                }
                ddlActivo.SelectedValue = item["ACTIVO"].ToString();

                TxtIdSoftCall.Text = item["ID_SOFTCALL"].ToString();
            }

            DataTable dtServicioNoti = new DataTable();
            dtServicioNoti = dal.GetBuscarServicioNotiticacion(Id, 0).Tables[0];
            GrvNofificacion.DataSource = dtServicioNoti;
            GrvNofificacion.DataBind();
        }

        void Script()
        {
            DdlScript.DataSource = dal.GetBuscarScript(0);
            DdlScript.DataTextField = "NOMBRE_SCRIPT";
            DdlScript.DataValueField = "ID_SCRIPT";
            DdlScript.DataBind();
        }

        protected void DdlScript_DataBound(object sender, EventArgs e)
        {
            DdlScript.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(TxtServicio.Text.Trim()))
                {
                    lblInfo.Text = "El servicio no puede estar vacío.";
                    divAlerta.Attributes["class"] = "alert alert-danger";
                    divAlerta.Visible = true;
                    return;
                }

                if (HfId.Value == "0")
                {
                    dal.setIngresarServicio(TxtServicio.Text.Trim(), DdlScript.SelectedValue, ddlActivo.SelectedValue, TxtIdSoftCall.Text.Trim());
                }
                else
                {
                    dal.setEditarServicio(HfId.Value, TxtServicio.Text.Trim(), DdlScript.SelectedValue, ddlActivo.SelectedValue, TxtIdSoftCall.Text.Trim());
                }

                Response.Redirect("Servicios.aspx");
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Visible = true;
            }
        }

        protected void btnGuardarDestinatario_Click(object sender, EventArgs e)
        {
            try
            {
                string nombre = TxtNombre.Text.Trim();
                string email = TxtEmail.Text.Trim();
                string telefono = TxtTelefono.Text.Trim();

                if (string.IsNullOrEmpty(nombre))
                {
                    lblInfo.Text = "El Nombre Destinatario es obligatorio.";
                    divAlerta.Attributes["class"] = "alert alert-warning";
                    divAlerta.Visible = true;
                    return;
                }

                if (string.IsNullOrEmpty(email) && string.IsNullOrEmpty(telefono))
                {
                    lblInfo.Text = "Debe ingresar al menos un Email o un Teléfono.";
                    divAlerta.Attributes["class"] = "alert alert-warning";
                    divAlerta.Visible = true;
                    return;
                }

                if (!string.IsNullOrEmpty(email))
                {
                    if (!com.IsValidEmail(email))
                    {
                        lblInfo.Text = "El formato de Email no es válido.";
                        divAlerta.Attributes["class"] = "alert alert-warning";
                        divAlerta.Visible = true;
                        return;
                    }
                }

                if (!string.IsNullOrEmpty(telefono))
                {
                    if (!com.EsNumerico(telefono))
                    {
                        lblInfo.Text = "El Teléfono tiene que ser numérico.";
                        divAlerta.Attributes["class"] = "alert alert-warning";
                        divAlerta.Visible = true;
                        return;
                    }

                    if (telefono.Length != 9)
                    {
                        lblInfo.Text = "El largo del Teléfono tiene que ser de 9 dígitos.";
                        divAlerta.Attributes["class"] = "alert alert-warning";
                        divAlerta.Visible = true;
                        return;
                    }
                }

                string valor = dal.setIngresarServicioNotiticacion(Convert.ToInt32(HfId.Value), nombre, email, telefono);

                if (valor != "1")
                {
                    lblInfo.Text = "El Nombre Destinatario ya existe para el Servicio.";
                    divAlerta.Attributes["class"] = "alert alert-warning";
                    divAlerta.Visible = true;
                    return;
                }

                DataTable dtServicioNoti = new DataTable();
                dtServicioNoti = dal.GetBuscarServicioNotiticacion(Convert.ToInt32(HfId.Value), 0).Tables[0];
                GrvNofificacion.DataSource = dtServicioNoti;
                GrvNofificacion.DataBind();
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Visible = true;
            }
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                GridViewRow row = (GridViewRow)btn.NamingContainer;

                Label _LblIdServicioNoti = (Label)GrvNofificacion.Rows[row.RowIndex].FindControl("LblIdServicioNoti");
                Label _LblIdentificador = (Label)GrvNofificacion.Rows[row.RowIndex].FindControl("LblIdentificador");

                dal.setEliminarServicioNotiticacion(Convert.ToInt32(_LblIdServicioNoti.Text), Convert.ToInt32(_LblIdentificador.Text));

                DataTable dtServicioNoti = new DataTable();
                dtServicioNoti = dal.GetBuscarServicioNotiticacion(Convert.ToInt32(HfId.Value), 0).Tables[0];
                GrvNofificacion.DataSource = dtServicioNoti;
                GrvNofificacion.DataBind();
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Visible = true;
            }
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("Servicios.aspx");
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Visible = true;
            }
        }
    }
}