﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;

namespace EsvalCrm
{
    public partial class Telefonos : System.Web.UI.Page
    {
        Datos dal = new Datos();
        TelefonoAccess DalTel = new TelefonoAccess();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.Page.IsPostBack)
                {
                    Buscar();
                }
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        void Buscar()
        {
            Grv.DataSource = DalTel.GetTelefonos();
            Grv.DataBind();
        }

        protected void Grv_PreRender(object sender, EventArgs e)
        {
            if (Grv.Rows.Count > 0)
            {
                Grv.UseAccessibleHeader = true;
                Grv.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }
    }
}