﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using System.Configuration;

namespace EsvalCrm
{
    static class Globales
    {
        public static bool rAdicional1;
        public static bool rAdicional2;
        public static bool rAdicional3;
        public static bool rAdicional4;

        public static void Inicializar()
        {
            rAdicional1 = false;
            rAdicional2 = false;
            rAdicional3 = false;
            rAdicional4 = false;

        }
    }

    public partial class IngresarTicket : System.Web.UI.Page
    {
        Datos dal = new Datos();
        Comun com = new Comun();
        //DatosOracle datos = new DatosOracle();
        string sistema = ConfigurationManager.AppSettings["Sistema"];

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                divAlerta.Visible = false;
               
                ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(this.btnGrabar);

                //2022-01-14 Comentado
                //btnGrabar.Attributes.Add("OnClick", string.Format("this.disabled = true; {0};", ClientScript.GetPostBackEventReference(btnGrabar, null)));

                if (!this.Page.IsPostBack)
                {
                    string idUsuario = Session["variableIdUsuario"].ToString();
                    string idPerfil = Session["variableIdPerfil"].ToString();

                    Servicio();
                    canal();
                    Emisor();
                    nivel1();
                    Comuna();
                    Socio();
                    //IngresarTicket.aspx?id_Cliente=141801873&id_agente=jrodriguez&ani=224804444&id_Tipificacion=1&id_local=1

                    if (idPerfil == "3")
                    {
                        //ddlServicio.Enabled = false;
                        //string idEmpresa = Session["variableIdEmpresa"].ToString();
                        //ddlServicio.SelectedValue = idEmpresa;
                    }
                    else
                    {
                        ddlServicio.Enabled = true;
                    }

                    //JRL 2018-09-28
                    adiconales(Convert.ToInt32(ddlCanal.SelectedValue));
                    divAdicioanl.Visible = false;

                    string _IdLlamada = Convert.ToString(Request.QueryString["IdLlamada"]);
                    string _telefonoIVR = Convert.ToString(Request.QueryString["IdAny"]);
                    //string _rutSinGuion = Convert.ToString(Request.QueryString["rut"]);//Rut completo sin guion
                    //string _rutConGuion = Convert.ToString(Request.QueryString["rut"]);//Rut completo sin guion
                    string _Rut = Convert.ToString(Request.QueryString["Rut"]);
                    string _Dv = Convert.ToString(Request.QueryString["Dv"]);
                    string _opciondelIVR = Convert.ToString(Request.QueryString["opciondelivr"]);
                    string _servicioSoftCall = Convert.ToString(Request.QueryString["servicio"]);//ID_SOFTCALL para obtener el id Servicio

                    if (!string.IsNullOrEmpty(_Rut))
                    {
                        //string rutConGuion = _rutSinGuion.Substring(0, _rutSinGuion.Length - 1) + "-" + _rutSinGuion.Substring(_rutSinGuion.Length - 1, 1);
                        string idServicio = string.Empty;

                        txtBuscarCliente.Text = _Rut;

                        txtBuscarCliente.Text = _Rut + "-" + _Dv;

                        //ddlCanal.SelectedValue = "2";                        
                        RadioButtonList1.SelectedValue = "Titular";
                        DataTable dtBuscaServicio = dal.GetBuscarServicioPorSoftCall(_servicioSoftCall, 1).Tables[0];
                        foreach (DataRow item in dtBuscaServicio.Rows)
                        {
                            idServicio = item["ID_SERVICIO"].ToString();
                        }

                        if (dtBuscaServicio.Rows.Count != 0)
                        {
                            ddlServicio.SelectedValue = idServicio;
                        }
                        else
                        {
                            ddlServicio.SelectedValue = "0";
                        }

                        ddlServicio_SelectedIndexChanged(sender, e);
                        btnBuscarCliente_Click(sender, e);
                        hfIdLlamada.Value = _IdLlamada;
                        hfTelefonoIVR.Value = _telefonoIVR;
                        hfOpcionIVR.Value = _opciondelIVR;
                        hfServicioIVR.Value = _servicioSoftCall;
                        TxtNumRegConsulta.Text = _IdLlamada;
                    }
                    else
                    {
                        hfIdLlamada.Value = _IdLlamada;
                        hfTelefonoIVR.Value = _telefonoIVR;
                        hfOpcionIVR.Value = _opciondelIVR;
                        hfServicioIVR.Value = _servicioSoftCall;
                        TxtNumRegConsulta.Text = string.Empty;
                    }

                    string _IdEmail = Convert.ToString(Request.QueryString["IdEmail"]);
                    if (!string.IsNullOrEmpty(_IdEmail))
                    {
                        ddlCanal.SelectedValue = "3";

                        DataTable dt = dal.getBuscarEmail(Convert.ToInt32(_IdEmail)).Tables[0];
                        foreach (DataRow item in dt.Rows)
                        {
                            txtComentario.Text = item["BODY"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Visible = true;
            }
        }
        //JRL 2018-05-03
        void SubirArchivoGestion()
        {
            try
            {
                if (fuArchivo.HasFile)
                {
                    string carpeta = "archivosGestion/Gestion_" + hfIdGestion.Value + "_" + fuArchivo.FileName.Replace(" ", "_");
                    fuArchivo.SaveAs(Server.MapPath(carpeta));
                    int idGestion = Convert.ToInt32(hfIdGestion.Value);//JRL Cambio de Int16 a Int32
                    dal.setEditarRutaArchivoGestion(idGestion, carpeta);
                }

                if (fuArchivo2.HasFile)
                {
                    string carpeta = "archivosGestion/Gestion_" + hfIdGestion.Value + "_" + fuArchivo2.FileName.Replace(" ", "_");
                    fuArchivo2.SaveAs(Server.MapPath(carpeta));
                    int idGestion = Convert.ToInt32(hfIdGestion.Value);//JRL Cambio de Int16 a Int32
                    dal.setEditarRutaArchivoGestion2(idGestion, carpeta);
                }

                //JRL 2018-05-03
                if (fuArchivo3.HasFile)
                {
                    string carpeta3 = "archivosGestion/Gestion_" + hfIdGestion.Value + "_" + fuArchivo3.FileName.Replace(" ", "_");
                    fuArchivo3.SaveAs(Server.MapPath(carpeta3));
                    int idGestion = Convert.ToInt32(hfIdGestion.Value);//JRL Cambiado de Int16 a Int32
                    dal.setEditarRutaArchivoGestion3(idGestion, carpeta3);
                }
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-warning";
                divAlerta.Visible = true;
            }
        }

        void Emisor()
        {
            DdlEmisor.DataSource = dal.GetBuscarEmisor(1);
            DdlEmisor.DataValueField = "ID_EMISOR";
            DdlEmisor.DataTextField = "EMISOR";
            DdlEmisor.DataBind();
        }

        void Socio()
        {
            DdlSocio.DataSource = dal.GetBuscarSocio(1);
            DdlSocio.DataValueField = "ID_SOCIO";
            DdlSocio.DataTextField = "SOCIO";
            DdlSocio.DataBind();
        }

        void canal()
        {
            ddlCanal.DataSource = dal.getBuscarCanal(null);
            ddlCanal.DataValueField = "ID_CANAL";
            ddlCanal.DataTextField = "CANAL";
            ddlCanal.DataBind();
        }

        void adiconales(int IdCanal)
        {
            lblAdicional1.Visible = false;
            txtAdicional1.Visible = false;
            lblAdicional2.Visible = false;
            txtAdicional2.Visible = false;
            lblAdicional3.Visible = false;
            txtAdicional3.Visible = false;
            lblAdicional4.Visible = false;
            txtAdicional4.Visible = false;
            divAdicioanl.Visible = false;
            Globales.Inicializar();

            DataTable dt = new DataTable();
            dt = dal.getBuscarCanalId(IdCanal).Tables[0];
            foreach (DataRow item in dt.Rows)
            {
                if (item["ADICIONAL_1"].ToString() != string.Empty)
                {
                    divAdicioanl.Visible = true;
                    lblAdicional1.Visible = true;
                    txtAdicional1.Visible = true;
                    lblAdicional1.Text = item["ADICIONAL_1"].ToString();
                    Globales.rAdicional1 = Convert.ToBoolean(item["OBL_ADIC_1"]);
                }
                else
                {
                    lblAdicional1.Visible = false;
                    txtAdicional1.Visible = false;
                    txtAdicional1.Text = "";
                    Globales.rAdicional1 = Convert.ToBoolean(item["OBL_ADIC_1"]);
                }

                if (item["ADICIONAL_2"].ToString() != string.Empty)
                {
                    divAdicioanl.Visible = true;
                    lblAdicional2.Visible = true;
                    txtAdicional2.Visible = true;
                    lblAdicional2.Text = item["ADICIONAL_2"].ToString();
                    Globales.rAdicional2 = Convert.ToBoolean(item["OBL_ADIC_2"]);
                }
                else
                {
                    lblAdicional2.Visible = false;
                    txtAdicional2.Visible = false;
                    txtAdicional2.Text = "";
                    Globales.rAdicional2 = Convert.ToBoolean(item["OBL_ADIC_2"]);
                }

                if (item["ADICIONAL_3"].ToString() != string.Empty)
                {
                    divAdicioanl.Visible = true;
                    lblAdicional3.Visible = true;
                    txtAdicional3.Visible = true;
                    lblAdicional3.Text = item["ADICIONAL_3"].ToString();
                    Globales.rAdicional3 = Convert.ToBoolean(item["OBL_ADIC_3"]);
                }
                else
                {
                    lblAdicional3.Visible = false;
                    txtAdicional3.Visible = false;
                    txtAdicional3.Text = "";
                    Globales.rAdicional3 = Convert.ToBoolean(item["OBL_ADIC_3"]);
                }

                if (item["ADICIONAL_4"].ToString() != string.Empty)
                {
                    divAdicioanl.Visible = true;
                    lblAdicional4.Visible = true;
                    txtAdicional4.Visible = true;
                    lblAdicional4.Text = item["ADICIONAL_4"].ToString();
                    Globales.rAdicional4 = Convert.ToBoolean(item["OBL_ADIC_4"]);
                }
                else
                {
                    lblAdicional4.Visible = false;
                    txtAdicional4.Visible = false;
                    txtAdicional4.Text = "";
                    Globales.rAdicional4 = Convert.ToBoolean(item["OBL_ADIC_4"]);
                }
            }
        }

        void nivel1()
        {
            ddlNivel1.DataSource = dal.getBuscarNivel1(null, null, ddlServicio.SelectedValue);
            ddlNivel1.DataValueField = "NIVEL_1";
            ddlNivel1.DataTextField = "NIVEL_1";
            ddlNivel1.DataBind();

            ddlNivel2.Items.Clear();
            ddlNivel3.Items.Clear();
            ddlNivel4.Items.Clear();
        }

        void nivel2()
        {
            ddlNivel2.DataSource = dal.getBuscarNivel2(null, null, ddlServicio.SelectedValue, ddlNivel1.SelectedValue);
            ddlNivel2.DataValueField = "NIVEL_2";
            ddlNivel2.DataTextField = "NIVEL_2";
            ddlNivel2.DataBind();

            ddlNivel3.Items.Clear();
            ddlNivel4.Items.Clear();
        }

        void nivel3()
        {
            ddlNivel3.DataSource = dal.getBuscarNivel3(null, null, ddlServicio.SelectedValue, ddlNivel1.SelectedValue, ddlNivel2.SelectedValue);
            ddlNivel3.DataValueField = "NIVEL_3";
            ddlNivel3.DataTextField = "NIVEL_3";
            ddlNivel3.DataBind();

            ddlNivel4.Items.Clear();
        }

        void nivel4()
        {

            ddlNivel4.DataSource = dal.getBuscarNivel4(null, null, ddlServicio.SelectedValue, ddlNivel1.SelectedValue, ddlNivel2.SelectedValue, ddlNivel3.SelectedValue);
            ddlNivel4.DataValueField = "NIVEL_4";
            ddlNivel4.DataTextField = "NIVEL_4";
            ddlNivel4.DataBind();
        }

        void Servicio()
        {
            ddlServicio.DataSource = dal.GetBuscarServicioConId(0, 1);
            ddlServicio.DataTextField = "SERVICIO";
            ddlServicio.DataValueField = "ID_SERVICIO";
            ddlServicio.DataBind();
        }

        void Comuna()
        {
            ddlComuna.DataSource = dal.getBuscarComuna(null);
            ddlComuna.DataTextField = "NOM_COMUNA";
            ddlComuna.DataValueField = "ID_COMUNA";
            ddlComuna.DataBind();
        }

        //protected void ddlCanal_DataBound(object sender, EventArgs e)
        //{
        //    ddlCanal.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        //}

        protected void ddlNivel1_DataBound(object sender, EventArgs e)
        {
            ddlNivel1.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        }

        protected void ddlNivel2_DataBound(object sender, EventArgs e)
        {
            ddlNivel2.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        }

        protected void ddlNivel3_DataBound(object sender, EventArgs e)
        {
            ddlNivel3.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        }

        protected void ddlNivel4_DataBound(object sender, EventArgs e)
        {
            ddlNivel4.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        }

        //protected void ddlNivel5_DataBound(object sender, EventArgs e)
        //{
        //    ddlNivel5.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        //}

        protected void ddlTicketSeleccionar_DataBound(object sender, EventArgs e)
        {
            ddlTicketSeleccionar.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        }

        protected void ddlServicio_DataBound(object sender, EventArgs e)
        {
            ddlServicio.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        }

        protected void ddlComuna_DataBound(object sender, EventArgs e)
        {
            ddlComuna.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        }

        //2021-08-02 No se utiliza
        //protected void ddlClienteInternoExterno_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        nivel1();
        //    }
        //    catch (Exception ex)
        //    {
        //        lblInfo.Text = ex.Message;
        //        divAlerta.Visible = true;
        //    }
        //}

        protected void ddlCanal_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Visible = true;

            }
        }

        protected void ddlServicio_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlServicio.SelectedValue == "2")
                {
                    DdlEmisor.SelectedValue = "1";
                    DdlEmisor.Enabled = false;
                }
                else
                {
                    DdlEmisor.Enabled = true;
                }

                nivel1();

                lblScript.Text = string.Empty;
                lblRutaArch1.Text = string.Empty;

                DataTable dt = new DataTable();
                dt = dal.GetBuscarServicioScript(Convert.ToInt16(ddlServicio.SelectedValue)).Tables[0];
                foreach (DataRow item in dt.Rows)
                {
                    lblScript.Text = item["NOMBRE_SCRIPT"].ToString();
                    lblRutaArch1.Text = item["RUTA_ARCHIVO_1"].ToString();
                    //if (lblRutaArch1.Text == string.Empty)                    
                }

                if (string.IsNullOrEmpty(lblRutaArch1.Text))
                {
                    btnArchivo1.Visible = false;
                }
                else
                {
                    btnArchivo1.Visible = true;
                }

                //if (string.IsNullOrEmpty(lblScript.Text))
                //{
                //    lblScript.Visible = false;
                //}
                //else
                //{
                //    lblScript.Visible = true;
                //}

                if (ddlServicio.SelectedValue == "2")//Clave 1 / Clave 2
                {
                    txtObservacionCliente.Text = "Estimados, Se informa siniestro en la ubicación:\n" +
                        "DIRECCIÓN:\n" +
                        "REFERENCIAS (SI EXISTEN):\n" +
                        "LOCALIDAD:\n" +
                        "Saludos";
                }
                else if (ddlServicio.SelectedValue == "4")//Carga Nocturna
                {
                    txtObservacionCliente.Text = "Estimados, Se informa Carga Nocturna con fecha: " + DateTime.Now.ToString("G") + "\n" +
                        "Localidad:\n" +
                        "Dirección de los trabajos:\n" +
                        "Recibe información (CENCO):\n" +
                        "Saludos";
                }
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Visible = true;
            }
        }

        public bool validarRut(string rut)
        {
            bool validacion = false;
            try
            {
                int index = rut.IndexOf("-");
                if (index == -1)
                {
                    validacion = false;
                    return validacion;
                }
                rut = rut.ToUpper();
                rut = rut.Replace(".", "");
                rut = rut.Replace("-", "");
                int rutAux = int.Parse(rut.Substring(0, rut.Length - 1));

                char dv = char.Parse(rut.Substring(rut.Length - 1, 1));

                int m = 0, s = 1;
                for (; rutAux != 0; rutAux /= 10)
                {
                    s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
                }
                if (dv == (char)(s != 0 ? s + 47 : 75))
                {
                    validacion = true;
                }
            }
            catch (Exception)
            {
            }
            return validacion;
        }

        protected void ddlNivel1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                nivel2();
                ddlNivel3.Items.Clear();
                ddlNivel4.Items.Clear();
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Visible = true;
            }
        }

        protected void ddlNivel2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                nivel3();
                ddlNivel4.Items.Clear();
                divTicketSeleccionar.Visible = false;
                //rblTransRecado.Enabled = true;
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Visible = true;
            }
        }

        protected void ddlNivel3_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                nivel4();
                divTicketSeleccionar.Visible = false;
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void ddlNivel4_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DataTable dtDeriva = new DataTable();

                dtDeriva = dal.getBuscarTipificacionPorIdEstatusIdSubEstatusIdEstatusGestion(ddlNivel1.SelectedValue, ddlNivel2.SelectedValue, ddlNivel3.SelectedValue, ddlNivel4.SelectedValue, Convert.ToInt32(ddlServicio.SelectedValue)).Tables[0];
                string deriva = "";
                string insistencia = "";
                string obligatorioCli = string.Empty;
                foreach (DataRow item in dtDeriva.Rows)
                {
                    deriva = item["DERIVA"].ToString();
                    insistencia = item["INSISTENCIA"].ToString();

                    if (item["TIPO"].ToString() == "D")
                    {

                    }
                    else if (item["TIPO"].ToString() == "B")
                    {
                        divTipo.Visible = true;
                    }
                    else
                    {
                        divTipo.Visible = false;
                    }
                }

                if (insistencia == "1")
                {
                    divTicketSeleccionar.Visible = true;

                    ddlTicketSeleccionar.DataSource = dal.getBuscarTicketBuscadorPorRut(lblRutCliente2.Text);
                    ddlTicketSeleccionar.DataValueField = "ID_ATENCION";
                    ddlTicketSeleccionar.DataTextField = "TICKET_INSISTENCIA";
                    ddlTicketSeleccionar.DataBind();
                }
                else
                {
                    divTicketSeleccionar.Visible = false;
                }
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        public string buscarIdUsuario()
        {
            string idUsuario = "";
            foreach (DataRow rowDs in dal.getBuscarUsuario(Session["variableUsuario"].ToString(), null).Tables[0].Rows)
            {
                idUsuario = Convert.ToString(rowDs["ID_USUARIO"]);
            }
            return idUsuario;
        }

        protected void btnAgregarCliente_Click(object sender, EventArgs e)
        {
            divIngresarGestion.Visible = false;
            divTipificacion.Visible = false;
        }

        void ocultarIngresoCliente()
        {
            divIngresarGestion.Visible = true;
            divTipificacion.Visible = true;
        }

        void mostrarIngresoCliente()
        {
            divIngresarGestion.Visible = false;
            divTipificacion.Visible = false;

            //divAgregarCliente.Visible = true;
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            ocultarIngresoCliente();
        }

        protected void bntEliminaClienteSeleccionado_Click(object sender, EventArgs e)
        {

            limpiarEliminar();

            divResultadoBusqueda.Visible = true;

            //sdkjdsjnksdjksdjk
            divClienteSeleccionado.Visible = true;
        }

        void limpiarEliminar()
        {
            lblRutCliente.Text = string.Empty;

            lblRutCliente2.Text = string.Empty;
            lblNombreCliente.Text = string.Empty;
            lblEmailCliente.Text = string.Empty;
        }

        protected void txtRutCliente_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //if (comunes.validarRut(txtRutCliente.Text) == false)
                //{
                //    lblInfo.Text = "El rut del cliente no es valido.";
                //    divAlerta.Attributes["class"] = "alert alert-danger";
                //    divAlerta.Visible = true;
                //}
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void lbtnIrHistorialCliente_Click(object sender, EventArgs e)
        {
            try
            {
                buscarGr(lblRutCliente2.Text);

                ScriptManager.RegisterStartupScript(UpdatePanel2, UpdatePanel2.GetType(), "show", "$(function () { $('#" + Panel1.ClientID + "').modal('show'); });", true);
                UpdatePanel2.Update();
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void lbtnIdTicket_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                GridViewRow row = (GridViewRow)btn.NamingContainer;

                Label _lblIdTicket = (Label)grvTickets.Rows[row.RowIndex].FindControl("lblIdTicket");

                ScriptManager.RegisterStartupScript(this, this.GetType(), UniqueID, "window.open('SeguimientoTicket.aspx?t=" + _lblIdTicket.Text + "','_blank');", true);
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void paginacion_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                //Label _lblArchivo = (Label)e.Row.FindControl("lblArchivo");

                //if (string.IsNullOrEmpty(_lblArchivo.Text) == true)
                //{
                //    _btnArchivo.Visible = false;
                //}
                //else
                //{
                //    _btnArchivo.Visible = true;
                //}
            }
        }

        void buscarGr(string rutCliente)
        {
            //divGrilla.Visible = true;
            string idUsuario = buscarIdUsuario();
            grvTickets.DataSource = dal.getBuscarTicketBuscador(rutCliente, rutCliente, idUsuario);
            grvTickets.DataBind();
        }

        public void limpiar(ControlCollection controles)
        {
            foreach (Control control in controles)
            {
                if (control is TextBox)
                    ((TextBox)control).Text = string.Empty;
                else if (control is DropDownList)
                    ((DropDownList)control).ClearSelection();
                else if (control is RadioButtonList)
                    ((RadioButtonList)control).ClearSelection();
                else if (control is CheckBoxList)
                    ((CheckBoxList)control).ClearSelection();
                else if (control is RadioButton)
                    ((RadioButton)control).Checked = false;
                else if (control is CheckBox)
                    ((CheckBox)control).Checked = false;
                else if (control.HasControls())
                    //Esta linea detécta un Control que contenga otros Controles
                    //Así ningún control se quedará sin ser limpiado.
                    limpiar(control.Controls);
            }

            hfIdGestion.Value = string.Empty;
        }

        protected void btnBuscarCliente_Click(object sender, EventArgs e)
        {
            try
            {
                limpiarCliente();

                if (RadioButtonList1.SelectedIndex < 0)
                {
                    lblInfo.Text = "Debe seleccionar Titular o No Titular";
                    divAlerta.Attributes["class"] = "alert alert-danger";
                    divAlerta.Visible = true;
                    return;
                }

                string buscar = txtBuscarCliente.Text.Trim().Replace("-", "");
                if (buscar.Length < 1)
                {
                    lblInfo.Text = "Texto de búsqueda debe tener al menos 3 caracteres";
                    divAlerta.Attributes["class"] = "alert alert-danger";
                    divAlerta.Visible = true;
                    return;
                }

                string rutCliente = buscar.Trim();
                string dv = string.Empty;
                //Comun com = new Comun();

                if (!com.validarRut(txtBuscarCliente.Text.Trim()))
                {
                    lblInfo.Text = "El rut no es válido";
                    divAlerta.Attributes["class"] = "alert alert-danger";
                    divAlerta.Visible = true;
                    return;
                }

                //ORACLE
                string nombres = string.Empty, paterno = string.Empty, materno = string.Empty, segmentoCliente = string.Empty,
                    email = string.Empty, fechaCompra = string.Empty, sernac = string.Empty, msj = string.Empty,
                    celular = string.Empty, fijo = string.Empty, local = string.Empty, calle = string.Empty, numero = string.Empty,
                    resto = string.Empty, idComuna = string.Empty, nomComuna = string.Empty;
                int? codError;

                if (rutCliente.Trim() == "")
                {
                    rutCliente = "0";
                }

                int rutNum = 0;
                if (com.IsNumeric(rutCliente))
                {
                    rutNum = Convert.ToInt32(rutCliente);
                }

                DataTable dt2 = dal.getBuscarClientePorRut(txtBuscarCliente.Text.Trim()).Tables[0];
                foreach (DataRow item in dt2.Rows)
                {
                    nombres = item["NOMBRE"].ToString();
                    paterno = item["PATERNO"].ToString();
                    materno = item["MATERNO"].ToString();
                    email = item["EMAIL"].ToString();
                    celular = item["CELULAR"].ToString();
                    fijo = item["TELEFONO"].ToString();
                    calle = item["CALLE"].ToString();
                    numero = item["NUMERO"].ToString();
                    resto = item["RESTO"].ToString();
                    idComuna = item["ID_COMUNA"].ToString();
                    nomComuna = item["NOM_COMUNA"].ToString();
                }

                if (dt2.Rows.Count != 0)
                {
                    btnNuevoCliente.Visible = false;
                }
                else
                {
                    btnNuevoCliente.Visible = true;
                }

                string rutConDV = string.Empty;
                rutConDV = rutCliente + "-" + dv;

                hfNombres.Value = nombres;
                hfPaterno.Value = paterno;
                hfMaterno.Value = materno;
                hfSegmentoCliente.Value = segmentoCliente;
                hfEmail.Value = email;
                hfFechaCompra.Value = fechaCompra;
                hfSernac.Value = sernac;
                hfCelular.Value = celular;
                hfFijo.Value = fijo;
                hfLocal.Value = local;
                hfCalle.Value = calle;
                hfNumero.Value = numero;
                hfResto.Value = resto;
                hfComuna.Value = idComuna;

                lblRutCliente.Text = rutConDV;
                lblRutCliente2.Text = txtBuscarCliente.Text.Trim();
                lblNombreCliente.Text = nombres + " " + paterno + " " + materno;

                lblEmailCliente.Text = email;
                lblCelular.Text = hfCelular.Value;
                lblTelefonoFijo.Text = fijo;
                lblCalle.Text = calle;
                lblNumero.Text = numero;
                lblResto.Text = resto;
                lblComuna.Text = nomComuna;

                divResultadoBusqueda.Visible = false;
                divClienteSeleccionado.Visible = true;

                txtEmail.Text = lblEmailCliente.Text;
                divEnriquecimientoContactabilidad.Visible = false;
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        void limpiarCliente()
        {
            txtRutClienteExt.Text = string.Empty;
            txtNombreClienteExt.Text = string.Empty;
            txtApellidoParternoClienteExt.Text = string.Empty;
            txtApellidoMaterno.Text = string.Empty;
            txtEmailClient.Text = string.Empty;
            txtPhone.Text = string.Empty;
            txtCellPhone.Text = string.Empty;
            ddlSernac.ClearSelection();
            txtLocal.Text = string.Empty;
            txtCalle.Text = string.Empty;
            txtNumero.Text = string.Empty;
            txtResto.Text = string.Empty;
            ddlComuna.ClearSelection();
        }

        protected void btnSeleccionarCliente_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton img = (LinkButton)sender;
                GridViewRow row = (GridViewRow)img.NamingContainer;
                Label _lblRutCliente = (Label)grvClientes.Rows[row.RowIndex].FindControl("lblRutCliente");
                Label _lblNombre = (Label)grvClientes.Rows[row.RowIndex].FindControl("lblNombre");
                Label _lblTelefono = (Label)grvClientes.Rows[row.RowIndex].FindControl("lblTelefono");
                Label _lblCelular = (Label)grvClientes.Rows[row.RowIndex].FindControl("lblCelular");
                Label _lblEmail = (Label)grvClientes.Rows[row.RowIndex].FindControl("lblEmail");

                Label _lblTarjeta = (Label)grvClientes.Rows[row.RowIndex].FindControl("lblTarjeta");

                Label _lblComuna = (Label)grvClientes.Rows[row.RowIndex].FindControl("lblComuna");
                Label _lblDireccion = (Label)grvClientes.Rows[row.RowIndex].FindControl("lblDireccion");
                Label _lblActivo = (Label)grvClientes.Rows[row.RowIndex].FindControl("lblActivo");

                lblRutCliente.Text = _lblRutCliente.Text;

                lblRutCliente2.Text = _lblRutCliente.Text;
                lblNombreCliente.Text = _lblNombre.Text;
                lblEmailCliente.Text = _lblEmail.Text;
                lblCelular.Text = _lblCelular.Text;

                divResultadoBusqueda.Visible = false;
                divClienteSeleccionado.Visible = true;
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void btnGrabar_Click(object sender, EventArgs e)
        {
            try
            {
                string idUsuario = Session["variableIdUsuario"].ToString();
                string idPerfil = Session["variableIdPerfil"].ToString();
                //string algo = rblTransRecado.SelectedValue.ToString();
                string tipo = string.Empty;
                //tipo = rblTransRecado.SelectedValue;

                //JRL va a la tabla GM_CLIENTE
                //DataTable dt10 = new DataTable();
                //dt10 = dal.getBuscarClientePorEmail(lblEmailCliente.Text).Tables[0];

                //string nombre = string.Empty;
                //string paterno = string.Empty;
                //string materno = string.Empty;
                //foreach (DataRow item in dt10.Rows)
                //{
                //    nombre = item["NOMBRE"].ToString();
                //    paterno = item["PATERNO"].ToString();
                //    materno = item["MATERNO"].ToString();
                //}

                if (txtAdicional1.Text == "" && Globales.rAdicional1 == true)
                {
                    lblInfo.Text = "El campo adicional 1 es requerido";
                    divAlerta.Attributes["class"] = "alert alert-warning";

                    divAlerta.Visible = true;
                    return;
                }
                if (txtAdicional2.Text == "" && Globales.rAdicional2 == true)
                {
                    lblInfo.Text = "El campo adicional 2 es requerido";
                    divAlerta.Attributes["class"] = "alert alert-warning";

                    divAlerta.Visible = true;
                    return;
                }
                if (txtAdicional3.Text == "" && Globales.rAdicional3 == true)
                {
                    lblInfo.Text = "El campo adicional 3 es requerido";
                    divAlerta.Attributes["class"] = "alert alert-warning";

                    divAlerta.Visible = true;
                    return;
                }
                if (txtAdicional4.Text == "" && Globales.rAdicional4 == true)
                {
                    lblInfo.Text = "El campo adicional 4 es requerido";
                    divAlerta.Attributes["class"] = "alert alert-warning";

                    divAlerta.Visible = true;
                    return;
                }

                if (ddlServicio.SelectedValue == "0")
                {
                    lblInfo.Text = "El Servicio es obligatorio";
                    divAlerta.Visible = true;
                    return;
                }

                if (ddlCanal.SelectedValue.ToString() == "0")
                {
                    lblInfo.Text = "El Canal es obligatorio";
                    divAlerta.Visible = true;
                    return;
                }

                if (!string.IsNullOrEmpty(txtIdCliente.Text.Trim()))
                {
                    if (!com.EsNumerico(txtIdCliente.Text.Trim()))
                    {
                        lblInfo.Text = "Sólo se aceptan números para Id Cliente.";
                        divAlerta.Attributes["class"] = "alert alert-warning";
                        divAlerta.Visible = true;
                        return;
                    }

                    if (txtIdCliente.Text.Trim().Length > 7)
                    {
                        lblInfo.Text = "Debe ingresar máximo 7 caracteres para Id Cliente.";
                        divAlerta.Attributes["class"] = "alert alert-warning";
                        divAlerta.Visible = true;
                        return;
                    }
                }

                if (!string.IsNullOrEmpty(txtNroAtencion.Text.Trim()))
                {
                    if (!com.EsNumerico(txtNroAtencion.Text.Trim()))
                    {
                        lblInfo.Text = "Sólo se aceptan números para N° Atención.";
                        divAlerta.Attributes["class"] = "alert alert-warning";
                        divAlerta.Visible = true;
                        return;
                    }

                    if (txtNroAtencion.Text.Trim().Length > 7)
                    {
                        lblInfo.Text = "Debe ingresar máximo 7 caracteres para N° Atención.";
                        divAlerta.Attributes["class"] = "alert alert-warning";
                        divAlerta.Visible = true;
                        return;
                    }
                }

                if (ddlNivel1.SelectedValue.ToString() == "0")
                {
                    lblInfo.Text = "El Nivel 1 es obligatorio";
                    divAlerta.Visible = true;
                    return;
                }
                if (ddlNivel2.SelectedValue.ToString() == "0")
                {
                    lblInfo.Text = "El Nivel 2 es obligatorio";
                    divAlerta.Visible = true;
                    return;
                }
                if (ddlNivel3.SelectedValue.ToString() == "0")
                {
                    lblInfo.Text = "El Nivel 3 es obligatorio";
                    divAlerta.Visible = true;
                    return;
                }
                if (ddlNivel4.SelectedValue.ToString() == "0")
                {
                    lblInfo.Text = "El Nivel 4 es obligatorio";
                    divAlerta.Visible = true;
                    return;
                }

                if (RadioButtonList1.SelectedIndex < 0)
                {
                    lblInfo.Text = "Debe seleccionar Titular o No Titular";
                    divAlerta.Visible = true;
                    return;
                }

                if (RadioButtonList1.SelectedItem.ToString() == "Titular" && string.IsNullOrEmpty(lblRutCliente2.Text))
                {
                    lblInfo.Text = "El cliente Titular es obligatoria";
                    divAlerta.Visible = true;
                    return;
                }

                if (RadioButtonList1.SelectedItem.ToString() == "No Titular" && (string.IsNullOrEmpty(lblRutCliente2.Text) || string.IsNullOrEmpty(txtIngreseTitular.Text)))
                {
                    lblInfo.Text = "Los clientes No Titular y Titular son obligatorios";
                    divAlerta.Visible = true;
                    return;
                }

                if (RadioButtonList1.SelectedItem.ToString() == "No Titular")
                {
                    txtIngreseTitular.Text = txtIngreseTitular.Text.Trim().Replace(".", "");

                    if (!com.validarRut(txtIngreseTitular.Text.Trim()))
                    {
                        lblInfo.Text = "El rut del Titular no es válido";
                        divAlerta.Attributes["class"] = "alert alert-danger";
                        divAlerta.Visible = true;
                        return;
                    }
                }

                //2021-12-15 Agregado
                if (string.IsNullOrEmpty(txtComentario.Text.Trim()))
                {
                    lblInfo.Text = "La Observación es obligatoria.";
                    divAlerta.Visible = true;
                    return;
                }

                //// Aca ver la validacion de la obs Cliente ALG
                //if (txtObservacionCliente.Text == string.Empty)
                //{
                //    lblInfo.Text = "La observación cliente es obligatoria para cerrar el caso.";
                //    divAlerta.Attributes["class"] = "alert alert-warning";
                //    divAlerta.Visible = true;
                //    return;
                //}

                //if (idPerfil == "3")
                //{
                //    if (txtUsuarioFirma.Text.Trim() == string.Empty)
                //    {
                //        lblInfo.Text = "El usuario es obligatorio";
                //        divAlerta.Visible = true;
                //        return;
                //    }
                //}

                DataTable dtBuscaClientePorRut = new DataTable();
                dtBuscaClientePorRut = dal.getBuscarClientePorRut(lblRutCliente2.Text.Trim().Replace(".", "")).Tables[0];

                string nombre = string.Empty;
                string paterno = string.Empty;
                string materno = string.Empty;
                foreach (DataRow item in dtBuscaClientePorRut.Rows)
                {
                    nombre = item["NOMBRE"].ToString();
                    paterno = item["PATERNO"].ToString();
                    materno = item["MATERNO"].ToString();
                }

                int IdEmisor = Convert.ToInt32(DdlEmisor.SelectedValue);

                string idTipificacion = string.Empty;
                int idArea = 0;
                //string sla1 = string.Empty;
                DataTable dtTipificacion1 = new DataTable();
                dtTipificacion1 = dal.getBuscarTipificacionPorIdEstatusIdSubEstatusIdEstatusGestion(ddlNivel1.SelectedValue, ddlNivel2.SelectedValue, ddlNivel3.SelectedValue, ddlNivel4.SelectedValue, Convert.ToInt32(ddlServicio.SelectedValue)).Tables[0];

                foreach (DataRow item in dtTipificacion1.Rows)
                {
                    idTipificacion = item["ID_TIPIFICACION"].ToString();
                    tipo = item["TIPO"].ToString();
                    idArea = Convert.ToInt32(item["ID_AREA"]);
                }

                string ticket = "";
                string idUsuarioAsig = idUsuario;

                //Insistencias
                // ***************************************
                // ********   NO SE OCUPA POR AHORA
                // ***************************************

                //if (ddlTicketSeleccionar.SelectedValue != "0" && ddlTicketSeleccionar.SelectedValue != "")
                //{
                //    string idUsuarioAsignado = "";
                //    string idEstadoTicket = "";
                //    foreach (DataRow item in dal.getBuscarTicket(ddlTicketSeleccionar.SelectedValue).Tables[0].Rows)
                //    {
                //        idUsuarioAsignado = item["ID_USUARIO_ASIGNADO"].ToString();
                //        idEstadoTicket = item["ID_ESTADO_ATENCION"].ToString();
                //    }
                //    string correlativo = string.Empty;
                //    correlativo = dal.setIngresarTicketHistorico(ddlTicketSeleccionar.SelectedValue, idUsuario, idUsuarioAsignado, idEstadoTicket, "INSISTENCIA DE TICKET " + ddlTicketSeleccionar.SelectedValue + " - " + txtComentario.Text, null, txtObservacionCliente.Text, idArea);
                //    dal.setEditarTicketInsistencia(ddlTicketSeleccionar.SelectedValue);

                //    string carpeta = "archivosGestion/" + ddlTicketSeleccionar.SelectedValue + "_" + correlativo + "_" + fuArchivo.FileName;
                //    fuArchivo.SaveAs(Server.MapPath(carpeta));
                //    dal.setEditarRutaArchivoAtencionHistorico(Convert.ToInt32(ddlTicketSeleccionar.SelectedValue), Convert.ToInt32(correlativo), carpeta, "");

                //    string email = "";

                //    foreach (DataRow item in dal.getBuscarUsuarioPorId(idUsuarioAsignado).Tables[0].Rows)
                //    {
                //        email = item["EMAIL"].ToString();
                //    }


                //    string bodyClienteInterno = "Estimada(o), <br>";
                //    bodyClienteInterno += "Hemos recibido tu requerimiento y generado el número de atención "  + ticket + " para darte solución <br><br>";
                //    bodyClienteInterno += "<br>";
                //    bodyClienteInterno += "Información del Caso";
                //    bodyClienteInterno += "<br> N° de ticket: "+ ticket;
                //    bodyClienteInterno += "<br> Fecha: " + DateTime.Now.ToString("G");
                //    bodyClienteInterno += "<br>Asunto: " + txtComentario.Text;
                //    //bodyClienteInterno += "<br>SLA: " + sla1 + " hrs";

                //    //SLA: (debe decir el SLA parametrizado).
                //    bodyClienteInterno += "<br> Gracias por contactar a Esval. ";
                //    bodyClienteInterno += "<br><br>";

                //    bodyClienteInterno += "<table style='width:100%' border='1'><tr><td><img src='http://190.96.2.124/imagenes/logocardif.png' alt='Firma Logo' /></td>";
                //    bodyClienteInterno += "<td>Cardif <br>Línea Única de Soporte Cardif<br>Huérfanos 670, PISO 4 Santiago<br>(56-2) 2410 5900</td></tr></table>";


                //    string bodyResolutor = "Estimado(a) Usuario:<br>";
                //    bodyResolutor += "Se ha generado el siguiente caso para su gestión:";
                //    bodyResolutor += "<br><br>";
                //    bodyResolutor += txtComentario.Text;

                //    bodyResolutor += "<br>";
                //    bodyResolutor += "Le agradecemos revisar a la brevedad CRM Cardif (UPCOM)​";
                //    bodyResolutor += "http://172.20.1.171/CrmCardif/SeguimientoTicket.aspx?t=" + ticket;

                //    bodyResolutor += "<br><br>";

                //    bodyResolutor += "<table style='width:100%' border='1'><tr><td><img src='http://190.96.2.124/imagenes/logocardif.png' alt='Firma Logo' /></td>";
                //    bodyResolutor += "<td>Cardif <br>Línea Única de Soporte Cardif<br>Huérfanos 670, PISO 4 Santiago<br>(56-2) 2410 5900<br>ldiaz@interweb.cl</td></tr></table>";

                //    if (email!=string.Empty)
                //    {
                //        //JRL 2018-05-03 agregado null al archivo adjunto
                //        comunes.EnviarEmailSSLImplicito(email, bodyResolutor.Replace("\r\n", "<br>"), "Se ha generado una insistencia para el siguiente caso " + ddlTicketSeleccionar.SelectedValue, null);
                //    }

                //}
                // ******************* Fin insistencias

                //Primera línea(Cerrado)
                if (tipo == "P")
                {
                    if (RadioButtonList1.SelectedItem.ToString() == "Titular")
                    {
                        ticket = dal.setIngresarTicket(lblRutCliente2.Text, idUsuario, idUsuario, "1",
                            txtComentario.Text, tipo, ddlNivel1.SelectedItem.ToString(), ddlServicio.SelectedValue,
                            idTipificacion, txtObservacionCliente.Text, lblNombreCliente.Text.Trim(), ddlCanal.SelectedValue.ToString(),
                            null, null, null, hfIdLlamada.Value, null, null, txtAdicional1.Text, txtAdicional2.Text,
                            txtAdicional3.Text, txtAdicional4.Text, null, IdEmisor, idArea, null, hfTelefonoIVR.Value, hfOpcionIVR.Value, 
                            hfServicioIVR.Value,txtIdCliente.Text.Trim(), txtNroAtencion.Text.Trim());
                    }
                    else if (RadioButtonList1.SelectedItem.ToString() == "No Titular")
                    {
                        ticket = dal.setIngresarTicket(txtIngreseTitular.Text, idUsuario, idUsuario, "1",
                            txtComentario.Text, tipo, ddlNivel1.SelectedItem.ToString(), ddlServicio.SelectedValue,
                            idTipificacion, txtObservacionCliente.Text, lblNombreCliente.Text.Trim(), ddlCanal.SelectedValue.ToString(),
                            null, null, null, hfIdLlamada.Value, null, null, txtAdicional1.Text, txtAdicional2.Text,
                            txtAdicional3.Text, txtAdicional4.Text, null, IdEmisor, idArea, lblRutCliente2.Text, hfTelefonoIVR.Value, hfOpcionIVR.Value, 
                            hfServicioIVR.Value, txtIdCliente.Text.Trim(), txtNroAtencion.Text.Trim());
                    }

                    //JRL 2018-09-26
                    ddlCanal.Enabled = true;

                    //JRL 2019-08-07
                    dal.setIngresarTicketHistorico(ticket, idUsuario, idUsuarioAsig, "3", txtComentario.Text, null, txtObservacionCliente.Text, idArea);

                    //2021-12-15 Comentado
                    //dal.setIngresarAtencionFormulario(ticket, lblRutCliente2.Text, TxtNombreProducto.Text, DdlSocio.SelectedItem.Text, TxtPoliza.Text, TxtCanalVentas.Text,
                    //    TxtCobertura.Text, TxtEstadoPoliza.Text, TxtAsistencia.Text, TxtNombreBeneficiario.Text, TxtRutBeneficiario.Text, TxtRelacion.Text,
                    //    TxtCelularBeneficiario.Text, TxtEmailBeneficiario.Text, TxtDomicilioBeneficiario.Text, TxtComunaBeneficiario.Text, TxtNombreProveedor.Text,
                    //    TxtTelefonoProveedor.Text, TxtCasaComercial.Text, TxtNumBoletaCompra.Text, TxtFechaCompra.Text, TxtValorCompra.Text, TxtMarcaProducto.Text,
                    //    TxtEmpDondeRealizoCompra.Text, TxtFormaPago.Text, TxtNumRegConsulta.Text, TxtNombreLiquidador.Text, TxtTelefonoLiquidador.Text,
                    //    TxtNombreCoordinador.Text, TxtTelefonoCoordinador.Text, TxtUltComLiquidador.Text, TxtNumSiniestro.Text, TxtEstadoSiniestro.Text,
                    //    TxtIngresoTaller.Text, TxtFechaAprobacion.Text, TxtFechaEntrega.Text, TxtSumaLiquidacion.Text, TxtNumeroCaso.Text, TxtNombreTaller.Text,
                    //    TxtDireccionTaller.Text, TxtTelefonoTaller.Text);

                    //dal.setIngresarTicketHistorico(ticket, idUsuario, idUsuarioAsig, "3", null, null);

                    dal.setEditarTicket(ticket, "3", idUsuario, null, null);

                    //EMAIL
                    //string email = "";
                    //DataTable dt = new DataTable();
                    //dt = dal.getBuscarParametro("1").Tables[0];
                    //foreach (DataRow item in dt.Rows)
                    //{
                    //    email = item["EMAIL_CC"].ToString();
                    //}
                    hfIdGestion.Value = ticket;
                    SubirArchivoGestion();

                    //JRL 2020-02-27
                    DataTable dtTicketIngresado = new DataTable();
                    dtTicketIngresado = dal.getBuscarGestionPorIdTicket(ticket).Tables[0];
                    string Adj1 = string.Empty;
                    string Adj2 = string.Empty;
                    foreach (DataRow item in dtTicketIngresado.Rows)
                    {
                        Adj1 = item["RUTA_ARCHIVO"].ToString();
                        Adj2 = item["RUTA_ARCHIVO2"].ToString();
                    }

                    DataTable dt2 = new DataTable();
                    dt2 = dal.getBuscarFechaIngresoTicket(ticket).Tables[0];

                    string fechaIngreso = "";
                    foreach (DataRow item in dt2.Rows)
                    {
                        fechaIngreso = item["FECHA"].ToString();
                    }

                    string bodyClientePrimera = "Hola! " + nombre + " " + paterno + " " + materno + "<br>";

                    bodyClientePrimera += "<br><p style='color: red;'>Su ticket ha sido cerrado con la siguiente observación:</p>";

                    bodyClientePrimera += "<b>Observación Cliente:</b><br>" + txtObservacionCliente.Text + "<br>";
                    //bodyPrimera += "<br>Te informamos que tu ticket " + ticket + " ingresado el " + fechaIngreso;
                    //bodyPrimera += "<br>ha sido resuelto y cerrado por el ejecutivo a cargo.<br>";
                    //bodyPrimera += "<br>Si no estás conforme con la resolución del caso, por favor contáctanos";
                    //bodyPrimera += "<br>a través del teléfono 800 800 008.<br>";
                    //bodyPrimera += "<br>Saludos!<br>";                   

                    //JRL 2020-02-27
                    if (!string.IsNullOrEmpty(Adj1))
                    {
                        bodyClientePrimera += "<br>Adjunto1: <a href='http://" + sistema + Adj1 + "'>" + Adj1.Replace("archivosGestion/", " ") + "</a>";
                    }

                    if (!string.IsNullOrEmpty(Adj2))
                    {
                        bodyClientePrimera += "<br>Adjunto2: <a href='http://" + sistema + Adj2 + "'>" + Adj2.Replace("archivosGestion/", " ") + "</a>";
                    }

                    bodyClientePrimera += "<br><br><b>Equipo de Servicio al Cliente Esval.</b>";

                    if (chkEnviarObsCliente.Checked)
                    {
                        if (!string.IsNullOrEmpty(lblEmailCliente.Text))
                        {
                            //JRL 2018-05-03 agregado null al archivo adjunto
                            com.EnviarEmailSSLImplicito(lblEmailCliente.Text.Trim(), bodyClientePrimera.Replace("\r\n", "<br>"), "Servicio al Cliente Esval // Cierre de Ticket " + ticket, null);

                        }
                    }

                    lblInfo.Text = "Ticket " + ticket + " ingresado correctamente.";
                    divAlerta.Attributes["class"] = "alert alert-success";
                    divAlerta.Visible = true;
                }
                //else if (tipo == "D")
                //{
                //    DataTable dtTipificacion = new DataTable();
                //    dtTipificacion = dal.getBuscarTipificacionPorIdEstatusIdSubEstatusIdEstatusGestion(ddlNivel1.SelectedValue, ddlNivel2.SelectedValue, ddlNivel3.SelectedValue, ddlNivel4.SelectedValue, Convert.ToInt32(ddlServicio.SelectedValue)).Tables[0];
                //    string idUsuarioEscN1 = "0";
                //    string idUsuarioEscN1Email = string.Empty;
                //    string sla1 = string.Empty;
                //    foreach (DataRow item in dtTipificacion.Rows)
                //    {
                //        idUsuarioEscN1 = item["ID_USUARIO_ESC_N1"].ToString();
                //        sla1 = item["SLA_N1"].ToString();
                //    }

                //    //foreach (DataRow item in dal.getBuscarUsuarioPorId(idUsuarioEscN1).Tables[0].Rows)
                //    //{
                //    //    idUsuarioEscN1Email = item["EMAIL"].ToString();
                //    //}
                //    //idUsuarioEscN1Email = "ldiaz@interweb.cl";
                //    idUsuarioAsig = idUsuarioEscN1;
                //    //si es derivar el usuario asignado es cero. osea no se asigna a ninguno.
                //    idUsuarioAsig = "0";
                //    ticket = dal.setIngresarTicket(lblRutCliente2.Text, idUsuarioAsig, idUsuario, "1",
                //        txtComentario.Text, tipo, ddlNivel1.SelectedItem.ToString(),ddlServicio.SelectedValue,
                //        idTipificacion, txtObservacionCliente.Text, lblNombreCliente.Text.Trim(), ddlCanal.SelectedValue.ToString(),
                //        null, null, null, null, null,null, txtAdicional1.Text, txtAdicional2.Text, txtAdicional3.Text, txtAdicional4.Text, null, IdEmisor, idArea);//JRL 2018-09-27 Soporta null

                //    //JRL 2018-09-26
                //    ddlCanal.Enabled = true;

                //    dal.setIngresarTicketHistorico(ticket, idUsuario, idUsuarioAsig, "1", txtComentario.Text, null, txtObservacionCliente.Text, idArea);

                //    string bodyClienteInterno = "Estimada(o), <br>";
                //    bodyClienteInterno += "Hemos recibido tu requerimiento y generado el número de ticket " + ticket + " para gestionar tu caso.  <br><br>";
                //    bodyClienteInterno += "<b>" + txtObservacionCliente.Text + "</b>";
                //    bodyClienteInterno += "<br><br>";

                //    bodyClienteInterno += "<b><u>Detalle Ticket</u></b>";
                //    bodyClienteInterno += "<br><br> N° de ticket: " + ticket;
                //    bodyClienteInterno += "<br>Fecha: " + DateTime.Now.ToString("G");
                //    bodyClienteInterno += "<br>Asunto: " + txtObservacionCliente.Text;
                //    bodyClienteInterno += "<br>SLA: " + sla1 + " hrs";

                //    bodyClienteInterno += "<br><br>En cuanto tengamos la respuesta de tu caso te llamaremos";
                //    bodyClienteInterno += "<br>Ante cualquier duda o consulta no dudes en contactarnos al 800 800 008";
                //    bodyClienteInterno += "<br><br>";
                //    bodyClienteInterno += "<br>Muchas gracias!";
                //    bodyClienteInterno += "<br><b>Equipo de Servicio al Cliente Cardif.</b>";
                //    //bodyClienteInterno += "<table style='width:100%' border='1'><tr><td><img src='http://190.96.2.124/imagenes/logocardif.png' alt='Firma Logo' /></td>";
                //    //bodyClienteInterno += "<td>Cardif <br>Línea Única de Soporte Cardif<br>Huérfanos 670, PISO 4 Santiago<br>(56-2) 2410 5900<br>ldiaz@interweb.cl</td></tr></table>";

                //    string bodyResolutor = "Estimado(a) Usuario:";
                //    bodyResolutor += "<br><br>";
                //    bodyResolutor += "Se ha generado el siguiente caso para su gestión:";
                //    bodyResolutor += "<br><br>";
                //    bodyResolutor += "<b>" + txtComentario.Text + "</b>";
                //    bodyResolutor += "<br><br>";
                //    bodyResolutor += "Le agradecemos revisar a la brevedad CRM Cardif";
                //    bodyResolutor += "<br> " + "http://172.20.1.171/CrmCardif/SeguimientoTicket.aspx?t=" + ticket;
                //    bodyResolutor += "<br><br>";
                //    bodyResolutor += "<br>Muchas gracias!";
                //    bodyResolutor += "<br><b>Equipo de Servicio al Cliente Cardif.</b>";

                //    //if (idUsuarioEscN1Email.Trim() != string.Empty)
                //    //{
                //    //    //JRL 2018-05-03 agregado null al archivo adjunto
                //    //    comunes.EnviarEmailSSLImplicito(idUsuarioEscN1Email.Trim(), bodyResolutor.Replace("\r\n", "<br>"), "Servicio al Cliente Cardif_ Atención N° " + ticket, null);
                //    //}

                //    lblInfo.Text = /*"Gestión " + hfIdGestion.Value + " ingresada correctamente." + "<br>" +*/ "Se generó el número de ticket " + ticket;
                //    divAlerta.Attributes["class"] = "alert alert-success";
                //    divAlerta.Visible = true;
                //}
                //JRL 2019-09-24 No se está utilizando. No ingresa aquí.
                else if (tipo == "B")
                {
                    if (ddlTipo.SelectedValue == "D")
                    {
                        ticket = Derivar(idArea, idUsuarioAsig, idUsuario, IdEmisor, tipo, idTipificacion);
                    }
                    else if (ddlTipo.SelectedValue == "P")
                    {
                        if (RadioButtonList1.SelectedItem.ToString() == "Titular")
                        {
                            ticket = dal.setIngresarTicket(lblRutCliente2.Text, idUsuario, idUsuario, "1",
                                txtComentario.Text, tipo, ddlNivel1.SelectedItem.ToString(), ddlServicio.SelectedValue,
                                idTipificacion, txtObservacionCliente.Text, lblNombreCliente.Text.Trim(), ddlCanal.SelectedValue.ToString(),
                                null, null, null, hfIdLlamada.Value, null, null,
                                txtAdicional1.Text, txtAdicional2.Text, txtAdicional3.Text, txtAdicional4.Text, null, IdEmisor, idArea, null,
                                hfTelefonoIVR.Value, hfOpcionIVR.Value, hfServicioIVR.Value, txtIdCliente.Text.Trim(), txtNroAtencion.Text.Trim());
                        }
                        else if (RadioButtonList1.SelectedItem.ToString() == "No Titular")
                        {
                            ticket = dal.setIngresarTicket(txtIngreseTitular.Text, idUsuario, idUsuario, "1",
                                txtComentario.Text, tipo, ddlNivel1.SelectedItem.ToString(), ddlServicio.SelectedValue,
                                idTipificacion, txtObservacionCliente.Text, lblNombreCliente.Text.Trim(), ddlCanal.SelectedValue.ToString(),
                                null, null, null, hfIdLlamada.Value, null, null,
                                txtAdicional1.Text, txtAdicional2.Text, txtAdicional3.Text, txtAdicional4.Text, null, IdEmisor, idArea, lblRutCliente2.Text,
                                hfTelefonoIVR.Value, hfOpcionIVR.Value, hfServicioIVR.Value, txtIdCliente.Text.Trim(), txtNroAtencion.Text.Trim());
                        }

                        //JRL 2018-09-26
                        //Session["GestionIn_Email_vId_Email"] = null;
                        ddlCanal.Enabled = true;

                        //JRL 2019-08-07
                        dal.setIngresarTicketHistorico(ticket, idUsuario, idUsuarioAsig, "3", txtComentario.Text, null, txtObservacionCliente.Text, idArea);
                        //dal.setIngresarTicketHistorico(ticket, idUsuario, idUsuarioAsig, "3", null, null);

                        //2021-12-15 Comentado
                        //dal.setIngresarAtencionFormulario(ticket, lblRutCliente2.Text, TxtNombreProducto.Text, DdlSocio.SelectedItem.Text, TxtPoliza.Text, TxtCanalVentas.Text,
                        //TxtCobertura.Text, TxtEstadoPoliza.Text, TxtAsistencia.Text, TxtNombreBeneficiario.Text, TxtRutBeneficiario.Text, TxtRelacion.Text,
                        //TxtCelularBeneficiario.Text, TxtEmailBeneficiario.Text, TxtDomicilioBeneficiario.Text, TxtComunaBeneficiario.Text, TxtNombreProveedor.Text,
                        //TxtTelefonoProveedor.Text, TxtCasaComercial.Text, TxtNumBoletaCompra.Text, TxtFechaCompra.Text, TxtValorCompra.Text, TxtMarcaProducto.Text,
                        //TxtEmpDondeRealizoCompra.Text, TxtFormaPago.Text, TxtNumRegConsulta.Text, TxtNombreLiquidador.Text, TxtTelefonoLiquidador.Text,
                        //TxtNombreCoordinador.Text, TxtTelefonoCoordinador.Text, TxtUltComLiquidador.Text, TxtNumSiniestro.Text, TxtEstadoSiniestro.Text,
                        //TxtIngresoTaller.Text, TxtFechaAprobacion.Text, TxtFechaEntrega.Text, TxtSumaLiquidacion.Text, TxtNumeroCaso.Text, TxtNombreTaller.Text,
                        //TxtDireccionTaller.Text, TxtTelefonoTaller.Text);

                        dal.setEditarTicket(ticket, "3", idUsuario, null, null);

                        //EMAIL
                        string email = "";
                        DataTable dt = new DataTable();
                        dt = dal.getBuscarParametro("1").Tables[0];
                        foreach (DataRow item in dt.Rows)
                        {
                            email = item["EMAIL_CC"].ToString();
                        }

                        // ALG Abajo debe ver si viene un email del cliente lblEmailCliente

                        //JRL Correo de cierre
                        DataTable dt2 = new DataTable();
                        dt2 = dal.getBuscarFechaIngresoTicket(ticket).Tables[0];

                        string fechaIngreso = "";
                        foreach (DataRow item in dt2.Rows)
                        {
                            fechaIngreso = item["FECHA"].ToString();
                        }

                        string body = "Hola! " + nombre + " " + paterno + " " + materno + "<br>";
                        body += "<br>Te informamos que tu ticket " + ticket + " ingresado el " + fechaIngreso;
                        body += "<br>ha sido resuelto y cerrado por el ejecutivo a cargo.<br>";
                        body += "<br>Si no estás conforme con la resolución del caso, por favor contáctanos";
                        body += "<br>a través del teléfono 800 800 008.<br>";
                        body += "<br>Saludos!<br>";
                        body += "<br><b>Equipo de Servicio al Cliente Esval.</b>";

                        //if (lblEmailCliente.Text != "")
                        //{
                        //    //JRL 2018-05-03 agregado null al archivo adjunto
                        //    comunes.EnviarEmailSSLImplicito(lblEmailCliente.Text.Trim(), body.Replace("\r\n", "<br>"), "Servicio al Cliente Cardif // Cierre de Ticket " + ticket, null);

                        //}


                        // ALG ENviar Email
                        lblInfo.Text = "Ticket " + ticket + " ingresada correctamente.";
                        divAlerta.Attributes["class"] = "alert alert-success";
                        divAlerta.Visible = true;
                    }
                }
                else if (tipo == "D")//Derivar(Pendiente)
                {
                    ticket = Derivar(idArea, idUsuarioAsig, idUsuario, IdEmisor, tipo, idTipificacion);
                }

                //hfIdGestion.Value = ticket;

                //JRL 2018-05-17 Inicio Prueba mail cliente archivo adjunto
                if (chkHabilitarEmail.Checked == true && chkHabilitarEmail.Enabled == true)
                {
                    string bodyClienteCheck = "Estimad@, <br><br>";
                    bodyClienteCheck += "Junto con saludar y de acuerdo a lo requerido, adjunto documentos solicitados.<br>";
                    bodyClienteCheck += "<br>";

                    if (txtComentario.Text != string.Empty)
                    {
                        bodyClienteCheck += "<b>Observación:</b> " + txtComentario.Text + "<br><br>";
                    }

                    bodyClienteCheck += "<b>Observación Cliente:</b> " + txtObservacionCliente.Text + "<br>";
                    bodyClienteCheck += "<br>";
                    bodyClienteCheck += "Saluda cordialmente, <br>";
                    bodyClienteCheck += "<br>";
                    bodyClienteCheck += "Equipo de Servicio al Cliente <br>";
                    bodyClienteCheck += "Empresas Esval <br>";

                    if (txtEmail.Text == string.Empty)
                    {
                        lblInfo.Text = "Debe ingresar la dirección de correo.";
                        divAlerta.Attributes["class"] = "alert alert-warning";
                        divAlerta.Visible = true;
                    }
                    else
                    {
                        if (fuArchivo.FileName != "" && fuArchivo2.FileName == "")
                        {
                            com.EnviarEmailSSLImplicito(txtEmail.Text.Trim(), bodyClienteCheck.Replace("\r\n", "<br>"), "Solicitud Esval - N° " + ticket, null, null);
                            //enviarEmail(txtEmail.Text.Trim(), bodyClienteCheck.Replace("\r\n", "<br>"), "Solicitud Cardif - N° " + ticket, new Attachment(fuArchivo.FileContent, fuArchivo.FileName, ""), null);
                        }
                        else
                        {
                            if (fuArchivo.FileName == "" && fuArchivo2.FileName != "")
                            {
                                com.EnviarEmailSSLImplicito(txtEmail.Text.Trim(), bodyClienteCheck.Replace("\r\n", "<br>"), "Solicitud Esval - N° " + ticket, null, null);
                                //enviarEmail(txtEmail.Text.Trim(), bodyClienteCheck.Replace("\r\n", "<br>"), "Solicitud Cardif - N° " + ticket, null, new Attachment(fuArchivo2.FileContent, fuArchivo2.FileName, ""));
                            }
                            else
                            {
                                if (fuArchivo.FileName != "" && fuArchivo2.FileName != "")
                                {
                                    com.EnviarEmailSSLImplicito(txtEmail.Text.Trim(), bodyClienteCheck.Replace("\r\n", "<br>"), "Solicitud Esval - N° " + ticket, null, null);

                                    //enviarEmail(txtEmail.Text.Trim(), bodyClienteCheck.Replace("\r\n", "<br>"), "Solicitud Cardif - N° " + ticket, new Attachment(fuArchivo.FileContent, fuArchivo.FileName, ""), new Attachment(fuArchivo2.FileContent, fuArchivo2.FileName, ""));
                                }
                                else
                                {
                                    if (fuArchivo.FileName == "" && fuArchivo2.FileName == "")
                                    {

                                        com.EnviarEmailSSLImplicito(txtEmail.Text.Trim(), bodyClienteCheck.Replace("\r\n", "<br>"), "Solicitud Esval - N° " + ticket, null, null);
                                        //enviarEmail(txtEmail.Text.Trim(), bodyClienteCheck.Replace("\r\n", "<br>"), "Solicitud Cardif - N° " + ticket, null, null);
                                    }
                                }
                            }
                        }
                    }
                }
                //JRL 2018-05-17 Fin Prueba mail cliente archivo adjunto


                string _IdEmail = Convert.ToString(Request.QueryString["IdEmail"]);
                if (!string.IsNullOrEmpty(_IdEmail))
                {
                    dal.setEditarEmail(Convert.ToInt32(_IdEmail), Convert.ToInt32(ticket));
                }

                lblRutCliente2.Text = string.Empty;
                lblNombreCliente.Text = string.Empty;
                lblEmailCliente.Text = string.Empty;
                lblCelular.Text = string.Empty;
                lblTelefonoFijo.Text = string.Empty;
                lblNombreCliTitular.Text = string.Empty;
                divClienteSeleccionado.Visible = false;
                divIngreseTitular.Visible = false;
                //SubirArchivoGestion();
                limpiar(this.Controls);

                //Enviar ticket
                Response.Redirect("BuscarTicket.aspx?IdTicket=" + ticket + "&s=1");
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        private string Derivar(int idArea, string idUsuarioAsig, string idUsuario, int IdEmisor, string tipo, string idTipificacion)
        {
            DataTable dtTipificacion = new DataTable();
            dtTipificacion = dal.getBuscarTipificacionPorIdEstatusIdSubEstatusIdEstatusGestion(ddlNivel1.SelectedValue, ddlNivel2.SelectedValue, ddlNivel3.SelectedValue, ddlNivel4.SelectedValue, Convert.ToInt32(ddlServicio.SelectedValue)).Tables[0];
            string idUsuarioEscN1 = string.Empty;
            string idUsuarioEscN1Email = string.Empty;
            string sla1 = string.Empty;
            string nombreUsuarioEscN1 = string.Empty;
            foreach (DataRow item in dtTipificacion.Rows)
            {
                idUsuarioEscN1 = item["ID_USUARIO_ESC_N1"].ToString();               
                sla1 = item["SLA_N1"].ToString();
                idArea = Convert.ToInt32(item["ID_AREA"]);
            }

            //2021-08-12 Comentado
            //foreach (DataRow item in dal.getBuscarUsuarioPorId(idUsuarioEscN1).Tables[0].Rows)
            //{
            //    idUsuarioEscN1Email = item["EMAIL"].ToString();
            //    nombreUsuarioEscN1 = item["NOMBRE"].ToString();
            //}

            //idUsuarioEscN1Email = "ldiaz@interweb.cl";

            //2021-08-12 Comentado
            //idUsuarioAsig = idUsuarioEscN1;
            idUsuarioAsig = "0";

            string ticket = string.Empty;

            if (RadioButtonList1.SelectedItem.ToString() == "Titular")
            {
                ticket = dal.setIngresarTicket(lblRutCliente2.Text, idUsuarioAsig, idUsuario, "1",
                    txtComentario.Text, tipo, ddlNivel1.SelectedItem.ToString(), ddlServicio.SelectedValue,
                    idTipificacion, txtObservacionCliente.Text, lblNombreCliente.Text.Trim(), ddlCanal.SelectedValue.ToString(),
                    null, null, null, hfIdLlamada.Value, null, null, txtAdicional1.Text, txtAdicional2.Text,
                     txtAdicional3.Text, txtAdicional4.Text, null, IdEmisor, idArea, null, hfTelefonoIVR.Value, hfOpcionIVR.Value, 
                     hfServicioIVR.Value,txtIdCliente.Text.Trim(), txtNroAtencion.Text.Trim());
            }
            else if (RadioButtonList1.SelectedItem.ToString() == "No Titular")
            {
                ticket = dal.setIngresarTicket(txtIngreseTitular.Text, idUsuarioAsig, idUsuario, "1",
                    txtComentario.Text, tipo, ddlNivel1.SelectedItem.ToString(), ddlServicio.SelectedValue,
                    idTipificacion, txtObservacionCliente.Text, lblNombreCliente.Text.Trim(), ddlCanal.SelectedValue.ToString(),
                    null, null, null, hfIdLlamada.Value, null, null, txtAdicional1.Text, txtAdicional2.Text,
                     txtAdicional3.Text, txtAdicional4.Text, null, IdEmisor, idArea, lblRutCliente2.Text, hfTelefonoIVR.Value, hfOpcionIVR.Value, 
                     hfServicioIVR.Value, txtIdCliente.Text.Trim(), txtNroAtencion.Text.Trim());
            }

            //JRL 2018-09-26
            ddlCanal.Enabled = true;

            dal.setIngresarTicketHistorico(ticket, idUsuario, idUsuarioAsig, "1", txtComentario.Text, null, txtObservacionCliente.Text, idArea);

            //2021-12-15 Comentado
            //dal.setIngresarAtencionFormulario(ticket, lblRutCliente2.Text, TxtNombreProducto.Text, DdlSocio.SelectedItem.Text, TxtPoliza.Text, TxtCanalVentas.Text,
            //TxtCobertura.Text, TxtEstadoPoliza.Text, TxtAsistencia.Text, TxtNombreBeneficiario.Text, TxtRutBeneficiario.Text, TxtRelacion.Text,
            //TxtCelularBeneficiario.Text, TxtEmailBeneficiario.Text, TxtDomicilioBeneficiario.Text, TxtComunaBeneficiario.Text, TxtNombreProveedor.Text,
            //TxtTelefonoProveedor.Text, TxtCasaComercial.Text, TxtNumBoletaCompra.Text, TxtFechaCompra.Text, TxtValorCompra.Text, TxtMarcaProducto.Text,
            //TxtEmpDondeRealizoCompra.Text, TxtFormaPago.Text, TxtNumRegConsulta.Text, TxtNombreLiquidador.Text, TxtTelefonoLiquidador.Text,
            //TxtNombreCoordinador.Text, TxtTelefonoCoordinador.Text, TxtUltComLiquidador.Text, TxtNumSiniestro.Text, TxtEstadoSiniestro.Text,
            //TxtIngresoTaller.Text, TxtFechaAprobacion.Text, TxtFechaEntrega.Text, TxtSumaLiquidacion.Text, TxtNumeroCaso.Text, TxtNombreTaller.Text,
            //TxtDireccionTaller.Text, TxtTelefonoTaller.Text);

            hfIdGestion.Value = ticket;
            SubirArchivoGestion();

            //JRL 2020-02-27
            DataTable dtTicketIngresado = new DataTable();
            dtTicketIngresado = dal.getBuscarGestionPorIdTicket(ticket).Tables[0];
            string Adj1 = string.Empty;
            string Adj2 = string.Empty;
            foreach (DataRow item in dtTicketIngresado.Rows)
            {
                Adj1 = item["RUTA_ARCHIVO"].ToString();
                Adj2 = item["RUTA_ARCHIVO2"].ToString();
            }

            //JRL Ingreso de ticket                    
            string bodyClienteInterno = "Hola! <br>";
            bodyClienteInterno += "<br>Tu requerimiento recibido el " + DateTime.Now.ToString() + " ha sido ingresado ";
            bodyClienteInterno += "<br>exitosamente bajo el siguiente número de ticket " + ticket + ".";
            bodyClienteInterno += "<br><br>Si tienes alguna consulta sobre el estado o resolución de este ticket, ";
            bodyClienteInterno += "<br>te puedes comunicar a través del teléfono 800 800 008.";
            bodyClienteInterno += "<br><br>Saludos!<br>";

            bodyClienteInterno += "<br><b>Equipo de Servicio al Cliente Esval.</b>";

            string bodyResolutor = "Estimado(a) " + nombreUsuarioEscN1 + "<br><br>";//JRL 2019-03-26 Nombre persona encargada de resolver
            bodyResolutor += "Le informamos que tiene un nuevo ticket en su bandeja de trabajo.<br><br>";
            bodyResolutor += "<b>Nro. de Ticket: </b>" + ticket + "<br>";
            bodyResolutor += "<b>Usuario solicitante: </b>" + lblNombreCliente.Text + "<br>";
            bodyResolutor += "<b>Descripción: </b>" + txtComentario.Text + "<br><br>";
            bodyResolutor += "Para gestionar el caso <a href='http://" + sistema + "SeguimientoTicket.aspx?t=" + ticket + "'>Pinche acá</a>";
            bodyResolutor += "<br><br>Atte.<br><br>";
            bodyResolutor += "<br><b>Equipo de Servicio al Cliente Esval.</b>";

            //string bodyResolutor = "Estimado(a) Usuario:";
            //bodyResolutor += "<br><br>";
            //bodyResolutor += "Se ha generado el siguiente caso para su gestión:";
            //bodyResolutor += "<br><br>";
            //bodyResolutor += "<b>" + txtComentario.Text + "</b>";
            //bodyResolutor += "<br><br>";
            //bodyResolutor += "Le agradecemos revisar a la brevedad CRM Esval";
            //bodyResolutor += "<br> http://" + sistema + "SeguimientoTicket.aspx?t=" + ticket;
            //bodyResolutor += "<br><br>";
            //bodyResolutor += "<br>Muchas gracias!";
            //bodyResolutor += "<br><b>Equipo de Servicio al Cliente Esval.</b>";


            //enviarEmail("ldiaz@interweb.cl", body.Replace("\r\n", "<br>"), "Gestión " + idGestion + ", se ha generado el siguiente caso para su gestión");
            //if (idUsuarioEscN1Email.Trim() != string.Empty)
            //{
            //    //JRL 2018-05-03 agregado null al archivo adjunto
            //    comunes.EnviarEmailSSLImplicito(idUsuarioEscN1Email.Trim(), bodyResolutor.Replace("\r\n", "<br>"), "Servicio al Cliente Cardif_ Atención N° " + ticket, null);
            //}
            /*"Gestión " + hfIdGestion.Value + " ingresada correctamente." + "<br>" +*/


            DataTable dt1 = new DataTable();
            dt1 = dal.getBuscarClientePorEmail(lblEmailCliente.Text).Tables[0];

            string nombre = string.Empty;
            string paterno = string.Empty;
            string materno = string.Empty;

            foreach (DataRow item in dt1.Rows)
            {
                nombre = item["NOMBRE"].ToString();
                paterno = item["PATERNO"].ToString();
                materno = item["MATERNO"].ToString();
            }

            DataTable dt2 = new DataTable();
            dt2 = dal.getBuscarFechaIngresoTicket(ticket).Tables[0];

            string fechaIngreso = "";
            foreach (DataRow item2 in dt2.Rows)
            {
                fechaIngreso = item2["FECHA"].ToString();
            }

            //2021-08-12 Comentado
            //if (!string.IsNullOrEmpty(idUsuarioEscN1Email))
            //{
            //    comunes.EnviarEmailSSLImplicito(idUsuarioEscN1Email.Trim(), bodyResolutor.Replace("\r\n", "<br>"), "Asignacion ticket: " + ticket, null);
            //}

            string bodyClienteDeriva = "Hola! " + nombre + " " + paterno + " " + materno + "<br>";

            bodyClienteDeriva += "<br><b>Observación Cliente:</b><br>" + txtObservacionCliente.Text + "<br>";
            //bodyPrimera += "<br>Te informamos que tu ticket " + ticket + " ingresado el " + fechaIngreso;
            //bodyPrimera += "<br>ha sido resuelto y cerrado por el ejecutivo a cargo.<br>";
            //bodyPrimera += "<br>Si no estás conforme con la resolución del caso, por favor contáctanos";
            //bodyPrimera += "<br>a través del teléfono 800 800 008.<br>";
            //bodyPrimera += "<br>Saludos!<br>";           

            //JRL 2020-02-27
            if (!string.IsNullOrEmpty(Adj1))
            {
                bodyClienteDeriva += "<br>Adjunto1: <a href='http://" + sistema + Adj1 + "'>" + Adj1.Replace("archivosGestion/", " ") + "</a>";
            }

            if (!string.IsNullOrEmpty(Adj2))
            {
                bodyClienteDeriva += "<br>Adjunto2: <a href='http://" + sistema + Adj2 + "'>" + Adj2.Replace("archivosGestion/", " ") + "</a>";
            }

            bodyClienteDeriva += "<br><br><b>Equipo de Servicio al Cliente Esval.</b>";

            if (chkEnviarObsCliente.Checked)
            {
                if (!string.IsNullOrEmpty(lblEmailCliente.Text))
                {
                    //JRL 2018-05-03 agregado null al archivo adjunto
                    com.EnviarEmailSSLImplicito(lblEmailCliente.Text.Trim(), bodyClienteDeriva.Replace("\r\n", "<br>"), "Servicio al Cliente Esval // Derivar Ticket " + ticket, null);
                }
                
                string nombreDest = string.Empty;
                string emailDest = string.Empty;
                string nomUsuSesion = string.Empty;

                foreach (DataRow item in dal.getBuscarUsuarioPorId(Convert.ToString(Session["variableIdUsuario"])).Tables[0].Rows)
                {
                    nomUsuSesion = item["NOMBRE"].ToString().Trim();
                }

                string obsCliEmail = txtObservacionCliente.Text.Trim() + "<br><br><b>" + nomUsuSesion + ".</b>";

                DataTable dtServicioNoti = new DataTable();
                dtServicioNoti = dal.GetBuscarServicioNotiticacion(Convert.ToInt32(ddlServicio.SelectedValue), 0).Tables[0];

                //2024-07-09 Comentado y modificado abajo
                //if (ddlServicio.SelectedValue == "2" && (idTipificacion == "6"))//Servicio Clave 1 / Clave 2     Nivel2 Clave 1 - Incendio Estructural
                //{
                //    foreach (DataRow item in dtServicioNoti.Rows)
                //    {
                //        nombreDest = item["NOMBRE"].ToString().Trim();
                //        emailDest = item["EMAIL"].ToString().Trim();

                //        if (!string.IsNullOrEmpty(emailDest))
                //        {
                //            if (com.IsValidEmail(emailDest))
                //            {
                //                com.EnviarEmailSSLImplicito(emailDest, obsCliEmail.Replace("\r\n", "<br>"), ddlNivel1.SelectedValue + " " + ddlNivel2.SelectedValue, null);
                //            }
                //        }
                //    }
                //}

                //if (ddlServicio.SelectedValue == "2" && (idTipificacion == "7"))//Servicio Clave 1 / Clave 2     Nivel2 Clave 2 - Incendio Forestal
                //{
                //    foreach (DataRow item in dtServicioNoti.Rows)
                //    {
                //        nombreDest = item["NOMBRE"].ToString().Trim();
                //        emailDest = item["EMAIL"].ToString().Trim();

                //        if (!string.IsNullOrEmpty(emailDest))
                //        {
                //            if (com.IsValidEmail(emailDest))
                //            {
                //                com.EnviarEmailSSLImplicito(emailDest, obsCliEmail.Replace("\r\n", "<br>"),ddlNivel1.SelectedValue + " " +  ddlNivel2.SelectedValue, null);
                //            }
                //        }
                //    }                    
                //}

                //if (ddlServicio.SelectedValue == "4")//Servicio Carga Nocturna
                //{
                //    foreach (DataRow item in dtServicioNoti.Rows)
                //    {
                //        nombreDest = item["NOMBRE"].ToString().Trim();
                //        emailDest = item["EMAIL"].ToString().Trim();

                //        if (!string.IsNullOrEmpty(emailDest))
                //        {
                //            if (com.IsValidEmail(emailDest))
                //            {
                //                com.EnviarEmailSSLImplicito(lblEmailCliente.Text.Trim(), obsCliEmail.Replace("\r\n", "<br>"), ddlServicio.SelectedItem.Text + " " + DateTime.Now.ToString("G"), null);
                //            }
                //        }
                //    }                    
                //}

                //2024-07-09 Agregado
                foreach (DataRow item in dtServicioNoti.Rows)
                {
                    nombreDest = item["NOMBRE"].ToString().Trim();
                    emailDest = item["EMAIL"].ToString().Trim();

                    if (!string.IsNullOrEmpty(emailDest))
                    {
                        if (com.IsValidEmail(emailDest))
                        {
                            com.EnviarEmailSSLImplicito(lblEmailCliente.Text.Trim(), obsCliEmail.Replace("\r\n", "<br>"), ddlServicio.SelectedItem.Text + " " + DateTime.Now.ToString("G"), null);
                        }
                    }
                }
            }

            lblInfo.Text = "Se generó el número de ticket " + ticket;
            divAlerta.Attributes["class"] = "alert alert-success";
            divAlerta.Visible = true;

            return ticket;
        }
        protected void btnVerCaseMaker_Click(object sender, EventArgs e)
        {
            try
            {
                buscarCaseMaker(lblRutCliente.Text);
                ScriptManager.RegisterStartupScript(UpdatePanel3, UpdatePanel3.GetType(), "show", "$(function () { $('#" + pnlCaseMaker.ClientID + "').modal('show'); });", true);
                UpdatePanel3.Update();
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        void buscarCaseMaker(string rut)
        {
            grvCaseMaker.DataSource = dal.getBuscarCaseMaker(rut);
            grvCaseMaker.DataBind();

        }

        protected void txtBuscarCliente_TextChanged(object sender, EventArgs e)
        {
            btnBuscarCliente_Click(sender, e);
        }

        protected void lbtnGrabarClienteExt_Click(object sender, EventArgs e)
        {
            try
            {
                int idUsuario = Convert.ToInt32(Session["variableIdUsuario"]);
                string msj;
                int? codError;
                //string rutCodDv = lblRutCliente2.Text;

                string rutCodDv = txtRutClienteExt.Text;
                string rut = string.Empty, dv = string.Empty;

                if (!com.validarRut(rutCodDv))
                {
                    lblInfo.Text = "Debe ingresar un RUT válido.";
                    divAlerta.Attributes["class"] = "alert alert-danger";
                    divAlerta.Visible = true;
                    return;
                }


                if (txtCellPhone.Text == string.Empty && txtPhone.Text == string.Empty && txtEmailClient.Text == string.Empty)
                {
                    lblInfo.Text = "Debe ingresar al menos un teléfono, celular o email respectivamente.";
                    divAlerta.Attributes["class"] = "alert alert-danger";
                    divAlerta.Visible = true;
                    return;
                }

                if (txtCellPhone.Text != string.Empty && txtCellPhone.Text.Length != 9)
                {
                    lblInfo.Text = "Debe ingresar 9 caracteres en Celular.";
                    divAlerta.Attributes["class"] = "alert alert-danger";
                    divAlerta.Visible = true;
                    return;
                }

                if (txtPhone.Text != string.Empty && txtPhone.Text.Length != 9)
                {
                    lblInfo.Text = "Debe ingresar 9 caracteres en teléfono.";
                    divAlerta.Attributes["class"] = "alert alert-danger";
                    divAlerta.Visible = true;
                    return;
                }

                if (ddlRutoEmail.SelectedValue == "RUT")
                {
                    if (txtRutClienteExt.Text.Trim() == string.Empty)
                    {
                        lblInfo.Text = "El campo RUT es obligatorio para guardar el cliente";
                        divAlerta.Attributes["class"] = "alert alert-danger";
                        divAlerta.Visible = true;
                        return;
                    }
                    com.separaRut(rutCodDv, out rut, out dv);
                }

                string celular = txtCellPhone.Text.Trim();
                if (ddlRutoEmail.SelectedValue == "RUT")
                {
                    if (celular == string.Empty)
                    {
                        lblInfo.Text = "El campo celular es obligatorio y númerico";
                        divAlerta.Attributes["class"] = "alert alert-danger";
                        divAlerta.Visible = true;
                        return;
                    }
                }

                if (txtEmailClient.Text.Trim() == string.Empty)
                {
                    lblInfo.Text = "El campo email es obligatorio";
                    divAlerta.Attributes["class"] = "alert alert-danger";
                    divAlerta.Visible = true;
                    return;
                }

                int? cel;
                if (com.IsNumeric(celular))
                {
                    cel = Convert.ToInt32(celular);
                }
                else
                {
                    cel = null;
                }

                string fijo = txtPhone.Text.Trim();
                int? tel;
                if (com.IsNumeric(fijo))
                {
                    tel = Convert.ToInt32(fijo);
                }
                else
                {
                    tel = null;
                }

                string local = txtLocal.Text.Trim();
                int? codLocal;
                if (com.IsNumeric(local))
                {
                    codLocal = Convert.ToInt32(local);
                }
                else
                {
                    codLocal = null;
                }


                string ruts = rut + '-' + dv;
                if (ddlRutoEmail.SelectedValue == "RUT")
                {
                    //datos.setIngresaCliente(1, Convert.ToInt32(rut), dv, "CRM", 
                    //    txtNombreClienteExt.Text.Trim(), txtApellidoParternoClienteExt.Text.Trim(),
                    //txtApellidoMaterno.Text.Trim(), "SB", txtEmailClient.Text.Trim(), 
                    //cel, tel, "", 100, ddlSernac.SelectedValue, out codError, out msj);
                    dal.setIngresarCliente(ruts, txtEmailClient.Text.Trim(), txtNombreClienteExt.Text.Trim().ToUpper(),
                       txtApellidoParternoClienteExt.Text.Trim().ToUpper(), txtApellidoMaterno.Text.Trim().ToUpper(),
                       tel.ToString(), cel.ToString(), idUsuario, txtCalle.Text, txtNumero.Text, txtResto.Text, ddlComuna.SelectedValue);
                }

                btnBuscarCliente_Click(sender, e);

                lblInfo.Text = "Cliente grabado correctamente.";
                divAlerta.Attributes["class"] = "alert alert-info";
                divAlerta.Visible = true;
                //datos.setIngresaCliente(1, 16394005, "1", "CRM", "Leonardo", "Diaz", "Corvalan", "SB", "ldiaz@interweb.cl", 98987654, 2323232, "29-11-2016", 100, "N", out codError, out msj);
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void btnNuevoCliente_Click(object sender, EventArgs e)
        {
            try
            {
                txtRutClienteExt.Enabled = true;
                limpiarClienteExt();
                divEnriquecimientoContactabilidad.Visible = true;
                divResultadoBusqueda.Visible = false;

                divClienteSeleccionado.Visible = false;
                txtRutClienteExt.Text = txtBuscarCliente.Text;

                //  <% --divClienteSeleccionado-- %>
                //<% --divEnriquecimientoContactabilidad-- %>
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void btnModificarCliente_Click(object sender, EventArgs e)
        {
            try
            {
                //JRL 2018-10-02
                txtRutClienteExt.Enabled = false;
                limpiarClienteExt();
                divEnriquecimientoContactabilidad.Visible = true;

                //hfNombres.Value = nombres;
                //hfPaterno.Value = paterno;
                //hfMaterno.Value = materno;
                //hfSegmentoCliente.Value = segmentoCliente;
                //hfEmail.Value = email;
                //hfFechaCompra.Value = fechaCompra;
                //hfSernac.Value = sernac;
                //hfCelular.Value = celular;
                //hfFijo.Value = fijo;
                //hfLocal.Value = local;

                if (hfNombres.Value == "null")
                {
                    hfNombres.Value = "";
                }
                if (hfPaterno.Value == "null")
                {
                    hfPaterno.Value = "";
                }
                if (hfMaterno.Value == "null")
                {
                    hfMaterno.Value = "";
                }
                if (hfEmail.Value == "null")
                {
                    hfEmail.Value = "";
                }
                if (hfFijo.Value == "null")
                {
                    hfFijo.Value = "";
                }
                if (hfCelular.Value == "null")
                {
                    hfCelular.Value = "";
                }
                if (hfSernac.Value == "null")
                {
                    hfSernac.Value = "";
                }
                if (hfCalle.Value == "null")
                {
                    hfCalle.Value = "";
                }
                if (hfNumero.Value == "null")
                {
                    hfNumero.Value = "";
                }
                if (hfResto.Value == "null")
                {
                    hfResto.Value = "";
                }
                if (hfComuna.Value == "null")
                {
                    hfComuna.Value = "";
                }

                txtRutClienteExt.Text = txtBuscarCliente.Text;
                //Comun com = new Comun();
                if (com.IsValidEmail(txtBuscarCliente.Text))
                {
                    ddlRutoEmail.SelectedValue = "EMAIL";
                }
                else
                {
                    ddlRutoEmail.SelectedValue = "RUT";
                }

                txtNombreClienteExt.Text = hfNombres.Value;
                txtApellidoParternoClienteExt.Text = hfPaterno.Value;
                txtApellidoMaterno.Text = hfMaterno.Value;
                txtEmailClient.Text = hfEmail.Value;
                txtPhone.Text = hfFijo.Value;
                txtCellPhone.Text = hfCelular.Value;
                txtCalle.Text = hfCalle.Value;
                txtNumero.Text = hfNumero.Value;
                txtResto.Text = hfResto.Value;

                if (hfSernac.Value != string.Empty)
                {
                    ddlSernac.SelectedValue = hfSernac.Value;
                }
                if (hfComuna.Value != string.Empty)
                {
                    ddlComuna.SelectedValue = hfComuna.Value;
                }
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        void limpiarClienteExt()
        {
            txtRutClienteExt.Text = string.Empty;
            txtNombreClienteExt.Text = string.Empty;
            txtApellidoParternoClienteExt.Text = string.Empty;
            txtApellidoMaterno.Text = string.Empty;
            txtEmailClient.Text = string.Empty;
            txtPhone.Text = string.Empty;
            txtCellPhone.Text = string.Empty;
            ddlSernac.ClearSelection();
            txtCalle.Text = string.Empty;
            txtNumero.Text = string.Empty;
            txtResto.Text = string.Empty;
            ddlComuna.ClearSelection();
        }

        protected void btnVerHistorialSernac_Click(object sender, EventArgs e)
        {
            try
            {
                //grvHistorialSernac.DataSource = dal.getBuscarHistorialSernac(lblRutCliente2.Text);
                //grvHistorialSernac.DataBind();

                //ScriptManager.RegisterStartupScript(UpdatePanel4, UpdatePanel4.GetType(), "show", "$(function () { $('#" + pnlHistoralSernac.ClientID + "').modal('show'); });", true);
                //UpdatePanel4.Update();
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void chkHabilitarEmail_CheckedChanged(object sender, EventArgs e)
        {
            if (chkHabilitarEmail.Checked == true)
            {
                txtEmail.Enabled = true;
                fuArchivo3.Enabled = true;
            }
            else
            {
                txtEmail.Enabled = false;
                fuArchivo3.Enabled = false;
            }
        }

        protected void chkEnviarObsCliente_CheckedChanged(object sender, EventArgs e)
        {
            //txtObservacionCliente.Text = string.Empty;
            if (chkEnviarObsCliente.Checked == true)
            {
                if (ddlServicio.SelectedValue == "2")//Clave 1 / Clave 2
                {
                    txtObservacionCliente.Text = "Estimados, Se informa siniestro en la ubicación:\n" +
                        "DIRECCIÖN:\n" +
                        "REFERENCIAS (SI EXISTEN):\n" +
                        "LOCALIDAD:\n" +
                        "Saludos";
                }
                else if (ddlServicio.SelectedValue == "4")//Carga Nocturna
                {
                    txtObservacionCliente.Text = "Estimados, Se informa Carga Nocturna con fecha: " + DateTime.Now.ToString("G") + "\n" +
                        "Localidad:\n" +
                        "Dirección de los trabajos:\n" +
                        "Recibe información (CENCO):\n" +
                        "Saludos";
                }
            }
        }

        protected void _iw_div_email_send_chk_CheckedChanged(object sender, EventArgs e)
        {
            //if (chkHabilitarEmail.Checked == true)
            //{
            //    //_iw_div_email_to.Text = lblEmailCliente.Text;
            //    //_iw_div_email.Visible = true;
            //    divArchivoCliente.Visible = true;                
            //}
            //else
            //{
            //    //_iw_div_email.Visible = false;
            //    divArchivoCliente.Visible = false;
            //}
        }

        protected void ddlServicio_DataBinding(object sender, EventArgs e)
        {
            nivel1();
        }

        protected void ddlCanal_DataBound(object sender, EventArgs e)
        {
            ddlCanal.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        }

        protected void ddlCanal_SelectedIndexChanged1(object sender, EventArgs e)
        {
            adiconales(Convert.ToInt32(ddlCanal.SelectedValue));
        }

        //JRL 2018-10-01
        protected void txtRutClienteExt_TextChanged(object sender, EventArgs e)
        {
            //Comun comu = new Comun();
            DataTable dt2 = new DataTable();
            if (com.IsValidEmail(txtRutClienteExt.Text.Trim()) == false)
            {
                dt2 = dal.getBuscarClientePorRut(txtRutClienteExt.Text.Trim()).Tables[0];
            }

            if (dt2.Rows.Count != 0)
            {
                divAlerta.Visible = true;
                lblInfo.Text = "El RUT del Cliente ya existe";
                divAlerta.Attributes["class"] = "alert alert-warning";

                //txtRutClienteExt.Text = string.Empty;
            }

            string nombres = string.Empty, paterno = string.Empty, materno = string.Empty, email = string.Empty,
                    celular = string.Empty, fijo = string.Empty;

            foreach (DataRow item in dt2.Rows)
            {
                nombres = item["NOMBRE"].ToString();
                paterno = item["PATERNO"].ToString();
                materno = item["MATERNO"].ToString();
                email = item["EMAIL"].ToString();
                celular = item["CELULAR"].ToString();
                fijo = item["TELEFONO"].ToString();
                // rutConDV = item["EMAIL"].ToString();
            }

            txtNombreClienteExt.Text = nombres;
            txtApellidoParternoClienteExt.Text = paterno;
            txtApellidoMaterno.Text = materno;
            txtEmailClient.Text = email;
            txtPhone.Text = fijo;
            txtCellPhone.Text = celular;
        }

        protected void txtIngreseTitular_TextChanged(object sender, EventArgs e)
        {
            //Comun comu = new Comun();
            DataTable dt = new DataTable();

            if (!com.validarRut(txtIngreseTitular.Text.Trim()))
            {
                lblInfo.Text = "El RUT Ingrese Titular no es válido";
                divAlerta.Attributes["class"] = "alert alert-warning";
                divAlerta.Visible = true;

                lblNombreCliTitular.Visible = false;
            }
            else
            {
                dt = dal.getBuscarClientePorRut(txtIngreseTitular.Text.Trim()).Tables[0];

                string nombres = string.Empty, paterno = string.Empty, materno = string.Empty;

                foreach (DataRow item in dt.Rows)
                {
                    nombres = item["NOMBRE"].ToString();
                    paterno = item["PATERNO"].ToString();
                    materno = item["MATERNO"].ToString();
                }

                if (dt.Rows.Count != 0)
                {
                    lblNombreCliTitular.Visible = true;
                    lblNombreCliTitular.Text = nombres + ' ' + paterno + ' ' + materno;
                }
                else
                {
                    lblNombreCliTitular.Visible = false;
                }
            }
        }

        protected void DdlEmisor_DataBound(object sender, EventArgs e)
        {
            DdlEmisor.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        }

        protected void DdlSocio_DataBound(object sender, EventArgs e)
        {
            DdlSocio.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        }

        protected void DdlEmisor_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnArchivo1_Click(object sender, EventArgs e)
        {
            try
            {
                //LinkButton btn = (LinkButton)sender;
                //GridViewRow row = (GridViewRow)btn.NamingContainer;
                //Label _lblArchivo1 = (Label)Grv.Rows[row.RowIndex].FindControl("lblArchivo1");
                ScriptManager.RegisterStartupScript(this, this.GetType(), UniqueID, "window.open('" + lblRutaArch1.Text + "','_blank');", true);
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (RadioButtonList1.SelectedItem.ToString() == "Titular")
            {
                txtIngreseTitular.Text = string.Empty;
                lblNombreCliTitular.Text = string.Empty;
                lblCliTitularNoTitular.Text = ":: Cliente Titular Seleccionado ::";
                divIngreseTitular.Visible = false;
            }
            if (RadioButtonList1.SelectedItem.ToString() == "No Titular")
            {
                txtIngreseTitular.Text = string.Empty;
                lblNombreCliTitular.Text = string.Empty;
                lblCliTitularNoTitular.Text = ":: Cliente No Titular Seleccionado ::";
                divIngreseTitular.Visible = true;
            }
        }

        protected void BtnVerScript_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtDeriva = new DataTable();

                dtDeriva = dal.getBuscarTipificacionPorIdEstatusIdSubEstatusIdEstatusGestion(ddlNivel1.SelectedValue, ddlNivel2.SelectedValue, ddlNivel3.SelectedValue, ddlNivel4.SelectedValue, Convert.ToInt32(ddlServicio.SelectedValue)).Tables[0];
               
                DataTable dt = new DataTable();
                foreach (DataRow item in dtDeriva.Rows)
                {
                    int tip = Convert.ToInt32(item["ID_TIPIFICACION"]);
            
                    dt = dal.getBuscarScriptRespuesta(tip).Tables[0];
                }

                GrvScript.DataSource = dt;
                GrvScript.DataBind();

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "MostrarModalScript();", true);
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void GrvScript_PreRender(object sender, EventArgs e)
        {
            if (GrvScript.Rows.Count > 0)
            {
                GrvScript.UseAccessibleHeader = true;
                GrvScript.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }
        protected void paginacionScript_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label _LblVerDocumentos = (Label)e.Row.FindControl("LblVerDocumentos");
                Button _BtnVerDocumentos = (Button)e.Row.FindControl("BtnVerDocumentos");

                if (_LblVerDocumentos.Text == "0")
                {
                    _BtnVerDocumentos.Visible = false;
                }
                else
                {
                    _BtnVerDocumentos.Visible = true;
                }
            }
        }

        protected void BtnVerDocumentos_Click(object sender, EventArgs e)
        {
            try
            {
                Button img = (Button)sender;
                GridViewRow row = (GridViewRow)img.NamingContainer;
                Label _LblIdRespuesta = (Label)GrvScript.Rows[row.RowIndex].FindControl("LblIdRespuesta");
                BuscarDocumentosRespuesta(Convert.ToInt32(_LblIdRespuesta.Text));
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "MostrarModalDocumentos();", true);

            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }
        void BuscarDocumentosRespuesta(int IdRespuesta)
        {
            DataTable dt = dal.getBuscarDocumentosRespuesta(IdRespuesta).Tables[0];
            GrvDocumentos.DataSource = dt;
            GrvDocumentos.DataBind();
        }

        protected void GrvDocumentos_PreRender(object sender, EventArgs e)
        {

        }

        protected void btnArchivo_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                GridViewRow row = (GridViewRow)btn.NamingContainer;
                Label _lblArchivo = (Label)GrvDocumentos.Rows[row.RowIndex].FindControl("lblArchivo");
                ScriptManager.RegisterStartupScript(this, this.GetType(), UniqueID, "window.open('" + _lblArchivo.Text + "','_blank');", true);
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }
    }
}