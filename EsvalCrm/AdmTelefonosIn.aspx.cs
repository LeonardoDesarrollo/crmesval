﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
namespace EsvalCrm
{
    public partial class AdmTelefonosIn : System.Web.UI.Page
    {
        TelefonoAccess DalTel = new TelefonoAccess();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.Page.IsPostBack)
                {
                    string _Id = Convert.ToString(Request.QueryString["Id"]);
                 
                    if (!string.IsNullOrEmpty(_Id))
                    {
                        HdId.Value = _Id;
                        TxtId.Text = _Id;
                        if (HdId.Value == "0")
                        {
                            return;
                        }

                        DataTable dt = new DataTable();
                        dt = DalTel.GetTelefonos(Convert.ToInt32(HdId.Value)).Tables[0];
                        foreach (DataRow item in dt.Rows)
                        {
                            TxtId.Text = item["ID"].ToString();
                            TxtNombre.Text = item["NOMBRE"].ToString();
                            TxtTelefono.Text = item["TELEFONO"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void BtnGrabar_Click(object sender, EventArgs e)
        {
            try
            {

                DalTel.SetInTelefono(HdId.Value, TxtNombre.Text.Trim(), TxtTelefono.Text.Trim());

                lblInfo.Text = "Telefono guardado.";
                divAlerta.Attributes["class"] = "alert alert-success";
                divAlerta.Visible = true;
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void BtnVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("AdmTelefonos");
        }
    }
}