﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EMails.aspx.cs" Inherits="EsvalCrm.EMails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Dashboard</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">

            <!-- Alertas -->
            <div id="divAlerta" runat="server" visible="false" class="alert alert-danger">
                <strong>Atención!: </strong>
                <asp:Label Text="" ID="lblInfo" runat="server" />
            </div>


            <div runat="server" id="_div_filtros" class="box box-danger" visible="true">
                <div class="box-header">
                </div>
                <div class="box-body">

                    <div class="row">
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="_div_filtros_fecha_desde">Desde:</label>
                                        <asp:TextBox ID="_div_filtros_fecha_desde" runat="server" CssClass="form-control input-sm class-date" AutoPostBack="true"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="_div_filtros_fecha_hasta">Hasta:</label>
                                        <asp:TextBox ID="_div_filtros_fecha_hasta" runat="server" CssClass="form-control input-sm class-date" AutoPostBack="true"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="_div_filtros_ddl_tipificado">Tipificado:</label>
                                        <asp:DropDownList ID="_div_filtros_ddl_tipificado" runat="server" CssClass="form-control input-sm" AutoPostBack="true">
                                            <asp:ListItem Text="Todos" Value="-1"></asp:ListItem>
                                            <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Si" Value="1"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="_div_filtros_ddl_spam">Spam:</label>
                                        <asp:DropDownList ID="_div_filtros_ddl_spam" runat="server" CssClass="form-control input-sm" AutoPostBack="true">
                                            <asp:ListItem Text="Todos" Value="-1"></asp:ListItem>
                                            <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Si" Value="1"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="_div_filtros_ddl_ejecutivo">Cuenta:</label>
                                        <asp:DropDownList ID="DdlCuenta" runat="server" CssClass="form-control input-sm" AutoPostBack="true" OnDataBound="DdlCuenta_DataBound">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                        <div class="col-sm-1">
                            <div class="form-group">
                                <br />
                                <asp:Button ID="_div_filtros_buscar" Text="Buscar" runat="server" CssClass="btn btn-success" OnClick="_div_filtros_buscar_Click"></asp:Button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                </div>
            </div>

            <asp:UpdatePanel runat="server">
                <ContentTemplate>

                    <div runat="server" id="_div_grid" class="box box-danger" visible="true">

                        <div class="box-body" style="overflow-x: auto">
                            <asp:GridView ID="_div_grid_emails" runat="server" AutoGenerateColumns="false" BorderColor="Transparent" ShowHeaderWhenEmpty="false" AllowPaging="true" OnRowDataBound="_div_grid_emails_RowDataBound" OnPreRender="_div_grid_emails_PreRender" PageSize="100" CssClass="table table-hover table-condensed">
                                <Columns>
                                    <asp:TemplateField HeaderText="ID">
                                        <ItemTemplate>
                                            <asp:Label ID="_div_grid_emails_id" runat="server" Text='<%# Bind("ID_EMAIL") %>' Visible="true"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fecha">
                                        <ItemTemplate>
                                            <asp:Label ID="_div_grid_emails_fecha" runat="server" Text='<%# Bind("FECHA") %>' Visible="true"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="De">
                                        <ItemTemplate>
                                            <asp:Label ID="_div_grid_emails_from" runat="server" Text='<%# Bind("DE") %>' Visible="true"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Para">
                                        <ItemTemplate>
                                            <asp:Label ID="_div_grid_emails_to" runat="server" Text='<%# Bind("PARA") %>' Visible="true"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Asunto">
                                        <ItemTemplate>
                                            <asp:Label ID="_div_grid_emails_subject" runat="server" Text='<%# Bind("SUBJECT") %>' Visible="true"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Obs">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="_div_grid_emails_subjectbtn" OnClick="_div_grid_emails_subjectbtn_Click" runat="server" CssClass="btn btn-xs btn-success"><span class="glyphicon glyphicon-duplicate"></span></asp:LinkButton>
                                            <asp:Label ID="_div_grid_emails_body" runat="server" Text='<%# Bind("BODY") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Spam">
                                        <ItemTemplate>
                                            <asp:Label ID="_div_grid_emails_spam" runat="server" Text='<%# Bind("SPAM") %>' Visible="false"></asp:Label>
                                            <asp:LinkButton ID="_div_grid_emails_uspam_btn" OnClick="_div_grid_emails_uspam_btn_Click" ToolTip="Desmarcar como SPAM o Agradecimiento" OnClientClick="return confirm('¿Desea desmarcar como SPAM el registro?');" runat="server" CssClass="btn btn-xs btn-success"><span class="glyphicon glyphicon-duplicate"></span></asp:LinkButton>
                                            <asp:LinkButton ID="_div_grid_emails_spam_btn" OnClick="_div_grid_emails_spam_btn_Click" ToolTip="Marcar como SPAM o Agradecimiento" OnClientClick="return confirm('¿Desea marcar como SPAM el registro?');" runat="server" CssClass="btn btn-xs btn-danger"><span class="glyphicon glyphicon-erase"></span></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Tipificar">
                                        <ItemTemplate>
                                            <asp:Label ID="_div_grid_emails_tipificar" runat="server" Text='<%# Bind("TIPIFICADO") %>' Visible="false"></asp:Label>
                                            <asp:LinkButton ID="_div_grid_emails_tipificar_btn" OnClick="_div_grid_emails_tipificar_btn_Click" ToolTip="Tipificar EMail" runat="server" CssClass="btn btn-xs btn-primary"><span class="glyphicon glyphicon-edit"></span></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="IdAtencion">
                                        <ItemTemplate>
                                            <asp:Label ID="_div_grid_emails_id_atencion" runat="server" Text='<%# Bind("ID_ATENCION") %>' Visible="true"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerTemplate>
                                    <div>
                                        <div style="float: left">
                                            <asp:ImageButton ID="imgFirst" runat="server"
                                                ImageUrl="~/img/grid/first.gif" OnClick="imgFirst_Click"
                                                Style="height: 15px" title="Navegación: Ir a la Primera Pagina" Width="26px" />
                                            <asp:ImageButton ID="imgPrev" runat="server"
                                                ImageUrl="~/img/grid/prev.gif" OnClick="imgPrev_Click"
                                                title="Navegación: Ir a la Pagina Anterior" Width="26px" />
                                            <asp:ImageButton ID="imgNext" runat="server"
                                                ImageUrl="~/img/grid/next.gif" OnClick="imgNext_Click"
                                                title="Navegación: Ir a la Siguiente Pagina" Width="26px" />
                                            <asp:ImageButton ID="imgLast" runat="server"
                                                ImageUrl="~/img/grid/last.gif" OnClick="imgLast_Click"
                                                title="Navegación: Ir a la Ultima Pagina" Width="26px" />
                                        </div>
                                        <div style="float: left">
                                            Registros por página: 100
                                        </div>
                                        <div style="float: right">
                                            Total Registros: 
                                       <asp:Label ID="lblTotalRegistros" runat="server"></asp:Label>
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                       Página
                                       <asp:Label ID="lblPagina" runat="server"></asp:Label>
                                            de
                                       <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                </PagerTemplate>

                            </asp:GridView>
                        </div>
                    </div>

                    <asp:Panel ID="_iw_modal_pEmail" runat="server" CssClass="modal fade bs-example-modal-lg" TabIndex="-1" role="dialog" aria-labelledby="myLabel">
                        <div class="modal-dialog modal-lg" role="document" runat="server">
                            <div class="modal-content">

                                <asp:UpdatePanel ID="_iw_modal_pEmailv" runat="server" ValidateRequestMode="Disabled" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="modal-header">
                                            <button type="button" class="close btn btn-danger" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myLabel">EMail Body</h4>
                                        </div>
                                        <div class="modal-body">
                                            <asp:Label runat="server" ID="_iw_modal_pEmailv_txt"></asp:Label>
                                        </div>
                                        <div class="modal-footer">
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </asp:Panel>


                </ContentTemplate>
            </asp:UpdatePanel>

        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="server">
</asp:Content>
