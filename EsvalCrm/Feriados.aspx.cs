﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;

namespace EsvalCrm
{
    public partial class Feriados : System.Web.UI.Page
    {
        Datos dal = new Datos();
        protected void Page_Load(object sender, EventArgs e)
        {
            //cl.dias.www.Feriado fer = new cl.dias.www.Feriado();
            //cl.dias.www.Desytec_Feriados feriado = new cl.dias.www.Desytec_Feriados();

            //feriado.GetHolidays(null, "CL", "2016", null);
            if (!this.Page.IsPostBack)
            {
                buscar();
            }
        }

        void buscar()
        {
            grvFeriados.DataSource = dal.getBuscarFeriado(1);
            grvFeriados.DataBind();
            grvFeriados.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                limpiar(this.Controls);
                divSearch.Visible = false;
                divAddEdit.Visible = true;
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-warning";
                divAlerta.Visible = true;

            }
        }

        protected void btnEditar_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                GridViewRow row = (GridViewRow)btn.NamingContainer;
                Label _lblIdFeriado = (Label)grvFeriados.Rows[row.RowIndex].FindControl("lblIdFeriado");
                hfIdFeriado.Value = _lblIdFeriado.Text;
                Label _lblFechaFeriado = (Label)grvFeriados.Rows[row.RowIndex].FindControl("lblFechaFeriado");
                txtFechaFeriado.Text = _lblFechaFeriado.Text;
                Label _lblActivo = (Label)grvFeriados.Rows[row.RowIndex].FindControl("lblActivo");
                ddlActivo.SelectedValue = _lblActivo.Text;

                divSearch.Visible = false;
                divAddEdit.Visible = true;
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-warning";
                divAlerta.Visible = true;

            }
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                GridViewRow row = (GridViewRow)btn.NamingContainer;
                Label _lblIdFeriado = (Label)grvFeriados.Rows[row.RowIndex].FindControl("lblIdFeriado");

                dal.setEliminarFeriado(Convert.ToInt32(_lblIdFeriado.Text));

                lblInfo.Text = "Feriado eliminado correctamente";
                divAlerta.Attributes["class"] = "alert alert-success";
                divAlerta.Visible = true;

                buscar();
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-warning";
                divAlerta.Visible = true;

            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtFechaFeriado.Text == string.Empty)
                {
                    lblInfo.Text = "Favor ingresar la fecha del feriado.";
                    divAlerta.Attributes["class"] = "alert alert-warning";
                    divAlerta.Visible = true;
                    return;
                }

                if (hfIdFeriado.Value == string.Empty)
                {

                    dal.setIngresarFeriado(txtFechaFeriado.Text, Convert.ToInt32(ddlActivo.SelectedValue));

                }
                else
                {
                    dal.setUpFeriado(Convert.ToInt32(hfIdFeriado.Value), txtFechaFeriado.Text, Convert.ToInt32(ddlActivo.SelectedValue));
                }


                buscar();

                lblInfo.Text = "El feriado fue grabado correctamente";
                divAlerta.Attributes["class"] = "alert alert-success";
                divAlerta.Visible = true;

                divSearch.Visible = true;
                divAddEdit.Visible = false;
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-warning";
                divAlerta.Visible = true;

            }
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                buscar();
                divSearch.Visible = true;
                divAddEdit.Visible = false;
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-warning";
                divAlerta.Visible = true;

            }
        }


        public void limpiar(ControlCollection controles)
        {
            foreach (Control control in controles)
            {
                if (control is TextBox)
                    ((TextBox)control).Text = string.Empty;
                else if (control is DropDownList)
                    ((DropDownList)control).ClearSelection();
                else if (control is RadioButtonList)
                    ((RadioButtonList)control).ClearSelection();
                else if (control is CheckBoxList)
                    ((CheckBoxList)control).ClearSelection();
                else if (control is RadioButton)
                    ((RadioButton)control).Checked = false;
                else if (control is CheckBox)
                    ((CheckBox)control).Checked = false;
                else if (control.HasControls())
                    //Esta linea detécta un Control que contenga otros Controles
                    //Así ningún control se quedará sin ser limpiado.
                    limpiar(control.Controls);
            }
        }
    }
}