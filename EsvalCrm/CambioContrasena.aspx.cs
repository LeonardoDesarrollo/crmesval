﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;

namespace EsvalCrm
{
    public partial class CambioContrasena : System.Web.UI.Page
    {
        Datos dal = new Datos();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.Page.IsPostBack)
                {
                    lblUsuario.Text = Session["variableUsuario"].ToString();
                    lblNombre.Text = Session["variableNomUsuario"].ToString() + " " + Session["variableApeUsuario"].ToString();

                    hfIdUsuario.Value = Session["variableIdUsuario"].ToString();

                }
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Visible = true;
            }
        }

        protected void lbtnGrabar_Click(object sender, EventArgs e)
        {
            try
            {

                string clave = txtContrasena.Text.Trim();

                if ((txtContrasena.Text != string.Empty) && (clave.Length >= 5))
                {
                    dal.setEditarContrasena(hfIdUsuario.Value, txtContrasena.Text.Trim());
                    lblInfo.Text = "Su Contraseña fue correctamente actualizada";
                    divAlerta.Attributes["class"] = "alert alert-success";
                    divAlerta.Visible = true;
                }
                else
                {

                    lblInfo.Text = "Debe ingresar una contraseña valida";
                    divAlerta.Attributes["class"] = "alert alert-danger";
                    divAlerta.Visible = true;
                }



            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Visible = true;
            }
        }

    }
}