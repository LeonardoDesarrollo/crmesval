﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;

namespace EsvalCrm
{
    public partial class SiteMaster : MasterPage
    {
        Datos dal = new Datos();
        protected void Page_Load(object sender, EventArgs e)
        {
            //Nombre del sistema
            //ltrTitle.Text = "CRM Esval";

            try
            {
                if (!Page.IsPostBack)
                {
                    Session["desaInterweb"] = "1";

                    if (Session["variableUsuario"] == null)
                    {
                        Response.Redirect("Login.aspx");
                    }
                    else
                    {
                        visible();
                        perfiles();
                    }
                }
            }
            catch (Exception ex)
            {
                string ss = ex.Message;
            }
        }

        void perfiles()
        {
            string idPerfil = Session["variableIdPerfil"].ToString();

            if (idPerfil == "1")
            {
                lblEmpresa.Text = Session["variablePerfil"].ToString();
            }

            if (idPerfil == "2")
            {
                liAdministracion.Visible = false;
                liUsuarios.Visible = false;
                liParametros.Visible = false;
                liConfiguracion.Visible = false;
             
                liReporteGestion.Visible = false;
                liReporteTickets.Visible = false;
                liReportes.Visible = false;

                lblEmpresa.Text = Session["variablePerfil"].ToString();
            }

            if (idPerfil == "3")
            {
                liAdministracion.Visible = false;
                liUsuarios.Visible = false;
                liParametros.Visible = false;
                liConfiguracion.Visible = false;
            
                liReporteGestion.Visible = false;
                liReporteTickets.Visible = false;
                liReportes.Visible = false;

                lblEmpresa.Text = Session["variablePerfil"].ToString();
            }

            if (idPerfil == "4")
            {
                liAdministracion.Visible = false;
                liUsuarios.Visible = false;
                liParametros.Visible = false;
                liConfiguracion.Visible = false;
             
                liReporteGestion.Visible = false;
                liReporteTickets.Visible = false;
                liReportes.Visible = false;

                lblEmpresa.Text = Session["variablePerfil"].ToString();
            }

            if (idPerfil == "5")
            {
                liAdministracion.Visible = false;
                liUsuarios.Visible = false;
                liParametros.Visible = false;
                liConfiguracion.Visible = false;
             
                liReporteGestion.Visible = false;
                liReporteTickets.Visible = false;
                liReportes.Visible = false;

                lblEmpresa.Text = Session["variablePerfil"].ToString();
            }

            if (idPerfil == "6")
            {
                liAdministracion.Visible = false;
                liUsuarios.Visible = false;
                liParametros.Visible = false;
                liConfiguracion.Visible = false;
             
                lblEmpresa.Text = Session["variablePerfil"].ToString();
            }
        }

        void visible()
        {
            DataTable dt = new DataTable();
            dt = dal.getBuscarParametro("1").Tables[0];

            foreach (DataRow item in dt.Rows)
            {
                string campo1 = item["NOMBRE_CAMPO1"].ToString();
                string campo2 = item["NOMBRE_CAMPO2"].ToString();
                string campo3 = item["NOMBRE_CAMPO3"].ToString();
                string campo4 = item["NOMBRE_CAMPO4"].ToString();
                string campo5 = item["NOMBRE_CAMPO5"].ToString();
                string gestion = item["GESTION_ADJUNTO"].ToString();

                //if (campo1 == string.Empty)
                //{
                //    liTabla1.Visible = false;
                //}
                //else
                //{
                //    liTabla1.Visible = true;
                //    lblTabla1.Text = campo1;
                //}

                //if (campo2 == string.Empty)
                //{
                //    liTabla2.Visible = false;
                //}
                //else
                //{
                //    liTabla2.Visible = true;
                //    lblTabla2.Text = campo2;
                //}
                //if (campo3 == string.Empty)
                //{
                //    liTabla3.Visible = false;
                //}
                //else
                //{
                //    liTabla3.Visible = true;
                //    lblTabla3.Text = campo3;
                //}

                //if (campo4 == string.Empty)
                //{
                //    liTabla4.Visible = false;
                //}
                //else
                //{
                //    liTabla4.Visible = true;
                //    lblTabla4.Text = campo4;
                //}

                //if (campo5 == string.Empty)
                //{
                //    liTabla5.Visible = false;
                //}
                //else
                //{
                //    liTabla5.Visible = true;
                //    lblTabla5.Text = campo5;
                //}
            }

        }

        protected void LbtnCerrarSesion_Click(object sender, EventArgs e)
        {
            try
            {
                Session["variableUsuario"] = null;
                Session.Clear();
                Session.Abandon();

                Response.Redirect("Login.aspx");
            }
            catch (Exception ex)
            {
                string ss = ex.Message;
            }
        }

   
    }
}