﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdmTelefonos.aspx.cs" Inherits="EsvalCrm.AdmTelefonos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Teléfonos</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                     <li class="breadcrumb-item"><a href="#">Administración</a></li>
                        <li class="breadcrumb-item"><a href="#">Mantenedores</a></li>
                        <li class="breadcrumb-item active">Teléfonos</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <div class="content">
        <div class="container-fluid">

             <!-- Alertas -->
                <div id="divAlerta" runat="server" visible="false" class="alert alert-danger">
                    <strong>Atención!: </strong>
                    <asp:Label Text="" ID="lblInfo" runat="server" />
                </div>

                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <asp:LinkButton ID="LbtnNuevoTelefono" runat="server" OnClick="LbtnNuevoTelefono_Click" ForeColor="White" Text="Agregar Teléfono" CssClass="btn btn-primary"></asp:LinkButton>
                        <asp:ImageButton ID="ibtnExportarExcel" ImageUrl="~/img/file_extension_xls.png" runat="server" OnClick="ibtnExportarExcel_Click" />
                    </div>
                    <div class="card-body">
                        <asp:GridView ID="Grv" runat="server" CssClass="table table-sm table-hover datatable" EmptyDataText="No hay registros" BorderColor="Transparent" HeaderStyle-CssClass="active" AutoGenerateColumns="false"  OnPreRender="Grv_PreRender">
                            <Columns>
                                <asp:TemplateField HeaderText="Id">
                                    <ItemTemplate>
                                        <asp:Label ID="LblId" runat="server" Visible="true" Text='<%# Bind("ID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Nombre">
                                    <ItemTemplate>
                                        <asp:Label ID="LblNombre" runat="server" Visible="true" Text='<%# Bind("Nombre") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Teléfono">
                                    <ItemTemplate>
                                        <asp:Label ID="LblTelefono" runat="server" Visible="true" Text='<%# Bind("telefono") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                             
                                <asp:TemplateField HeaderStyle-Width="7%">
                                    <ItemTemplate>
                                       <asp:LinkButton ID="btnEditar" OnClick="btnEditar_Click" ForeColor="White" runat="server" ToolTip="Editar" CssClass="btn btn-xs btn-primary"><span class=" fa fa-edit"></span></asp:LinkButton>
                                        <asp:LinkButton ID="btnEliminar" OnClick="btnEliminar_Click" ForeColor="White"  OnClientClick="return confirm('¿Desea eliminar el registro?');" ToolTip="Eliminar" runat="server" CssClass="btn btn-xs btn-danger"><span class="fa fa-eraser"></span></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>

                        </asp:GridView>
                    </div>
                </div>


        </div>
    </div>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="server">
</asp:Content>
