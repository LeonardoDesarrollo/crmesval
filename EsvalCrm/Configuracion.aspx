﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Configuracion.aspx.cs" Inherits="EsvalCrm.Configuracion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Tipificaciones</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Administración</a></li>
                        <li class="breadcrumb-item"><a href="#">Mantenedores</a></li>
                        <li class="breadcrumb-item active">Tipificaciones</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">

            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>

                    <!-- Alertas -->
                    <div id="divAlerta" runat="server" visible="false" class="alert alert-danger">
                        <strong>Atención!: </strong>
                        <asp:Label Text="" ID="lblInfo" runat="server" />
                    </div>

                    <%--<asp:ImageButton ID="ibtnExportarExcel" ImageUrl="~/img/file_extension_xls.png" runat="server" OnClick="ibtnExportarExcel_Click" />--%>

                    <asp:HiddenField ID="hfNivel1" runat="server" />
                    <asp:HiddenField ID="hfNivel2" runat="server" />
                    <asp:HiddenField ID="hfNivel3" runat="server" />
                    <asp:HiddenField ID="hfNivel4" runat="server" />

                    <%--<asp:LinkButton ID="bntNuevo" OnClick="bntNuevo_Click" runat="server" CssClass="btn btn-xs btn-success"><span class="glyphicon glyphicon-plus"></span></asp:LinkButton>--%>

                    <asp:HiddenField ID="hfIdTipificacion" runat="server" />

                    <div runat="server" id="divGrilla" class="card card-danger card-outline table-responsive" visible="false">
                        <div class="card-header">
                            <asp:ImageButton ID="ibtnExportarExcel" ImageUrl="~/img/file_extension_xls.png" runat="server" OnClick="ibtnExportarExcel_Click" />
                            <asp:LinkButton ID="bntNuevo" OnClick="bntNuevo_Click" ForeColor="White" runat="server" CssClass="btn btn-xs btn-success"><span class="fa fa-plus"></span></asp:LinkButton>
                        </div>
                        <div class="card-body">

                            <asp:GridView ID="grvConfiguracion" runat="server" ClientIDMode="Static" CssClass="table table-hover table-sm table-bordered text-sm datatable" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField HeaderText="Id">
                                        <ItemTemplate>
                                            <asp:Label ID="lblId" runat="server" Text='<%# Bind("ID_TIPIFICACION") %>'></asp:Label>

                                            <asp:Label ID="lblIdUsuario1" runat="server" Visible="false" Text='<%# Bind("ID_USUARIO_ESC_N1") %>'></asp:Label>
                                            <asp:Label ID="lblIdUsuario2" runat="server" Visible="false" Text='<%# Bind("ID_USUARIO_ESC_N2") %>'></asp:Label>
                                            <asp:Label ID="lblIdUsuario3" runat="server" Visible="false" Text='<%# Bind("ID_USUARIO_ESC_N3") %>'></asp:Label>
                                            <asp:Label ID="lblIdUsuario4" runat="server" Visible="false" Text='<%# Bind("ID_USUARIO_ESC_N4") %>'></asp:Label>
                                            <asp:Label ID="lblIdTpo" runat="server" Visible="false" Text='<%# Bind("TIPO") %>'></asp:Label>
                                            <asp:Label ID="lblIdTipoMotivoCierre" runat="server" Visible="false" Text='<%# Bind("ID_TIPO_MOTIVO_CIERRE") %>'></asp:Label>

                                            <asp:Label ID="lblVisibleClienteInterno" runat="server" Visible="false" Text='<%# Bind("VISIBLE_CLI_INTERNO") %>'></asp:Label>
                                            <asp:Label ID="lblVisibleClienteExterno" runat="server" Visible="false" Text='<%# Bind("VISIBLE_CLI_EXTERNO") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- <asp:TemplateField HeaderText="Nivel 1">--%>
                                    <asp:TemplateField HeaderText="Servicio">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmpresa" runat="server" Visible="true" Text='<%# Bind("SERVICIO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="TIPO" HeaderText="Tipo" />

                                    <asp:TemplateField HeaderText="Clase" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblClase" runat="server" Visible="true" Text='<%# Bind("CLASE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Nivel 1">
                                        <%--<asp:TemplateField HeaderText="Tipo">--%>
                                        <ItemTemplate>
                                            <asp:Label ID="lblNivel1" runat="server" Visible="true" Text='<%# Bind("NIVEL_1") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Nivel 2">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNivel2" runat="server" Visible="true" Text='<%# Bind("NIVEL_2") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Nivel 3">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNivel3" runat="server" Visible="true" Text='<%# Bind("NIVEL_3") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Nivel 4">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNivel4" runat="server" Visible="true" Text='<%# Bind("NIVEL_4") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Área">
                                        <ItemTemplate>
                                            <asp:Label ID="LblIdArea" runat="server" Visible="false" Text='<%# Bind("ID_AREA") %>'></asp:Label>
                                            <asp:Label ID="lblAreaTip" runat="server" Visible="true" Text='<%# Bind("AreaTip") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Área 1" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblArea1" runat="server" Visible="true" Text='<%# Bind("AREA1") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Usuario 1" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblUsuario1" runat="server" Visible="true" Text='<%# Bind("USUARIO1") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SLA">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSla1" runat="server" Visible="true" Text='<%# Bind("SLA_N1") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Usuario 2" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblUsuario2" runat="server" Text='<%# Bind("USUARIO2") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Área 2" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblArea2" runat="server" Visible="true" Text='<%# Bind("AREA2") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SLA 2" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSla2" runat="server" Visible="true" Text='<%# Bind("SLA_N2") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Usuario 3" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblUsuario3" runat="server" Text='<%# Bind("USUARIO3") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Área 3" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblArea3" runat="server" Visible="true" Text='<%# Bind("AREA3") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SLA 3" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSla3" runat="server" Visible="true" Text='<%# Bind("SLA_N3") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Usuario 4" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblUsuario4" runat="server" Text='<%# Bind("USUARIO4") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Área 4" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblArea4" runat="server" Visible="true" Text='<%# Bind("AREA4") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Detenido" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblidDetenido" runat="server" Visible="true" Text='<%# Bind("DETENIDO") %>'></asp:Label>
                                            <asp:Label ID="lblDetenido" runat="server" Visible="true" Text='<%# Bind("DETENIDO2") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SLA 4" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSla4" runat="server" Visible="true" Text='<%# Bind("SLA_N4") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Activo" Visible="true">
                                        <ItemTemplate>
                                            <asp:Label ID="lblidActivo" runat="server" Visible="false" Text='<%# Bind("ACTIVO") %>'></asp:Label>
                                            <asp:Label ID="lblActivo" runat="server" Visible="true" Text='<%# Bind("ACTIVO2") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Modificar">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnEditar" Text="" OnClick="btnEditar_Click" CssClass="btn btn-success btn-xs" runat="server" ForeColor="White"><span class="fa fa-edit"></span></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>

                    <div runat="server" id="divEditarConfiguracion" class="card card-danger" visible="false">
                        <div class="card-header">
                            Editar Tipificación
                        </div>
                        <div class="card-body">

                            <table class="table table-condensed small">
                                <tr class="active">
                                    <td>
                                        <strong>Servicio</strong>
                                        <asp:TextBox ID="txtEmpresa" runat="server" CssClass="form-control input-sm" Enabled="false"></asp:TextBox>
                                    </td>
                                    <td>
                                        <strong>Nivel 1</strong>
                                        <asp:TextBox ID="txtNivel1" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </td>
                                    <td>
                                        <strong>Nivel 2</strong>
                                        <asp:TextBox ID="txtNivel2" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </td>
                                    <td>
                                        <strong>Nivel 3</strong>
                                        <asp:TextBox ID="txtNivel3" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </td>
                                    <td>
                                        <strong>Nivel 4</strong>
                                        <asp:TextBox ID="txtNivel4" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group" runat="server" visible="false">
                                        <label for="">Usuario:</label>
                                        <asp:DropDownList ID="ddlUsuario1" runat="server" OnDataBound="ddlUsuario1_DataBound" CssClass="form-control input-sm">
                                        </asp:DropDownList>
                                    </div>
                                    <%--2021-08-02 Oculto--%>
                                    <div class="hidden" runat="server" visible="false">
                                        <div class="form-group">
                                            <label for="">Usuario 2:</label>
                                            <asp:DropDownList ID="ddlUsuario2" runat="server" OnDataBound="ddlUsuario2_DataBound" CssClass="form-control input-sm">
                                            </asp:DropDownList>
                                        </div>

                                        <div class="form-group">
                                            <label for="">Usuario 3:</label>
                                            <asp:DropDownList ID="ddlUsuario3" runat="server" OnDataBound="ddlUsuario3_DataBound" CssClass="form-control input-sm">
                                            </asp:DropDownList>
                                        </div>

                                        <div class="form-group">
                                            <label for="">Usuario 4:</label>
                                            <asp:DropDownList ID="ddlUsuario4" runat="server" OnDataBound="ddlUsuario4_DataBound" CssClass="form-control input-sm">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group">
                                            <label for="">SLA 2:</label>
                                            <asp:TextBox ID="txtSLA2" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <label for="ddlPerfil">SLA 3:</label>
                                            <asp:TextBox ID="txtSLA3" runat="server" CssClass="form-control input-sm"></asp:TextBox>

                                        </div>
                                        <div class="form-group">
                                            <label for="ddlPerfil">SLA 4:</label>
                                            <asp:TextBox ID="txtSLA4" runat="server" CssClass="form-control input-sm"></asp:TextBox>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">SLA (Hrs.):</label>
                                        <asp:TextBox ID="txtSLA1" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">Área:</label>
                                        <asp:DropDownList ID="DdlArea" runat="server" CssClass="form-control input-sm" OnDataBound="DdlArea_DataBound">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">Tipo:</label>
                                        <asp:DropDownList ID="ddlTipo" runat="server" CssClass="form-control input-sm">
                                            <%-- <asp:ListItem Text="Caso Deriva" Value="D"></asp:ListItem>--%>
                                            <asp:ListItem Text="Resuelto Primera Linea" Value="P"></asp:ListItem>
                                            <asp:ListItem Text="Derivar" Value="D"></asp:ListItem>
                                            <%--<asp:ListItem Text="Dual" Value="B"></asp:ListItem>--%>
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <%--2021-08-02 Oculto--%>
                                <div class="col-md-3" runat="server" visible="false">
                                    <div class="form-group " runat="server" visible="true">
                                        <label for="">Tipo Motivo Cierre:</label>
                                        <asp:DropDownList ID="ddlTipoMotivoCierre" runat="server" CssClass="form-control input-sm" OnDataBound="ddlTipoMotivoCierre_DataBound">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="form-group hidden">
                                        <label for="">Visible Cliente Interno:</label>
                                        <asp:DropDownList ID="ddlVisibleClienteInterno" runat="server" CssClass="form-control input-sm">
                                            <asp:ListItem Text="Si" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="form-group hidden">
                                        <label for="">Visible Cliente Externo:</label>
                                        <asp:DropDownList ID="ddlVisibleClienteExterno" runat="server" CssClass="form-control input-sm">
                                            <asp:ListItem Text="Si" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <%--2021-08-02 Oculto--%>
                                <div class="col-md-3" runat="server" visible="false">
                                    <div class="form-group">
                                        <label for="">Detenido:</label>
                                        <asp:DropDownList ID="ddlDetenido" runat="server" CssClass="form-control input-sm">
                                            <asp:ListItem Text="Si" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form-group hidden">
                                        <label for="">Clase:</label>
                                        <asp:DropDownList ID="ddlClase" runat="server" CssClass="form-control input-sm">
                                            <asp:ListItem Text="RECLAMO" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="CONSULTA" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="ddlActivo">Activo:</label>
                                        <asp:DropDownList ID="ddlActivo" runat="server" CssClass="form-control input-sm">
                                            <asp:ListItem Text="Si" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="card-footer">
                            <asp:Button Text="Guardar" ID="btnGuardar" CssClass="btn btn-success" OnClick="btnGuardar_Click" runat="server" />
                            <asp:Button Text="Cancelar" ID="btnCancelar" CssClass="btn btn-default" OnClick="btnCancelar_Click1" runat="server" />
                        </div>

                    </div>

                </ContentTemplate>
            </asp:UpdatePanel>

        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="server">
</asp:Content>
