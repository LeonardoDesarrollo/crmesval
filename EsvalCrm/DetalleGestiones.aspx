﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DetalleGestiones.aspx.cs" Inherits="EsvalCrm.DetalleGestiones" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Detalle Tipificaciones</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Reportes</a></li>
                        <li class="breadcrumb-item"><a href="#">Reporte de Gestión</a></li>
                        <li class="breadcrumb-item active">Detalle de Tipificaciones</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            
        <!-- Alertas -->
        <div id="divAlerta" runat="server" visible="false" class="alert alert-danger">
            <strong>Atención!: </strong>
            <asp:Label Text="" ID="lblInfo" runat="server" />
        </div>

        <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Detalle de Tipificaciones</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
            <div class="box-body">
                <asp:GridView ID="grvDetalleGestiones" EmptyDataText="Selección realizada no obtuvo registros" runat="server" HeaderStyle-CssClass="active" PagerStyle-CssClass="active" CssClass="table table-bordered table-hover table-condensed table small panel table-responsive" AutoGenerateColumns="false" ShowFooter="True">
                    <Columns>
                        <asp:TemplateField HeaderText="Ticket">
                            <ItemTemplate>
                                <asp:Label ID="lblIdTicket" runat="server" Text='<%# Bind("TICKET") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Usuario Ingreso">
                            <ItemTemplate>
                                <asp:Label ID="lblUsuario" runat="server" Text='<%# Bind("USUARIO_INGRESO") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Usuario Asignado">
                            <ItemTemplate>
                                <asp:Label ID="lblUsuario" runat="server" Text='<%# Bind("USUARIO_ASIGNADO") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Estado Ticket">
                            <ItemTemplate>
                                <asp:Label ID="lblEstadoAtencion" runat="server" Text='<%# Bind("ESTADO_ATENCION") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Tipo">
                            <ItemTemplate>
                                <asp:Label ID="lblTipo" runat="server" Text='<%# Bind("ESTATUS") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Sub Tipo">
                            <ItemTemplate>
                                <asp:Label ID="lblSubTipo" runat="server" Text='<%# Bind("SUB_ESTATUS") %>'></asp:Label>
                                
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Motivo">
                            <ItemTemplate>
                                <asp:Label ID="lblMotivo" runat="server" Text='<%# Bind("ESTATUS_SEGUIMIENTO") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Fecha de Ingreso">
                            <ItemTemplate>
                                <asp:Label ID="lblFechaIngreso" runat="server" Text='<%# Bind("Fecha_Ingreso","{0:dd/MM/yyyy}") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="Canal">
                            <ItemTemplate>
                                <asp:Label ID="lblCanal" runat="server" Text='<%# Bind("CANAL") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="Obs">
                            <ItemTemplate>
                                <asp:Image ID="imgInfo" runat="server" ImageUrl="~/img/page.png" ToolTip='<%# Bind("OBSERVACION") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
                <!-- /.box-body -->
                <div class="box-footer">
                </div>
            </div><!-- /.box -->

        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="server">
</asp:Content>
