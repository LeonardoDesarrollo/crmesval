﻿using System;
using DAL;
using System.Data;
using System.Web.UI.WebControls;

namespace EsvalCrm
{
    public partial class Dashboard : System.Web.UI.Page
    {
        Datos dal = new Datos();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.IsPostBack)
                {
                    //fill combobox
                    area();
                    usuarios("999999");

                    string idPerfil = Session["variableIdPerfil"].ToString();
                    string verTodos = Session["variableVerTodos"].ToString();
                    string idUsuario = "";

                    //
                    if (idPerfil == "1")
                    {
                        ddlArea.Enabled = true;
                    }
                    else
                    {
                        ddlArea.Enabled = false;
                        ddlArea.SelectedValue = Session["variableIdArea"].ToString();
                    }

                    if (verTodos == "1")
                    {
                        divUsuario.Visible = true;
                        cargarDashboard(null, Convert.ToInt32(ddlArea.SelectedValue));
                        divUsuariosRegistrados.Visible = true;
                    }
                    else
                    {
                        divUsuario.Visible = false;
                        idUsuario = Session["variableIdUsuario"].ToString();
                        cargarDashboard(idUsuario, Convert.ToInt32(ddlArea.SelectedValue));
                        divUsuariosRegistrados.Visible = false;
                    }

                    if (idPerfil == "1" || idPerfil == "5" || idPerfil == "6")
                    {
                        //divFilaAdmin.Visible = true;
                    }
                    else
                    {
                        //divFilaAdmin.Visible = false;
                    }

                    divUsuariosRegistrados.Visible = false;
                }
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        void cargarDashboard(string idUsuario, int idArea)
        {
            DataTable dt = new DataTable();

            dt = dal.getGenerarDashboard(idUsuario, idArea).Tables[0];
            foreach (DataRow item in dt.Rows)
            {
                lblTotalCasosAbiertos.Text = item["TOTAL_ABIERTOS"].ToString();
                lblTotalCasosCerrados.Text = item["TOTAL_CERRADOS"].ToString();
                lblTotalDetenidos.Text = item["TOTAL_DETENIDOS"].ToString();
                lblTotalUsuarios.Text = item["TOTAL_USUARIOS"].ToString();
                lblTotalCasosEnProceso.Text = item["TOTAL_EN_PROCESO"].ToString();

                lblTicketConInsistencias.Text = item["TOTAL_INSISTENCIA"].ToString();
                string tiempoResolucionPromedio = item["TOTAL_PROMEDIO_RESOLUCION"].ToString();

                if (tiempoResolucionPromedio == "")
                {
                    lblTiempoResolucionPromedio.Text = "0";
                }
                else
                {
                    lblTiempoResolucionPromedio.Text = tiempoResolucionPromedio;
                }

                lblCasosEscaladosPorDemora.Text = item["TOTAL_NIVEL_ESCALAMIENTO"].ToString();

                lblTipificacionesConsulta.Text = item["TOTAL_TIPIFICACIONES_CONSULTA"].ToString();
                lblTipificacionesReclamo.Text = item["TOTAL_TIPIFICACIONES_RECLAMO"].ToString();
                lblTipificacionesSolicitud.Text = item["TOTAL_TIPIFICACIONES_SOLICITUD"].ToString();

                lblTipificacionesIncistencias.Text = item["TOTAL_TIPIFICACIONES_INCIDENCIA"].ToString();

                lblTotalTicketDerivados.Text = item["TOTAL_DERIVADOS"].ToString();

                LblTotalEmailTipificados.Text = item["totalEmailTipificados"].ToString();
                LblTotalEmailNoTipificados.Text = item["totalEmailNoTipificados"].ToString();
                //TOTAL_PROMEDIO_RESOLUCION
            }
        }

        protected void lbtnTicketAbiertos_Click(object sender, EventArgs e)
        {
            try
            {
                Session["strTituloBuscadorTicket"] = "Buscador de tickets (Pendientes)";
                string idArea = ddlArea.SelectedValue;
                string idUsuario = ddlUsuario.SelectedValue;
                Response.Redirect("BuscarTicket.aspx?e=1&a=" + idArea + "&u=" + idUsuario);
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void lbtnCasosEscaladosPorDemora_Click(object sender, EventArgs e)
        {
            try
            {
                Session["strTituloBuscadorTicket"] = "Buscador de tickets (Casos escalados por demora)";
                //Response.Redirect("BuscarTicket.aspx?conEscalamiento=1");

                string idArea = ddlArea.SelectedValue;
                string idUsuario = ddlUsuario.SelectedValue;
                Response.Redirect("BuscarTicket.aspx?conEscalamiento=1&a=" + idArea + "&u=" + idUsuario);
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void lbtnTiempoResolucionPromedio_Click(object sender, EventArgs e)
        {
            try
            {
                Session["strTituloBuscadorTicket"] = "Buscador de tickets (Tiempo de Resolución Promedio)";
                //Response.Redirect("BuscarTicket.aspx?e=3");

                string idArea = ddlArea.SelectedValue;
                string idUsuario = ddlUsuario.SelectedValue;
                Response.Redirect("BuscarTicket.aspx?e=3&a=" + idArea + "&u=" + idUsuario);
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void lbtnTicketConInsistencia_Click(object sender, EventArgs e)
        {
            try
            {
                Session["strTituloBuscadorTicket"] = "Buscador de tickets (con insistencias)";
                //Response.Redirect("BuscarTicket.aspx?conInsistencia=1");

                string idArea = ddlArea.SelectedValue;
                string idUsuario = ddlUsuario.SelectedValue;
                Response.Redirect("BuscarTicket.aspx?conInsistencia=1&a=" + idArea + "&u=" + idUsuario);
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }


        protected void lbtnTicketCerrados_Click(object sender, EventArgs e)
        {

            try
            {
                Session["strTituloBuscadorTicket"] = "Buscador de tickets (Cerrados)";
                //Response.Redirect("BuscarTicket.aspx?e=3");

                string idArea = ddlArea.SelectedValue;
                string idUsuario = ddlUsuario.SelectedValue;
                Response.Redirect("BuscarTicket.aspx?e=3&a=" + idArea + "&u=" + idUsuario);
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void lbtnTicketEnProceso_Click(object sender, EventArgs e)
        {
            try
            {
                Session["strTituloBuscadorTicket"] = "Buscador de tickets (En Proceso)";
                //Response.Redirect("BuscarTicket.aspx?e=3");

                string idArea = ddlArea.SelectedValue;
                string idUsuario = ddlUsuario.SelectedValue;
                Response.Redirect("BuscarTicket.aspx?e=4&a=" + idArea + "&u=" + idUsuario);
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void lbtnTicketDetenidos_Click(object sender, EventArgs e)
        {
            try
            {
                Session["strTituloBuscadorTicket"] = "Buscador de tickets (Detenidos)";
                //Response.Redirect("BuscarTicket.aspx?e=2");

                string idArea = ddlArea.SelectedValue;
                string idUsuario = ddlUsuario.SelectedValue;
                Response.Redirect("BuscarTicket.aspx?e=2&a=" + idArea + "&u=" + idUsuario);
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void lbtnTipificacionesConsulta_Click(object sender, EventArgs e)
        {
            try
            {
                Session["strTituloBuscadorTipificacion"] = "Buscador de tipificaciones (Consulta)";
                //Response.Redirect("BuscarTicket.aspx?tipo=D");

                string idArea = ddlArea.SelectedValue;
                string idUsuario = ddlUsuario.SelectedValue;
                Response.Redirect("BuscarTicket.aspx?tipo=D&a=" + idArea + "&u=" + idUsuario);
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void lbtnTipificacionesReclamo_Click(object sender, EventArgs e)
        {
            try
            {
                Session["strTituloBuscadorTipificacion"] = "Buscador de tipificaciones (Reclamo)";
                //Response.Redirect("BuscarTicket.aspx?e=P");

                string idArea = ddlArea.SelectedValue;
                string idUsuario = ddlUsuario.SelectedValue;
                Response.Redirect("BuscarTicket.aspx?e=P&a=" + idArea + "&u=" + idUsuario);
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }


        protected void lbtnTipificacionesSolicitud_Click(object sender, EventArgs e)
        {
            try
            {
                Session["strTituloBuscadorTipificacion"] = "Buscador de tipificaciones (Solicitud)";
                //Response.Redirect("BuscarTicket.aspx?e=B");

                string idArea = ddlArea.SelectedValue;
                string idUsuario = ddlUsuario.SelectedValue;
                Response.Redirect("BuscarTicket.aspx?e=B&a=" + idArea + "&u=" + idUsuario);
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void lbtnTipificacionIncidencia_Click(object sender, EventArgs e)
        {
            try
            {
                Session["strTituloBuscadorTipificacion"] = "Buscador de tipificaciones (Incidencia)";
                //Response.Redirect("BuscarTipificaciones.aspx?e=Reporte Incidencia");

                string idArea = ddlArea.SelectedValue;
                string idUsuario = ddlUsuario.SelectedValue;
                Response.Redirect("BuscarTicket.aspx?e=Reporte Incidencia&a=" + idArea + "&u=" + idUsuario);
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        void area()
        {
            ddlArea.DataSource = dal.getBuscarArea();
            ddlArea.DataValueField = "ID_AREA";
            ddlArea.DataTextField = "AREA";
            ddlArea.DataBind();
        }

        protected void ddlArea_DataBound(object sender, EventArgs e)
        {
            ddlArea.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Todos", "0"));
        }

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string idUsuario = "";
                string verTodos = Session["variableVerTodos"].ToString();
                string idArea = ddlArea.SelectedValue;

                if (verTodos == "1")
                {
                    usuarios(idArea);
                    idUsuario = ddlUsuario.SelectedValue;
                    cargarDashboard(idUsuario, Convert.ToInt32(ddlArea.SelectedValue));
                    divUsuariosRegistrados.Visible = false;
                }
                else
                {
                    idUsuario = Session["variableIdUsuario"].ToString();
                    cargarDashboard(idUsuario, Convert.ToInt32(ddlArea.SelectedValue));
                    divUsuariosRegistrados.Visible = false;
                }
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void ddlUsuario_DataBound(object sender, EventArgs e)
        {
            ddlUsuario.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Todos", "0"));
        }

        protected void ddlUsuario_SelectedIndexChanged(object sender, EventArgs e)
        {
            string idUsuario = "";
            string verTodos = Session["variableVerTodos"].ToString();
            if (verTodos == "1")
            {
                idUsuario = ddlUsuario.SelectedValue;
                cargarDashboard(idUsuario, Convert.ToInt32(ddlArea.SelectedValue));
                divUsuariosRegistrados.Visible = false;
            }
            else
            {
                idUsuario = Session["variableIdUsuario"].ToString();
                cargarDashboard(idUsuario, Convert.ToInt32(ddlArea.SelectedValue));
                divUsuariosRegistrados.Visible = false;
            }
        }

        void usuarios(string idArea)
        {
            ddlUsuario.DataSource = dal.getBuscarUsuarioPorIdArea(idArea);
            //ddlUsuario.DataSource = dal.getBuscarUsuario(null, null);
            ddlUsuario.DataValueField = "ID_USUARIO";
            ddlUsuario.DataTextField = "USUARIO";
            ddlUsuario.DataBind();
        }

        protected void lbtnTicketDerivados_Click(object sender, EventArgs e)
        {
            try
            {
                Session["strTituloBuscadorTicket"] = "Buscador de tickets (Derivados)";
                //Response.Redirect("BuscarTicket.aspx?d=1");

                string idArea = ddlArea.SelectedValue;
                string idUsuario = ddlUsuario.SelectedValue;
                Response.Redirect("BuscarTicket.aspx?d=1&a=" + idArea + "&u=" + idUsuario);
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void LbtnEmailTipificados_Click(object sender, EventArgs e)
        {
            Response.Redirect("Emails.aspx?Tip=1");
        }

        protected void LbtnEmailNoTipificados_Click(object sender, EventArgs e)
        {
            Response.Redirect("Emails.aspx?Tip=0");
        }
    }
}