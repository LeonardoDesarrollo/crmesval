﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ConfiguracionIn.aspx.cs" Inherits="EsvalCrm.ConfiguracionIn" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Tipificaciones</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Administración</a></li>
                        <li class="breadcrumb-item"><a href="#">Mantenedores</a></li>
                        <li class="breadcrumb-item active">Tipificaciones</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <!-- Alertas -->
            <div id="divAlerta" runat="server" visible="false" class="alert alert-danger">
                <strong>Atención!: </strong>
                <asp:Label Text="" ID="lblInfo" runat="server" />
            </div>

            <div class="box box-danger">
                <div class="box-header">
                    Ingresar Tipificación
                </div>
                <div class="box-body">
                    <table class="table table-condensed small">
                        <tr class="active">
                            <td>
                                <strong>Servicio</strong>
                                <asp:DropDownList ID="ddlServicio" Width="100%" runat="server" CssClass="form-control input-sm" OnSelectedIndexChanged="ddlServicio_SelectedIndexChanged" OnDataBound="ddlServicio_DataBound" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <strong>Nivel 1</strong>
                                <asp:DropDownList ID="ddlNivel1" Width="100%" runat="server" CssClass="form-control input-sm" OnSelectedIndexChanged="ddlNivel1_SelectedIndexChanged" OnDataBound="ddlNivel1_DataBound" AutoPostBack="true">
                                </asp:DropDownList>
                                <asp:TextBox ID="txtNivel1" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </td>
                            <td>
                                <strong>Nivel 2</strong>
                                <asp:DropDownList ID="ddlNivel2" Width="100%" runat="server" CssClass="form-control input-sm" OnSelectedIndexChanged="ddlNivel2_SelectedIndexChanged" OnDataBound="ddlNivel2_DataBound" AutoPostBack="true">
                                </asp:DropDownList>
                                <asp:TextBox ID="txtNivel2" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </td>
                            <td>
                                <strong>Nivel 3</strong>
                                <asp:DropDownList ID="ddlNivel3" Width="100%" runat="server" CssClass="form-control input-sm" OnSelectedIndexChanged="ddlNivel3_SelectedIndexChanged" OnDataBound="ddlNivel3_DataBound" AutoPostBack="true">
                                </asp:DropDownList>
                                <asp:TextBox ID="txtNivel3" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </td>
                            <td>
                                <strong>Nivel 4</strong>
                                <asp:DropDownList ID="ddlNivel4" Width="100%" runat="server" CssClass="form-control input-sm" OnSelectedIndexChanged="ddlNivel4_SelectedIndexChanged" OnDataBound="ddlNivel4_DataBound" AutoPostBack="true">
                                </asp:DropDownList>
                                <asp:TextBox ID="txtNivel4" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </td>
                        </tr>
                    </table>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group" runat="server" visible="false">
                                <label for="">Usuario:</label>
                                <asp:DropDownList ID="ddlUsuario1" runat="server" OnDataBound="ddlUsuario1_DataBound" CssClass="form-control input-sm">
                                </asp:DropDownList>
                            </div>

                            <%--2021-08-02 Oculto--%>
                            <div class="hidden" runat="server" visible="false">
                                <div class="form-group">
                                    <label for="">Usuario 2:</label>
                                    <asp:DropDownList ID="ddlUsuario2" runat="server" OnDataBound="ddlUsuario2_DataBound" CssClass="form-control input-sm">
                                    </asp:DropDownList>
                                </div>

                                <div class="form-group">
                                    <label for="">Usuario 3:</label>
                                    <asp:DropDownList ID="ddlUsuario3" runat="server" OnDataBound="ddlUsuario3_DataBound" CssClass="form-control input-sm">
                                    </asp:DropDownList>
                                </div>

                                <div class="form-group">
                                    <label for="">Usuario 4:</label>
                                    <asp:DropDownList ID="ddlUsuario4" runat="server" OnDataBound="ddlUsuario4_DataBound" CssClass="form-control input-sm">
                                    </asp:DropDownList>
                                </div>
                                <div class="form-group">
                                    <label for="">SLA 2:</label>
                                    <asp:TextBox ID="txtSLA2" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <label for="ddlPerfil">SLA 3:</label>
                                    <asp:TextBox ID="txtSLA3" runat="server" CssClass="form-control input-sm"></asp:TextBox>

                                </div>
                                <div class="form-group">
                                    <label for="ddlPerfil">SLA 4:</label>
                                    <asp:TextBox ID="txtSLA4" runat="server" CssClass="form-control input-sm"></asp:TextBox>

                                </div>

                            </div>

                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">SLA (Hrs.):</label>
                                <asp:TextBox ID="txtSLA1" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Área:</label>
                                <asp:DropDownList ID="DdlArea" runat="server" CssClass="form-control input-sm" OnDataBound="DdlArea_DataBound">
                                </asp:DropDownList>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group" runat="server" visible="true">
                                <label for="">Tipo:</label>
                                <asp:DropDownList ID="ddlTipo" runat="server" CssClass="form-control input-sm">

                                    <asp:ListItem Text="Resuelto Primera Linea" Value="P"></asp:ListItem>
                                    <asp:ListItem Text="Derivar" Value="D"></asp:ListItem>
                                    <%--<asp:ListItem Text="Dual" Value="B"></asp:ListItem>--%>
                                </asp:DropDownList>
                            </div>

                            <div class="form-group" runat="server" visible="false">
                                <label for="">Visible Atento:</label>
                                <asp:DropDownList ID="ddlVisibleAtento" runat="server" CssClass="form-control input-sm">
                                    <asp:ListItem Text="Si" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                            </div>

                            <%--2021-08-02 Oculto--%>
                            <div class="form-group hidden" runat="server" visible="false">
                                <label for="">Visible Cliente Interno:</label>
                                <asp:DropDownList ID="ddlVisibleClienteInterno" runat="server" CssClass="form-control input-sm">
                                    <asp:ListItem Text="Si" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                            </div>

                        </div>

                        <%--2021-02-08 Oculto--%>
                        <div class="col-md-3" runat="server" visible="false">
                            <div class="form-group">
                                <label for="">Tipo Motivo Cierre:</label>
                                <asp:DropDownList ID="ddlTipoMotivoCierre" runat="server" CssClass="form-control input-sm" OnDataBound="ddlTipoMotivoCierre_DataBound">
                                </asp:DropDownList>
                            </div>
                        </div>

                        <%--2021-08-02 Oculto--%>
                        <div class="col-md-3" runat="server" visible="false">
                            <div class="form-group">
                                <label for="">Detenido:</label>
                                <asp:DropDownList ID="ddlDetenido" runat="server" CssClass="form-control input-sm">
                                    <asp:ListItem Text="Si" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                            </div>

                            <div class="form-group hidden">
                                <label for="">Clase:</label>
                                <asp:DropDownList ID="ddlClase" runat="server" CssClass="form-control input-sm">
                                    <asp:ListItem Text="RECLAMO" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="CONSULTA" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                            </div>

                            <div class="form-group hidden">
                                <label for="">Visible Cliente Externo:</label>
                                <asp:DropDownList ID="ddlVisibleClienteExterno" runat="server" CssClass="form-control input-sm">
                                    <asp:ListItem Text="Si" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                            </div>

                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="ddlActivo">Activo:</label>
                                <asp:DropDownList ID="ddlActivo" runat="server" CssClass="form-control input-sm">
                                    <asp:ListItem Text="Si" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="box-footer">
                    <asp:Button Text="Guardar" ID="btnGuardar" CssClass="btn btn-success" OnClick="btnGuardar_Click" runat="server" />
                    <asp:Button Text="Cancelar" ID="btnCancelar" CssClass="btn btn-default" OnClick="btnCancelar_Click" runat="server" />
                </div>

            </div>

        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="server">
</asp:Content>
