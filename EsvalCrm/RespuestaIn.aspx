﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RespuestaIn.aspx.cs" Inherits="EsvalCrm.RespuestaIn" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Respuesta</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Administrador</a></li>
                        <li class="breadcrumb-item active">Respuesta</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <!-- Alertas -->
            <div id="divAlerta" runat="server" visible="false" class="alert alert-danger">
                <strong>Atención!: </strong>
                <asp:Label Text="" ID="lblInfo" runat="server" />
            </div>

            <asp:HiddenField ID="HdId" runat="server" />
            <div class="card card-danger card-outline">
                <div class="card-header">
                    Respuesta
                </div>
                <div class="card-body">
                    <table class="table table-sm">
                        <tr class="active">
                            <td>
                                <strong>Tipología</strong>
                                <asp:DropDownList ID="ddlNivel2" Width="100%" runat="server" CssClass="form-control input-sm" OnSelectedIndexChanged="ddlNivel2_SelectedIndexChanged" OnDataBound="ddlNivel2_DataBound" AutoPostBack="true">
                                </asp:DropDownList>
                                <asp:TextBox ID="txtNivel2" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </td>
                            <td>
                                <strong>Tema</strong>
                                <asp:DropDownList ID="ddlNivel3" Width="100%" runat="server" CssClass="form-control input-sm" OnSelectedIndexChanged="ddlNivel3_SelectedIndexChanged" OnDataBound="ddlNivel3_DataBound" AutoPostBack="true">
                                </asp:DropDownList>
                                <asp:TextBox ID="txtNivel3" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </td>
                            <td>
                                <strong>Subtema</strong>
                                <asp:DropDownList ID="ddlNivel4" Width="100%" runat="server" CssClass="form-control input-sm" OnSelectedIndexChanged="ddlNivel4_SelectedIndexChanged" OnDataBound="ddlNivel4_DataBound" AutoPostBack="true">
                                </asp:DropDownList>
                                <asp:TextBox ID="txtNivel4" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </td>
                            <td>
                                <strong>Palabras Claves</strong>
                                <asp:DropDownList ID="ddlNivel5" Width="100%" runat="server" CssClass="form-control input-sm" OnSelectedIndexChanged="ddlNivel5_SelectedIndexChanged" OnDataBound="ddlNivel5_DataBound" AutoPostBack="true">
                                </asp:DropDownList>
                                <asp:TextBox ID="txtNivel5" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <br />


                    <div class="row">
                        <div class="col-md-6">
                            <strong>Pregunta</strong>
                            <asp:TextBox ID="TxtPregunta" runat="server" TextMode="MultiLine" Height="200px" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <strong>Respuesta</strong>
                            <asp:TextBox ID="TxtRespuesta" runat="server" TextMode="MultiLine" Height="200px" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <br />
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <div id="DivDocumentos" runat="server" visible="false">
                                <div class="row">
                                    <div class="col-md-5">
                                        <strong>Documentos</strong>
                                        <asp:DropDownList ID="DdlDocumentos" runat="server" CssClass="form-control select" OnDataBound="DdlDocumentos_DataBound"></asp:DropDownList>
                                    </div>
                                    <div class="col-md-1">
                                        <br />
                                        <asp:Button ID="BtnAgregar" runat="server" CssClass="btn btn-primary" Text="Agregar" OnClick="BtnAgregar_Click" />
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-6">
                                        <asp:GridView ID="GrvDocumentos" runat="server" CssClass="table" HeaderStyle-CssClass="active" BorderColor="Transparent" AutoGenerateColumns="false" OnRowDataBound="GrvDocumentos_RowDataBound" OnPreRender="GrvDocumentos_PreRender" EmptyDataText="documento no encontrado" EmptyDataRowStyle-CssClass="active h3" PageSize="50" AllowPaging="true">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Id">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblIdDocumento" runat="server" Visible="false" Text='<%# Bind("ID_DOCUMENTO") %>'></asp:Label>
                                                        <asp:LinkButton ID="LbtnIdDocumento" runat="server" ForeColor="White" Visible="true" Text='<%# Bind("ID_DOCUMENTO") %>' OnClick="LbtnIdDocumento_Click" CssClass="btn btn-xs btn-primary"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Nombre">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblNombre" runat="server" Visible="true" Text='<%# Bind("NOMBRE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Descripción">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblDescripcion" runat="server" Visible="true" Text='<%# Bind("DESCRIPCION") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Categoría">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblIdCategoria" runat="server" Visible="false" Text='<%# Bind("ID_CATEGORIA_DOCUMENTO") %>'></asp:Label>
                                                        <asp:Label ID="LblCategoria" runat="server" Text='<%# Bind("NOM_CATEGORIA_DOCUMENTO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Archivo">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblArchivo" runat="server" Visible="false" Text='<%# Bind("ARCHIVO") %>'></asp:Label>
                                                        <asp:LinkButton ID="btnArchivo" runat="server" OnClick="btnArchivo_Click" Text="text"><i class="fa fa-files-o"></i></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Eliminar">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnEliminarDocumento" ForeColor="White" OnClick="btnEliminarDocumento_Click1" OnClientClick="return confirm('¿Desea eliminar el registro?');" runat="server" CssClass="btn btn-xs btn-danger"><span class="fa fa-eraser"></span></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>

                                            </Columns>

                                        </asp:GridView>
                                    </div>

                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>


                </div>
                <div class="card-footer">
                    <asp:Button ID="BtnGrabar" runat="server" OnClick="BtnGrabar_Click" Text="Grabar" CssClass="btn btn-success" />
                    <asp:Button ID="BtnCancelar" runat="server" OnClick="BtnCancelar_Click" Text="Cancelar" CssClass=" btn btn-default" />
                </div>
            </div>
        </div>
    </div>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="server">
</asp:Content>
