﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using DataTableToExcel;

namespace EsvalCrm
{
    public partial class Respuestas : System.Web.UI.Page
    {
        Datos dal = new Datos();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(this.ibtnExportarExcel);
                if (!this.Page.IsPostBack)
                {
                    //DataTable dt = dal.getBuscarRespuestasId(0).Tables[0];
                    //Grv.DataSource = dt;
                    //Grv.DataBind();
                    buscar();
                }
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void Grv_PreRender(object sender, EventArgs e)
        {
            if (Grv.Rows.Count > 0)
            {
                Grv.UseAccessibleHeader = true;
                Grv.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void LbtnPregunta_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                GridViewRow row = (GridViewRow)btn.NamingContainer;
                LinkButton _LblTexto = (LinkButton)Grv.Rows[row.RowIndex].FindControl("LbtnPregunta");

                TxtTexto.Text = _LblTexto.ToolTip;

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "MostrarModal();", true);
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void LbtnRespuesta_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                GridViewRow row = (GridViewRow)btn.NamingContainer;
                LinkButton _LblTexto = (LinkButton)Grv.Rows[row.RowIndex].FindControl("LbtnRespuesta");

                TxtTexto.Text = _LblTexto.ToolTip;

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "MostrarModal();", true);
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void LbtnId_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                GridViewRow row = (GridViewRow)btn.NamingContainer;
                Label _lblId = (Label)Grv.Rows[row.RowIndex].FindControl("lblId");
                Response.Redirect("RespuestaIn.aspx?Id=" + _lblId.Text);
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void LbtnNuevoRespuesta_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("RespuestaIn.aspx?Id=0");
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                GridViewRow row = (GridViewRow)btn.NamingContainer;

                Label _LblId = (Label)Grv.Rows[row.RowIndex].FindControl("LblId");
                dal.setEliminarRespuesta(Convert.ToInt16(_LblId.Text));
                buscar();

                lblInfo.Text = "Respuesta Eliminada Correctamente";
                divAlerta.Attributes["class"] = "alert alert-success";
                divAlerta.Visible = true;
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        private void buscar()
        {
            DataTable dt = dal.getBuscarRespuestasId(0).Tables[0];
            Grv.DataSource = dt;
            Grv.DataBind();
        }

        protected void ibtnExportarExcel_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = dal.getBuscarRespuestaExporte().Tables[0];
                //Utilidad.ExportDataTableToExcel(dt, "exporte.xls", "", "", "", "");
                //Utilidad.ExportDataTableToExcel(dt, "exporte.xls", "", "", "", "");
                Response.ContentType = "Application/x-msexcel";
                Response.AddHeader("content-disposition", "attachment;filename=" + "exporte_respuesta" + ".csv");
                Response.ContentEncoding = Encoding.Unicode;
                Response.Write(Utilidad.ExportToCSVFile(dt));
                Response.End();
                //Utilidad.ExportDataTableToExcel(dt, "Exporte.xls", "", "", "", "");
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }
    }
}