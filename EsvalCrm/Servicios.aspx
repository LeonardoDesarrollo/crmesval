﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Servicios.aspx.cs" Inherits="EsvalCrm.Servicios" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Servicios</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                              <li class="breadcrumb-item"><a href="#">Administración</a></li>
                        <li class="breadcrumb-item"><a href="#">Mantenedores</a></li>
                        <li class="breadcrumb-item active">Servicios</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            
        <!-- Alertas -->
        <div id="divAlerta" runat="server" visible="false" class="alert alert-danger alerta">
            <strong>Atención!: </strong>
            <asp:Label Text="" ID="lblInfo" runat="server" />
        </div>


        <div class="card card-primary card-outline" id="divSearch" runat="server">
            <div class=" card-header">
                <asp:LinkButton ID="btnNew" OnClick="btnNew_Click"  runat="server" CssClass="btn btn-xs btn-success" ForeColor="White"><span class="fa fa-plus"></span></asp:LinkButton>
            </div>
            <div class="card-body">
                <asp:GridView ID="GrvServicios" runat="server"  ClientIDMode="Static" CssClass="table table-hover table-sm table-bordered text-sm"
                   AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" OnRowDataBound="GrvServicios_RowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderText="ID">
                            <ItemTemplate>
                                <asp:Label ID="LblIdServicio" runat="server" Text='<%# Bind("ID_SERVICIO") %>' Visible="true"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Servicio">
                            <ItemTemplate>
                                <asp:Label ID="LblServicio" runat="server" Text='<%# Bind("SERVICIO") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Script" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="LblIdScript" runat="server" Text='<%# Bind("NOMBRE_SCRIPT") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ID SoftCall" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="LblIdSoftCall" runat="server" Text='<%# Bind("ID_SOFTCALL") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Activo">
                            <ItemTemplate>
                                <asp:Label ID="lblActivo" runat="server" Visible="true" Text='<%# Bind("Activo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnEditar" OnClick="btnEditar_Click" runat="server" ToolTip="Editar" CssClass="btn btn-xs btn-primary" ForeColor="White"><span class="fa fa-edit"></span></asp:LinkButton>
                                <asp:LinkButton ID="btnEliminar" OnClick="btnEliminar_Click" OnClientClick="return confirm('¿Desea eliminar el registro?');" ForeColor="White" runat="server" ToolTip="Eliminar" CssClass="btn btn-xs btn-danger"><span class="fa fa-eraser    "></span></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="server">
</asp:Content>
