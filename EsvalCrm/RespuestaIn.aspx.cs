﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;

namespace EsvalCrm
{
    public partial class RespuestaIn : System.Web.UI.Page
    {
        Datos dal = new Datos();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.Page.IsPostBack)
                {
                    nivel2();
                    nivel3();
                    nivel4();
                    nivel5();


                    string _Id = Convert.ToString(Request.QueryString["Id"]);
                    BuscarDocumentos();
                    if (!string.IsNullOrEmpty(_Id))
                    {
                        HdId.Value = _Id;
                        if (HdId.Value == "0")
                        {
                            return;
                        }
                        DivDocumentos.Visible = true;
                        DataTable dt = dal.getBuscarRespuestasId(Convert.ToInt32(_Id)).Tables[0];
                        foreach (DataRow item in dt.Rows)
                        {
                            string tipologia = item["Tipologia"].ToString();
                            ddlNivel2.SelectedValue = tipologia;
                            ddlNivel2_SelectedIndexChanged(sender, e);
                            ddlNivel3.SelectedValue = item["Tema"].ToString();
                            ddlNivel3_SelectedIndexChanged(sender, e);
                            ddlNivel4.SelectedValue = item["SubTema"].ToString();
                            ddlNivel4_SelectedIndexChanged(sender, e);
                            ddlNivel5.SelectedValue = item["Palabras_claves"].ToString();
                            ddlNivel5_SelectedIndexChanged(sender, e);
                            TxtPregunta.Text = item["Pregunta"].ToString();
                            TxtRespuesta.Text = item["Respuesta"].ToString();
                        }

                        BuscarDocumentosRespuesta(Convert.ToInt32(HdId.Value));

                    }

                }
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Visible = true;
            }
        }

        protected void ddlNivel2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                nivel3();
                txtNivel2.Text = ddlNivel2.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Visible = true;
            }
        }

        protected void ddlNivel2_DataBound(object sender, EventArgs e)
        {
            ddlNivel2.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        }

        protected void ddlNivel3_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                nivel4();
                //  ddlNivel4.Items.Clear();
                txtNivel3.Text = ddlNivel3.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Visible = true;
            }
        }

        protected void ddlNivel3_DataBound(object sender, EventArgs e)
        {
            ddlNivel3.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        }

        protected void ddlNivel4_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                nivel5();


                txtNivel4.Text = ddlNivel4.SelectedItem.ToString();

            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Visible = true;
            }
        }

        protected void ddlNivel4_DataBound(object sender, EventArgs e)
        {
            ddlNivel4.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        }

        void nivel2()
        {
            ddlNivel2.DataSource = dal.getBuscarNivel2(null, null, null, null);
            ddlNivel2.DataValueField = "NIVEL_2";
            ddlNivel2.DataTextField = "NIVEL_2";
            ddlNivel2.DataBind();

            ddlNivel3.Items.Clear();
            ddlNivel4.Items.Clear();
        }

        void nivel3()
        {
            //ddlNivel3.DataSource = dal.getNivel3(ddlNivel1.SelectedValue, ddlNivel2.SelectedValue);
            DataTable dt = dal.getBuscarNivel3Respuesta(null, null, null, null, ddlNivel2.SelectedValue).Tables[0];
            ddlNivel3.DataSource = dt;
            ddlNivel3.DataValueField = "NIVEL_3";
            ddlNivel3.DataTextField = "NIVEL_3";
            ddlNivel3.DataBind();

            ddlNivel4.Items.Clear();
        }

        void nivel4()
        {
            //ddlNivel4.DataSource = dal.getNivel4(ddlNivel1.SelectedValue, ddlNivel2.SelectedValue, ddlNivel3.SelectedValue);
            ddlNivel4.DataSource = dal.getBuscarNivel4(null, null, null, null, ddlNivel2.SelectedValue, ddlNivel3.SelectedValue);
            ddlNivel4.DataValueField = "NIVEL_4";
            ddlNivel4.DataTextField = "NIVEL_4";
            ddlNivel4.DataBind();
        }

        void nivel5()
        {
            ddlNivel5.DataSource = dal.getBuscarNivel5(ddlNivel3.SelectedValue, ddlNivel2.SelectedValue, ddlNivel4.SelectedValue);
            ddlNivel5.DataValueField = "PALABRAS_CLAVES";
            ddlNivel5.DataTextField = "PALABRAS_CLAVES";
            ddlNivel5.DataBind();
        }

        protected void ddlNivel5_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                txtNivel5.Text = ddlNivel5.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void ddlNivel5_DataBound(object sender, EventArgs e)
        {
            ddlNivel5.Items.Insert(0, new ListItem("Seleccione", "0"));
        }

        protected void BtnGrabar_Click(object sender, EventArgs e)
        {
            try
            {
                if (HdId.Value == "0")
                {
                    dal.setInRespuesta(txtNivel2.Text.Trim(), txtNivel3.Text.Trim(), txtNivel4.Text.Trim(), txtNivel5.Text.Trim(), TxtPregunta.Text, TxtRespuesta.Text, null, null);
                }
                else
                {
                    dal.SetEditarRespuesta(Convert.ToInt32(HdId.Value), txtNivel2.Text.Trim(), txtNivel3.Text.Trim(), txtNivel4.Text.Trim(), txtNivel5.Text.Trim(), TxtPregunta.Text, TxtRespuesta.Text, null, null);
                }
                lblInfo.Text = "Respuesta grabada";
                divAlerta.Attributes["class"] = "alert alert-success";
                divAlerta.Visible = true;
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void BtnCancelar_Click(object sender, EventArgs e)
        {
            Response.Redirect("Respuestas.aspx");
        }

        protected void DdlDocumentos_DataBound(object sender, EventArgs e)
        {
            DdlDocumentos.Items.Insert(0, new ListItem("Seleccione Documento", "0"));
        }


        void BuscarDocumentos()
        {
            DdlDocumentos.DataSource = dal.getBuscarDocumento(0, null, 0, null, 1).Tables[0];
            DdlDocumentos.DataTextField = "Nombre2";
            DdlDocumentos.DataValueField = "ID_DOCUMENTO";
            DdlDocumentos.DataBind();
        }

        protected void BtnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                dal.setIngresarDocumentoRespuesta(Convert.ToInt32(HdId.Value), Convert.ToInt32(DdlDocumentos.SelectedValue));
                BuscarDocumentosRespuesta(Convert.ToInt32(HdId.Value));
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        void BuscarDocumentosRespuesta(int IdRespuesta)
        {
            DataTable dt = dal.getBuscarDocumentosRespuesta(IdRespuesta).Tables[0];
            GrvDocumentos.DataSource = dt;
            GrvDocumentos.DataBind();
        }

        protected void LbtnIdDocumento_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                GridViewRow row = (GridViewRow)btn.NamingContainer;
                Label _lblId = (Label)GrvDocumentos.Rows[row.RowIndex].FindControl("LblIdDocumento");
                Response.Redirect("DocumentoIn.aspx?Id=" + _lblId.Text);
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void btnArchivo_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                GridViewRow row = (GridViewRow)btn.NamingContainer;
                Label _lblArchivo = (Label)GrvDocumentos.Rows[row.RowIndex].FindControl("lblArchivo");
                ScriptManager.RegisterStartupScript(this, this.GetType(), UniqueID, "window.open('" + _lblArchivo.Text + "','_blank');", true);
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }


        protected void btnEliminarDocumento_Click1(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                GridViewRow row = (GridViewRow)btn.NamingContainer;
                Label _LblIdDocumento = (Label)GrvDocumentos.Rows[row.RowIndex].FindControl("LblIdDocumento");

                dal.setEliminarDocumentoRespuesta(Convert.ToInt32(_LblIdDocumento.Text), Convert.ToInt32(HdId.Value));

                BuscarDocumentosRespuesta(Convert.ToInt32(HdId.Value));
                //  BuscarDocumentos();
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void GrvDocumentos_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void GrvDocumentos_PreRender(object sender, EventArgs e)
        {

        }
    }
}