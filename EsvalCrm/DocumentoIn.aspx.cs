﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;

namespace EsvalCrm
{
    public partial class DocumentoIn : System.Web.UI.Page
    {
        Datos dal = new Datos();
        Comun com = new Comun();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.IsPostBack)
                {
                    BuscarCategoria();

                    string _Id = Convert.ToString(Request.QueryString["Id"]);
                    if (!string.IsNullOrEmpty(_Id))
                    {
                        HdId.Value = _Id;
                        if (HdId.Value != "0")
                        {
                            Buscar(Convert.ToInt32(_Id));
                        }

                    }
                    else
                    {
                        HdId.Value = "0";
                    }
                }
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        void Buscar(int Id)
        {
            DataTable dt = dal.getBuscarDocumento(Id, null, 0, null).Tables[0];
            foreach (DataRow item in dt.Rows)
            {
                LblId.Text = Id.ToString();
                TxtNombre.Text = item["NOMBRE"].ToString();
                TxtDescripcion.Text = item["DESCRIPCION"].ToString();
                DdlCategoria.SelectedValue = item["ID_CATEGORIA_DOCUMENTO"].ToString();
                HlArchivo.NavigateUrl = item["Archivo"].ToString();
                HlArchivo.Text = item["Archivo"].ToString();
                ddlEstado.SelectedValue = item["ACTIVO"].ToString();
                TxtPalabraClave.Text = item["PALABRA_CLAVE"].ToString();
            }
        }

        void BuscarCategoria()
        {
            DataTable dt = new DataTable();
            dt = dal.getBuscarCategoriaDocumento(0).Tables[0];
            DdlCategoria.DataSource = dt;
            DdlCategoria.DataTextField = "NOM_CATEGORIA_DOCUMENTO";
            DdlCategoria.DataValueField = "ID_CATEGORIA_DOCUMENTO";
            DdlCategoria.DataBind();
        }

        //public class DocumentoEntity
        //{
        //    public int IDDOCUMENTO { get; set; }
        //    public string NOMBRE { get; set; }
        //    public string DESCRIPCION { get; set; }
        //    public int? IDCATEGORIADOCUMENTO { get; set; }
        //    public string ARCHIVO { get; set; }
        //    public string FECHAINGRESO { get; set; }
        //    public int? IDUSUARIOINGRESO { get; set; }
        //    public string IDUSUARIOINGRESOName { get; set; }
        //    public System.Nullable<bool> BANNER { get; set; }
        //    public int ACTIVO { get; set; }
        //}

        protected void DdlCategoria_DataBound(object sender, EventArgs e)
        {
            DdlCategoria.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        }

        protected void BtnGrabar_Click(object sender, EventArgs e)
        {
            try
            {
                int IdUsuario = Convert.ToInt32(Session["variableIdUsuario"]);
                int activo = Convert.ToInt16(ddlEstado.SelectedValue);
                if (FuArchivo.HasFile)
                {
                    string archivo = string.Empty;

                    if (HdId.Value == "0")
                    {
                        int idDoc = dal.setInDocumento(TxtNombre.Text.Trim(), TxtDescripcion.Text.Trim(), Convert.ToInt32(DdlCategoria.SelectedValue), null, IdUsuario, null, activo, TxtPalabraClave.Text.Trim());

                        archivo = "archivosGestion/Documentos/" + idDoc.ToString() + "_" + FuArchivo.FileName.Replace(" ", "_");

                        FuArchivo.SaveAs(Server.MapPath(archivo));
                        dal.SetEditarDocumento(idDoc, TxtNombre.Text.Trim(), TxtDescripcion.Text.Trim(), Convert.ToInt32(DdlCategoria.SelectedValue), archivo, IdUsuario, null, activo, TxtPalabraClave.Text.Trim());
                    }
                    else if (HdId.Value != "0")
                    {

                        //string archivo = HlArchivo.NavigateUrl;

                        archivo = "archivosGestion/Documentos/" + HdId.Value + "_" + FuArchivo.FileName.Replace(" ", "_");

                        FuArchivo.SaveAs(Server.MapPath(archivo));
                        dal.SetEditarDocumento(Convert.ToInt32(HdId.Value), TxtNombre.Text.Trim(), TxtDescripcion.Text.Trim(), Convert.ToInt32(DdlCategoria.SelectedValue), archivo, IdUsuario, null, activo, TxtPalabraClave.Text.Trim());
                    }
                    //else
                    //{
                    //    //dal.SetEditarDocumento(Convert.ToInt32(HdId.Value), TxtNombre.Text.Trim(), TxtDescripcion.Text.Trim(), Convert.ToInt32(DdlCategoria.SelectedValue), "", IdUsuario, null, 1);
                    //}

                }
                else if (HdId.Value != "0")
                {
                    string archivo = HlArchivo.NavigateUrl;

                    dal.SetEditarDocumento(Convert.ToInt32(HdId.Value), TxtNombre.Text.Trim(), TxtDescripcion.Text.Trim(), Convert.ToInt32(DdlCategoria.SelectedValue), archivo, IdUsuario, null, activo, TxtPalabraClave.Text.Trim());
                }
                else
                {
                    lblInfo.Text = "Favor de adjuntar documento.";
                    divAlerta.Attributes["class"] = "alert alert-warning";
                    divAlerta.Visible = true;
                    return;
                }

                lblInfo.Text = "Documento grabado";
                divAlerta.Attributes["class"] = "alert alert-success";
                divAlerta.Visible = true;
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void BtnCancelar_Click(object sender, EventArgs e)
        {
            Response.Redirect("Documentos.aspx");
        }
    }
}