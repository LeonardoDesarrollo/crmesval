﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Usuarios.aspx.cs" Inherits="EsvalCrm.Usuarios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Usuarios</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Administración</a></li>
                        <li class="breadcrumb-item"><a href="#">Mantenedores</a></li>
                        <li class="breadcrumb-item active">Usuarios</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <!-- Alertas -->
            <div id="divAlerta" runat="server" visible="false" class="alert alert-danger">
                <strong>Atención!: </strong>
                <asp:Label Text="" ID="lblInfo" runat="server" />
            </div>

            <div runat="server" id="divUsuarios" class="card card-danger card-outline">
                <div class="card-header">
                    <asp:LinkButton ID="bntNuevo" OnClick="btnNuevo_Click" runat="server" CssClass="btn btn-xs btn-success" ForeColor="White"><span class="fa fa-plus"></span></asp:LinkButton>
                    <asp:ImageButton ID="ibtnExportarExcel" ImageUrl="~/img/file_extension_xls.png" runat="server" OnClick="ibtnExportarExcel_Click" />
                </div>
                <div class="card-body">
                    <asp:GridView ID="grvUsuarios" runat="server" ClientIDMode="Static" CssClass="table table-hover table-sm datatable" BorderColor="Transparent"
                        HeaderStyle-CssClass="active" PagerStyle-CssClass="active" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
                        <Columns>
                            <asp:TemplateField HeaderText="ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblIdUsuario" runat="server" Text='<%# Bind("ID_USUARIO") %>' Visible="true"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Login">
                                <ItemTemplate>
                                    <asp:Label ID="lblRutaFoto" runat="server" Text='<%# Bind("RUTA_FOTO") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblUsuario" runat="server" Text='<%# Bind("USUARIO") %>'></asp:Label>
                                    <asp:Label ID="lblClave" runat="server" Text='<%# Bind("CONTRASENA") %>' Visible="false"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Nombre">
                                <ItemTemplate>
                                    <asp:Label ID="lblNombres" runat="server" Text='<%# Bind("NOMBRE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Email">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmail" runat="server" Text='<%# Bind("EMAIL") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Área">
                                <ItemTemplate>
                                    <asp:Label ID="lblIdArea" runat="server" Visible="false" Text='<%# Bind("ID_AREA") %>'></asp:Label>
                                    <asp:Label ID="lblArea" runat="server" Visible="true" Text='<%# Bind("AREA") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Perfil">
                                <ItemTemplate>
                                    <asp:Label ID="lblPerfil" runat="server" Text='<%# Bind("NOM_PERFIL") %>'></asp:Label>
                                    <asp:Label ID="lblIdPerfil" Visible="false" runat="server" Text='<%# Bind("ID_PERFIL") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Servicio">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmpresa" runat="server" Text='<%# Bind("SERVICIO") %>'></asp:Label>
                                    <asp:Label ID="lblIdEmpresa" runat="server" Visible="false" Text='<%# Bind("ID_SERVICIO") %>'></asp:Label>
                                    <asp:Label ID="LblIdCargo" runat="server" Visible="false" Text='<%# Bind("ID_CARGO") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Zona">
                                <ItemTemplate>
                                    <asp:Label ID="lblIdZona" runat="server" Visible="false" Text='<%# Bind("ID_ZONA") %>'></asp:Label>
                                    <asp:Label ID="lblZona" runat="server" Visible="true" Text='<%# Bind("ZONA") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Activo">
                                <ItemTemplate>
                                    <asp:Label ID="lblActivo" runat="server" Visible="false" Text='<%# Bind("ACTIVO") %>'></asp:Label>
                                    <asp:Label ID="lblActivo2" runat="server" Text='<%# Bind("ACTIVO2") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderStyle-Width="8%">
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnEditar" OnClick="btnEditar_Click" runat="server" ToolTip="Editar" CssClass="btn btn-xs btn-primary" ForeColor="White"><span class="fa fa-edit"></span></asp:LinkButton>
                                    <asp:LinkButton ID="btnEliminar" OnClick="btnEliminar_Click" OnClientClick="return confirm('¿Desea eliminar el registro?');" runat="server" ToolTip="Eliminar" CssClass="btn btn-xs btn-danger" ForeColor="White"><span class="fa fa-eraser"></span></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>

                    </asp:GridView>
                </div>
            </div>

            <div runat="server" id="divAddEditUsuario" class="card card-danger card-outline" visible="false">
                <div class="card-header">
                </div>
                <div class="card-body">

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <asp:HiddenField ID="hfIdUsuario" runat="server" />
                                <label for="txtNombres">Nombre:</label>
                                <asp:TextBox ID="txtNombres" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label for="txtLogin">Login:</label>
                                <asp:TextBox ID="txtLogin" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label for="txtClave">Clave:</label>
                                <asp:TextBox ID="txtClave" runat="server" TextMode="Password" CssClass="form-control input-sm"></asp:TextBox>
                            </div>

                            <div class="form-group" runat="server" visible="true" id="divServicio">
                                <label for="ddlServicio">Servicio:</label>
                                <asp:DropDownList ID="ddlServicio" runat="server" OnDataBound="ddlServicio_DataBound" CssClass="form-control input-sm">
                                </asp:DropDownList>
                            </div>

                            <div class="form-group" runat="server">
                                <label for="ddlZona">Zona:</label>
                                <asp:DropDownList ID="ddlZona" runat="server" OnDataBound="ddlZona_DataBound" CssClass="form-control input-sm">
                                </asp:DropDownList>
                            </div>

                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="txtEmail">Email:</label>
                                <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>

                            <div class="form-group">
                                <label for="ddlArea">Área:</label>
                                <asp:DropDownList ID="ddlArea" runat="server" CssClass="form-control input-sm" OnDataBound="ddlArea_DataBound">
                                </asp:DropDownList>
                            </div>
                            <div class="form-group">
                                <label for="ddlPerfil">Cargo:</label>
                                <asp:DropDownList ID="DdlCargo" runat="server" CssClass="form-control input-sm" OnDataBound="DdlCargo_DataBound">
                                </asp:DropDownList>
                            </div>
                            <div class="form-group">
                                <label for="ddlPerfil">Perfil:</label>
                                <asp:DropDownList ID="ddlPerfil" runat="server" CssClass="form-control input-sm" AutoPostBack="true" OnSelectedIndexChanged="ddlPerfil_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>


                            <div class="form-group">
                                <label for="ddlActivo">Activo:</label>
                                <asp:DropDownList ID="ddlActivo" runat="server" Width="150px" CssClass="form-control input-sm">
                                    <asp:ListItem Text="Si" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-2" id="divImagenUsuario" runat="server" visible="false">
                            <div class="form-group">
                                <label style="text-align: center">Fotografía Perfil:</label>
                                <img runat="server" id="imgUsuario" src="/" class="img-circle img-responsive" style="height: 160px; width: 160px" alt="" />
                            </div>
                            <div class="form-group">
                                <label></label>
                                <asp:FileUpload runat="server" CssClass="form-control" ID="fluFotoUsuario" />
                            </div>
                            <div class="form-group">
                                <label></label>
                                <asp:Button ID="btnSubirImagen" OnClick="btnSubirImagen_Click" CssClass="btn btn-info btn-sm" Text="Subir Imagen" runat="server" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <asp:Button Text="Guardar" CssClass="btn btn-success" OnClick="Guardar_Click" runat="server" />
                    <asp:Button Text="Cancelar" CssClass="btn btn-default" OnClick="Cancelar_Click" runat="server" />
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="server">
</asp:Content>
