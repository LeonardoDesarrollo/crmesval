﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BuscarTicket.aspx.cs" Inherits="EsvalCrm.BuscarTicket" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">
                        <asp:Label ID="lblTituloBuscadorTicket" runat="server"></asp:Label></h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Módulo de Gestión</a></li>
                        <li class="breadcrumb-item active">Buscar Ticket</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">

            <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>--%>

            <!-- Alertas -->
            <div id="divAlerta" runat="server" visible="false" class="alert alert-danger">
                <strong>Atención!: </strong>
                <asp:Label Text="" ID="lblInfo" runat="server" />
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <h5 class="card-title">Buscar</h5>
                        </div>
                        <div class="card-body">
                            <div class="row">

                                <div class="col-3">
                                    <div class="input-group input-group-sm">
                                        <asp:TextBox ID="txtNroAtencion" runat="server" CssClass="form-control" placeholder="Buscar por Nº Atención"></asp:TextBox>
                                        <div class="input-group-append">
                                            <asp:Button ID="btnBuscarNroAtencion" runat="server" CssClass="btn btn-sm btn-primary"
                                                Text="Buscar" OnClick="btnBuscarNroAtencion_Click" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-3">
                                    <div class="input-group input-group-sm">
                                        <asp:TextBox ID="txtBuscar" runat="server" CssClass="form-control" placeholder="Buscar por Nº Ticket"></asp:TextBox>
                                        <div class="input-group-append">
                                            <%--2021-08-12 Comentado y modificado--%>
                                            <%--                                                    <asp:Button ID="btnBuscar" runat="server" CssClass="btn btn-sm btn-primary"
                                                        Text="Buscar" OnClick="btnBuscar_Click" />--%>
                                            <asp:Button ID="btnBuscar" runat="server" CssClass="btn btn-sm btn-primary"
                                                Text="Buscar" OnClick="btnBuscar2_Click" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-3" runat="server" visible="true">
                                    <div class="input-group input-group-sm">
                                        <asp:TextBox ID="txtBuscarPorRut" runat="server" CssClass="form-control" placeholder="Buscar por Rut"></asp:TextBox>
                                        <div class="input-group-append">
                                            <asp:Button ID="btnBuscarPorRut" runat="server" CssClass="btn btn-sm btn-primary"
                                                Text="Buscar" OnClick="btnBuscarPorRut_Click" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-1" runat="server" visible="true">
                                </div>
                                <div class="col-3" runat="server" visible="false">
                                    <asp:TextBox ID="txtBuscarPorNombreCliente" runat="server" CssClass="form-control input-sm" placeholder="Buscar por Cliente"></asp:TextBox>
                                </div>
                                <div class="col-1" runat="server" visible="false">
                                    <asp:Button ID="btnBuscarPorCliente" runat="server" CssClass="btn btn-sm btn-primary"
                                        Text="Buscar" OnClick="btnBuscarPorCliente_Click" />
                                </div>

                                <div class="col-3" runat="server" visible="false">
                                    <asp:TextBox ID="txtBuscarPorRutCi" runat="server" CssClass="form-control input-sm" placeholder="Buscar por Rut CI"></asp:TextBox>
                                </div>
                                <div class="col-1" runat="server" visible="false">
                                    <asp:Button ID="btnBuscarPorRutCi" runat="server" CssClass="btn btn-sm btn-primary"
                                        Text="Buscar" OnClick="btnBuscarPorRutCi_Click" />
                                </div>

                                <div class="col-3" runat="server" visible="false">
                                    <asp:TextBox ID="txtBuscarEmailCi" runat="server" CssClass="form-control input-sm" placeholder="Buscar por Email CI"></asp:TextBox>
                                </div>
                                <div class="col-1" runat="server" visible="false">
                                    <asp:Button ID="btnBuscarEmailCi" runat="server" CssClass="btn btn-sm btn-primary"
                                        Text="Buscar" OnClick="btnBuscarEmailCi_Click" />
                                </div>

                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
                <div class="col-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            Buscador Avanzado
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-2">
                                    <label for="ddlEstado">Fecha Inicio</label>
                                    <div class="input-group">
                                        <asp:TextBox ID="txtFechaDesde" runat="server" ClientIDMode="Static" CssClass="form-control class-date"></asp:TextBox>
                                        <div class="input-group-append">
                                            <button class="btn btn-secondary btn-default" type="button"><span class=" fa fa-calendar"></span></button>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-2">
                                    <label for="ddlEstado">Fecha Hasta</label>
                                    <div class="input-group">
                                        <asp:TextBox ID="txtFechaHasta" ClientIDMode="Static" runat="server" CssClass="form-control class-date"></asp:TextBox>
                                        <div class="input-group-append">
                                            <button class="btn btn-secondary btn-default" type="button"><span class="fa fa-calendar"></span></button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-2">
                                    <label for="ddlUsuarioAsig">Usuario Asig.</label>
                                    <asp:DropDownList ID="ddlUsuarioAsig" runat="server" OnDataBound="ddlUsuarioAsig_DataBound" CssClass="form-control select2">
                                    </asp:DropDownList>

                                </div>

                                <div class="col-2">
                                    <label for="ddlEstado">Estado</label>
                                    <asp:DropDownList ID="ddlEstado" runat="server" OnDataBound="ddlEstado_DataBound" CssClass="form-control select2">
                                    </asp:DropDownList>
                                </div>

                                <div class="col-2">
                                    <label for="ddlArea">Área</label>
                                    <asp:DropDownList ID="ddlArea" runat="server" OnDataBound="ddlArea_DataBound" CssClass="form-control select2"></asp:DropDownList>
                                </div>
                                <div class="col-2">
                                    <label for="ddlArea">Servicio</label>
                                    <asp:DropDownList ID="DdlServicio" runat="server" OnDataBound="DdlServicio_DataBound" CssClass="form-control select2"></asp:DropDownList>
                                </div>
                                <div class="col-2">
                                    <label>Canal</label>
                                    <asp:DropDownList ID="ddlCanal" runat="server" CssClass="form-control select2" AutoPostBack="true" OnDataBound="ddlCanal_DataBound">
                                    </asp:DropDownList>
                                </div>
                                <div class="col-2">
                                    <label>SLA</label>
                                    <asp:DropDownList ID="ddlSLA" runat="server" CssClass="form-control">
                                        <asp:ListItem Text="Todos" Value="Todos"></asp:ListItem>
                                        <asp:ListItem Text="Atrasados" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="No Atrasados" Value="0"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col-2">
                                    <br />
                                    <asp:LinkButton ID="btnExcel" Text="text" ToolTip="Exportar a CSV" OnClick="ibtnExcel_Click" ForeColor="White" CssClass="btn btn-success btn-sm" runat="server"><i class="fa fa-lg fa-file-excel"></i></asp:LinkButton>
                                </div>
                                <div class="col-2"></div>
                                <div class="col-2">
                                    <asp:CheckBox ID="chkDerivado" runat="server" Text="&nbsp;&nbsp;Derivado"></asp:CheckBox><br />
                                    <asp:CheckBox ID="chkResumen" runat="server" Text="&nbsp;&nbsp;Cuadro Resumen"></asp:CheckBox>
                                </div>
                                <div class="col-2">
                                    <br />
                                    <asp:Button ID="btnBuscarTicket" runat="server" CssClass="btn btn-sm btn-block btn-primary"
                                        Text="Buscar" OnClick="btnBuscarTicket_Click" DisableOnSubmit="true" />
                                </div>
                                <div class="col-2 d-none">
                                    <label for="ddlCentral">Central</label>
                                    <asp:DropDownList ID="ddlCentral" runat="server" CssClass="from-control input-sm" AutoPostBack="true" OnSelectedIndexChanged="ddlCentral_SelectedIndexChanged" OnDataBound="ddlCentral_DataBound">
                                    </asp:DropDownList>
                                </div>

                                <div class="col-xs-1 d-none">
                                    <label for="ddlInsistencia">Insistencia</label>
                                    <asp:DropDownList ID="ddlInsistencia" runat="server" CssClass="form-control input-sm">
                                        <asp:ListItem Text="Todos" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="SI" Value="SI"></asp:ListItem>
                                        <asp:ListItem Text="NO" Value="NO"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>

                                <div class="col-2 d-none">
                                    <label for="ddlEscalamiento">Escalamiento</label>
                                    <asp:DropDownList ID="ddlEscalamiento" runat="server" CssClass="form-control input-sm">
                                        <asp:ListItem Text="Todos" Value="999999"></asp:ListItem>
                                        <asp:ListItem Text="CON ESCALAMIENTO" Value="666666"></asp:ListItem>
                                        <asp:ListItem Text="SIN ESCALAMIENTO" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="ESCALAMIENTO NIVEL 1" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="ESCALAMIENTO NIVEL 2" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="ESCALAMIENTO NIVEL 3" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="ESCALAMIENTO NIVEL 4" Value="4"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col-2 d-none">
                                    <label for="ddlEstado">Tipo</label>
                                    <asp:DropDownList ID="ddlTipo" runat="server" CssClass="form-control input-sm">
                                        <asp:ListItem Text="Todos" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="Caso Deriva" Value="D"></asp:ListItem>
                                        <asp:ListItem Text="Resuelto Primera Linea" Value="P"></asp:ListItem>
                                        <asp:ListItem Text="Dual" Value="B"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="card card-danger">
                <div class="row">
                    <div class="col-md-12">
                        <div runat="server" id="divGrilla" class="table-responsive">
                            <asp:GridView ID="grvTickets" runat="server" CssClass="table table-hover table-bordered table-sm text-sm"
                                AutoGenerateColumns="false" OnRowDataBound="paginacion_RowDataBound" EmptyDataText="Tickets no encontrado"
                                PageSize="50" AllowPaging="true" OnPreRender="grvTickets_PreRender">
                                <Columns>
                                    <asp:TemplateField HeaderText="Id Ticket">
                                        <ItemTemplate>
                                            <asp:Label ID="lblIdTicket" runat="server" Visible="false" Text='<%# Bind("ID_ATENCION") %>'></asp:Label>
                                            <asp:LinkButton ID="lbtnIdTicket" runat="server" Text='<%# Bind("ID_ATENCION") %>' OnClick="lbtnIdTicket_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Id Cliente">
                                        <ItemTemplate>
                                            <asp:Label ID="lblIdCliente" runat="server" Visible="true" Text='<%# Bind("ID_CLIENTE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="N° Atención">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNroAencion" runat="server" Visible="true" Text='<%# Bind("NRO_ATENCION") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Rut">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRutCliente" runat="server" Visible="true" Text='<%# Bind("RUT_CLIENTE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Nombre">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNombreCliente" runat="server" Visible="true" Text='<%# Bind("NOMBRE_CLIENTE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="U.Creación">
                                        <ItemTemplate>
                                            <asp:Label ID="lblUsuarioCreacion" runat="server" Text='<%# Bind("USUARIO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="F.Creación">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFechaCreacion" runat="server" Text='<%# Bind("FECHA") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="U.Asignado">
                                        <ItemTemplate>
                                            <asp:Label ID="lblUsuarioAsignado" runat="server" Text='<%# Bind("USUARIO_ASIG") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Estado">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEstado" runat="server" Text='<%# Bind("ESTADO_ATENCION") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Área">
                                        <ItemTemplate>
                                            <asp:Label ID="LblArea" runat="server" Text='<%# Bind("AREA") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Servicio">
                                        <ItemTemplate>
                                            <asp:Label ID="LblServicio" runat="server" Text='<%# Bind("SERVICIO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="T.Res.">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTiempoResolucion" runat="server" Text='<%# Bind("TIEMPO_RESOLUCION") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Nivel 1">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNivel1" runat="server" Text='<%# Bind("NIVEL_1") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Ins.">
                                        <ItemTemplate>
                                            <asp:Label ID="lblInsistencias" runat="server" Text='<%# Bind("INSISTENCIAS") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Obs.">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ibtnObservacion" runat="server" OnClick="ibtnObservacion_Click" ImageUrl="~/img/page.png" ToolTip='<%# Bind("OBSERVACION") %>' />
                                            <asp:Label ID="lblObservacion" runat="server" Visible="false" Text='<%# Bind("OBSERVACION") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Obs.Cli">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ibtnObservacionCli" runat="server" OnClick="ibtnObservacionCli_Click" ImageUrl="~/img/page.png" ToolTip='<%# Bind("OBSERVACION_CLIENTE") %>' />
                                            <asp:Label ID="lblObservacionCli" runat="server" Visible="false" Text='<%# Bind("OBSERVACION_CLIENTE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Adj.1">
                                        <ItemTemplate>
                                            <asp:Label ID="lblArchivo" runat="server" Visible="false" Text='<%# Bind("RUTA_ARCHIVO") %>'></asp:Label>
                                            <asp:LinkButton ID="btnArchivo" runat="server" OnClick="btnArchivo_Click" Text="text"><i class="fa fa-file"></i></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Adj.2">
                                        <ItemTemplate>
                                            <asp:Label ID="lblArchivo2" runat="server" Visible="false" Text='<%# Bind("RUTA_ARCHIVO2") %>'></asp:Label>
                                            <asp:LinkButton ID="btnArchivo2" runat="server" OnClick="btnArchivo2_Click" Text="text"><i class="fa fa-file"></i></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Pdf">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtnGenerarPdf" runat="server"
                                                OnClick="lbtnGenerarPdf_Click"><i aria-hidden="true" class="fa fa-file-pdf"></i> </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>

                                <PagerTemplate>
                                    <div>
                                        <div style="float: left">
                                            <asp:ImageButton ID="imgFirst" runat="server"
                                                ImageUrl="~/img/grid/first.gif" OnClick="imgFirst_Click"
                                                Style="height: 15px" title="Navegación: Ir a la Primera Pagina" Width="26px" />
                                            <asp:ImageButton ID="imgPrev" runat="server"
                                                ImageUrl="~/img/grid/prev.gif" OnClick="imgPrev_Click"
                                                title="Navegación: Ir a la Pagina Anterior" Width="26px" />
                                            <asp:ImageButton ID="imgNext" runat="server"
                                                ImageUrl="~/img/grid/next.gif" OnClick="imgNext_Click"
                                                title="Navegación: Ir a la Siguiente Pagina" Width="26px" />
                                            <asp:ImageButton ID="imgLast" runat="server"
                                                ImageUrl="~/img/grid/last.gif" OnClick="imgLast_Click"
                                                title="Navegación: Ir a la Ultima Pagina" Width="26px" />
                                        </div>

                                        <div style="float: left">
                                            Registros por página: 50
                                        </div>

                                        <div style="float: right">
                                            Total Registros: 
                                <asp:Label ID="lblTotalRegistros" runat="server"></asp:Label>
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                Página
                                <asp:Label ID="lblPagina" runat="server"></asp:Label>
                                            de
                                <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                </PagerTemplate>

                            </asp:GridView>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-md-12">
                        <div runat="server" class="box box-danger" id="divResumen" visible="false">
                            <asp:GridView ID="grvResumen" runat="server" Width="50%" CssClass="table table-bordered table-hover table-condensed small" HeaderStyle-CssClass="active" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField HeaderText="Estado">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEstado" runat="server" Visible="true" Text='<%# Bind("ESTADO_ATENCION") %>'></asp:Label>
                                            <asp:Label ID="lblIdEstado" runat="server" Visible="false" Text='<%# Bind("ID_ESTADO_ATENCION") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtnCantidad" runat="server" Text='<%# Bind("CANTIDAD") %>' OnClick="lbtnCantidad_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Exportar" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnExportar" Text="text" OnClick="btnExportar_Click" CssClass="btn btn-success btn-xs" runat="server"><i class="fa fa-lg fa-file-excel-o"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>

            <asp:Panel ID="Panel1" runat="server" CssClass="modal fade bs-example-modal-lg" TabIndex="-1" role="dialog" aria-labelledby="myLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="modal-header">
                                    <h4 class="modal-title" id=" myLabel ">Observación del Ticket</h4>
                                </div>
                                <div class="modal-body">
                                    <p>
                                        <asp:Label ID="lblObservacionTicket" runat="server"></asp:Label>
                                    </p>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </asp:Panel>

            <div class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Modal title</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Modal body text goes here.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary">Save changes</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <%--</ContentTemplate>
            </asp:UpdatePanel>--%>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="server">
</asp:Content>
