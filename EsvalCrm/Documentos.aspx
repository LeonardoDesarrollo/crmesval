﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Documentos.aspx.cs" Inherits="EsvalCrm.Documentos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Documentos</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                             <li class="breadcrumb-item"><a href="#">Administración</a></li>
                        <li class="breadcrumb-item"><a href="#">Mantenedores</a></li>
                        <li class="breadcrumb-item active">Documentos</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>

                    <!-- Alertas -->
                    <div id="divAlerta" runat="server" visible="false" class="alert alert-danger">
                        <strong>Atención!: </strong>
                        <asp:Label Text="" ID="lblInfo" runat="server" />
                    </div>
                    <br />

                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-info card-outline">
                                <div class="card-header">
                                    <h3 class="card-title">Buscador de Documentos</h3>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <strong>Nombre</strong>
                                            <asp:TextBox ID="TxtNombre" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="col-md-3">
                                            <strong>Palabra Clave</strong>
                                            <asp:TextBox ID="TxtPalabraClave" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="col-xs-4">
                                            <strong>Categoría</strong>
                                            <asp:DropDownList ID="DdlCategoria" runat="server" OnDataBound="DdlCategoria_DataBound" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-md-1">
                                            <br />

                                            <asp:Button ID="btnBuscarTicket" runat="server" CssClass="btn btn-sm btn-primary"
                                                Text="Buscar" OnClick="btnBuscarTicket_Click" DisableOnSubmit="true" />
                                        </div>

                                        <div class="col-md-1">
                                            <br />
                                            <asp:Button ID="BtnNuevo" runat="server" CssClass="btn btn-sm btn-success"
                                                Text="Nuevo" OnClick="BtnNuevo_Click" DisableOnSubmit="true" />
                                        </div>
                                    </div>
                                </div>

                                <asp:GridView ID="GrvDocumentos" runat="server" CssClass="table table-sm table-hover " HeaderStyle-CssClass="active" BorderColor="Transparent" AutoGenerateColumns="false" OnRowDataBound="GrvDocumentos_RowDataBound" OnPreRender="GrvDocumentos_PreRender" EmptyDataText="documento no encontrado" EmptyDataRowStyle-CssClass="active h3" PageSize="50" AllowPaging="true">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Id">
                                            <ItemTemplate>
                                                <asp:Label ID="LblIdDocumento" runat="server" Visible="false" Text='<%# Bind("ID_DOCUMENTO") %>'></asp:Label>
                                                <asp:LinkButton ID="LbtnIdDocumento" runat="server" ToolTip="Editar" ForeColor="White" Visible="true" Text='<%# Bind("ID_DOCUMENTO") %>' OnClick="LbtnIdDocumento_Click" CssClass="btn btn-xs btn-primary"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Nombre">
                                            <ItemTemplate>
                                                <asp:Label ID="LblNombre" runat="server" Visible="true" Text='<%# Bind("NOMBRE") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Palabra Clave">
                                            <ItemTemplate>
                                                <asp:Label ID="LblPalabraClave" runat="server" Visible="true" Text='<%# Bind("PALABRA_CLAVE") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Descripción">
                                            <ItemTemplate>
                                                <asp:Label ID="LblDescripcion" runat="server" Visible="true" Text='<%# Bind("DESCRIPCION") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Categoría">
                                            <ItemTemplate>
                                                <asp:Label ID="LblIdCategoria" runat="server" Visible="false" Text='<%# Bind("ID_CATEGORIA_DOCUMENTO") %>'></asp:Label>
                                                <asp:Label ID="LblCategoria" runat="server" Text='<%# Bind("NOM_CATEGORIA_DOCUMENTO") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Estado">
                                            <ItemTemplate>
                                                <asp:Label ID="LblEstado" runat="server" Visible="true" Text='<%# Bind("ESTADO") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Archivo">
                                            <ItemTemplate>
                                                <asp:Label ID="LblIdUsuarioIngreso" runat="server" Visible="false" Text='<%# Bind("ID_USUARIO_INGRESO") %>'></asp:Label>
                                                <asp:Label ID="lblArchivo" runat="server" Visible="false" Text='<%# Bind("ARCHIVO") %>'></asp:Label>
                                                <asp:LinkButton ID="btnArchivo" runat="server" OnClick="btnArchivo_Click" Text="text"><i class="fa fa-files-o"></i></asp:LinkButton>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Banner">
                                            <ItemTemplate>
                                                <asp:Label ID="LblBanner" runat="server" Visible="false" Text='<%# Bind("BANNER") %>'></asp:Label>
                                                <asp:CheckBox ID="ChkBanner" runat="server" AutoPostBack="true" OnCheckedChanged="ChkBanner_CheckedChanged" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnEliminar" OnClick="btnEliminar_Click" OnClientClick="return confirm('¿Está seguro que desea eliminar el documento?');" runat="server" ToolTip="Eliminar" CssClass="btn btn-xs btn-danger" ForeColor="White"><span class="fa fa-eraser"></span></asp:LinkButton>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>


                                    </Columns>

                                    <PagerTemplate>
                                        <div>
                                            <div style="float: left">
                                                <asp:ImageButton ID="imgFirst" runat="server"
                                                    ImageUrl="~/img/grid/first.gif" OnClick="imgFirst_Click"
                                                    Style="height: 15px" title="Navegación: Ir a la Primera Pagina" Width="26px" />
                                                <asp:ImageButton ID="imgPrev" runat="server"
                                                    ImageUrl="~/img/grid/prev.gif" OnClick="imgPrev_Click"
                                                    title="Navegación: Ir a la Pagina Anterior" Width="26px" />
                                                <asp:ImageButton ID="imgNext" runat="server"
                                                    ImageUrl="~/img/grid/next.gif" OnClick="imgNext_Click"
                                                    title="Navegación: Ir a la Siguiente Pagina" Width="26px" />
                                                <asp:ImageButton ID="imgLast" runat="server"
                                                    ImageUrl="~/img/grid/last.gif" OnClick="imgLast_Click"
                                                    title="Navegación: Ir a la Ultima Pagina" Width="26px" />
                                            </div>

                                            <div style="float: left">
                                                Registros por página: 50
                                            </div>

                                            <div style="float: right">
                                                Total Registros:
                                <asp:Label ID="lblTotalRegistros" runat="server"></asp:Label>
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                Página
                                <asp:Label ID="lblPagina" runat="server"></asp:Label>
                                                de
                                <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </PagerTemplate>
                                </asp:GridView>

                                <!-- /.box-body -->
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->


                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="server">
</asp:Content>
