﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using System.Data;
using DataTableToExcel;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.IO;
using System.Text;

namespace EsvalCrm
{
    public partial class BuscarTicket : System.Web.UI.Page
    {
        Datos dal = new Datos();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lblInfo.Text = "";
                divAlerta.Visible = false;
                if (!Page.IsPostBack)
                {
                    string perfil = Session["variableIdPerfil"].ToString();
                    string idUsuario = Session["variableIdUsuario"].ToString();
                    string verTodos = Session["variableVerTodos"].ToString();

                    usuarioAsig();
                    estado();
                    area();
                    central();
                    canal();
                    Servicio();

                    //
                    if (perfil == "1")
                    {
                        ddlArea.Enabled = true;
                    }
                    else
                    {
                        ddlArea.Enabled = false;
                        ddlArea.SelectedValue = Session["variableIdArea"].ToString();
                    }

                    if (verTodos != "1")
                    {
                        ddlUsuarioAsig.SelectedValue = idUsuario;
                        ddlUsuarioAsig.Enabled = false;
                        chkResumen.Visible = false;
                    }
                    else
                    {
                        ddlUsuarioAsig.Enabled = true;
                        chkResumen.Visible = true;
                    }

                    string _idEstado = Convert.ToString(Request.QueryString["e"]);
                    if (_idEstado != null)
                    {
                        //string _idUsuario = Convert.ToString(Request.QueryString["u"]);

                        ddlEstado.SelectedValue = _idEstado;
                        divResumen.Visible = false;
                        divGrilla.Visible = true;

                        if (verTodos == "1")
                        {

                        }
                        else
                        {
                            ddlUsuarioAsig.SelectedValue = idUsuario;
                        }

                        //if (_idUsuario != null)
                        //{
                        //    ddlUsuarioAsig.SelectedValue = _idUsuario;
                        //}
                        ddlArea.SelectedValue = Convert.ToString(Request.QueryString["a"]);
                        ddlUsuarioAsig.SelectedValue = Convert.ToString(Request.QueryString["u"]);


                        buscarTicket(_idEstado, null, null, 0, null);
                    }

                    string _rutCliente = Convert.ToString(Request.QueryString["rutCli"]);
                    if (_rutCliente != null)
                    {
                        divResumen.Visible = false;
                        divGrilla.Visible = true;
                        txtBuscarPorRut.Text = _rutCliente;
                        btnBuscarPorRut_Click(sender, e);
                        //btnBuscar_Click(sender, e);
                        //buscarGr();
                    }

                    string _ticketConInsistencia = Convert.ToString(Request.QueryString["conInsistencia"]);
                    if (_ticketConInsistencia != null)
                    {
                        divResumen.Visible = false;
                        divGrilla.Visible = true;

                        ddlArea.SelectedValue = Convert.ToString(Request.QueryString["a"]);
                        ddlUsuarioAsig.SelectedValue = Convert.ToString(Request.QueryString["u"]);


                        ddlInsistencia.SelectedValue = "SI";
                        buscarTicket(ddlEstado.SelectedValue, null, null, 0, null);
                    }

                    string _ticketConEscalamiento = Convert.ToString(Request.QueryString["conEscalamiento"]);
                    if (_ticketConEscalamiento != null)
                    {
                        divResumen.Visible = false;
                        divGrilla.Visible = true;
                        //ddlEscalamiento.SelectedValue = "SI";
                        ddlArea.SelectedValue = Convert.ToString(Request.QueryString["a"]);
                        ddlUsuarioAsig.SelectedValue = Convert.ToString(Request.QueryString["u"]);


                        ddlEscalamiento.SelectedValue = "666666";
                        buscarTicket(ddlEstado.SelectedValue, null, null, 0, null);
                    }

                    string _estado = Convert.ToString(Request.QueryString["estado"]);
                    if (_estado != null)
                    {
                        string _idEstatus = Convert.ToString(Request.QueryString["idEstatus"]);
                        string _escalamiento = Convert.ToString(Request.QueryString["escalamiento"]);
                        string _fechaDesde = Convert.ToString(Request.QueryString["fechaDesde"]);
                        string _fechaHasta = Convert.ToString(Request.QueryString["fechaHasta"]);
                        string _user = Convert.ToString(Request.QueryString["user"]);

                        ddlArea.SelectedValue = Convert.ToString(Request.QueryString["a"]);
                        ddlUsuarioAsig.SelectedValue = Convert.ToString(Request.QueryString["u"]);

                        ddlEstado.SelectedValue = _estado;
                        ddlEscalamiento.SelectedValue = _escalamiento;
                        txtFechaDesde.Text = _fechaDesde;
                        txtFechaHasta.Text = _fechaHasta;
                        ddlUsuarioAsig.SelectedValue = _user;

                        divResumen.Visible = false;
                        divGrilla.Visible = true;

                        buscarTicket(ddlEstado.SelectedValue, _idEstatus, "0", 0, null);
                        return;
                    }

                    string _tipo = Convert.ToString(Request.QueryString["tipo"]);
                    if (_tipo != null)
                    {
                        ddlTipo.SelectedValue = _tipo;
                        divResumen.Visible = false;
                        divGrilla.Visible = true;

                        ddlArea.SelectedValue = Convert.ToString(Request.QueryString["a"]);
                        ddlUsuarioAsig.SelectedValue = Convert.ToString(Request.QueryString["u"]);

                        buscarTicket(ddlEstado.SelectedValue, null, ddlTipo.SelectedValue, 0, null);

                        return;
                    }

                    string _idEstatus2 = Convert.ToString(Request.QueryString["idEstatus"]);
                    if (_idEstatus2 != null)
                    {
                        _idEstatus2 = Convert.ToString(Request.QueryString["idEstatus"]);
                        string _escalamiento = Convert.ToString(Request.QueryString["escalamiento"]);
                        string _fechaDesde = Convert.ToString(Request.QueryString["fechaDesde"]);
                        string _fechaHasta = Convert.ToString(Request.QueryString["fechaHasta"]);
                        string _user = Convert.ToString(Request.QueryString["user"]);

                        ddlEscalamiento.SelectedValue = _escalamiento;
                        //ddlEstado.SelectedValue = _estado;

                        txtFechaDesde.Text = _fechaDesde;
                        txtFechaHasta.Text = _fechaHasta;
                        ddlUsuarioAsig.SelectedValue = _user;

                        divResumen.Visible = false;
                        divGrilla.Visible = true;

                        if (_idEstatus2 == "TOTAL")
                        {
                            buscarTicket("0", null, null, 0, null);
                        }
                        else
                        {
                            buscarTicket("0", _idEstatus2, null, 0, null);
                        }

                        return;
                    }


                    string _fechaDesde2 = Convert.ToString(Request.QueryString["fechaDesde"]);
                    if (_fechaDesde2 != null)
                    {
                        string _escalamiento = Convert.ToString(Request.QueryString["escalamiento"]);
                        _fechaDesde2 = Convert.ToString(Request.QueryString["fechaDesde"]);
                        string _fechaHasta = Convert.ToString(Request.QueryString["fechaHasta"]);
                        string _user = Convert.ToString(Request.QueryString["user"]);


                        txtFechaDesde.Text = _fechaDesde2;
                        txtFechaHasta.Text = _fechaHasta;
                        ddlUsuarioAsig.SelectedValue = _user;

                        divResumen.Visible = false;
                        divGrilla.Visible = true;

                        buscarTicket(null, null, null, 0, null);
                    }

                    string _derivado = Convert.ToString(Request.QueryString["d"]);
                    if (_derivado != null)
                    {
                        if (_derivado == "1")
                        {
                            ddlArea.SelectedValue = Convert.ToString(Request.QueryString["a"]);
                            ddlUsuarioAsig.SelectedValue = Convert.ToString(Request.QueryString["u"]);

                            chkDerivado.Checked = true;
                            buscarTicket("0", "0", null, 0, null);

                            divResumen.Visible = false;
                            divGrilla.Visible = true;
                        }
                    }

                    string _IdTicket = Convert.ToString(Request.QueryString["IdTicket"]);
                    if (_IdTicket != null)
                    {
                        divResumen.Visible = false;
                        divGrilla.Visible = true;

                        txtBuscar.Text = _IdTicket;

                        //buscarTicket("0", "0", null, Convert.ToInt32(_IdTicket));

                        int? _s = Convert.ToInt32(Request.QueryString["s"]);
                        if (_s == 1)
                        {
                            //string msj = "Ticket ingresado correctamente";
                            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "MensajeSatisfactorio2('" + msj + "')", false);
                            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "MensajeError('" + msj + "')", true);
                            lblInfo.Text = "Ticket ingresado correctamente";
                            divAlerta.Attributes["class"] = "alert alert-success";
                            divAlerta.Visible = true;
                        }

                        btnBuscar_Click(sender, e);
                    }

                    if (Session["strTituloBuscadorTicket"] == null)
                    {
                        lblTituloBuscadorTicket.Text = "Buscador de Tickets";
                    }
                    else
                    {
                        lblTituloBuscadorTicket.Text = Session["strTituloBuscadorTicket"].ToString();
                    }

                    Session["strTituloBuscadorTicket"] = null;
                }
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }
        void central()
        {
            ddlCentral.DataSource = dal.getBuscarCentral(null);
            ddlCentral.DataValueField = "ID_CENTRAL";
            ddlCentral.DataTextField = "CENTRAL";
            ddlCentral.DataBind();
        }

        public string buscarIdUsuario()
        {
            string idUsuario = "";
            foreach (DataRow rowDs in dal.getBuscarUsuario(Session["variableUsuario"].ToString(), null).Tables[0].Rows)
            {
                idUsuario = Convert.ToString(rowDs["ID_USUARIO"]);
            }
            return idUsuario;
        }

        void canal()
        {
            ddlCanal.DataSource = dal.getBuscarCanal(null);
            ddlCanal.DataValueField = "ID_CANAL";
            ddlCanal.DataTextField = "CANAL";
            ddlCanal.DataBind();
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                string buscar = txtBuscar.Text.Trim();
                if (buscar.Length < 1)
                {
                    lblInfo.Text = "Texto de búsqueda debe tener al menos 1 caracter";
                    divAlerta.Attributes["class"] = "alert alert-warning";
                    divAlerta.Visible = true;
                    return;
                }
                Comun comu = new Comun();
                if (!comu.IsNumeric(buscar))
                {
                    lblInfo.Text = "Texto de búsqueda sólo acepta tipo de dato numérico";
                    divAlerta.Attributes["class"] = "alert alert-warning";
                    divAlerta.Visible = true;
                    return;
                }

                string verTodos = Session["variableVerTodos"].ToString();
                string idUsuario2 = Session["variableIdUsuario"].ToString();

                divGrilla.Visible = true;
                string idUsuario = buscarIdUsuario();

                DataTable dt = new DataTable();

                //dt = dal.getBuscarTicketBuscadorPorIdTicket(txtBuscar.Text.Trim()).Tables[0];
                //BUSCACR ID USUARIO

                //if (verTodos == "1")
                //{
                //    dt = dal.getBuscarTicketBuscadorPorIdTicketUsuario(txtBuscar.Text.Trim(), null).Tables[0];

                //}
                //else
                //{
                //    //dt = dal.getBuscarTicketBuscadorPorIdTicketUsuario(txtBuscar.Text.Trim(), idUsuario2).Tables[0];
                //    dt = dal.getBuscarTicketBuscadorPorIdTicketUsuario(txtBuscar.Text.Trim(), null).Tables[0];
                //}

                buscarTicket(null, null, null, Convert.ToInt32(buscar), null);

                //buscarGr();
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void btnBuscarPorRut_Click(object sender, EventArgs e)
        {
            try
            {
                string buscar = txtBuscarPorRut.Text.Trim();
                if (buscar.Length < 1)
                {
                    lblInfo.Text = "Texto de busqueda debe tener al menos 1 caracteres";
                    divAlerta.Attributes["class"] = "alert alert-warning";
                    divAlerta.Visible = true;
                    return;
                }

                //divGrilla.Visible = true;
                //string idUsuario = buscarIdUsuario();

                //DataTable dt = new DataTable();
                //dt = dal.getBuscarTicketBuscadorPorRut(txtBuscarPorRut.Text.Trim()).Tables[0];
                //Session["sessionDtTicket"] = dt;
                //grvTickets.DataSource = dt;
                //grvTickets.DataBind();
                buscarTicket(null, null, null, 0, buscar);
                //buscarGr();
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }


        protected void btnBuscarPorCliente_Click(object sender, EventArgs e)
        {
            try
            {
                string buscar = txtBuscarPorNombreCliente.Text.Trim();
                if (buscar.Length < 1)
                {
                    lblInfo.Text = "Texto de busqueda debe tener al menos 1 caracteres";
                    divAlerta.Attributes["class"] = "alert alert-warning";
                    divAlerta.Visible = true;
                    return;
                }

                divGrilla.Visible = true;
                string idUsuario = buscarIdUsuario();
                DataTable dt = new DataTable();
                dt = dal.getBuscarTicketBuscadorPorCliente(txtBuscarPorNombreCliente.Text.Trim()).Tables[0];
                Session["sessionDtTicket"] = dt;
                grvTickets.DataSource = dt;
                grvTickets.DataBind();


                //buscarGr();
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        void buscarGr()
        {
            divGrilla.Visible = true;
            string idUsuario = buscarIdUsuario();
            DataTable dt = new DataTable();
            dt = dal.getBuscarTicketBuscador(txtBuscar.Text, txtBuscar.Text, idUsuario).Tables[0];
            Session["sessionDtTicket"] = dt;
            grvTickets.DataSource = dt;
            grvTickets.DataBind();
        }

        protected void lbtnIdTicket_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtn = sender as LinkButton;
                GridViewRow row = (GridViewRow)lbtn.NamingContainer;
                Label _lblIdTicket = (Label)grvTickets.Rows[row.RowIndex].FindControl("lblIdTicket");
                //Response.Redirect("SeguimientoTicket.aspx?t=" + _lblIdTicket.Text);
                ScriptManager.RegisterStartupScript(this, this.GetType(), UniqueID, "window.open('SeguimientoTicket.aspx?t=" + _lblIdTicket.Text + "','_blank');", true);
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void imgAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("IngresoTicket.aspx");
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void btnBuscarTicket_Click(object sender, EventArgs e)
        {
            try
            {
                if (chkResumen.Checked == true)
                {
                    divResumen.Visible = true;
                    divGrilla.Visible = false;
                    buscarTicketResumen();
                }
                else
                {
                    divResumen.Visible = false;
                    divGrilla.Visible = true;
                    buscarTicket(ddlEstado.SelectedValue, null, ddlTipo.SelectedValue, 0, null);
                }
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        void buscarTicket(string idEstado, string nivel1, string tipo, int idTicket, string rutCliente)
        {
            string derivado;
            if (chkDerivado.Checked == true)
            {
                derivado = "1";
            }
            else
            {
                derivado = null;
            }


            DataTable dt = new DataTable();
            dt = dal.getBuscarTicketBuscadorParametros(ddlUsuarioAsig.SelectedValue, txtFechaDesde.Text, txtFechaHasta.Text,
                idEstado, ddlInsistencia.SelectedValue, ddlEscalamiento.SelectedValue, nivel1,
                Convert.ToInt32(ddlArea.SelectedValue), tipo, derivado, Convert.ToInt32(DdlServicio.SelectedValue), idTicket, rutCliente, ddlSLA.SelectedValue, txtNroAtencion.Text.Trim()).Tables[0];

            Session["sessionDtTicket"] = dt;
            grvTickets.DataSource = dt;
            grvTickets.DataBind();
        }

        //2021-08-12 sólo para botón Buscar por N° Ticket
        protected void btnBuscar2_Click(object sender, EventArgs e)
        {
            try
            {
                string buscar = txtBuscar.Text.Trim();
                if (buscar.Length < 1)
                {
                    lblInfo.Text = "Texto de búsqueda debe tener al menos 1 caracter";
                    divAlerta.Attributes["class"] = "alert alert-warning";
                    divAlerta.Visible = true;
                    return;
                }
                Comun comu = new Comun();
                if (!comu.IsNumeric(buscar))
                {
                    lblInfo.Text = "Texto de búsqueda sólo acepta tipo de dato numérico";
                    divAlerta.Attributes["class"] = "alert alert-warning";
                    divAlerta.Visible = true;
                    return;
                }

                divGrilla.Visible = true;

                //buscarTicket(null, null, null, Convert.ToInt32(buscar), null);

                string derivado;
                if (chkDerivado.Checked == true)
                {
                    derivado = "1";
                }
                else
                {
                    derivado = null;
                }

                DataTable dt = new DataTable();
                dt = dal.getBuscarTicketBuscadorParametros("0", txtFechaDesde.Text.Trim(), txtFechaHasta.Text.Trim(),
                    null, ddlInsistencia.SelectedValue, ddlEscalamiento.SelectedValue, null,
                    Convert.ToInt32(0), null, derivado, Convert.ToInt32(DdlServicio.SelectedValue), Convert.ToInt32(buscar),
                    null, ddlSLA.SelectedValue, txtNroAtencion.Text.Trim()).Tables[0];

                Session["sessionDtTicket"] = dt;
                grvTickets.DataSource = dt;
                grvTickets.DataBind();
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void btnBuscarNroAtencion_Click(object sender, EventArgs e)
        {
            try
            {
                string buscar = txtNroAtencion.Text.Trim();
                if (buscar.Length < 1)
                {
                    lblInfo.Text = "N° Atención debe tener al menos 1 caracter";
                    divAlerta.Attributes["class"] = "alert alert-warning";
                    divAlerta.Visible = true;
                    return;
                }
                //Comun comu = new Comun();
                //if (!comu.IsNumeric(buscar))
                //{
                //    lblInfo.Text = "Texto de búsqueda sólo acepta tipo de dato numérico";
                //    divAlerta.Attributes["class"] = "alert alert-warning";
                //    divAlerta.Visible = true;
                //    return;
                //}

                divGrilla.Visible = true;

                string derivado;
                if (chkDerivado.Checked == true)
                {
                    derivado = "1";
                }
                else
                {
                    derivado = null;
                }

                DataTable dt = new DataTable();
                dt = dal.getBuscarTicketBuscadorParametros("0", txtFechaDesde.Text.Trim(), txtFechaHasta.Text.Trim(),
                    null, ddlInsistencia.SelectedValue, ddlEscalamiento.SelectedValue, null,
                    Convert.ToInt32(0), null, derivado, Convert.ToInt32(DdlServicio.SelectedValue), 0,
                    null, ddlSLA.SelectedValue, txtNroAtencion.Text.Trim()).Tables[0];

                Session["sessionDtTicket"] = dt;
                grvTickets.DataSource = dt;
                grvTickets.DataBind();
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void ddlUsuarioAsig_DataBound(object sender, EventArgs e)
        {
            ddlUsuarioAsig.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Todos", "0"));
        }

        protected void ddlEstado_DataBound(object sender, EventArgs e)
        {
            ddlEstado.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Todos", "0"));
        }

        void usuarioAsig()
        {
            ddlUsuarioAsig.DataSource = dal.getBuscarUsuario(null, null);
            ddlUsuarioAsig.DataValueField = "ID_USUARIO";
            ddlUsuarioAsig.DataTextField = "USUARIO";
            ddlUsuarioAsig.DataBind();
        }

        void estado()
        {
            ddlEstado.DataSource = dal.getBuscarEstadoAtencion();
            ddlEstado.DataValueField = "ID_ESTADO_ATENCION";
            ddlEstado.DataTextField = "ESTADO_ATENCION";
            ddlEstado.DataBind();
        }

        protected void lbtnCantidad_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtn = sender as LinkButton;
                GridViewRow row = (GridViewRow)lbtn.NamingContainer;
                Label _lblIdEstado = (Label)grvResumen.Rows[row.RowIndex].FindControl("lblIdEstado");

                divGrilla.Visible = true;
                divResumen.Visible = false;
                buscarTicket(_lblIdEstado.Text, null, ddlTipo.SelectedValue, 0, null);
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        void buscarTicketResumen()
        {
            grvResumen.DataSource = dal.getBuscarTicketResumen(txtFechaDesde.Text, txtFechaHasta.Text);
            grvResumen.DataBind();

            foreach (GridViewRow grd_Row in grvResumen.Rows)
            {
                LinkButton _btnExportar = (LinkButton)grvResumen.Rows[grd_Row.RowIndex].FindControl("btnExportar");
                ScriptManager scriptManager = ScriptManager.GetCurrent(Page);
                scriptManager.RegisterPostBackControl(_btnExportar);
            }
        }

        protected void btnArchivo_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                GridViewRow row = (GridViewRow)btn.NamingContainer;
                Label _lblArchivo = (Label)grvTickets.Rows[row.RowIndex].FindControl("lblArchivo");
                ScriptManager.RegisterStartupScript(this, this.GetType(), UniqueID, "window.open('" + _lblArchivo.Text + "','_blank');", true);
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }


        protected void btnArchivo2_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                GridViewRow row = (GridViewRow)btn.NamingContainer;
                Label _lblArchivo = (Label)grvTickets.Rows[row.RowIndex].FindControl("lblArchivo2");
                ScriptManager.RegisterStartupScript(this, this.GetType(), UniqueID, "window.open('" + _lblArchivo.Text + "','_blank');", true);
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void btnExportar_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;

                GridViewRow row = (GridViewRow)btn.NamingContainer;
                Label _lblIdEstado = (Label)grvResumen.Rows[row.RowIndex].FindControl("lblIdEstado");

                DataTable dt = new DataTable();
                dt = dal.getBuscarTicketBuscadorParametrosExporte("0", txtFechaDesde.Text, txtFechaHasta.Text, _lblIdEstado.Text).Tables[0];

                Utilidad.ExportDataTableToExcel(dt, "Exporte.xls", "", "", "", "");
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void ibtnObservacion_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                lblObservacionTicket.Text = string.Empty;
                ImageButton btn = (ImageButton)sender;
                GridViewRow row = (GridViewRow)btn.NamingContainer;
                Label _lblObservacion = (Label)grvTickets.Rows[row.RowIndex].FindControl("lblObservacion");

                if (string.IsNullOrEmpty(_lblObservacion.Text))
                {
                    lblObservacionTicket.Text = string.Empty;
                }
                else
                {
                    lblObservacionTicket.Text = _lblObservacion.Text;
                }

                ScriptManager.RegisterStartupScript(UpdatePanel2, UpdatePanel2.GetType(), "show", "$(function () { $('#" + Panel1.ClientID + "').modal('show'); });", true);
                UpdatePanel2.Update();
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }





        protected void imgFirst_Click(object sender, EventArgs e)
        {
            //buscar();
            if (Session["sessionDtTicket"] != null)
            {
                grvTickets.DataSource = Session["sessionDtTicket"];
                grvTickets.DataBind();
            }
            else
            {
                //buscar();
            }
            grvTickets.PageIndex = 0;
            grvTickets.DataBind();
        }

        protected void imgPrev_Click(object sender, EventArgs e)
        {
            //buscar();
            if (Session["sessionDtTicket"] != null)
            {
                grvTickets.DataSource = Session["sessionDtTicket"];
                grvTickets.DataBind();
            }
            else
            {
                //buscar();
            }
            if (grvTickets.PageIndex != 0)
                grvTickets.PageIndex--;
            grvTickets.DataBind();
        }

        protected void imgNext_Click(object sender, EventArgs e)
        {
            //buscar();
            if (Session["sessionDtTicket"] != null)
            {
                grvTickets.DataSource = Session["sessionDtTicket"];
                grvTickets.DataBind();
            }
            else
            {
                //buscar();
            }

            if (grvTickets.PageIndex != (grvTickets.PageCount - 1))
                grvTickets.PageIndex++;
            grvTickets.DataBind();
        }

        protected void imgLast_Click(object sender, EventArgs e)
        {
            //buscar();
            if (Session["sessionDtTicket"] != null)
            {
                grvTickets.DataSource = Session["sessionDtTicket"];
                grvTickets.DataBind();
            }
            else
            {
                //buscar();
            }

            grvTickets.PageIndex = grvTickets.PageCount - 1;
            grvTickets.DataBind();
        }



        protected void paginacion_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //JRL 2018-05-17
                Label _lblObservacion = (Label)e.Row.FindControl("lblObservacion");
                Label _lblObservacionCli = (Label)e.Row.FindControl("lblObservacionCli");

                Label _lblArchivo = (Label)e.Row.FindControl("lblArchivo");
                Label _lblArchivo2 = (Label)e.Row.FindControl("lblArchivo2");

                //JRL 2018-05-17
                ImageButton _ibtnObservacion = (ImageButton)e.Row.FindControl("ibtnObservacion");
                ImageButton _ibtnObservacionCli = (ImageButton)e.Row.FindControl("ibtnObservacionCli");

                LinkButton _btnArchivo = (LinkButton)e.Row.FindControl("btnArchivo");
                LinkButton _btnArchivo2 = (LinkButton)e.Row.FindControl("btnArchivo2");

                if (string.IsNullOrEmpty(_lblArchivo.Text) == true)
                {
                    _btnArchivo.Visible = false;
                }
                else
                {
                    _btnArchivo.Visible = true;
                }

                if (string.IsNullOrEmpty(_lblArchivo2.Text) == true)
                {
                    _btnArchivo2.Visible = false;
                }
                else
                {
                    _btnArchivo2.Visible = true;
                }

                //JRL 2018-05-17
                if (string.IsNullOrEmpty(_lblObservacion.Text) == true)
                {
                    _ibtnObservacion.Visible = false;
                }
                else
                {
                    _ibtnObservacion.Visible = true;
                }

                if (string.IsNullOrEmpty(_lblObservacionCli.Text) == true)
                {
                    _ibtnObservacionCli.Visible = false;
                }
                else
                {
                    _ibtnObservacionCli.Visible = true;
                }
            }

            if (e.Row.RowType == DataControlRowType.Pager)
            {
                Label _lblPagina = (Label)e.Row.FindControl("lblPagina");
                Label _lblTotal = (Label)e.Row.FindControl("lblTotal");
                Label _lblTotalRegistros = (Label)e.Row.FindControl("lblTotalRegistros");
                _lblPagina.Text = Convert.ToString(grvTickets.PageIndex + 1);
                _lblTotal.Text = Convert.ToString(grvTickets.PageCount);

                DataTable dt = new DataTable();
                dt = Session["sessionDtTicket"] as DataTable;
                _lblTotalRegistros.Text = dt.Rows.Count.ToString();
            }
        }

        protected void ddlArea_DataBound(object sender, EventArgs e)
        {
            ddlArea.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Todos", "0"));
        }

        void area()
        {
            ddlArea.DataSource = dal.getBuscarArea();
            ddlArea.DataValueField = "ID_AREA";
            ddlArea.DataTextField = "AREA";
            ddlArea.DataBind();
        }

        protected void btnBuscarPorRutCi_Click(object sender, EventArgs e)
        {
            try
            {
                string buscar = txtBuscarPorRutCi.Text.Trim();
                if (buscar.Length < 1)
                {
                    lblInfo.Text = "Texto de busqueda debe tener al menos 1 caracteres";
                    divAlerta.Attributes["class"] = "alert alert-warning";
                    divAlerta.Visible = true;
                    return;
                }

                divGrilla.Visible = true;
                string idUsuario = buscarIdUsuario();

                DataTable dt = new DataTable();
                dt = dal.getBuscarTicketBuscadorPorTelefonoCi(txtBuscarPorRutCi.Text.Trim()).Tables[0];
                Session["sessionDtTicket"] = dt;
                grvTickets.DataSource = dt;
                grvTickets.DataBind();

                //buscarGr();
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void btnBuscarEmailCi_Click(object sender, EventArgs e)
        {
            try
            {
                string buscar = txtBuscarEmailCi.Text.Trim();
                if (buscar.Length < 1)
                {
                    lblInfo.Text = "Texto de busqueda debe tener al menos 1 caracteres";
                    divAlerta.Attributes["class"] = "alert alert-warning";
                    divAlerta.Visible = true;
                    return;
                }

                divGrilla.Visible = true;
                string idUsuario = buscarIdUsuario();

                DataTable dt = new DataTable();
                dt = dal.getBuscarTicketBuscadorPorEmailCi(txtBuscarEmailCi.Text.Trim()).Tables[0];
                Session["sessionDtTicket"] = dt;
                grvTickets.DataSource = dt;
                grvTickets.DataBind();

                //buscarGr();
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void ibtnObservacionCli_Click(object sender, ImageClickEventArgs e)
        {
            lblObservacionTicket.Text = string.Empty;

            ImageButton btn = (ImageButton)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            Label _lblObservacionCli = (Label)grvTickets.Rows[row.RowIndex].FindControl("lblObservacionCli");

            if (string.IsNullOrEmpty(_lblObservacionCli.Text))
            {
                lblObservacionTicket.Text = string.Empty;
            }
            else
            {
                lblObservacionTicket.Text = _lblObservacionCli.Text;
            }

            ScriptManager.RegisterStartupScript(UpdatePanel2, UpdatePanel2.GetType(), "show", "$(function () { $('#" + Panel1.ClientID + "').modal('show'); });", true);
            UpdatePanel2.Update();
        }

        protected void lbtnGenerarPdf_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                GridViewRow row = (GridViewRow)btn.NamingContainer;
                Label _lblIdTicket = (Label)grvTickets.Rows[row.RowIndex].FindControl("lblIdTicket");
                string ruta = generaPdf(_lblIdTicket.Text);
                //ruta += _lblIdPago.Text + ".pdf";
                ScriptManager.RegisterStartupScript(this, this.GetType(), UniqueID, "window.open('" + ruta + "','_blank');", true);
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }

        }


        public string generaPdf(string idTicket)
        {
            DataTable dt = new DataTable();
            dt = dal.getBuscarTicket(idTicket).Tables[0];
            string rutCliente = "";
            string observacion = string.Empty;
            string usuarioAsignado = string.Empty;
            string estadoTicket = string.Empty;
            string nivel1 = string.Empty;
            string nivel2 = string.Empty;
            string nivel3 = string.Empty;
            string nivel4 = string.Empty;
            string telefono = string.Empty;
            string email = string.Empty;
            string nombreCliente = string.Empty;
            string fechaTicket = string.Empty;

            foreach (DataRow item in dt.Rows)
            {
                rutCliente = item["RUT_CLIENTE"].ToString();
                observacion = item["OBSERVACION"].ToString();

                usuarioAsignado = item["USUARIO_ASIGNADO"].ToString();
                estadoTicket = item["ESTADO_ATENCION"].ToString();

                fechaTicket = item["FECHA"].ToString();

                nivel1 = item["NIVEL_1"].ToString();
                nivel2 = item["NIVEL_2"].ToString();
                nivel3 = item["NIVEL_3"].ToString();
                nivel4 = item["NIVEL_4"].ToString();
            }

            //string nombres, paterno, materno, segmentoCliente, fechaCompra, sernac, msj, celular, fijo, local;
            int? codError;
            string dv = string.Empty;

            if (!string.IsNullOrEmpty(rutCliente.Trim()))
            {
                Comun com = new Comun();
                rutCliente = com.formatearRutSinPuntos(rutCliente);
            }

            String[] arRut = rutCliente.Split('-');
            for (int i = 0; i < arRut.Length; i++)
            {
                rutCliente = arRut[0];
                if (arRut.Length > 0)
                {
                    dv = arRut[1];
                }
            }

            //  datos.getBuscarCliente(1, Convert.ToInt32(rutCliente), dv, "CRM", out nombres, out paterno, out materno, out segmentoCliente, out email, out celular, out fijo, out fechaCompra, out local, out sernac, out codError, out msj);
            //if (nombres == "null")
            //{
            //    nombres = string.Empty;
            //}
            //if (paterno == "null")
            //{
            //    paterno = string.Empty;
            //}
            //if (materno == "null")
            //{
            //    materno = string.Empty;
            //}
            //if (segmentoCliente == "null")
            //{
            //    segmentoCliente = string.Empty;
            //}
            //if (email == "null")
            //{
            //    email = string.Empty;
            //}
            //if (fechaCompra == "null")
            //{
            //    fechaCompra = string.Empty;
            //}
            //if (sernac == "null")
            //{
            //    sernac = string.Empty;
            //}
            //if (celular == "null")
            //{
            //    celular = string.Empty;
            //}
            //if (fijo == "null")
            //{
            //    fijo = string.Empty;
            //}
            //if (local == "null")
            //{
            //    local = string.Empty;
            //}

            //nombreCliente = nombres + " " + paterno + " " + materno;
            //telefono = celular;
            //rutCliente = rutCliente + "-" + dv;

            DataTable dtHistorico = new DataTable();
            dtHistorico = dal.getBuscarTicketHistorico(idTicket).Tables[0];
            string correlativo = string.Empty;
            string fecha = string.Empty;
            string usuarioCreacion = string.Empty;
            string usuarioAsig = string.Empty;
            string estado = string.Empty;
            string motivoCierre = string.Empty;
            string obs = string.Empty;
            string obsCli = string.Empty;

            //AQUIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
            string titulo = "TICKET";
            string nombreArchivoPdf = "ticket_" + idTicket + ".pdf";
            BaseFont bfTimes = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, false);

            Font times = new Font(bfTimes, 7, Font.NORMAL);
            Font timesRojo = new Font(bfTimes, 9, Font.BOLD, BaseColor.RED);
            Font timesCorrelativo = new Font(bfTimes, 9, Font.BOLD);
            Font fontCabecera = new Font(bfTimes, 8, Font.BOLD);
            Font fontFirma = new Font(bfTimes, 8, Font.BOLD);

            Document doc = new Document(PageSize.A4, 25, 25, 30, 30);
            PdfWriter writePdf = PdfWriter.GetInstance(doc, new FileStream(Server.MapPath("pdfTicket/" + nombreArchivoPdf), FileMode.Create));
            doc.Open();

            iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(Server.MapPath("img/Logo_Esval.png"));
            jpg.ScaleToFit(80, 80);
            jpg.Alignment = iTextSharp.text.Image.ALIGN_LEFT;
            doc.Add(jpg);

            PdfPTable tableNumeroComprobante = new PdfPTable(2);
            PdfPCell celdaNumeroComprobante = new PdfPCell(new Paragraph("Nro.Ticket :", fontCabecera));
            //celdaNumeroComprobante.HorizontalAlignment = 2;
            PdfPCell celdaNumeroComprobanteFecha = new PdfPCell(new Paragraph("Fecha Ticket :", fontCabecera));
            //celdaNumeroComprobanteFecha.HorizontalAlignment = 2;
            tableNumeroComprobante.AddCell(celdaNumeroComprobante);
            tableNumeroComprobante.AddCell(new Paragraph(idTicket, times));

            tableNumeroComprobante.AddCell(celdaNumeroComprobanteFecha);
            tableNumeroComprobante.AddCell(new Paragraph(fechaTicket, times));

            tableNumeroComprobante.DefaultCell.Border = Rectangle.NO_BORDER;

            tableNumeroComprobante.HorizontalAlignment = Element.ALIGN_RIGHT;
            tableNumeroComprobante.WidthPercentage = 25.0f;

            foreach (PdfPCell celda in tableNumeroComprobante.Rows[0].GetCells())
            {
                celda.Border = Rectangle.NO_BORDER;
            }

            foreach (PdfPCell celda in tableNumeroComprobante.Rows[1].GetCells())
            {
                celda.Border = Rectangle.NO_BORDER;
                //celda.HorizontalAlignment = 2;
            }

            doc.Add(tableNumeroComprobante);


            Chunk tituloTipoExamen = new Chunk(titulo, FontFactory.GetFont("ARIAL", 12, iTextSharp.text.Font.BOLD));
            tituloTipoExamen.SetUnderline(0.1f, -2f);

            Paragraph par = new Paragraph(tituloTipoExamen);
            par.Alignment = Element.ALIGN_CENTER;
            doc.Add(par);

            //doc.Add(tituloTipoExamen);
            doc.Add(new Paragraph(" ", times));

            Chunk datosDeudor = new Chunk("Datos Cliente", FontFactory.GetFont("ARIAL", 9, iTextSharp.text.Font.BOLD));
            datosDeudor.SetUnderline(0.1f, -2f);
            doc.Add(datosDeudor);

            PdfPTable tableDatosDeudor = new PdfPTable(6);

            float[] widthsDatosDeudor = new float[] { 35f, 85f, 50f, 55f, 35f, 35f };
            tableDatosDeudor.SetWidths(widthsDatosDeudor);

            tableDatosDeudor.AddCell(new Paragraph("Rut Cliente :", fontCabecera));
            tableDatosDeudor.AddCell(new Paragraph(rutCliente, times));
            tableDatosDeudor.AddCell(new Paragraph("Nombre :", fontCabecera));
            tableDatosDeudor.AddCell(new Paragraph(nombreCliente, times));

            tableDatosDeudor.AddCell(new Paragraph("Teléfono :", fontCabecera));
            tableDatosDeudor.AddCell(new Paragraph(telefono, times));
            tableDatosDeudor.AddCell(new Paragraph("Email :", fontCabecera));
            tableDatosDeudor.AddCell(new Paragraph(email, times));

            tableDatosDeudor.AddCell(new Paragraph("Usuario Asignado :", fontCabecera));
            tableDatosDeudor.AddCell(new Paragraph(usuarioAsignado, times));
            tableDatosDeudor.AddCell(new Paragraph("Estado :", fontCabecera));
            tableDatosDeudor.AddCell(new Paragraph(estadoTicket, times));

            tableDatosDeudor.HorizontalAlignment = Element.ALIGN_LEFT;
            tableDatosDeudor.WidthPercentage = 100.0f;

            doc.Add(tableDatosDeudor);
            doc.Add(new Paragraph(" ", times));
            doc.Add(new Paragraph(" ", times));

            Chunk datosTipificacion = new Chunk("Tipificación", FontFactory.GetFont("ARIAL", 9, iTextSharp.text.Font.BOLD));
            datosTipificacion.SetUnderline(0.1f, -2f);
            doc.Add(datosTipificacion);


            PdfPTable tableDatosTipificacion = new PdfPTable(4);
            float[] widthsDatosTipificacion = new float[] { 25f, 45f, 55f, 55f };
            tableDatosTipificacion.SetWidths(widthsDatosTipificacion);

            tableDatosTipificacion.AddCell(new Paragraph("Tipo :", fontCabecera));
            tableDatosTipificacion.AddCell(new Paragraph("Gestión :", fontCabecera));
            tableDatosTipificacion.AddCell(new Paragraph("Categoría de la gestión :", fontCabecera));
            tableDatosTipificacion.AddCell(new Paragraph("Detalle de la categoría :", fontCabecera));
            tableDatosTipificacion.AddCell(new Paragraph(nivel1, times));
            tableDatosTipificacion.AddCell(new Paragraph(nivel2, times));
            tableDatosTipificacion.AddCell(new Paragraph(nivel3, times));
            tableDatosTipificacion.AddCell(new Paragraph(nivel4, times));

            tableDatosTipificacion.HorizontalAlignment = Element.ALIGN_LEFT;
            tableDatosTipificacion.WidthPercentage = 100.0f;

            foreach (PdfPCell celda in tableDatosTipificacion.Rows[0].GetCells())
            {
                celda.BackgroundColor = BaseColor.LIGHT_GRAY;
                celda.HorizontalAlignment = 0;
                celda.Padding = 2;
            }

            doc.Add(tableDatosTipificacion);
            doc.Add(new Paragraph(" ", times));
            doc.Add(new Paragraph(" ", times));

            Chunk datosObservacionTicket = new Chunk("Descripción general del Ticket", FontFactory.GetFont("ARIAL", 9, iTextSharp.text.Font.BOLD));
            datosObservacionTicket.SetUnderline(0.1f, -2f);
            doc.Add(datosObservacionTicket);

            doc.Add(new Paragraph(" ", times));

            PdfPTable tableObs = new PdfPTable(1);
            tableObs.AddCell(new Paragraph("Observacion", fontCabecera));
            tableObs.AddCell(new Paragraph(observacion, times));

            tableObs.HorizontalAlignment = Element.ALIGN_LEFT;
            tableObs.WidthPercentage = 100.0f;

            foreach (PdfPCell celda in tableObs.Rows[0].GetCells())
            {
                celda.BackgroundColor = BaseColor.LIGHT_GRAY;
                celda.HorizontalAlignment = 0;
                celda.Padding = 2;
            }

            doc.Add(tableObs);

            doc.Add(new Paragraph(" ", times));
            doc.Add(new Paragraph(" ", times));

            Chunk datosHistorico = new Chunk("Histórico del Ticket", FontFactory.GetFont("ARIAL", 9, iTextSharp.text.Font.BOLD));
            datosHistorico.SetUnderline(0.1f, -2f);
            doc.Add(datosHistorico);


            PdfPTable tableDetalle = new PdfPTable(7);
            tableDetalle.AddCell(new Paragraph("Correlativo", fontCabecera));
            tableDetalle.AddCell(new Paragraph("Fecha", fontCabecera));
            tableDetalle.AddCell(new Paragraph("Usuario Creación", fontCabecera));
            tableDetalle.AddCell(new Paragraph("Usuario Asig", fontCabecera));
            tableDetalle.AddCell(new Paragraph("Estado", fontCabecera));
            tableDetalle.AddCell(new Paragraph("Motivo Cierre", fontCabecera));
            tableDetalle.AddCell(new Paragraph("Obs", fontCabecera));

            foreach (DataRow item in dtHistorico.Rows)
            {
                tableDetalle.AddCell(new Paragraph(item["CORRELATIVO"].ToString(), times));
                tableDetalle.AddCell(new Paragraph(item["FECHA"].ToString(), times));
                tableDetalle.AddCell(new Paragraph(item["USUARIO"].ToString(), times));
                tableDetalle.AddCell(new Paragraph(item["USUARIO_ASIG"].ToString(), times));
                tableDetalle.AddCell(new Paragraph(item["ESTADO_ATENCION"].ToString(), times));
                tableDetalle.AddCell(new Paragraph(item["NOM_MOTIVO_CIERRE"].ToString(), times));
                tableDetalle.AddCell(new Paragraph(item["OBSERVACION"].ToString(), times));
            }

            float[] widthsDatosDetalle = new float[] { 25f, 50f, 50f, 50f, 50f, 20f, 60f };
            tableDetalle.SetWidths(widthsDatosDetalle);

            tableDetalle.HorizontalAlignment = Element.ALIGN_LEFT;
            tableDetalle.WidthPercentage = 100.0f;

            foreach (PdfPCell celda in tableDetalle.Rows[0].GetCells())
            {
                celda.BackgroundColor = BaseColor.LIGHT_GRAY;
                celda.HorizontalAlignment = 1;
                celda.Padding = 2;
            }

            doc.Add(tableDetalle);

            //foreach (DataRow item in dtHistorico.Rows)
            //{
            //    correlativo = item["CORRELATIVO"].ToString();
            //    fecha = item["FECHA"].ToString();
            //    usuarioCreacion = item["USUARIO"].ToString();
            //    usuarioAsig = item["USUARIO_ASIG"].ToString();
            //    estado = item["ESTADO_ATENCION"].ToString();
            //    motivoCierre = item["NOM_MOTIVO_CIERRE"].ToString();
            //    obs = item["OBSERVACION"].ToString();
            //    obsCli = item["OBSERVACION_CLIENTE"].ToString();
            //}
            doc.Add(new Paragraph(" ", times));
            doc.Close();

            string ruta = "pdfTicket/" + nombreArchivoPdf; ;
            return ruta;

        }
        protected void ddlCentral_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddlCentral_DataBound(object sender, EventArgs e)
        {
            ddlCentral.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione", "0"));

        }

        protected void ddlCanal_DataBound(object sender, EventArgs e)
        {
            ddlCanal.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Todos", "0"));
        }

        protected void DdlServicio_DataBound(object sender, EventArgs e)
        {
            DdlServicio.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Todos", "0"));
        }

        void Servicio()
        {
            DdlServicio.DataSource = dal.GetBuscarServicio(0);
            DdlServicio.DataTextField = "SERVICIO";
            DdlServicio.DataValueField = "ID_SERVICIO";
            DdlServicio.DataBind();
        }

        protected void grvTickets_PreRender(object sender, EventArgs e)
        {
            if (grvTickets.Rows.Count > 0)
            {
                grvTickets.UseAccessibleHeader = true;
                grvTickets.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void ibtnExcel_Click(object sender, EventArgs e)
        {
            try
            {
                string idTicket = txtBuscar.Text.Trim();

                Comun comu = new Comun();
                if (!string.IsNullOrEmpty(idTicket))
                {
                    if (!comu.IsNumeric(idTicket))
                    {
                        lblInfo.Text = "El número de ticket debe ser numérico";
                        divAlerta.Attributes["class"] = "alert alert-warning";
                        divAlerta.Visible = true;
                        return;
                    }
                }
                else
                {
                    idTicket = "0";
                }

                string derivado;
                if (chkDerivado.Checked == true)
                {
                    derivado = "1";
                }
                else
                {
                    derivado = null;
                }

                DataTable dt = new DataTable();

                dt = dal.getBuscarTicketBuscadorParametrosExpGeneral(ddlUsuarioAsig.SelectedValue, txtFechaDesde.Text.Trim(), txtFechaHasta.Text.Trim(),
                    ddlEstado.SelectedValue, ddlInsistencia.SelectedValue, ddlEscalamiento.SelectedValue, null,
                    Convert.ToInt32(ddlArea.SelectedValue), ddlTipo.SelectedValue, derivado, Convert.ToInt32(DdlServicio.SelectedValue), 
                    Convert.ToInt32(idTicket), txtBuscarPorRut.Text.Trim(), ddlSLA.SelectedValue, txtNroAtencion.Text.Trim()).Tables[0];

                Response.ContentType = "Application/x-msexcel";
                Response.AddHeader("content-disposition", "attachment;filename=exporte_ticket.csv");
                Response.ContentEncoding = Encoding.Unicode;
                Response.Write(Utilidad.ExportToCSVFile(dt));
                Response.End();
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-warning";
                divAlerta.Visible = true;
            }
        }
    }
}