﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AegisImplicitMail;
using System.ComponentModel;
using DAL;
using System.Configuration;

namespace EsvalCrm
{
    public partial class SeguimientoTicket : System.Web.UI.Page
    {
        Datos dal = new Datos();
        Comun comunes = new Comun();
        string sistema = ConfigurationManager.AppSettings["Sistema"];

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(this.btnGrabarGestion);
                scriptManager.RegisterPostBackControl(this.lbtnGrabarClienteExt);

                divAlerta.Visible = false;
                lblInfo.Text = "";

                if (!this.Page.IsPostBack)
                {
                    buscarEstado();
                    Comuna();
                    Socio();
                    area();

                    string perfil = Session["variableIdPerfil"].ToString();

                    if (perfil == "1")
                    {
                        txtObservacion.Enabled = true;
                        //ddlArea.Enabled = true;
                    }
                    else
                    {
                        //ddlArea.Enabled = false;
                        txtObservacion.Enabled = false;
                    }

                    string _numeroTicket = Convert.ToString(Request.QueryString["t"]);
                    if (_numeroTicket == null)
                    {
                        divPanelHistoricoCaso.Visible = false;
                    }
                    else
                    {
                        divPanelHistoricoCaso.Visible = true;
                        buscarCaso(_numeroTicket);
                        hfNumeroTicket.Value = _numeroTicket;
                    }

                    string usuario = Session["variableUsuario"].ToString();
                    string usuarioAsignado = lblUsuarioAsig.Text;

                    if (usuario == usuarioAsignado)
                    {
                        divIngresoGestion.Visible = true;
                    }
                    else
                    {
                        //if (perfil=="1" || perfil=="5")
                        //{
                        //    divIngresoGestion.Visible = true;
                        //}
                        //else
                        //{
                        //    divIngresoGestion.Visible = false;
                        //}
                        //JRL 2018-05-03
                        if (perfil == "1" || perfil == "5" || (lblEstadoTicket.Text != "CERRADO"))
                        {
                            divIngresoGestion.Visible = true;
                        }
                        else
                        {
                            divIngresoGestion.Visible = false;
                        }

                    }
                    //esto es para inicializar con esta regla, ya que siempre el estado es igual al ultimo ingreso
                    //entonces si llega a tener un cambio de estado hay que verlo en el evento del drpdownlist donde cambia estado
                    //entonces si cambia de estado  el atributo del boton btnGrabarGestion cambia a nulo

                    //si viene abierto el ticket, deja por defecto en proceso y eliminar el selector abierto.
                    System.Web.UI.WebControls.ListItem removeItem = ddlEstado.Items.FindByValue("1");
                    ddlEstado.Items.Remove(removeItem);
                    if (lblIdEstadoTicket.Text == "3")
                    {
                        ddlEstado.SelectedValue = "3";
                    }
                    else
                    {
                        ddlEstado.SelectedValue = "4";
                    }

                    btnGrabarGestion.OnClientClick = "return confirm(' El ticket ya se encuentra en estado " + lblEstadoTicket.Text + ", ¿Está seguro de ingresar el ticket nuevamente con estado " + ddlEstado.SelectedItem.ToString() + "?');";
                }
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Visible = true;
            }
        }

        void area()
        {
            ddlArea.DataSource = dal.getBuscarArea();
            ddlArea.DataValueField = "ID_AREA";
            ddlArea.DataTextField = "AREA";
            ddlArea.DataBind();
        }

        protected void ddlEstado_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlEstado.SelectedValue == "3")
                {
                    DataTable dt = dal.getBuscarTipificacionPorId(hfIdTipificacion.Value).Tables[0];
                    string idTipoMotivoCierre = string.Empty;

                    foreach (DataRow item in dt.Rows)
                    {
                        idTipoMotivoCierre = item["ID_TIPO_MOTIVO_CIERRE"].ToString();
                        break;
                    }

                    if (idTipoMotivoCierre == string.Empty)
                    {
                        ddlMotivoCierre.ClearSelection();
                        divMotivoCierre.Visible = false;
                    }
                    else
                    {
                        motivoCierre(idTipoMotivoCierre);
                        divMotivoCierre.Visible = true;
                    }
                }
                else
                {
                    ddlMotivoCierre.ClearSelection();
                    divMotivoCierre.Visible = false;

                }

                if (lblEstadoTicket.Text == ddlEstado.SelectedItem.ToString())
                {
                    btnGrabarGestion.OnClientClick = "return confirm(' El ticket ya se encuentra en estado " + lblEstadoTicket.Text + ", ¿Está seguro de ingresar el ticket nuevamente con estado " + ddlEstado.SelectedItem.ToString() + "?');";
                }
                else
                {
                    btnGrabarGestion.OnClientClick = null;
                }
            }
            catch (Exception ex)
            {
                lblInfo.Text = "Error: " + ex.Message;
                divAlerta.Visible = true;
            }
        }

        void motivoCierre(string idTipoMotivoCierre)
        {
            ddlMotivoCierre.DataSource = dal.getBuscarMotivoCierrePorIdTipo(idTipoMotivoCierre);
            ddlMotivoCierre.DataTextField = "NOM_MOTIVO_CIERRE";
            ddlMotivoCierre.DataValueField = "ID_MOTIVO_CIERRE";
            ddlMotivoCierre.DataBind();
        }

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                usuarioDerivar();
            }
            catch (Exception ex)
            {
                lblInfo.Text = "Error: " + ex.Message;
                divAlerta.Visible = true;
            }
        }

        protected void ddlArea_DataBound(object sender, EventArgs e)
        {
            ddlArea.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        }


        void buscarCaso(string numeroTicket)
        {
            DataTable dt = new DataTable();
            DataTable dt2 = new DataTable();

            dt = dal.getBuscarTicket(numeroTicket).Tables[0];
            string rutCliente = "";
            string idTicket = "";
            foreach (DataRow item in dt.Rows)
            {
                lblIdTicket.Text = "Ticket: " + item["ID_ATENCION"].ToString();
                lblIdCliente.Text = item["ID_CLIENTE"].ToString();
                idTicket = item["ID_ATENCION"].ToString();
                rutCliente = item["RUT_CLIENTE"].ToString();

                lblRutCliente.Text = rutCliente;
                lblRutNoTitular.Text = item["RUT_NO_TITULAR"].ToString();
                if (!string.IsNullOrEmpty(lblRutNoTitular.Text))
                {
                    lblNombreNoTitular.Text = item["NOMBRE_CLIENTE"].ToString();
                    dt2 = dal.getBuscarClientePorRut(rutCliente.Trim()).Tables[0];
                    foreach (DataRow item2 in dt2.Rows)
                    {
                        lblNombreCliente.Text = item2["NOMBRE"].ToString() + ' ' + item2["PATERNO"].ToString() + ' ' + item2["MATERNO"].ToString();
                        lblCalle.Text = item2["CALLE"].ToString();
                        lblNumero.Text = item2["NUMERO"].ToString();
                        lblResto.Text = item2["RESTO"].ToString();
                        lblComuna.Text = item2["NOM_COMUNA"].ToString();
                    }
                }
                else
                {
                    dt2 = dal.getBuscarClientePorRut(rutCliente.Trim()).Tables[0];
                    foreach (DataRow item2 in dt2.Rows)
                    {
                        lblNombreCliente.Text = item2["NOMBRE"].ToString() + ' ' + item2["PATERNO"].ToString() + ' ' + item2["MATERNO"].ToString();
                        lblCalle.Text = item2["CALLE"].ToString();
                        lblNumero.Text = item2["NUMERO"].ToString();
                        lblResto.Text = item2["RESTO"].ToString();
                        lblComuna.Text = item2["NOM_COMUNA"].ToString();
                    }
                    //lblNombreCliente.Text = item["NOMBRE_CLIENTE"].ToString();
                }

                txtObservacion.Text = item["OBSERVACION"].ToString();
                txtDescCliente.Text = item["OBSERVACION_CLIENTE"].ToString();

                lblUsuarioAsig.Text = item["USUARIO_ASIGNADO"].ToString();
                
                //2021-08-11 Agregado
                lblIdUsuarioAsig.Text = item["ID_USUARIO_ASIGNADO"].ToString().Trim();
                lblIdAreaTicket.Text = item["AreaTicket"].ToString().Trim();

                //2021-08-11 Agregado
                if (!string.IsNullOrEmpty(lblIdUsuarioAsig.Text) && lblIdUsuarioAsig.Text != "0")
                {
                    DivResolutor.Visible = true;
                }
                else
                {
                    DivResolutor.Visible = false;
                }

                lblEstadoTicket.Text = item["ESTADO_ATENCION"].ToString();
                lblIdEstadoTicket.Text = item["ID_ESTADO_ATENCION"].ToString();
                hfIdTipificacion.Value = item["ID_TIPIFICACION"].ToString();

                if (lblIdEstadoTicket.Text == "3")
                {
                    ddlEstado.SelectedValue = "3";
                }
                else
                {
                    ddlEstado.SelectedValue = "4";
                }

                //nuevos 20092017
                lblIdVendedor.Text = item["CENTRAL"].ToString();
                lblLocal.Text = item["CANAL"].ToString();
                //***********
                string perfil = Session["variableIdPerfil"].ToString();

                if (perfil == "1")
                {
                    txtObservacion.Enabled = true;
                }
                else
                {
                    txtObservacion.Enabled = false;
                }

                ddlArea.SelectedValue = item["ID_AREA"].ToString();
                lblNivel1.Text = item["NIVEL_1"].ToString();
                lblNivel2.Text = item["NIVEL_2"].ToString();
                lblNivel3.Text = item["NIVEL_3"].ToString();
                lblNivel4.Text = item["NIVEL_4"].ToString();

                if (item["RUTA_ARCHIVO"].ToString() == string.Empty)
                {
                    ibtnArchivo.Visible = false;
                }
                else
                {
                    ibtnArchivo.Visible = true;
                }

                //JRL 2018-05-17
                if (item["RUTA_ARCHIVO2"].ToString() == string.Empty)
                {
                    ibtnArchivo22.Visible = false;
                }
                else
                {
                    ibtnArchivo22.Visible = true;
                }

                lblRutaArchivo.Text = item["RUTA_ARCHIVO"].ToString();
                //JRL 2018-05-17
                lblRutaArchivo2.Text = item["RUTA_ARCHIVO2"].ToString();

                hfIdUsuarioCreacion.Value = item["ID_USUARIO_CREACION"].ToString();

                foreach (DataRow row in dal.getBuscarUsuarioPorId(hfIdUsuarioCreacion.Value).Tables[0].Rows)
                {
                    lblUsuarioCreacion.Text = row["USUARIO"].ToString() + " / " + row["NOMBRE"].ToString();
                }
            }

            DataTable dtTipif = new DataTable();
            dtTipif = dal.getBuscarTipificacionPorId(hfIdTipificacion.Value).Tables[0];

            string idServicio = string.Empty;
            foreach (DataRow item in dtTipif.Rows)
            {
                  idServicio = item["ID_SERVICIO"].ToString();
            }

            DataTable dtServicio = new DataTable();
            dtServicio = dal.GetBuscarServicio(Convert.ToInt32(idServicio), 1).Tables[0];

            string nomServicio = string.Empty;
            foreach (DataRow item in dtServicio.Rows)
            {
                nomServicio = item["SERVICIO"].ToString();
            }

            lblServicio.Text = nomServicio;



            DataTable dtGestion = new DataTable();
            dtGestion = dal.getBuscarGestionPorIdTicket(idTicket).Tables[0];

            foreach (DataRow item in dtGestion.Rows)
            {
                if (item["TIPO_CLIENTE"].ToString() == "I")
                {
                    _iwc_hf_tipocliente.Value = "I";


                }
                else if (item["TIPO_CLIENTE"].ToString() == "E")
                {
                    _iwc_hf_tipocliente.Value = "E";
                }
                else
                {
                    //divCliente.Visible = true;
                }
            }

            //DataTable dtCliente = new DataTable();
            //dtCliente = dal.getBuscarClientePrincipal(rutCliente, rutCliente).Tables[0];
            //foreach (DataRow item in dtCliente.Rows)
            //{
            //    lblRutCliente.Text = rutCliente;
            //    lblNombreCliente.Text = item["NOMBRE"].ToString();
            //    lblCelularCliente.Text = item["CELULAR"].ToString();
            //    lblEmailCliente.Text = item["EMAIL"].ToString();
            //    lblTelefonoCliente.Text = item["TELEFONO"].ToString();
            //}

            //ORACLE
            //string nombres, paterno, materno, segmentoCliente, email, fechaCompra, sernac, msj, celular, fijo, local;
            string nombres = string.Empty, paterno = string.Empty, materno = string.Empty, segmentoCliente = string.Empty,
                    email = string.Empty, fechaCompra = string.Empty, sernac = string.Empty, msj = string.Empty,
                    celular = string.Empty, fijo = string.Empty, local = string.Empty;
            int? codError;
            string dv = string.Empty;

            Comun com = new Comun();


            DataTable dtCliente = new DataTable();
            //   dtCliente = dal.getBuscarClientePorEmail(rutCliente).Tables[0];
            dtCliente = dal.getBuscarClientePorRut(rutCliente).Tables[0];

            foreach (DataRow item in dtCliente.Rows)
            {
                nombres = item["NOMBRE"].ToString();
                paterno = item["PATERNO"].ToString();
                materno = item["MATERNO"].ToString();
                //segmentoCliente= item[""].ToString();
                email = item["EMAIL"].ToString();
                //fechaCompra= item[""].ToString();
                //sernac= item[""].ToString();
                celular = item["CELULAR"].ToString();
                fijo = item["TELEFONO"].ToString();
                //local= item[""].ToString();
                //rutConDV = item["EMAIL"].ToString();
            }

            //if (com.IsValidEmail(rutCliente) == false)
            //{
            //    if (!string.IsNullOrEmpty(rutCliente.Trim()))
            //    {
            //        rutCliente = com.formatearRutSinPuntos(rutCliente);
            //    }
            //    String[] arRut = rutCliente.Split('-');
            //    for (int i = 0; i < arRut.Length; i++)
            //    {
            //        rutCliente = arRut[0];
            //        if (arRut.Length > 0)
            //        {
            //            dv = arRut[1];
            //        }
            //    }

            //    // ALG datos.getBuscarCliente(1, Convert.ToInt32(rutCliente), dv, "CRM", out nombres, out paterno, out materno, out segmentoCliente, out email, out celular, out fijo, out fechaCompra, out local, out sernac, out codError, out msj);

            //}
            //else
            //{
            //    DataTable dtCliente = new DataTable();
            //    //   dtCliente = dal.getBuscarClientePorEmail(rutCliente).Tables[0];
            //         dtCliente = dal.getBuscarClientePorRut(rutCliente).Tables[0];

            //    foreach (DataRow item in dtCliente.Rows)
            //    {
            //        nombres = item["NOMBRE"].ToString();
            //        paterno = item["PATERNO"].ToString();
            //        materno = item["MATERNO"].ToString();
            //        //segmentoCliente= item[""].ToString();
            //        email = item["EMAIL"].ToString();
            //        //fechaCompra= item[""].ToString();
            //        //sernac= item[""].ToString();
            //        celular = item["CELULAR"].ToString();
            //        fijo = item["TELEFONO"].ToString();
            //        //local= item[""].ToString();
            //        //rutConDV = item["EMAIL"].ToString();
            //    }
            //}

            //if (!string.IsNullOrEmpty(rutCliente.Trim()))
            //{

            //    rutCliente = com.formatearRutSinPuntos(rutCliente);
            //}

            //String[] arRut = rutCliente.Split('-');
            //for (int i = 0; i < arRut.Length; i++)
            //{
            //    rutCliente = arRut[0];
            //    if (arRut.Length > 0)
            //    {
            //        dv = arRut[1];
            //    }
            //}

            if (nombres == "null")
            {
                nombres = string.Empty;
            }
            if (paterno == "null")
            {
                paterno = string.Empty;
            }
            if (materno == "null")
            {
                materno = string.Empty;
            }
            if (segmentoCliente == "null")
            {
                segmentoCliente = string.Empty;
            }
            if (email == "null")
            {
                email = string.Empty;
            }
            if (fechaCompra == "null")
            {
                fechaCompra = string.Empty;
            }
            if (sernac == "null")
            {
                sernac = string.Empty;
            }
            if (celular == "null")
            {
                celular = string.Empty;
            }
            if (fijo == "null")
            {
                fijo = string.Empty;
            }
            if (local == "null")
            {
                local = string.Empty;
            }

            //lblRutCliente.Text = rutCliente;

            if (lblNombreCliente.Text == "")
            {
                lblNombreCliente.Text = nombres;
            }

            lblCelularCliente.Text = celular;
            lblEmailCliente.Text = email;
            lblTelefonoCliente.Text = fijo;

            DataTable dtHistorico = new DataTable();
            dtHistorico = dal.getBuscarTicketHistorico(numeroTicket).Tables[0];
            string idArea = string.Empty;
            string idUsuarioAsignado = string.Empty;
            string usuarioAsignado = string.Empty;
            foreach (DataRow item in dtHistorico.Rows)
            {
                if (item["ID_ESTADO_ATENCION"].ToString().Equals("1"))
                {
                    ddlEstado.SelectedValue = "4";
                }
                else
                {
                    ddlEstado.SelectedValue = item["ID_ESTADO_ATENCION"].ToString();
                }

                idArea = item["ID_AREA1"].ToString();
                idUsuarioAsignado = item["ID_USUARIO_ASIG"].ToString();
                usuarioAsignado = item["ID_USUARIO_ASIG"].ToString();
            }
            //ddlArea.SelectedValue = idArea;

            if (ddlEstado.SelectedValue == "1")
            {
                string idUsuario = Session["variableIdUsuario"].ToString();
                string nombUsuAtenCont = string.Empty;
                string idUsuarioAteCont = string.Empty;

                DataTable dtAtencionControl = new DataTable();
                dtAtencionControl = dal.getBuscarAtencionControl(numeroTicket.Trim()).Tables[0];

                foreach (DataRow item in dtAtencionControl.Rows)
                {
                    nombUsuAtenCont = item["NOM_USU_AT_CONT"].ToString();
                    idUsuarioAteCont = item["ID_USUARIO"].ToString();
                }

                if (dtAtencionControl.Rows.Count != 0)
                {
                    if (idUsuario.Equals(idUsuarioAteCont))
                    {
                        btnGrabarGestion.Visible = true;
                    }
                    else
                    {
                        btnGrabarGestion.Visible = false;
                        lblMensajeAtencionControl.Text += nombUsuAtenCont.ToUpper();
                        lblMensajeAtencionControl.Visible = true;
                    }
                }
                else
                {
                    btnGrabarGestion.Visible = true;
                    lblMensajeAtencionControl.Text += nombUsuAtenCont.ToUpper();
                    lblMensajeAtencionControl.Visible = false;
                    dal.setIngresarAtencionControl(numeroTicket, idUsuario, "1");
                }
            }

            usuarioDerivar();
            ddlDerivar.SelectedValue = idUsuarioAsignado;

            grvGestiones.DataSource = dtHistorico;
            grvGestiones.DataBind();

            //mostrar estado detenido dependiendo la tipificacion
            DataTable dtTip = new DataTable();
            dtTip = dal.getBuscarTipificacionPorId(hfIdTipificacion.Value).Tables[0];
            foreach (DataRow item in dtTip.Rows)
            {
                if (item["DETENIDO"].ToString() != "1")
                {
                    System.Web.UI.WebControls.ListItem removeItem = ddlEstado.Items.FindByValue("2");
                    ddlEstado.Items.Remove(removeItem);
                }
                break;
            }
        }

        protected void paginacion_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label _lblArchivo1 = (Label)e.Row.FindControl("lblArchivo1");
                Label _lblArchivo2 = (Label)e.Row.FindControl("lblArchivo2");

                ImageButton _ibtnArchivo1 = (ImageButton)e.Row.FindControl("ibtnArchivo1");
                ImageButton _ibtnArchivo2 = (ImageButton)e.Row.FindControl("ibtnArchivo2");

                if (string.IsNullOrEmpty(_lblArchivo1.Text) == true)
                {
                    _ibtnArchivo1.Visible = false;
                }
                else
                {
                    _ibtnArchivo1.Visible = true;
                }

                if (string.IsNullOrEmpty(_lblArchivo2.Text) == true)
                {
                    _ibtnArchivo2.Visible = false;
                }
                else
                {
                    _ibtnArchivo2.Visible = true;
                }
            }
        }

        protected void ibtnArchivo_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), UniqueID, "window.open('" + lblRutaArchivo.Text + "','_blank');", true);
            }
            catch (Exception ex)
            {
                lblInfo.Text = "Error: " + ex.Message;
                divAlerta.Visible = true;
            }
        }

        //JRL 2018-05-17
        protected void ibtnArchivo22_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), UniqueID, "window.open('" + lblRutaArchivo2.Text + "','_blank');", true);
            }
            catch (Exception ex)
            {
                lblInfo.Text = "Error: " + ex.Message;
                divAlerta.Visible = true;
            }
        }

        protected void ibtnArchivo1_Click(object sender, EventArgs e)
        {
            try
            {
                ImageButton img = (ImageButton)sender;
                GridViewRow row = (GridViewRow)img.NamingContainer;
                Label _lblArchivo1 = (Label)grvGestiones.Rows[row.RowIndex].FindControl("lblArchivo1");

                ScriptManager.RegisterStartupScript(this, this.GetType(), UniqueID, "window.open('" + _lblArchivo1.Text + "','_blank');", true);
            }
            catch (Exception ex)
            {
                lblInfo.Text = "Error: " + ex.Message;
                divAlerta.Visible = true;
            }
        }

        protected void ibtnArchivo2_Click(object sender, EventArgs e)
        {
            try
            {
                ImageButton img = (ImageButton)sender;
                GridViewRow row = (GridViewRow)img.NamingContainer;
                Label _lblArchivo2 = (Label)grvGestiones.Rows[row.RowIndex].FindControl("lblArchivo2");

                ScriptManager.RegisterStartupScript(this, this.GetType(), UniqueID, "window.open('" + _lblArchivo2.Text + "','_blank');", true);
            }
            catch (Exception ex)
            {
                lblInfo.Text = "Error: " + ex.Message;
                divAlerta.Visible = true;
            }
        }

        //llenar los Ddl
        void buscarEstado()
        {
            ddlEstado.DataSource = dal.getBuscarEstadoAtencion();
            ddlEstado.DataTextField = "ESTADO_ATENCION";
            ddlEstado.DataValueField = "ID_ESTADO_ATENCION";
            ddlEstado.DataBind();
        }
        void usuarioDerivar()
        {
            DataTable dt = new DataTable();
            dt = dal.getBuscarUsuarioPorIdArea(ddlArea.SelectedValue).Tables[0];

            ddlDerivar.DataSource = dt;
            ddlDerivar.DataValueField = "ID_USUARIO";
            ddlDerivar.DataTextField = "NOMBRE";
            ddlDerivar.DataBind();

            string idUsuSistema = string.Empty;
            string idUsuarioAsignado = string.Empty;
            string usuarioAsignado = string.Empty;
            string nombUsuarioAsignado = string.Empty;
            string encontrado = string.Empty;

            foreach (DataRow item in dt.Rows)
            {
                if (item["ID_USUARIO"].ToString().Equals("0"))
                {
                    idUsuSistema = item["ID_USUARIO"].ToString();
                    break;
                }
            }
            //Agrega usuario SISTEMA si no existe en el ddlDerivar
            if (string.IsNullOrEmpty(idUsuSistema))
            {
                ddlDerivar.Items.Insert(0, new System.Web.UI.WebControls.ListItem("SISTEMA", "0"));
            }

            DataTable dtHistorico = new DataTable();
            dtHistorico = dal.getBuscarTicketHistorico(lblIdTicket.Text.Replace("Ticket: ", "")).Tables[0];

            if (ddlEstado.SelectedValue == "3")
            {
                foreach (DataRow item in dtHistorico.Rows)
                {
                    idUsuarioAsignado = item["ID_USUARIO_ASIG"].ToString();
                    usuarioAsignado = item["USUARIO_ASIG"].ToString();
                    nombUsuarioAsignado = item["NOMBRE"].ToString();
                }
            }

            if (!string.IsNullOrEmpty(idUsuarioAsignado))
            {
                foreach (DataRow item in dt.Rows)
                {
                    if (item["ID_USUARIO"].ToString().Equals(idUsuarioAsignado))
                    {
                        encontrado = item["ID_USUARIO"].ToString();
                        break;
                    }
                }
                //Agrega Usuario Asignado si no existe en el ddlDerivar
                if (string.IsNullOrEmpty(encontrado))
                {
                    int contar = Convert.ToInt32(ddlDerivar.Items.Count.ToString());
                    ddlDerivar.Items.Insert(contar, new System.Web.UI.WebControls.ListItem(nombUsuarioAsignado, idUsuarioAsignado));
                }
            }
        }

        //Para los DDL Opciones por defecto.
        protected void ddlEstado_DataBound(object sender, EventArgs e)
        {
            ddlEstado.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        }

        protected void ddlMotivoCierre_DataBound(object sender, EventArgs e)
        {
            ddlMotivoCierre.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        }

        protected void ddlDerivar_DataBound(object sender, EventArgs e)
        {
            //ddlDerivar.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        }

        protected void btnGrabarGestion_Click(object sender, EventArgs e)
        {
            try
            {
                string numeroTicket = hfNumeroTicket.Value;
                // string usuario = buscarIdUsuario();

                string usuario = Session["variableIdUsuario"].ToString();
                int idPerfil = Convert.ToInt32(Session["variableIdPerfil"]);

                //DataTable dtBuscaCliente = new DataTable();
                //dtBuscaCliente = dal.getBuscarClientePorEmail(lblEmailCliente.Text).Tables[0];

                //string nombre = string.Empty;
                //string paterno = string.Empty;
                //string materno = string.Empty;
                //foreach (DataRow item in dtBuscaCliente.Rows)
                //{
                //    nombre = item["NOMBRE"].ToString();
                //    paterno = item["PATERNO"].ToString();
                //    materno = item["MATERNO"].ToString();
                //}

                if (ddlEstado.SelectedValue == "0" /* || ddlDerivar.SelectedValue == "0" || txtObservacionGestion.Text == string.Empty*/)
                {
                    lblInfo.Text = "Los campos Estado, derivar deben completarse para crear la gestión";
                    divAlerta.Attributes["class"] = "alert alert-warning";
                    divAlerta.Visible = true;
                    return;
                }

                if (ddlEstado.SelectedValue == "3")
                {
                    if (txtObservacionCliente.Text == string.Empty)
                    {
                        lblInfo.Text = "La observación cliente es obligatoria para cerrar el caso.";
                        divAlerta.Attributes["class"] = "alert alert-warning";
                        divAlerta.Visible = true;
                        return;
                    }
                }

                if (ddlEstado.SelectedValue == lblIdEstadoTicket.Text)
                {
                    //El ticket ya se encuentra en estado PENDIENTE, Está seguro de ingresar el ticket nuevamente con estado PENDIENTE
                    lblInfo.Text = "El ticket ya se encuentra en estado " + lblEstadoTicket.Text + ", Está seguro de ingresar el ticket nuevamente con estado " + lblEstadoTicket.Text;
                    divAlerta.Attributes["class"] = "alert alert-warning";
                    divAlerta.Visible = true;
                }

                string idMotivoCierre = ddlMotivoCierre.SelectedValue;
                if (idMotivoCierre == "0")
                {
                    idMotivoCierre = null;
                }

                string idUsuarioAsigCmb = ddlDerivar.SelectedValue;
                string idUsuarioCreacion = hfIdUsuarioCreacion.Value;
                //JRL
                string IdlblUsuarioAsig = "";
                IdlblUsuarioAsig = dal.obtenerIdUsuarioAsig(lblUsuarioAsig.Text);

                if (idPerfil != 1)
                {
                    if (ddlEstado.SelectedValue == "3")
                    {
                        if ((idUsuarioAsigCmb != idUsuarioCreacion) && (idUsuarioAsigCmb != Session["variableIdUsuario"].ToString()))
                        {
                            lblInfo.Text = "No posee permisos para cerrar los Tickets pertenecientes a otros usuarios";
                            divAlerta.Attributes["class"] = "alert alert-warning";
                            divAlerta.Visible = true;
                            return;
                        }
                    }
                    else
                    {
                        //if ((idUsuarioAsigCmb != idUsuarioCreacion) && (idUsuarioAsigCmb != IdlblUsuarioAsig))

                        //JRL 2018-05-30
                        //if ((idUsuarioAsigCmb != idUsuarioCreacion) && (idUsuarioAsigCmb != Session["variableIdUsuario"].ToString()))
                        //if ((IdlblUsuarioAsig != idUsuarioCreacion) && (idUsuarioAsigCmb != Session["variableIdUsuario"].ToString()))                       
                        //{
                        //lblInfo.Text = "No posee permisos para modificar los Tickets pertenecientes a otros usuarios";
                        //divAlerta.Attributes["class"] = "alert alert-warning";
                        //divAlerta.Visible = true;
                        //return;
                        //}
                    }
                }

                //2021-08-11 Agregado
                if (string.IsNullOrEmpty(lblIdUsuarioAsig.Text) || lblIdUsuarioAsig.Text == "0")
                {
                    DataTable dtUsuarioAreaSesion = dal.getBuscarUsuarioPorId(Session["variableIdUsuario"].ToString()).Tables[0];

                    string idAreaUsuSesion = string.Empty;
                    foreach (DataRow item in dtUsuarioAreaSesion.Rows)
                    {
                        idAreaUsuSesion = item["ID_AREA"].ToString();
                    }

                    if (lblIdAreaTicket.Text != idAreaUsuSesion)
                    {
                        lblInfo.Text = "Usuario en Sesión no pertenece al Área del Ticket.";
                        divAlerta.Attributes["class"] = "alert alert-warning";
                        divAlerta.Visible = true;
                        return;
                    }
                }

                DataTable dtBuscaClientePorRut = new DataTable();
                dtBuscaClientePorRut = dal.getBuscarClientePorRut(lblRutCliente.Text.Trim().Replace(".", "")).Tables[0];

                string nombre = string.Empty;
                string paterno = string.Empty;
                string materno = string.Empty;

                foreach (DataRow item in dtBuscaClientePorRut.Rows)
                {
                    nombre = item["NOMBRE"].ToString();
                    paterno = item["PATERNO"].ToString();
                    materno = item["MATERNO"].ToString();
                }

                string correlativo = "";

                if (ddlEstado.SelectedValue == "3")
                {
                    idUsuarioAsigCmb = usuario;
                }

                if (string.IsNullOrEmpty(lblIdUsuarioAsig.Text) || lblIdUsuarioAsig.Text == "0")
                {
                    idUsuarioAsigCmb = usuario;
                }

                correlativo = dal.setIngresarTicketHistorico(numeroTicket, usuario, idUsuarioAsigCmb, ddlEstado.SelectedValue, txtObservacionGestion.Text, null, txtObservacionCliente.Text, Convert.ToInt32(ddlArea.SelectedValue));
                dal.setEditarTicket(numeroTicket, ddlEstado.SelectedValue, idUsuarioAsigCmb, null, idMotivoCierre);
                dal.setEditarGestionPorId(numeroTicket, txtObservacion.Text);

                if (ddlDerivar.SelectedValue != "0")
                {
                    string idEmpleado = ddlDerivar.SelectedValue;
                    DataTable dtEmpleado = new DataTable();
                    dtEmpleado = dal.getBuscarUsuarioPorId(idEmpleado).Tables[0];

                    string email = string.Empty;
                    string nombreAsignado = string.Empty;
                    foreach (DataRow item in dtEmpleado.Rows)
                    {
                        email = item["EMAIL"].ToString();
                        nombreAsignado = item["NOMBRE"].ToString();
                    }
                }

                //dos por seguimiento...
                if (fuArchivo1.HasFile)
                {
                    string carpeta = "archivosGestion/" + numeroTicket + "_" + fuArchivo1.FileName.Replace(" ", "_");
                    fuArchivo1.SaveAs(Server.MapPath(carpeta));
                    //JRL Cambio de Int16 a Int32
                    dal.setEditarRutaArchivoAtencionHistorico(Convert.ToInt32(numeroTicket), Convert.ToInt32(correlativo), carpeta, "");
                }

                if (fuArchivo2.HasFile)
                {
                    string carpeta = "archivosGestion/" + numeroTicket + "_" + fuArchivo2.FileName.Replace(" ", "_");
                    fuArchivo2.SaveAs(Server.MapPath(carpeta));
                    //JRL Cambio de Int16 a Int32
                    dal.setEditarRutaArchivoAtencionHistorico(Convert.ToInt32(numeroTicket), Convert.ToInt32(correlativo), "", carpeta);
                }








                string nombreDest = string.Empty;
                string emailDest = string.Empty;

                DataTable dtTipi = new DataTable();
                dtTipi = dal.getBuscarTipificacionPorId(hfIdTipificacion.Value).Tables[0];

                string idServicio = string.Empty;
                string nivel1 = string.Empty;
                string nivel2 = string.Empty;

                foreach (DataRow item in dtTipi.Rows)
                {
                    idServicio = item["ID_SERVICIO"].ToString();
                    nivel1 = item["NIVEL_1"].ToString().Trim();
                    nivel2 = item["NIVEL_2"].ToString().Trim();
                }

                DataTable dtServicio = new DataTable();
                dtServicio = dal.GetBuscarServicio(Convert.ToInt32(idServicio), 1).Tables[0];

                string nomServicio = string.Empty;
                foreach (DataRow item in dtServicio.Rows)
                {
                    nomServicio = item["SERVICIO"].ToString();
                }

                DataTable dtServicioNoti = new DataTable();
                dtServicioNoti = dal.GetBuscarServicioNotiticacion(Convert.ToInt32(idServicio), 0).Tables[0];




                if (ddlEstado.SelectedValue == "3")
                {
                    DataTable dtHistoricoUltimoAdj = new DataTable();
                    dtHistoricoUltimoAdj = dal.getBuscarTicketHistoricoUltimo(numeroTicket).Tables[0];
                    string Adj1 = string.Empty;
                    string Adj2 = string.Empty;
                    foreach (DataRow item in dtHistoricoUltimoAdj.Rows)
                    {
                        Adj1 = item["RUTA_ARCHIVO_1"].ToString();
                        Adj2 = item["RUTA_ARCHIVO_2"].ToString();
                    }

                    string body = "";
                    switch (_iwc_hf_tipocliente.Value)//Tipo Cliente es null, no se utiliza el switch
                    {
                        case "I":
                            body = "Estimada(o),   <br>";
                            body += "Te informamos que su caso N° " + numeroTicket + " ha sido resuelto con la siguiente solución:";
                            body += "<br><br><b>" + txtObservacionGestion.Text + "</b>";
                            body += "<br><br>";
                            body += "<b><u>Información de la Atención:</u></b>";
                            body += "<br>N° de ticket: " + numeroTicket;
                            body += "<br>Fecha: " + DateTime.Now.ToString("G");
                            body += "<br>Asunto: " + txtObservacion.Text;
                            body += "<br><b>Cualquier duda o consulta no dude en contactarnos al 800 800 008   </b>";
                            body += "<br><br>";
                            body += "<br>Muchas gracias!";
                            body += "<br><b>Equipo de Servicio al Cliente Esval.</b>";
                            //body += "<br><b>Gracias por contactar a Cardif.</b>";
                            //body += "<br><br>";
                            //body += "<table style='width:100%' border='1'><tr><td><img src='http://190.96.2.124/imagenes/logo_Cardif.png' alt='Firma Logo' /></td>";
                            //body += "<td>Cardif <br>Línea Única de Soporte Cardif<br>Huérfanos 670, PISO 4 Santiago<br>(56-2) 2410 5900<br>ldiaz@interweb.cl</td></tr></table>";
                            // enviarEmail(lblEmailClienteCi.Text.Trim(), body.Replace("\r\n", "<br>"), "Respuesta Atención N° "+numeroTicket+", SERVICIO CLIENTE Cardif");
                            break;
                        case "E":



                            break;
                    }

                    string bodyCliCierre = "Hola! " + nombre + " " + paterno + " " + materno + "<br>";

                    bodyCliCierre += "<br><p style='color: red;'>Su ticket ha sido cerrado con la siguiente observación:</p>";

                    bodyCliCierre += "<b>Observación Cliente:</b><br>" + txtObservacionCliente.Text + "<br>";
                    //bodyPrimera += "<br>Te informamos que tu ticket " + ticket + " ingresado el " + fechaIngreso;
                    //bodyPrimera += "<br>ha sido resuelto y cerrado por el ejecutivo a cargo.<br>";
                    //bodyPrimera += "<br>Si no estás conforme con la resolución del caso, por favor contáctanos";
                    //bodyPrimera += "<br>a través del teléfono 800 800 008.<br>";
                    //bodyPrimera += "<br>Saludos!<br>";

                    if (!string.IsNullOrEmpty(Adj1))
                    {
                        bodyCliCierre += "<br>Adjunto1: <a href='http://" + sistema + Adj1 + "'>" + Adj1.Replace("archivosGestion/", " ") + "</a>";
                    }

                    if (!string.IsNullOrEmpty(Adj2))
                    {
                        bodyCliCierre += "<br>Adjunto2: <a href='http://" + sistema + Adj2 + "'>" + Adj2.Replace("archivosGestion/", " ") + "</a>";
                    }

                    bodyCliCierre += "<br><br><b>Equipo de Servicio al Cliente Esval.</b>";

                    if (chkEnviarObsCliente.Checked)
                    {
                        string emaiCopiaNoti = string.Empty;

                        if (hfIdTipificacion.Value == "6" || hfIdTipificacion.Value == "7")
                        {
                            foreach (DataRow item in dtServicioNoti.Rows)
                            {
                                emailDest = item["EMAIL"].ToString().Trim();

                                if (!string.IsNullOrEmpty(emailDest))
                                {
                                    if (comunes.IsValidEmail(emailDest))
                                    {
                                        emaiCopiaNoti += emailDest + ";";
                                    }
                                }
                            }
                        }

                        if (!string.IsNullOrEmpty(emaiCopiaNoti))
                        {
                            emaiCopiaNoti = emaiCopiaNoti.TrimEnd(';');
                        }
                        
                        if (!string.IsNullOrEmpty(lblEmailCliente.Text))
                        {
                            comunes.EnviarEmailSSLImplicito(lblEmailCliente.Text.Trim(), bodyCliCierre.Replace("\r\n", "<br>"), "Servicio al Cliente Esval // Cierre de Ticket " + numeroTicket, null, emaiCopiaNoti);
                        }
                    }
                }
                else if (ddlEstado.SelectedValue == "1" || ddlEstado.SelectedValue == "4")
                {
                    //2021-08-11 Agregado
                    string idEmpleado = string.Empty;
                    if (string.IsNullOrEmpty(lblIdUsuarioAsig.Text) || lblIdUsuarioAsig.Text == "0")
                    {
                        idEmpleado = Session["variableIdUsuario"].ToString();
                    }
                    else
                    {
                        idEmpleado = ddlDerivar.SelectedValue;
                    }

                    //2021-08-11 Comentado
                    //string idEmpleado = ddlDerivar.SelectedValue;
                    DataTable dtEmpleado = new DataTable();
                    dtEmpleado = dal.getBuscarUsuarioPorId(idEmpleado).Tables[0];

                    string email = string.Empty;
                    string nombreAsignado = string.Empty;
                    foreach (DataRow item in dtEmpleado.Rows)
                    {
                        email = item["EMAIL"].ToString();
                        nombreAsignado = item["NOMBRE"].ToString();
                    }

                    DataTable dtHistoricoUltimo = new DataTable();
                    dtHistoricoUltimo = dal.getBuscarTicketHistoricoUltimo(numeroTicket).Tables[0];
                    string obsInterna = string.Empty;
                    string obsCliente = string.Empty;
                    string Adj1 = string.Empty;
                    string Adj2 = string.Empty;
                    foreach (DataRow item in dtHistoricoUltimo.Rows)
                    {
                        obsInterna = item["OBSERVACION"].ToString();
                        obsCliente = item["OBSERVACION_CLIENTE"].ToString();
                        Adj1 = item["RUTA_ARCHIVO_1"].ToString();
                        Adj2 = item["RUTA_ARCHIVO_2"].ToString();
                    }

                    string bodyResolutor = "Estimado(a) " + nombreAsignado + "<br><br>";//JRL 2019-03-26 Nombre persona encargada de resolver
                    bodyResolutor += "Le informamos que tiene un nuevo ticket en su bandeja de trabajo.<br><br>";
                    bodyResolutor += "<b>Nro. de Ticket: </b>" + numeroTicket + "<br>";
                    bodyResolutor += "<b>Usuario solicitante: </b>" + lblNombreCliente.Text + "<br>";
                    bodyResolutor += "<b>Descripción: </b>" + txtObservacionGestion.Text + "<br><br>";
                    bodyResolutor += "Para gestionar el caso <a href='http://" + sistema + "SeguimientoTicket.aspx?t=" + numeroTicket + "'>Pinche acá</a>";
                    bodyResolutor += "<br><br>Atte.<br><br>";
                    bodyResolutor += "<br><b>Equipo de Servicio al Cliente Esval.</b>";

                    //string bodyResolutor = "Estimado(a) Usuario:";
                    //bodyResolutor += "<br><br>";
                    //bodyResolutor += "Se ha generado el siguiente caso para su gestión:";
                    //bodyResolutor += "<br><br>";
                    //bodyResolutor += "<b>Obs. Inicial: </b>" + txtObservacion.Text;
                    //bodyResolutor += "<br><br>";
                    //bodyResolutor += "<b>Última Obs. Interna: </b>" + obsInterna;
                    //bodyResolutor += "<br><br>";
                    //bodyResolutor += "<b>Última Obs. Cliente: </b>" + obsCliente;
                    //bodyResolutor += "<br><br>";
                    //bodyResolutor += "Le agradecemos revisar a la brevedad CRM Esval";
                    //bodyResolutor += "<br> " + "http://172.20.1.171/CrmCardif/SeguimientoTicket.aspx?t=" + numeroTicket;
                    //bodyResolutor += "<br><br>";
                    //bodyResolutor += "<br>Muchas gracias!";
                    //bodyResolutor += "<br><b>Equipo de Servicio al Cliente Esval.</b>";

                    //bodyResolutor += "<img src='http://190.96.2.124/imagenes/logo_Cardif.png' alt='Firma Logo' />";
                    //bodyResolutor += "<br><br>";
                    //bodyResolutor += "<table style='width:100%' border='1'><tr><td><img src='http://190.96.2.124/imagenes/logo_Cardif.png' alt='Firma Logo' /></td>";
                    //bodyResolutor += "<td>Cardif <br>Línea Única de Soporte Cardif<br>Huérfanos 670, PISO 4 Santiago<br>(56-2) 2410 5900<br>ldiaz@interweb.cl</td></tr></table>";

                    if (idEmpleado != usuario)
                    {
                        if (email != string.Empty)
                        {
                            //Habilitar luego
                            //enviarEmail(email, bodyResolutor.Replace("\r\n", "<br>"), " Ticket N° "+numeroTicket+" , Cardif. Se ha generado el siguiente caso para su gestión. ");
                        }
                    }

                    string bodyCliDeriva = "Hola! " + nombre + " " + paterno + " " + materno + "<br>";
                    bodyCliDeriva += "<br><b>Observación Cliente:</b><br>" + txtObservacionCliente.Text + "<br>";
                    //bodyPrimera += "<br>Te informamos que tu ticket " + ticket + " ingresado el " + fechaIngreso;
                    //bodyPrimera += "<br>ha sido resuelto y cerrado por el ejecutivo a cargo.<br>";
                    //bodyPrimera += "<br>Si no estás conforme con la resolución del caso, por favor contáctanos";
                    //bodyPrimera += "<br>a través del teléfono 800 800 008.<br>";
                    //bodyPrimera += "<br>Saludos!<br>";

                    if (!string.IsNullOrEmpty(Adj1))
                    {
                        bodyCliDeriva += "<br>Adjunto1: <a href='http://" + sistema + Adj1 + "'>" + Adj1.Replace("archivosGestion/", " ") + "</a>";
                    }

                    if (!string.IsNullOrEmpty(Adj2))
                    {
                        bodyCliDeriva += "<br>Adjunto2: <a href='http://" + sistema + Adj2 + "'>" + Adj2.Replace("archivosGestion/", " ") + "</a><br>";
                    }

                    bodyCliDeriva += "<br><b>Equipo de Servicio al Cliente Esval.</b>";

                    if (!string.IsNullOrEmpty(email))
                    {
                        comunes.EnviarEmailSSLImplicito(email.Trim(), bodyResolutor.Replace("\r\n", "<br>"), "Asignacion ticket: " + numeroTicket, null);
                    }
                   
                    if (chkEnviarObsCliente.Checked)
                    {
                        if (!string.IsNullOrEmpty(lblEmailCliente.Text))
                        {
                            //JRL 2018-05-03 agregado null al archivo adjunto
                            comunes.EnviarEmailSSLImplicito(lblEmailCliente.Text.Trim(), bodyCliDeriva.Replace("\r\n", "<br>"), "Servicio al Cliente Esval // Derivar Ticket " + numeroTicket, null);
                        }

                        //2021-08-13 Comentado y cambiado a otro lugar
                        //string nombreDest = string.Empty;
                        //string emailDest = string.Empty;

                        string nomUsuSesion = string.Empty;

                        foreach (DataRow item in dal.getBuscarUsuarioPorId(Convert.ToString(Session["variableIdUsuario"])).Tables[0].Rows)
                        {
                            nomUsuSesion = item["NOMBRE"].ToString().Trim();
                        }


                        //2021-08-13 Comentado y cambiado a otro lugar
                        //DataTable dtTipi = new DataTable();
                        //dtTipi = dal.getBuscarTipificacionPorId(hfIdTipificacion.Value).Tables[0];

                        //string idServicio = string.Empty;
                        //string nivel1 = string.Empty;
                        //string nivel2 = string.Empty;

                        //foreach (DataRow item in dtTipi.Rows)
                        //{
                        //    idServicio = item["ID_SERVICIO"].ToString();
                        //    nivel1 = item["NIVEL_1"].ToString().Trim();
                        //    nivel2 = item["NIVEL_2"].ToString().Trim();
                        //}

                        //DataTable dtServicio = new DataTable();
                        //dtServicio = dal.GetBuscarServicio(Convert.ToInt32(idServicio), 1).Tables[0];

                        //string nomServicio = string.Empty;
                        //foreach (DataRow item in dtTipi.Rows)
                        //{
                        //    nomServicio = item["SERVICIO"].ToString();
                        //}

                        string obsCliEmail = txtObservacionCliente.Text.Trim() + "<br><br><b>" + nomUsuSesion + ".</b>";


                        //2021-08-13 Comentado y cambiado a otro lugar
                        //DataTable dtServicioNoti = new DataTable();
                        //dtServicioNoti = dal.GetBuscarServicioNotiticacion(Convert.ToInt32(idServicio), 0).Tables[0];

                        if (hfIdTipificacion.Value == "6")//Servicio Clave 1 / Clave 2     Nivel2 Clave 1 - Incendio Estructural
                        {
                            foreach (DataRow item in dtServicioNoti.Rows)
                            {
                                nombreDest = item["NOMBRE"].ToString().Trim();
                                emailDest = item["EMAIL"].ToString().Trim();

                                if (!string.IsNullOrEmpty(emailDest))
                                {
                                    if (comunes.IsValidEmail(emailDest))
                                    {
                                        comunes.EnviarEmailSSLImplicito(emailDest, obsCliEmail.Replace("\r\n", "<br>"), nivel1 + " " + nivel2, null);
                                    }
                                }
                            }
                        }

                        if (hfIdTipificacion.Value == "7")//Servicio Clave 1 / Clave 2     Nivel2 Clave 2 - Incendio Forestal
                        {
                            foreach (DataRow item in dtServicioNoti.Rows)
                            {
                                nombreDest = item["NOMBRE"].ToString().Trim();
                                emailDest = item["EMAIL"].ToString().Trim();

                                if (!string.IsNullOrEmpty(emailDest))
                                {
                                    if (comunes.IsValidEmail(emailDest))
                                    {
                                        comunes.EnviarEmailSSLImplicito(emailDest, obsCliEmail.Replace("\r\n", "<br>"), nivel1 + " " + nivel2, null);
                                    }
                                }
                            }
                        }

                        if (idServicio == "4")//Servicio Carga Nocturna
                        {
                            foreach (DataRow item in dtServicioNoti.Rows)
                            {
                                nombreDest = item["NOMBRE"].ToString().Trim();
                                emailDest = item["EMAIL"].ToString().Trim();

                                if (!string.IsNullOrEmpty(emailDest))
                                {
                                    if (comunes.IsValidEmail(emailDest))
                                    {
                                        comunes.EnviarEmailSSLImplicito(lblEmailCliente.Text.Trim(), obsCliEmail.Replace("\r\n", "<br>"), nomServicio + " " + DateTime.Now.ToString("G"), null);
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    //string body = "Estimado(a) Usuario Call Center: <br>";
                    //body += "Se ha generado el siguiente caso para su gestión: <br><br>";
                    //body += txtObservacionGestion.Text;
                    //body += "<br>";
                    //body += "Le agradecemos contactar a la brevedad Contac Center Upcom";
                    //body += "<br> Upcom";

                    //enviarEmail(idUsuarioEscN1Email.Trim(), body.Replace("\r\n", "<br>"), "Gestión " + idGestion + ", se ha generado el siguiente caso para su gestión");
                }

                lblInfo.Text = "Gestión histórica creada correctamete";
                divAlerta.Attributes["class"] = "alert alert-success";
                divAlerta.Visible = true;

                buscarCaso(hfNumeroTicket.Value);
                dal.setEliminarAtencionControl(numeroTicket);
                //window.close();
                //if(this.clos)

                // Response.Redirect("SeguimientoTicket.aspx?t=" + numeroTicket);
                //divAlerta.Attributes.Add("class", "alert alert-success");
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Visible = true;
            }

        }


        private void enviarEmail(string email, string body, string sub)
        {
            var from = "notificacionescrmesval@upcom.cl";
            var host = "mail.upcom.cl";
            var user = "notificacionescrmesval@upcom.cl";
            var pass = "UpC0m1026..2803";

            var mymessage = new MimeMailMessage();
            mymessage.From = new MimeMailAddress(from);

            String[] AMailto = email.Split(';');
            foreach (String mail in AMailto)
            {
                mymessage.To.Add(new MailAddress(mail));
            }

            mymessage.Subject = sub;
            mymessage.Body = body;
            mymessage.SubjectEncoding = System.Text.Encoding.UTF8;
            mymessage.HeadersEncoding = System.Text.Encoding.UTF8;
            mymessage.IsBodyHtml = true;
            mymessage.Priority = MailPriority.High;

            var mailer = new MimeMailer(host, 25);
            mailer.User = user;
            mailer.Password = pass;

            mailer.SendCompleted += compEvent;
            mailer.SendMailAsync(mymessage);

            //MailMessage correo = new MailMessage();

            //correo.From = new MailAddress("<ldiaz@interweb.cl>");
            //string bodyEmail = body;

            //String[] AMailto = email.Split(';');
            //foreach (String mail in AMailto)
            //{
            //    correo.To.Add(new MailAddress(mail));
            //}

            //correo.Subject = sub;
            //correo.IsBodyHtml = true;
            //correo.Body = bodyEmail;


            //SmtpClient client = new SmtpClient();

            //client.Send(correo);
            //¿

        }

        //Call back function
        private void compEvent(object sender, AsyncCompletedEventArgs e)
        {
            if (e.UserState != null)
                Console.Out.WriteLine(e.UserState.ToString());

            Console.Out.WriteLine("is it canceled? " + e.Cancelled);

            if (e.Error != null)
                Console.Out.WriteLine("Error : " + e.Error.Message);
        }

        protected void lbtnGenerarPdf_Click(object sender, EventArgs e)
        {
            try
            {
                string ruta = generaPdf(hfNumeroTicket.Value);
                //ruta += _lblIdPago.Text + ".pdf";
                ScriptManager.RegisterStartupScript(this, this.GetType(), UniqueID, "window.open('" + ruta + "','_blank');", true);

            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Visible = true;
            }
        }

        public string generaPdf(string idTicket)
        {
            DataTable dt = new DataTable();
            dt = dal.getBuscarTicket(idTicket).Tables[0];
            string rutCliente = "";
            //string idGestion = "";
            string observacion = string.Empty;
            string usuarioAsignado = string.Empty;
            string estadoTicket = string.Empty;
            string nivel1 = string.Empty;
            string nivel2 = string.Empty;
            string nivel3 = string.Empty;
            string nivel4 = string.Empty;
            string telefono = string.Empty;
            string email = string.Empty;
            string nombreCliente = string.Empty;
            string fechaTicket = string.Empty;

            foreach (DataRow item in dt.Rows)
            {
                rutCliente = item["RUT_CLIENTE"].ToString();
                observacion = item["OBSERVACION"].ToString();
                //idEmpleado = item["ID_EMPLEADO"].ToString();
                //txtFechaAlarma.Text = item["FECHA_AGEND"].ToString();

                usuarioAsignado = item["USUARIO_ASIGNADO"].ToString();
                estadoTicket = item["ESTADO_ATENCION"].ToString();
                lblIdEstadoTicket.Text = item["ID_ESTADO_ATENCION"].ToString();
                hfIdTipificacion.Value = item["ID_TIPIFICACION"].ToString();
                //idGestion = item["ID_GESTION"].ToString();
                fechaTicket = item["FECHA"].ToString();

                nivel1 = item["NIVEL_1"].ToString();
                nivel2 = item["NIVEL_2"].ToString();
                nivel3 = item["NIVEL_3"].ToString();
                nivel4 = item["NIVEL_4"].ToString();

            }

            string nombres, paterno, materno, segmentoCliente, fechaCompra, sernac, msj, celular, fijo, local;
            int? codError;
            string dv = string.Empty;

            if (!string.IsNullOrEmpty(rutCliente.Trim()))
            {
                Comun com = new Comun();
                rutCliente = com.formatearRutSinPuntos(rutCliente);
            }

            String[] arRut = rutCliente.Split('-');
            for (int i = 0; i < arRut.Length; i++)
            {
                rutCliente = arRut[0];
                if (arRut.Length > 0)
                {
                    dv = arRut[1];
                }
            }

            //datos.getBuscarCliente(1, Convert.ToInt32(rutCliente), dv, "CRM", out nombres, out paterno, out materno, out segmentoCliente, out email, out celular, out fijo, out fechaCompra, out local, out sernac, out codError, out msj);
            //if (nombres == "null")
            //{
            //    nombres = string.Empty;
            //}
            //if (paterno == "null")
            //{
            //    paterno = string.Empty;
            //}
            //if (materno == "null")
            //{
            //    materno = string.Empty;
            //}
            //if (segmentoCliente == "null")
            //{
            //    segmentoCliente = string.Empty;
            //}
            //if (email == "null")
            //{
            //    email = string.Empty;
            //}
            //if (fechaCompra == "null")
            //{
            //    fechaCompra = string.Empty;
            //}
            //if (sernac == "null")
            //{
            //    sernac = string.Empty;
            //}
            //if (celular == "null")
            //{
            //    celular = string.Empty;
            //}
            //if (fijo == "null")
            //{
            //    fijo = string.Empty;
            //}
            //if (local == "null")
            //{
            //    local = string.Empty;
            //}

            //nombreCliente = nombres + " " + paterno + " " + materno;
            //telefono = celular;
            //rutCliente = rutCliente + "-" + dv;

            DataTable dtHistorico = new DataTable();
            dtHistorico = dal.getBuscarTicketHistorico(idTicket).Tables[0];
            string correlativo = string.Empty;
            string fecha = string.Empty;
            string usuarioCreacion = string.Empty;
            string usuarioAsig = string.Empty;
            string estado = string.Empty;
            string motivoCierre = string.Empty;
            string obs = string.Empty;
            string obsCli = string.Empty;


            //AQUIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
            string titulo = "TICKET";
            string nombreArchivoPdf = "ticket_" + idTicket + ".pdf";
            BaseFont bfTimes = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, false);

            Font times = new Font(bfTimes, 7, Font.NORMAL);
            Font timesRojo = new Font(bfTimes, 9, Font.BOLD, BaseColor.RED);
            Font timesCorrelativo = new Font(bfTimes, 9, Font.BOLD);
            Font fontCabecera = new Font(bfTimes, 8, Font.BOLD);
            Font fontFirma = new Font(bfTimes, 8, Font.BOLD);


            Document doc = new Document(PageSize.A4, 25, 25, 30, 30);
            PdfWriter writePdf = PdfWriter.GetInstance(doc, new FileStream(Server.MapPath("pdfTicket/" + nombreArchivoPdf), FileMode.Create));
            doc.Open();

            iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(Server.MapPath("img/logocardif.png"));
            jpg.ScaleToFit(80, 80);
            jpg.Alignment = iTextSharp.text.Image.ALIGN_LEFT;
            doc.Add(jpg);

            PdfPTable tableNumeroComprobante = new PdfPTable(2);
            PdfPCell celdaNumeroComprobante = new PdfPCell(new Paragraph("Nro.Ticket :", fontCabecera));
            //celdaNumeroComprobante.HorizontalAlignment = 2;
            PdfPCell celdaNumeroComprobanteFecha = new PdfPCell(new Paragraph("Fecha Ticket :", fontCabecera));
            //celdaNumeroComprobanteFecha.HorizontalAlignment = 2;
            tableNumeroComprobante.AddCell(celdaNumeroComprobante);
            tableNumeroComprobante.AddCell(new Paragraph(idTicket, times));

            tableNumeroComprobante.AddCell(celdaNumeroComprobanteFecha);
            tableNumeroComprobante.AddCell(new Paragraph(fechaTicket, times));

            tableNumeroComprobante.DefaultCell.Border = Rectangle.NO_BORDER;

            tableNumeroComprobante.HorizontalAlignment = Element.ALIGN_RIGHT;
            tableNumeroComprobante.WidthPercentage = 25.0f;

            foreach (PdfPCell celda in tableNumeroComprobante.Rows[0].GetCells())
            {
                celda.Border = Rectangle.NO_BORDER;
            }

            foreach (PdfPCell celda in tableNumeroComprobante.Rows[1].GetCells())
            {
                celda.Border = Rectangle.NO_BORDER;
                //celda.HorizontalAlignment = 2;
            }

            doc.Add(tableNumeroComprobante);


            Chunk tituloTipoExamen = new Chunk(titulo, FontFactory.GetFont("ARIAL", 12, iTextSharp.text.Font.BOLD));
            tituloTipoExamen.SetUnderline(0.1f, -2f);

            Paragraph par = new Paragraph(tituloTipoExamen);
            par.Alignment = Element.ALIGN_CENTER;
            doc.Add(par);

            //doc.Add(tituloTipoExamen);
            doc.Add(new Paragraph(" ", times));

            Chunk datosDeudor = new Chunk("Datos Cliente", FontFactory.GetFont("ARIAL", 9, iTextSharp.text.Font.BOLD));
            datosDeudor.SetUnderline(0.1f, -2f);
            doc.Add(datosDeudor);

            PdfPTable tableDatosDeudor = new PdfPTable(6);

            float[] widthsDatosDeudor = new float[] { 35f, 85f, 50f, 55f, 35f, 35f };
            tableDatosDeudor.SetWidths(widthsDatosDeudor);

            tableDatosDeudor.AddCell(new Paragraph("Rut Cliente :", fontCabecera));
            tableDatosDeudor.AddCell(new Paragraph(rutCliente, times));
            tableDatosDeudor.AddCell(new Paragraph("Nombre :", fontCabecera));
            tableDatosDeudor.AddCell(new Paragraph(nombreCliente, times));

            tableDatosDeudor.AddCell(new Paragraph("Teléfono :", fontCabecera));
            tableDatosDeudor.AddCell(new Paragraph(telefono, times));
            tableDatosDeudor.AddCell(new Paragraph("Email :", fontCabecera));
            tableDatosDeudor.AddCell(new Paragraph(email, times));

            tableDatosDeudor.AddCell(new Paragraph("Usuario Asignado :", fontCabecera));
            tableDatosDeudor.AddCell(new Paragraph(usuarioAsignado, times));
            tableDatosDeudor.AddCell(new Paragraph("Estado :", fontCabecera));
            tableDatosDeudor.AddCell(new Paragraph(estadoTicket, times));

            tableDatosDeudor.HorizontalAlignment = Element.ALIGN_LEFT;
            tableDatosDeudor.WidthPercentage = 100.0f;

            doc.Add(tableDatosDeudor);
            doc.Add(new Paragraph(" ", times));
            doc.Add(new Paragraph(" ", times));

            Chunk datosTipificacion = new Chunk("Tipificación", FontFactory.GetFont("ARIAL", 9, iTextSharp.text.Font.BOLD));
            datosTipificacion.SetUnderline(0.1f, -2f);
            doc.Add(datosTipificacion);


            PdfPTable tableDatosTipificacion = new PdfPTable(4);
            float[] widthsDatosTipificacion = new float[] { 25f, 45f, 55f, 55f };
            tableDatosTipificacion.SetWidths(widthsDatosTipificacion);

            tableDatosTipificacion.AddCell(new Paragraph("Tipo :", fontCabecera));
            tableDatosTipificacion.AddCell(new Paragraph("Gestión :", fontCabecera));
            tableDatosTipificacion.AddCell(new Paragraph("Categoría de la gestión :", fontCabecera));
            tableDatosTipificacion.AddCell(new Paragraph("Detalle de la categoría :", fontCabecera));
            tableDatosTipificacion.AddCell(new Paragraph(nivel1, times));
            tableDatosTipificacion.AddCell(new Paragraph(nivel2, times));
            tableDatosTipificacion.AddCell(new Paragraph(nivel3, times));
            tableDatosTipificacion.AddCell(new Paragraph(nivel4, times));

            tableDatosTipificacion.HorizontalAlignment = Element.ALIGN_LEFT;
            tableDatosTipificacion.WidthPercentage = 100.0f;

            foreach (PdfPCell celda in tableDatosTipificacion.Rows[0].GetCells())
            {
                celda.BackgroundColor = BaseColor.LIGHT_GRAY;
                celda.HorizontalAlignment = 0;
                celda.Padding = 2;
            }

            doc.Add(tableDatosTipificacion);
            doc.Add(new Paragraph(" ", times));
            doc.Add(new Paragraph(" ", times));

            Chunk datosObservacionTicket = new Chunk("Descripción general del Ticket", FontFactory.GetFont("ARIAL", 9, iTextSharp.text.Font.BOLD));
            datosObservacionTicket.SetUnderline(0.1f, -2f);
            doc.Add(datosObservacionTicket);

            doc.Add(new Paragraph(" ", times));

            PdfPTable tableObs = new PdfPTable(1);
            tableObs.AddCell(new Paragraph("Observacion", fontCabecera));
            tableObs.AddCell(new Paragraph(observacion, times));

            tableObs.HorizontalAlignment = Element.ALIGN_LEFT;
            tableObs.WidthPercentage = 100.0f;

            foreach (PdfPCell celda in tableObs.Rows[0].GetCells())
            {
                celda.BackgroundColor = BaseColor.LIGHT_GRAY;
                celda.HorizontalAlignment = 0;
                celda.Padding = 2;
            }

            doc.Add(tableObs);

            doc.Add(new Paragraph(" ", times));
            doc.Add(new Paragraph(" ", times));

            Chunk datosHistorico = new Chunk("Histórico del Ticket", FontFactory.GetFont("ARIAL", 9, iTextSharp.text.Font.BOLD));
            datosHistorico.SetUnderline(0.1f, -2f);
            doc.Add(datosHistorico);


            PdfPTable tableDetalle = new PdfPTable(7);
            tableDetalle.AddCell(new Paragraph("Correlativo", fontCabecera));
            tableDetalle.AddCell(new Paragraph("Fecha", fontCabecera));
            tableDetalle.AddCell(new Paragraph("Usuario Creación", fontCabecera));
            tableDetalle.AddCell(new Paragraph("Usuario Asig", fontCabecera));
            tableDetalle.AddCell(new Paragraph("Estado", fontCabecera));
            tableDetalle.AddCell(new Paragraph("Motivo Cierre", fontCabecera));
            tableDetalle.AddCell(new Paragraph("Obs", fontCabecera));

            foreach (DataRow item in dtHistorico.Rows)
            {
                tableDetalle.AddCell(new Paragraph(item["CORRELATIVO"].ToString(), times));
                tableDetalle.AddCell(new Paragraph(item["FECHA"].ToString(), times));
                tableDetalle.AddCell(new Paragraph(item["USUARIO"].ToString(), times));
                tableDetalle.AddCell(new Paragraph(item["USUARIO_ASIG"].ToString(), times));
                tableDetalle.AddCell(new Paragraph(item["ESTADO_ATENCION"].ToString(), times));
                tableDetalle.AddCell(new Paragraph(item["NOM_MOTIVO_CIERRE"].ToString(), times));
                tableDetalle.AddCell(new Paragraph(item["OBSERVACION"].ToString(), times));
            }

            float[] widthsDatosDetalle = new float[] { 25f, 50f, 50f, 50f, 50f, 20f, 60f };
            tableDetalle.SetWidths(widthsDatosDetalle);

            tableDetalle.HorizontalAlignment = Element.ALIGN_LEFT;
            tableDetalle.WidthPercentage = 100.0f;

            foreach (PdfPCell celda in tableDetalle.Rows[0].GetCells())
            {
                celda.BackgroundColor = BaseColor.LIGHT_GRAY;
                celda.HorizontalAlignment = 1;
                celda.Padding = 2;
            }

            doc.Add(tableDetalle);

            //foreach (DataRow item in dtHistorico.Rows)
            //{
            //    correlativo = item["CORRELATIVO"].ToString();
            //    fecha = item["FECHA"].ToString();
            //    usuarioCreacion = item["USUARIO"].ToString();
            //    usuarioAsig = item["USUARIO_ASIG"].ToString();
            //    estado = item["ESTADO_ATENCION"].ToString();
            //    motivoCierre = item["NOM_MOTIVO_CIERRE"].ToString();
            //    obs = item["OBSERVACION"].ToString();
            //    obsCli = item["OBSERVACION_CLIENTE"].ToString();
            //}
            doc.Add(new Paragraph(" ", times));
            doc.Close();

            string ruta = "pdfTicket/" + nombreArchivoPdf; ;
            return ruta;

        }

        protected void BtnVerFormulario_Click(object sender, EventArgs e)
        {
            try
            {
                string Ticket = Convert.ToString(Request.QueryString["t"]);
                DataTable dt = dal.getBuscarAtencionFormularioPorId(Ticket).Tables[0];

                foreach (DataRow item in dt.Rows)
                {
                    TxtNombreProducto.Text = item["PRO_NOMBRE_PRODUCTO"].ToString();
                    if (string.IsNullOrEmpty(item["PRO_SOCIO"].ToString()))
                    {
                        DdlSocio.SelectedValue = "0";
                    }
                    else
                    {
                        DdlSocio.SelectedItem.Text = item["PRO_SOCIO"].ToString();
                    }
                    TxtPoliza.Text = item["PRO_POLIZA"].ToString();
                    TxtCanalVentas.Text = item["PRO_CANAL_VENTAS"].ToString();
                    TxtCobertura.Text = item["PRO_COBERTURA"].ToString();
                    TxtEstadoPoliza.Text = item["PRO_ESTADO_POLIZA"].ToString();
                    TxtAsistencia.Text = item["PRO_ASISTENCIA"].ToString();
                    TxtNombreBeneficiario.Text = item["BEN_NOMBRE_BENEFICIARIO"].ToString();
                    TxtRutBeneficiario.Text = item["BEN_RUT_BENEFICIARIO"].ToString();
                    TxtRelacion.Text = item["BEN_RELACION"].ToString();
                    TxtCelularBeneficiario.Text = item["BEN_CELULAR_BENEFICIARIO"].ToString();
                    TxtEmailBeneficiario.Text = item["BEN_MAIL_BENEFICIARIO"].ToString();
                    TxtDomicilioBeneficiario.Text = item["BEN_DOMICILIO_BENEFICIARIO"].ToString();
                    TxtComunaBeneficiario.Text = item["BEN_COMUNA"].ToString();
                    TxtNombreProveedor.Text = item["PROV_NOMBRE_PROVEEDOR"].ToString();
                    TxtTelefonoProveedor.Text = item["PROV_TELEFONO_PROVEEDOR"].ToString();
                    TxtCasaComercial.Text = item["PROV_CASA_COMERCIAL"].ToString();
                    TxtNumBoletaCompra.Text = item["COM_NUMERO_BOLETA_COMPRA"].ToString();
                    TxtFechaCompra.Text = item["COM_FECHA_COMPRA"].ToString();
                    TxtValorCompra.Text = item["COM_VALOR_COMPRA"].ToString();
                    TxtMarcaProducto.Text = item["COM_MARCA_PRODUCTO"].ToString();
                    TxtEmpDondeRealizoCompra.Text = item["COM_EMP_REALIZO_COMPRA"].ToString();
                    TxtFormaPago.Text = item["COM_FORMA_PAGO"].ToString();
                    TxtNumRegConsulta.Text = item["OTR_NUM_REGISTRO_CONSULTA"].ToString();
                    TxtNombreLiquidador.Text = item["LIQ_NOMBRE_LIQUIDADOR"].ToString();
                    TxtTelefonoLiquidador.Text = item["LIQ_TELEFONO_LIQUIDADOR"].ToString();
                    TxtNombreCoordinador.Text = item["LIQ_NOMBRE_COORDINADOR"].ToString();
                    TxtTelefonoCoordinador.Text = item["LIQ_TELEFONO_COORDINADOR"].ToString();
                    TxtUltComLiquidador.Text = item["LIQ_ULT_COM_LIQUIDADOR"].ToString();
                    TxtNumSiniestro.Text = item["SIN_NUM_SINIESTRO"].ToString();
                    TxtEstadoSiniestro.Text = item["SIN_ESTADO_SINIESTRO"].ToString();
                    TxtIngresoTaller.Text = item["SIN_INGRESO_TALLER"].ToString();
                    TxtFechaAprobacion.Text = item["SIN_FECHA_APROBACION"].ToString();
                    TxtFechaEntrega.Text = item["SIN_FECHA_ENTREGA"].ToString();
                    TxtSumaLiquidacion.Text = item["SIN_SUMA_LIQUIDACION"].ToString();
                    TxtNumeroCaso.Text = item["SIN_NUMERO_CASO"].ToString();
                    TxtNombreTaller.Text = item["TALL_NOMBRE_TALLER"].ToString();
                    TxtDireccionTaller.Text = item["TALL_DIRECCION_TALLER"].ToString();
                    TxtTelefonoTaller.Text = item["TALL_TELEFONO_TALLER"].ToString();
                }

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "MostrarModal();", true);
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "MostrarModal();", true);
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Visible = true;
            }
        }

        protected void lbtnGrabarClienteExt_Click(object sender, EventArgs e)
        {
            try
            {
                int idUsuario = Convert.ToInt32(Session["variableIdUsuario"]);
                //string msj;
                //int? codError;

                string rutCodDv = txtRutClienteExt.Text;
                string rut = string.Empty, dv = string.Empty;

                if (!comunes.validarRut(rutCodDv))
                {
                    lblInfo.Text = "Debe ingresar un rut válido.";
                    divAlerta.Attributes["class"] = "alert alert-warning";
                    divAlerta.Visible = true;
                    return;
                }

                if (txtCellPhone.Text == string.Empty && txtPhone.Text == string.Empty && txtEmailClient.Text == string.Empty)
                {
                    lblInfo.Text = "Debe ingresar al menos un teléfono, celular o email respectivamente.";
                    divAlerta.Attributes["class"] = "alert alert-warning";
                    divAlerta.Visible = true;
                    return;
                }

                if (txtCellPhone.Text != string.Empty && txtCellPhone.Text.Length != 9)
                {
                    lblInfo.Text = "Debe ingresar 9 caracteres en  Celular.";
                    divAlerta.Attributes["class"] = "alert alert-warning";
                    divAlerta.Visible = true;
                    return;
                }

                if (txtPhone.Text != string.Empty && txtPhone.Text.Length != 9)
                {
                    lblInfo.Text = "Debe ingresar 9 caracteres en teléfono.";
                    divAlerta.Attributes["class"] = "alert alert-warning";
                    divAlerta.Visible = true;
                    return;
                }

                //if (ddlRutoEmail.SelectedValue == "RUT")
                //{
                if (txtRutClienteExt.Text.Trim() == string.Empty)
                {
                    lblInfo.Text = "El campo Rut es obligatorio para guardar el cliente";
                    divAlerta.Attributes["class"] = "alert alert-warning";
                    divAlerta.Visible = true;
                    return;
                }
                comunes.separaRut(rutCodDv, out rut, out dv);
                //}

                string celular = txtCellPhone.Text.Trim();
                //if (ddlRutoEmail.SelectedValue == "RUT")
                //{
                if (celular == string.Empty)
                {
                    lblInfo.Text = "El campo celular es obligatorio y númerico";
                    divAlerta.Attributes["class"] = "alert alert-warning";
                    divAlerta.Visible = true;
                    return;
                }
                //}

                if (txtEmailClient.Text.Trim() == string.Empty)
                {
                    lblInfo.Text = "El campo email es obligatorio";
                    divAlerta.Attributes["class"] = "alert alert-warning";
                    divAlerta.Visible = true;
                    return;
                }

                int? cel;
                if (comunes.IsNumeric(celular))
                {
                    cel = Convert.ToInt32(celular);
                }
                else
                {
                    cel = null;
                }

                string fijo = txtPhone.Text.Trim();
                int? tel;
                if (comunes.IsNumeric(fijo))
                {
                    tel = Convert.ToInt32(fijo);
                }
                else
                {
                    tel = null;
                }

                //string local = txtLocal.Text.Trim();
                //int? codLocal;
                //if (comunes.IsNumeric(local))
                //{
                //    codLocal = Convert.ToInt32(local);
                //}
                //else
                //{
                //    codLocal = null;
                //}

                string ruts = rut + '-' + dv;
                //if (ddlRutoEmail.SelectedValue == "RUT")
                //{
                dal.setIngresarCliente(ruts, txtEmailClient.Text.Trim(), txtNombreClienteExt.Text.Trim().ToUpper(),
                   txtApellidoParternoClienteExt.Text.Trim().ToUpper(), txtApellidoMaterno.Text.Trim().ToUpper(),
                   tel.ToString(), cel.ToString(), idUsuario, txtCalle.Text, txtNumero.Text, txtResto.Text, ddlComuna.SelectedValue);
                //}

                lblInfo.Text = "Cliente grabado correctamente.";
                divAlerta.Attributes["class"] = "alert alert-info";
                divAlerta.Visible = true;
                //datos.setIngresaCliente(1, 16394005, "1", "CRM", "Leonardo", "Diaz", "Corvalan", "SB", "ldiaz@interweb.cl", 98987654, 2323232, "29-11-2016", 100, "N", out codError, out msj);

                buscarCaso(hfNumeroTicket.Value);
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void btnModificarCliente_Click(object sender, EventArgs e)
        {
            try
            {
                //JRL 2018-10-02
                txtRutClienteExt.Enabled = false;
                limpiarClienteExt();
                divEnriquecimientoContactabilidad.Visible = true;

                //if (hfNombres.Value == "null")
                //{
                //    hfNombres.Value = "";
                //}
                //if (hfPaterno.Value == "null")
                //{
                //    hfPaterno.Value = "";
                //}
                //if (hfMaterno.Value == "null")
                //{
                //    hfMaterno.Value = "";
                //}
                //if (hfEmail.Value == "null")
                //{
                //    hfEmail.Value = "";
                //}
                //if (hfFijo.Value == "null")
                //{
                //    hfFijo.Value = "";
                //}
                //if (hfCelular.Value == "null")
                //{
                //    hfCelular.Value = "";
                //}
                //if (hfSernac.Value == "null")
                //{
                //    hfSernac.Value = "";
                //}
                //if (hfCalle.Value == "null")
                //{
                //    hfCalle.Value = "";
                //}
                //if (hfNumero.Value == "null")
                //{
                //    hfNumero.Value = "";
                //}
                //if (hfResto.Value == "null")
                //{
                //    hfResto.Value = "";
                //}
                //if (hfComuna.Value == "null")
                //{
                //    hfComuna.Value = "";
                //}

                //txtRutClienteExt.Text = txtBuscarCliente.Text;
                //Comun com = new Comun();
                //if (com.IsValidEmail(txtBuscarCliente.Text))
                //{
                //    ddlRutoEmail.SelectedValue = "EMAIL";
                //}
                //else
                //{
                //    ddlRutoEmail.SelectedValue = "RUT";
                //}

                string nombres = string.Empty, paterno = string.Empty, materno = string.Empty, email = string.Empty,
                celular = string.Empty, fijo = string.Empty, calle = string.Empty, numero = string.Empty,
                resto = string.Empty, idComuna = string.Empty;

                DataTable dt = dal.getBuscarClientePorRut(lblRutCliente.Text.Trim()).Tables[0];
                foreach (DataRow item in dt.Rows)
                {
                    nombres = item["NOMBRE"].ToString();
                    paterno = item["PATERNO"].ToString();
                    materno = item["MATERNO"].ToString();
                    email = item["EMAIL"].ToString();
                    celular = item["CELULAR"].ToString();
                    fijo = item["TELEFONO"].ToString();
                    calle = item["CALLE"].ToString();
                    numero = item["NUMERO"].ToString();
                    resto = item["RESTO"].ToString();
                    idComuna = item["ID_COMUNA"].ToString();
                }

                txtRutClienteExt.Text = lblRutCliente.Text;
                txtNombreClienteExt.Text = nombres;
                txtApellidoParternoClienteExt.Text = paterno;
                txtApellidoMaterno.Text = materno;
                txtEmailClient.Text = email;
                txtPhone.Text = fijo;
                txtCellPhone.Text = celular;
                txtCalle.Text = calle;
                txtNumero.Text = numero;
                txtResto.Text = resto;
                ddlComuna.SelectedValue = idComuna;
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void DdlSocio_DataBound(object sender, EventArgs e)
        {
            DdlSocio.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        }

        void Comuna()
        {
            ddlComuna.DataSource = dal.getBuscarComuna(null);
            ddlComuna.DataTextField = "NOM_COMUNA";
            ddlComuna.DataValueField = "ID_COMUNA";
            ddlComuna.DataBind();
        }

        void Socio()
        {
            DdlSocio.DataSource = dal.GetBuscarSocio(1);
            DdlSocio.DataValueField = "ID_SOCIO";
            DdlSocio.DataTextField = "SOCIO";
            DdlSocio.DataBind();
        }
        protected void ddlComuna_DataBound(object sender, EventArgs e)
        {
            ddlComuna.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        }

        void limpiarClienteExt()
        {
            txtRutClienteExt.Text = string.Empty;
            txtNombreClienteExt.Text = string.Empty;
            txtApellidoParternoClienteExt.Text = string.Empty;
            txtApellidoMaterno.Text = string.Empty;
            txtEmailClient.Text = string.Empty;
            txtPhone.Text = string.Empty;
            txtCellPhone.Text = string.Empty;
            txtCalle.Text = string.Empty;
            txtNumero.Text = string.Empty;
            txtResto.Text = string.Empty;
            ddlComuna.ClearSelection();
        }

        protected void chkEnviarObsCliente_CheckedChanged(object sender, EventArgs e)
        {
            //txtObservacionCliente.Text = string.Empty;
            if (chkEnviarObsCliente.Checked == true)
            {
                if (hfIdTipificacion.Value == "6" || hfIdTipificacion.Value == "7")//Clave 1 / Clave 2
                {
                    txtObservacionCliente.Text = "Estimados, Se informa siniestro en la ubicación:\n" +
                        "DIRECCIÖN:\n" +
                        "REFERENCIAS (SI EXISTEN):\n" +
                        "LOCALIDAD:\n" +
                        "Saludos";
                }
                else if (hfIdTipificacion.Value == "12" || hfIdTipificacion.Value == "13")//Carga Nocturna
                {
                    txtObservacionCliente.Text = "Estimados, Se informa Carga Nocturna con fecha: " + DateTime.Now.ToString("G") + "\n" +
                        "Localidad:\n" +
                        "Dirección de los trabajos:\n" +
                        "Recibe información (CENCO):\n" +
                        "Saludos";
                }
            }
        }
    }
}