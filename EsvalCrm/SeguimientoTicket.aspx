﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SeguimientoTicket.aspx.cs" Inherits="EsvalCrm.SeguimientoTicket" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Seguimiento</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Buscador ticket</li>
                        <li class="breadcrumb-item active">Seguimiento ticket</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">

            <!-- Alertas -->
            <div id="divAlerta" runat="server" visible="false" class="alert alert-danger">
                <strong>Atención!: </strong>
                <asp:Label Text="" ID="lblInfo" runat="server" />
            </div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:HiddenField ID="hfNumeroTicket" runat="server" />
                    <asp:HiddenField ID="hfIdUsuarioCreacion" runat="server" />
                    <asp:HiddenField ID="_iwc_hf_tipocliente" runat="server" />

                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-success card-outline">
                                <div class="card-header">
                                    <h3 class="card-title">Cliente |
                                        <asp:Label ID="lblIdTicket" runat="server" Text=""></asp:Label>
                                        <asp:LinkButton ID="lbtnGenerarPdf" CssClass="btn btn-danger btn-xs" ForeColor="White" runat="server"
                                            OnClick="lbtnGenerarPdf_Click"><i aria-hidden="true" class=" fa fa-file-pdf"></i> </asp:LinkButton>
                                        <asp:Button ID="BtnVerFormulario" runat="server" CssClass="btn btn-warning btn-xs" ForeColor="White" Text="Ver Formulario" Visible="false" OnClick="BtnVerFormulario_Click" />
                                        <asp:Button ID="btnModificarCliente" OnClick="btnModificarCliente_Click" Text="Editar" CssClass="btn btn-success btn-xs" runat="server" />
                                        <asp:Label ID="lblIdCliente" runat="server" Text=""></asp:Label>
                                    </h3>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div id="divCliente">
                                            <table class="table table-condensed small" style="width: 100%">
                                                <tr class="active">
                                                    <td>
                                                        <strong>Rut Titular:</strong><br />
                                                        <asp:Label ID="lblRutCliente" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <strong>Nombre Titular:</strong><br />
                                                        <asp:Label ID="lblNombreCliente" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <strong>Teléfono:</strong><br />
                                                        <asp:Label ID="lblTelefonoCliente" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <strong>Celular:</strong><br />
                                                        <asp:Label ID="lblCelularCliente" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <strong>Email:</strong><br />
                                                        <asp:Label ID="lblEmailCliente" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <strong>Calle:</strong><br />
                                                        <asp:Label ID="lblCalle" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <strong>Número:</strong><br />
                                                        <asp:Label ID="lblNumero" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <strong>Resto:</strong><br />
                                                        <asp:Label ID="lblResto" runat="server"></asp:Label>
                                                    </td>

                                                </tr>
                                                <tr class="active">
                                                    <td runat="server" visible="false">
                                                        <strong>Central:</strong><br />
                                                        <asp:Label ID="lblIdVendedor" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <strong>Comuna:</strong><br />
                                                        <asp:Label ID="lblComuna" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <strong>Usuario Asig:</strong><br />
                                                        <asp:Label ID="lblUsuarioAsig" runat="server"></asp:Label>
                                                        <asp:Label ID="lblIdUsuarioAsig" runat="server" Visible="false"></asp:Label>
                                                        <asp:Label ID="lblIdAreaTicket" runat="server" Visible="false"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <strong>Usuario Creación:</strong><br />
                                                        <asp:Label ID="lblUsuarioCreacion" runat="server"></asp:Label>
                                                    </td>
                                                    <td><strong>Estado:</strong><br />
                                                        <asp:Label ID="lblEstadoTicket" runat="server"></asp:Label>
                                                        <asp:Label ID="lblIdEstadoTicket" runat="server" Visible="false" />
                                                    </td>
                                                    <td>
                                                        <strong>Canal:</strong><br />
                                                        <asp:Label ID="lblLocal" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <strong>Rut No Titular:</strong><br />
                                                        <asp:Label ID="lblRutNoTitular" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <strong>Nombre No Titular:</strong><br />
                                                        <asp:Label ID="lblNombreNoTitular" runat="server"></asp:Label>
                                                    </td>
                                                    <td colspan="2"></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>


                                <div class="card-body">
                                    <div id="divEnriquecimientoContactabilidad" runat="server" visible="false">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Rut</label>
                                                <asp:TextBox ID="txtRutClienteExt" runat="server" MaxLength="10" onkeypress="return numbersonlyRut(event);" oninput="checkRut(this)" CssClass="form-control input-sm" placeholder="Rut" Enabled="false"></asp:TextBox>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Email</label>
                                                <asp:TextBox ID="txtEmailClient" runat="server" CssClass="form-control input-sm" placeholder="Email"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" Display="Dynamic" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtEmailClient" ErrorMessage="formato del email no válido"></asp:RegularExpressionValidator>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Nombre</label>
                                                <asp:TextBox ID="txtNombreClienteExt" runat="server" onkeypress="return esLetra(event)" CssClass="form-control input-sm" placeholder="Nombre"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Apellido Paterno</label>
                                                <asp:TextBox ID="txtApellidoParternoClienteExt" runat="server" onkeypress="return esLetra(event)" CssClass="form-control input-sm" placeholder="Apellido Paterno"></asp:TextBox>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Apellido Materno</label>
                                                <asp:TextBox ID="txtApellidoMaterno" runat="server" onkeypress="return esLetra(event)" CssClass="form-control input-sm" placeholder="Apellido Materno"></asp:TextBox>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Teléfono</label>
                                                <asp:TextBox ID="txtPhone" runat="server" MaxLength="9" CssClass="form-control input-sm" placeholder="Telefono"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Celular</label>
                                                <asp:TextBox ID="txtCellPhone" runat="server" MaxLength="9" CssClass="form-control input-sm" placeholder="Celular"></asp:TextBox>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Calle</label>
                                                <asp:TextBox ID="txtCalle" runat="server" CssClass="form-control input-sm" placeholder="Calle"></asp:TextBox>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Número</label>
                                                <asp:TextBox ID="txtNumero" runat="server" CssClass="form-control input-sm" onkeypress="return soloNumeros(event);" placeholder="Número"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Resto</label>
                                                <asp:TextBox ID="txtResto" runat="server" CssClass="form-control input-sm" placeholder="Resto"></asp:TextBox>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Comuna</label>
                                                <asp:DropDownList ID="ddlComuna" runat="server" CssClass="form-control" Width="100%" OnDataBound="ddlComuna_DataBound"></asp:DropDownList>
                                            </div>
                                            <div class="col-xs-4">
                                                <br />
                                                <asp:LinkButton ID="lbtnGrabarClienteExt" runat="server" CssClass="btn btn-danger btn-sm" OnClick="lbtnGrabarClienteExt_Click"><span class="glyphicon glyphicon-floppy-disk"></span>Grabar cliente</asp:LinkButton>
                                            </div>
                                        </div>

                                    </div>

                                </div>





                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-info card-outline">
                                <div class="card-header">
                                    <%--<h4 class="card-title">Tipificación</h4>--%>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <strong>Tipificación</strong>
                                        </div>
                                        <div class="col-md-4">
                                            <strong>Servicio: </strong>
                                            <asp:Label ID="lblServicio" runat="server"></asp:Label>
                                        </div>
                                    </div>                                                                     
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <strong>Nivel 1</strong>
                                            <br />
                                            <asp:Label ID="lblNivel1" runat="server"></asp:Label>
                                            <asp:HiddenField ID="hfIdTipificacion" runat="server" />
                                        </div>
                                        <div class="col-md-2">
                                            <strong>Nivel 2</strong><br />
                                            <asp:Label ID="lblNivel2" runat="server"></asp:Label>
                                        </div>
                                        <div class="col-md-3">
                                            <strong>Nivel 3</strong><br />
                                            <asp:Label ID="lblNivel3" runat="server"></asp:Label>
                                        </div>
                                        <div class="col-md-3">
                                            <strong>Nivel 4</strong><br />
                                            <asp:Label ID="lblNivel4" runat="server"></asp:Label>
                                        </div>
                                        <div class="col-md-2">
                                            <strong>Archivos:</strong><br />
                                            <asp:Label ID="lblRutaArchivo" runat="server" Text="" Visible="false"></asp:Label>
                                            <asp:ImageButton ID="ibtnArchivo" runat="server" ToolTip="Adjunto 1" ImageUrl="~/img/box.png" Visible="false" OnClick="ibtnArchivo_Click" />
                                            <%--JRL 2018-05-17--%>
                                            <asp:Label ID="lblRutaArchivo2" runat="server" Text="" Visible="false"></asp:Label>
                                            <asp:ImageButton ID="ibtnArchivo22" runat="server" ToolTip="Adjunto 2" ImageUrl="~/img/box.png" Visible="false" OnClick="ibtnArchivo22_Click" />
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="card card-warning card-outline">
                                <div class="card-header">
                                    <h3 class="card-title">Descripción general del Ticket</h3>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-6">
                                            <label>Observación</label>
                                            <asp:TextBox ID="txtObservacion" runat="server" TextMode="MultiLine" CssClass="form-control input-sm" ReadOnly="true" Height="100px"></asp:TextBox>
                                        </div>
                                        <div class="col-6">
                                            <label>Descripción Cliente</label>
                                            <asp:TextBox ID="txtDescCliente" runat="server" TextMode="MultiLine" CssClass="form-control input-sm" ReadOnly="true" Height="100px"></asp:TextBox>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row" runat="server" id="divOtrosDatos" visible="false">
                        <div class="col-md-12">
                            <div class="card card-success card-outline">
                                <div class="card-header">
                                    <h3 class="card-title">Otros Datos</h3>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div id="divCampo1" runat="server" class="col-md-2" visible="false">
                                            <strong>
                                                <asp:Label ID="lblCampo1" runat="server" Text=""></asp:Label></strong>
                                        </div>
                                        <div id="divCampo2" runat="server" class="col-md-2" visible="false">
                                            <strong>
                                                <asp:Label ID="lblCampo2" runat="server" Text=""></asp:Label></strong>
                                        </div>
                                        <div id="divCampo3" runat="server" class="col-md-2" visible="false">
                                            <strong>
                                                <asp:Label ID="lblCampo3" runat="server" Text=""></asp:Label></strong>
                                        </div>
                                        <div id="divCampo4" runat="server" class="col-md-2" visible="false">
                                            <strong>
                                                <asp:Label ID="lblCampo4" runat="server" Text=""></asp:Label></strong>
                                        </div>
                                        <div id="divCampo5" runat="server" class="col-md-2" visible="false">
                                            <strong>
                                                <asp:Label ID="lblCampo5" runat="server" Text=""></asp:Label></strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- Modal bounceIn animated-->
                    <div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content ">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="myModalLabel">Ver Formulario Atención</h4>
                                </div>
                                <div class="modal-body">
                                        <div class="row">
                                        <div class="col-3 col-sm-3">
                                            <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
                                                <a class="nav-link active" id="vert-tabs-Producto-tab" role="tab" aria-selected="true" aria-controls="vert-tabs-Producto" href="#vert-tabs-Producto" data-toggle="pill">Producto</a>
                                                <a class="nav-link" id="vert-tabs-Beneficiario-tab" role="tab" aria-selected="false" aria-controls="vert-tabs-Beneficiario" href="#vert-tabs-Beneficiario" data-toggle="pill">Beneficiario</a>
                                                <a class="nav-link" id="vert-tabs-Proveedor-tab" role="tab" aria-selected="false" aria-controls="vert-tabs-Proveedor" href="#vert-tabs-Proveedor" data-toggle="pill">Proveedor</a>
                                                <a class="nav-link" id="vert-tabs-Compra-tab" role="tab" aria-selected="false" aria-controls="vert-tabs-Compra" href="#vert-tabs-Compra" data-toggle="pill">Datos de la Compra</a>
                                                <a class="nav-link" id="vert-tabs-NumeroRegistroConsulta-tab" role="tab" aria-selected="false" aria-controls="vert-tabs-NumeroRegistroConsulta" href="#vert-tabs-NumeroRegistroConsulta" data-toggle="pill">Número Registro Consulta</a>
                                                <a class="nav-link" id="vert-tabs-Liquidador-tab" role="tab" aria-selected="false" aria-controls="vert-tabs-Liquidador" href="#vert-tabs-Liquidador" data-toggle="pill">Liquidador</a>
                                                <a class="nav-link" id="vert-tabs-Siniestro-tab" role="tab" aria-selected="false" aria-controls="vert-tabs-Siniestro" href="#vert-tabs-Siniestro" data-toggle="pill">Siniestro</a>
                                                <a class="nav-link" id="vert-tabs-Taller-tab" role="tab" aria-selected="false" aria-controls="vert-tabs-Taller" href="#vert-tabs-Taller" data-toggle="pill">Taller</a>
                                            </div>
                                        </div>
                                        <div class="col-9 col-sm-9">
                                            <div class="tab-content" id="vert-tabs-tabContent">
                                                <div class="tab-pane text-left fade active show" id="vert-tabs-Producto" role="tabpanel" aria-labelledby="vert-tabs-Producto-tab">
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Nombre:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtNombreProducto" runat="server" CssClass="form-control" placeholder="Nombre Producto"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Socio:</label>
                                                                </div>
                                                                <asp:DropDownList ID="DdlSocio" runat="server" CssClass="custom-select" ClientIDMode="Static" OnDataBound="DdlSocio_DataBound">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">

                                                        <div class="col-6">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Poliza:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtPoliza" runat="server" CssClass="form-control" placeholder="Poliza"></asp:TextBox>
                                                            </div>

                                                        </div>
                                                        <div class="col-6">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Canal Ventas:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtCanalVentas" runat="server" CssClass="form-control" placeholder="Canal de ventas"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Cobertura:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtCobertura" runat="server" CssClass="form-control" placeholder="Cobertura"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Estado Poliza:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtEstadoPoliza" runat="server" CssClass="form-control" placeholder="Estado Poliza"></asp:TextBox>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="row">

                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Asistencia:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtAsistencia" runat="server" CssClass="form-control" placeholder="Asistencia"></asp:TextBox>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="vert-tabs-Beneficiario" role="tabpanel" aria-labelledby="vert-tabs-Beneficiario-tab">
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">RUT:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtRutBeneficiario" runat="server" CssClass="form-control" placeholder="RUT"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-8">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Nombre:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtNombreBeneficiario" runat="server" CssClass="form-control" placeholder="Nombre Beneficiario"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-6">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Relación:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtRelacion" runat="server" CssClass="form-control" placeholder="Relación"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Celular:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtCelularBeneficiario" runat="server" CssClass="form-control" placeholder="Celular"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">

                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Email:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtEmailBeneficiario" runat="server" CssClass="form-control" placeholder="Email"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Domicilio:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtDomicilioBeneficiario" runat="server" CssClass="form-control" placeholder="Domicilio"></asp:TextBox>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Comuna:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtComunaBeneficiario" runat="server" CssClass="form-control" placeholder="Comuna"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="vert-tabs-Proveedor" role="tabpanel" aria-labelledby="vert-tabs-Proveedor-tab">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Nombre:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtNombreProveedor" runat="server" CssClass="form-control" placeholder="Nombre Proveedor"></asp:TextBox>
                                                            </div>

                                                        </div>
                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Teléfono:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtTelefonoProveedor" runat="server" CssClass="form-control" placeholder="Teléfono"></asp:TextBox>
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Casa Comercial:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtCasaComercial" runat="server" CssClass="form-control" placeholder="Casa Comercial"></asp:TextBox>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="vert-tabs-Compra" role="tabpanel" aria-labelledby="vert-tabs-Compra-tab">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">N° Boleta Compra:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtNumBoletaCompra" runat="server" CssClass="form-control" placeholder="N° Boleta Compra"></asp:TextBox>
                                                            </div>

                                                        </div>
                                                        <%--                                                <div class="col-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><strong>Fecha Compra:</strong></span>
                                                        <asp:TextBox ID="TxtFechaCompra" runat="server" CssClass="form-control" placeholder="Fecha Compra"></asp:TextBox>
                                                    </div>
                                                </div>--%>

                                                        <div class="col-12">

                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend ">
                                                                    <label class="input-group-text">Fecha Compra:</label>
                                                                   
                                                                </div>
                                                                 <asp:TextBox ID="TxtFechaCompra" runat="server" CssClass="form-control class-date" AutoCompleteType="Disabled"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Valor Compra:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtValorCompra" runat="server" CssClass="form-control" placeholder="Valor Compra"></asp:TextBox>
                                                            </div>

                                                        </div>
                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Marca Producto:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtMarcaProducto" runat="server" CssClass="form-control" placeholder="Marca Producto"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Emp. Donde Realizó Compra:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtEmpDondeRealizoCompra" runat="server" CssClass="form-control" placeholder="Emp. Donde Realizó Compra"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Forma Pago:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtFormaPago" runat="server" CssClass="form-control" placeholder="Forma de Pago"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="vert-tabs-NumeroRegistroConsulta" role="tabpanel" aria-labelledby="vert-tabs-NumeroRegistroConsulta-tab">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">N° Registro Consulta:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtNumRegConsulta" runat="server" CssClass="form-control" placeholder="N° Registro Consulta"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="vert-tabs-Liquidador" role="tabpanel" aria-labelledby="vert-tabs-Liquidador-tab">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Nombre:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtNombreLiquidador" runat="server" CssClass="form-control" placeholder="Nombre Liquidador"></asp:TextBox>
                                                            </div>

                                                        </div>

                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Teléfono:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtTelefonoLiquidador" runat="server" CssClass="form-control" placeholder="Teléfono"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Nombre Coordinador:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtNombreCoordinador" runat="server" CssClass="form-control" placeholder="Nombre Coordinador"></asp:TextBox>
                                                            </div>

                                                        </div>

                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Teléfono Coordinador:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtTelefonoCoordinador" runat="server" CssClass="form-control" placeholder="Teléfono Coordinador"></asp:TextBox>
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Últ. Com. con Liquidador:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtUltComLiquidador" runat="server" CssClass="form-control" placeholder="Últ. Com. con Liquidador"></asp:TextBox>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="vert-tabs-Siniestro" role="tabpanel" aria-labelledby="vert-tabs-Siniestro-tab">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">N° Siniestro:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtNumSiniestro" runat="server" CssClass="form-control" placeholder="Número Siniestro"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Estado Siniestro:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtEstadoSiniestro" runat="server" CssClass="form-control" placeholder="Estado Siniestro"></asp:TextBox>
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Ingreso Taller:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtIngresoTaller" runat="server" CssClass="form-control" placeholder="Ingreso Taller"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <%--                                                <div class="col-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><strong>Fecha Aprobación:</strong></span>
                                                        <asp:TextBox ID="TxtFechaAprobacion" runat="server" CssClass="form-control" placeholder="Fecha Aprobación"></asp:TextBox>
                                                    </div>
                                                </div>--%>

                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Fecha Aprobación:</label>
                                                                    
                                                                </div>
                                                                <asp:TextBox ID="TxtFechaAprobacion" runat="server" ClientIDMode="Static" CssClass="form-control class-date" AutoCompleteType="Disabled"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <%--                                                <div class="col-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><strong>Fecha Entrega:</strong></span>
                                                        <asp:TextBox ID="TxtFechaEntrega" runat="server" CssClass="form-control" placeholder="Fecha Entrega"></asp:TextBox>
                                                    </div>
                                                </div>--%>

                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Fecha Entrega:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtFechaEntrega" runat="server" ClientIDMode="Static" CssClass="form-control class-date" AutoCompleteType="Disabled"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Suma Liquidación:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtSumaLiquidacion" runat="server" CssClass="form-control" placeholder="Suma Liquidación"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">N° Caso:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtNumeroCaso" runat="server" CssClass="form-control" placeholder="Número de Caso"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="vert-tabs-Taller" role="tabpanel" aria-labelledby="vert-tabs-Taller-tab">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Nombre:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtNombreTaller" runat="server" CssClass="form-control" placeholder="Nombre Taller"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Dirección:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtDireccionTaller" runat="server" CssClass="form-control" placeholder="Dirección"></asp:TextBox>
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Teléfono:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtTelefonoTaller" runat="server" CssClass="form-control" placeholder="Teléfono"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default btn-quirk" data-dismiss="modal">Cerrar</button>
                                </div>
                            </div>
                            <!-- modal-content -->
                        </div>
                        <!-- modal-dialog -->
                    </div>
                    <!-- modal -->
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="card card-success card-outline" id="divIngresoGestion" runat="server" visible="false">
                <div class="card-header">
                    <h3 class="card-title">Seguimiento del Ticket
                    </h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Estado</label>
                                <asp:DropDownList ID="ddlEstado" runat="server" OnDataBound="ddlEstado_DataBound" CssClass="form-control input-sm" OnSelectedIndexChanged="ddlEstado_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>

                            <div class="form-group" id="divMotivoCierre" runat="server" visible="false">
                                <label for="">Motivo Cierre</label>
                                <asp:DropDownList ID="ddlMotivoCierre" runat="server" OnDataBound="ddlMotivoCierre_DataBound" CssClass="form-control input-sm">
                                </asp:DropDownList>
                            </div>

                            <div class="form-group">
                                <label for="">Área resolutora o derivación a</label>
                                <asp:DropDownList ID="ddlArea" runat="server" CssClass="form-control input-sm" OnDataBound="ddlArea_DataBound" OnSelectedIndexChanged="ddlArea_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>

                            <div class="form-group" id="DivResolutor" runat="server">
                                <label for="">Resolutor asignado o Derivado a</label>
                                <asp:DropDownList ID="ddlDerivar" OnDataBound="ddlDerivar_DataBound" runat="server" CssClass="form-control input-sm">
                                </asp:DropDownList>
                            </div>

                            <div class="form-group">
                                <label for="">Adjunto 1</label>
                                <asp:FileUpload ID="fuArchivo1" runat="server" CssClass="form-control input-sm"></asp:FileUpload>

                                <div class="alert-info alert-dismissible" role="alert">
                                    <strong>Archivos permitidos!</strong> PDF, DOC, DOCX, XLSX, XLS, TXT, JPG, GIF, PNG, ZIP y RAR
                                </div>

                            </div>

                        </div>
                        <div class="col-md-8">

                            <div class="form-group">
                                <label for="">Observación</label>
                                <asp:TextBox ID="txtObservacionGestion" runat="server" TextMode="MultiLine" Height="100px" CssClass="form-control input-sm" Rows="20"></asp:TextBox>
                            </div>

                            <div class="form-group">
                                <label for="">Observación cliente</label>
                                <div class="pull-right">
                                    <asp:CheckBox ID="chkEnviarObsCliente" runat="server" AutoPostBack="true" OnCheckedChanged="chkEnviarObsCliente_CheckedChanged" Text="Enviar correo" />
                                </div>
                                <asp:TextBox ID="txtObservacionCliente" runat="server" TextMode="MultiLine" Height="100px" CssClass="form-control input-sm" Rows="20"></asp:TextBox>
                            </div>

                            <div class="form-group">
                                <label for="">Adjunto 2</label>
                                <asp:FileUpload ID="fuArchivo2" runat="server" Width="50%" CssClass="form-control input-sm"></asp:FileUpload>
                            </div>

                        </div>
                    </div>

                </div>
                <div class="card-footer">
                    <div class="form-group">
                        <asp:Button ID="btnGrabarGestion" runat="server" Text="Grabar Gestión" CssClass="btn btn-sm btn-danger"
                            OnClick="btnGrabarGestion_Click" />
                    </div>
                    <asp:Label Visible="false" Text="Ticket se encuentra en uso por " Style="background-color: red; color: white" ID="lblMensajeAtencionControl" runat="server" />
                </div>
            </div>

            <div class="card card-warning card-outline" runat="server" id="divPanelHistoricoCaso" visible="false">
                <div class="card-header">
                    <h3 class="card-title">Histórico del Ticket</h3>
                </div>
                <div class="card-body">
                    <asp:GridView ID="grvGestiones" runat="server" CssClass="table table-bordered table-hover table-condensed small" BorderColor="Transparent" HeaderStyle-CssClass="active" AutoGenerateColumns="false" OnRowDataBound="paginacion_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="Correlativo">
                                <ItemTemplate>
                                    <asp:Label ID="lblCorrelativo" runat="server" Text='<%# Bind("CORRELATIVO") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Fecha">
                                <ItemTemplate>
                                    <asp:Label ID="lblFecha" runat="server" Visible="true" Text='<%# Bind("FECHA") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Usuario Creación">
                                <ItemTemplate>
                                    <asp:Label ID="lblUsuarioCreacion" runat="server" Text='<%# Bind("USUARIO") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Usuario Asig">
                                <ItemTemplate>
                                    <asp:Label ID="lblUsuarioAsignado" runat="server" Text='<%# Bind("USUARIO_ASIG") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Estado">
                                <ItemTemplate>
                                    <asp:Label ID="lblEstado" runat="server" Text='<%# Bind("ESTADO_ATENCION") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Area">
                                <ItemTemplate>
                                    <asp:Label ID="LblArea" runat="server" Text='<%# Bind("Area") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Motivo Cierre">
                                <ItemTemplate>
                                    <asp:Label ID="lblNombreMotivoCierre" runat="server" Text='<%# Bind("NOM_MOTIVO_CIERRE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Archivo 1">
                                <ItemTemplate>
                                    <asp:Label ID="lblArchivo1" runat="server" Visible="false" Text='<%# Bind("RUTA_ARCHIVO_1") %>'></asp:Label>
                                    <asp:ImageButton ID="ibtnArchivo1" runat="server" ImageUrl="~/img/box.png" Visible="false" OnClick="ibtnArchivo1_Click" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Archivo 2">
                                <ItemTemplate>
                                    <asp:Label ID="lblArchivo2" runat="server" Visible="false" Text='<%# Bind("RUTA_ARCHIVO_2") %>'></asp:Label>
                                    <asp:ImageButton ID="ibtnArchivo2" runat="server" ImageUrl="~/img/box.png" Visible="false" OnClick="ibtnArchivo2_Click" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Obs">
                                <ItemTemplate>
                                    <asp:Label ID="lblObservacion" runat="server" Text='<%# Bind("OBSERVACION") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Obs Cli">
                                <ItemTemplate>
                                    <asp:Label ID="lblObservacionCli" runat="server" Text='<%# Bind("OBSERVACION_CLIENTE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>


        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="server">


    
    <script>
        function MostrarModal() {
            $('#myModal').modal('show');
        }

        function numbersonlyRut(e) {
            var unicode = e.charCode ? e.charCode : e.keyCode;
            if (unicode != 75 && unicode != 107) {//letra k y K
                if (unicode < 48 || unicode > 57) //if not a number
                { return false } //disable key press    
            }
        }

        function checkRut(rut)
        {
            // Despejar Puntos
            //alert(rut);
            var valor = rut.value.replace('.', '');
            // Despejar Guión
            valor = valor.replace('-', '');

            // Aislar Cuerpo y Dígito Verificador
            cuerpo = valor.slice(0, -1);
            dv = valor.slice(-1).toUpperCase();

            // Formatear RUN
            rut.value = cuerpo + '-' + dv

            // Si no cumple con el mínimo ej. (n.nnn.nnn)
            if (cuerpo.length < 7) { rut.setCustomValidity("RUT Incompleto"); return false; }

            // Calcular Dígito Verificador
            suma = 0;
            multiplo = 2;

            // Para cada dígito del Cuerpo
            for (i = 1; i <= cuerpo.length; i++) {

                // Obtener su Producto con el Múltiplo Correspondiente
                index = multiplo * valor.charAt(cuerpo.length - i);

                // Sumar al Contador General
                suma = suma + index;

                // Consolidar Múltiplo dentro del rango [2,7]
                if (multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }

            }

            // Calcular Dígito Verificador en base al Módulo 11
            dvEsperado = 11 - (suma % 11);

            // Casos Especiales (0 y K)
            dv = (dv == 'K') ? 10 : dv;
            dv = (dv == 0) ? 11 : dv;

            // Validar que el Cuerpo coincide con su Dígito Verificador
            if (dvEsperado != dv) { rut.setCustomValidity("RUT Inválido"); return false; }

            // Si todo sale bien, eliminar errores (decretar que es válido)
            rut.setCustomValidity('');
        }

        function esLetra(evt)
        {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return true;
            return false;
        }

        function soloNumeros(e)
        {
            var unicode = e.charCode ? e.charCode : e.keyCode;
            if (unicode != 8) {
                if (unicode < 48 || unicode > 57) //if not a number
                { return false } //disable key press    
            }
        }
    </script>

</asp:Content>
