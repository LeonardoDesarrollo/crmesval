﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BuscarTipificaciones.aspx.cs" Inherits="EsvalCrm.BuscarTipificaciones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><asp:Label ID="lblTituloBuscadorTipificaciones" runat="server"></asp:Label></h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Buscar Tipificaciones</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">

            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>


                    <!-- Alertas -->
                    <div id="divAlerta" runat="server" visible="false" class="alert alert-danger">
                        <strong>Atención!: </strong>
                        <asp:Label Text="" ID="lblInfo" runat="server" />
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-success">
                                <div class="box-header">
                                    <h3 class="box-title">Buscar</h3>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <asp:TextBox ID="txtBuscar" runat="server" CssClass="form-control input-sm" placeholder="Buscar por Id Gestión"></asp:TextBox>
                                        </div>
                                        <div class="col-xs-1">
                                            <asp:Button ID="btnBuscar" runat="server" CssClass="btn btn-sm btn-primary"
                                                Text="Buscar" OnClick="btnBuscar_Click" />
                                        </div>
                                        <div class="col-xs-3">
                                            <asp:TextBox ID="txtBuscarPorRut" runat="server" CssClass="form-control input-sm" placeholder="Buscar por Rut"></asp:TextBox>
                                        </div>
                                        <div class="col-xs-1">
                                            <asp:Button ID="btnBuscarPorRut" runat="server" CssClass="btn btn-sm btn-primary"
                                                Text="Buscar" OnClick="btnBuscarPorRut_Click" />
                                        </div>
                                        <div class="col-xs-3">
                                            <asp:TextBox ID="txtBuscarPorNombreCliente" runat="server" CssClass="form-control input-sm" placeholder="Buscar por Cliente"></asp:TextBox>
                                        </div>
                                        <div class="col-xs-1">
                                            <asp:Button ID="btnBuscarPorCliente" runat="server" CssClass="btn btn-sm btn-primary"
                                                Text="Buscar" OnClick="btnBuscarPorCliente_Click" />
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-info">
                                <div class="box-header">
                                    <h3 class="box-title">Buscador Avanzado</h3>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-xs-2">
                                            <label for="ddlEstado">Fecha Inicio</label>
                                            <div class="input-group">
                                                <asp:TextBox ID="txtFechaDesde" runat="server" ClientIDMode="Static" AutoCompleteType="Disabled" CssClass="form-control input-sm class-date"></asp:TextBox>
                                                <asp:Calendar ID="calFechaDesde" runat="server" Visible="false"></asp:Calendar>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-secondary btn-sm" type="button"><span class="glyphicon glyphicon-calendar"></span></button>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-xs-2">
                                            <label for="ddlEstado">Fecha Hasta</label>
                                            <div class="input-group">
                                                <asp:TextBox ID="txtFechaHasta" ClientIDMode="Static" runat="server" AutoCompleteType="Disabled" CssClass="form-control input-sm class-date"></asp:TextBox>
                                                <asp:Calendar ID="calFechaHasta" runat="server" Visible="false"></asp:Calendar>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-secondary btn-sm" type="button"><span class="glyphicon glyphicon-calendar"></span></button>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-xs-2">
                                            <label for="ddlServicio">Empresa</label>
                                            <asp:DropDownList ID="ddlServicio" runat="server" OnDataBound="ddlServicio_DataBound" CssClass="form-control input-sm">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-xs-2" runat="server" visible="false">
                                            <label for="ddlCanal">Canal</label>
                                            <asp:DropDownList ID="ddlCanal" runat="server" OnDataBound="ddlCanal_DataBound" CssClass="form-control input-sm">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-xs-1">
                                            <label for="btnBuscarTicket"></label>
                                            <asp:Button ID="btnBuscarTicket" runat="server" CssClass="btn btn-sm btn-block btn-primary"
                                                Text="Buscar" OnClick="btnBuscarTicket_Click" />
                                        </div>

                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <div class="row">
                        <div class="col-md-12">
                            <div runat="server" id="divGrilla" class="box box-danger" visible="false">
                                <asp:GridView ID="grvTickets" runat="server" CssClass="table table-bordered table-hover table-condensed small" HeaderStyle-CssClass="active" AutoGenerateColumns="false" OnRowDataBound="paginacion_RowDataBound" EmptyDataText="Tipificación no encontrado" EmptyDataRowStyle-CssClass="active h3" PageSize="50" AllowPaging="true">
                                    <Columns>

                                        <asp:TemplateField HeaderText="Id Ticket">
                                            <ItemTemplate>
                                                <asp:Label ID="lblIdTicket" runat="server" Visible="false" Text='<%# Bind("ID_ATENCION") %>'></asp:Label>
                                                <asp:LinkButton ID="lbtnIdTicket" runat="server" Text='<%# Bind("ID_ATENCION") %>' OnClick="lbtnIdTicket_Click"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Rut">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRutCliente" runat="server" Visible="true" Text='<%# Bind("RUT_CLIENTE") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Nombre">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNombreCliente" runat="server" Visible="true" Text='<%# Bind("NOMBRE_CLIENTE") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="U.Creación">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUsuarioCreacion" runat="server" Text='<%# Bind("NOMBRE") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="F.Creación">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFechaCreacion" runat="server" Text='<%# Bind("FECHA_INGRESO") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Empresa">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEmpresa" runat="server" Text='<%# Bind("EMPRESA") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Canal">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCanal" runat="server" Text='<%# Bind("CANAL") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Tipo">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTipo" runat="server" Text='<%# Bind("TIPO_TIP") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Tipo de ticket">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNivel1" runat="server" Text='<%# Bind("NIVEL_1") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Tipo de gestión">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNivel2" runat="server" Text='<%# Bind("NIVEL_2") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Categoría de la gestión">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNivel3" runat="server" Text='<%# Bind("NIVEL_3") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Detalle de la categoría">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNivel4" runat="server" Text='<%# Bind("NIVEL_4") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Ins.">
                                            <ItemTemplate>
                                                <asp:Label ID="lblInsistencias" runat="server" Text='<%# Bind("INSISTENCIAS") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Obs">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="ibtnObservacion" runat="server" OnClick="ibtnObservacion_Click" ImageUrl="~/img/page.png" ToolTip='<%# Bind("OBSERVACION") %>' />
                                                <asp:Label ID="lblObservacion" runat="server" Visible="false" Text='<%# Bind("OBSERVACION") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Adjunto">
                                            <ItemTemplate>
                                                <asp:Label ID="lblArchivo" runat="server" Visible="false" Text='<%# Bind("RUTA_ARCHIVO") %>'></asp:Label>
                                                <asp:LinkButton ID="btnArchivo" runat="server" OnClick="btnArchivo_Click" Text="text"><i class="fa fa-files-o"></i></asp:LinkButton>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>

                                    <PagerTemplate>
                                        <div>
                                            <div style="float: left">
                                                <asp:ImageButton ID="imgFirst" runat="server"
                                                    ImageUrl="~/img/grid/first.gif" OnClick="imgFirst_Click"
                                                    Style="height: 15px" title="Navegación: Ir a la Primera Pagina" Width="26px" />
                                                <asp:ImageButton ID="imgPrev" runat="server"
                                                    ImageUrl="~/img/grid/prev.gif" OnClick="imgPrev_Click"
                                                    title="Navegación: Ir a la Pagina Anterior" Width="26px" />
                                                <asp:ImageButton ID="imgNext" runat="server"
                                                    ImageUrl="~/img/grid/next.gif" OnClick="imgNext_Click"
                                                    title="Navegación: Ir a la Siguiente Pagina" Width="26px" />
                                                <asp:ImageButton ID="imgLast" runat="server"
                                                    ImageUrl="~/img/grid/last.gif" OnClick="imgLast_Click"
                                                    title="Navegación: Ir a la Ultima Pagina" Width="26px" />
                                            </div>

                                            <div style="float: left">
                                                Registros por página: 50
                                            </div>

                                            <div style="float: right">
                                                Total Registros: 
                                    <asp:Label ID="lblTotalRegistros" runat="server"></asp:Label>
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                    Página
                                    <asp:Label ID="lblPagina" runat="server"></asp:Label>
                                                de
                                    <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </PagerTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->




                    <asp:Panel ID="Panel1" runat="server" CssClass="modal fade bs-example-modal-lg" TabIndex="-1" role="dialog" aria-labelledby="myLabel">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="modal-header">
                                            <h4 class="modal-title" id=" myLabel ">Observación del Ticket</h4>
                                        </div>
                                        <div class="modal-body">
                                            <asp:Label ID="lblObservacionTicket" runat="server" CssClass="form-control input-sm"></asp:Label>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </asp:Panel>



                </ContentTemplate>
            </asp:UpdatePanel>


        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="server">
</asp:Content>
