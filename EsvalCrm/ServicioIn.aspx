﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ServicioIn.aspx.cs" Inherits="EsvalCrm.ServicioIn" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Servicio</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Administración</a></li>
                        <li class="breadcrumb-item active">Servicio</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <!-- Alertas -->
            <div id="divAlerta" runat="server" visible="false" class="alert alert-danger alerta">
                <strong>Atención!: </strong>
                <asp:Label Text="" ID="lblInfo" runat="server" />
            </div>

            <div class="card card-danger card-outline">
                <div class="card-header">
                    <h3 class="card-title">
                        <asp:Label ID="lblIdServicio" runat="server" Text=""></asp:Label>
                    </h3>
                </div>
                <div class="card-body">

                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <asp:HiddenField ID="HfId" runat="server" />
                                <label for="TxtServicio">Servicio:</label>
                                <asp:TextBox ID="TxtServicio" runat="server" MaxLength="100" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-4" runat="server" visible="false">
                            <div class="form-group">
                                <label for="DdlScript">Script:</label>
                                <asp:DropDownList ID="DdlScript" runat="server" CssClass="form-control input-sm" OnDataBound="DdlScript_DataBound">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="ddlActivo">Activo:</label>
                                <asp:DropDownList ID="ddlActivo" runat="server" Width="150px" CssClass="form-control input-sm">
                                    <asp:ListItem Text="Si" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="row" runat="server" visible="false">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="TxtIdSoftCall">ID SoftCall:</label>
                                <asp:TextBox ID="TxtIdSoftCall" runat="server" MaxLength="30" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="card card-primary card-" runat="server" id="DivNotificaciones">
                        <div class="card-header with-border">
                            <h3 class="card-title">Lista de distribución</h3>
                        </div>

                        <div class="card-body">

                            <div class="row">

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="TxtNombre">Nombre:</label>
                                        <asp:TextBox ID="TxtNombre" runat="server" MaxLength="80" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                    <asp:Button ID="btnGuardarDestinatario" Text="Guardar Destinatario" CssClass="btn btn-info" OnClick="btnGuardarDestinatario_Click" runat="server" />
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="TxtEmail">Email:</label>
                                        <asp:TextBox ID="TxtEmail" runat="server" MaxLength="50" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="TxtTelefono">Teléfono:</label>
                                        <asp:TextBox ID="TxtTelefono" runat="server" MaxLength="9" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <br />
                                    <asp:GridView ID="GrvNofificacion" runat="server" ClientIDMode="Static" CssClass="table table-hover table-sm table-bordered text-sm"
                                        AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" EmptyDataText="¡No se encontraron registros.!">
                                        <Columns>
                                            <asp:TemplateField HeaderText="ID" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="LblIdServicioNoti" runat="server" Text='<%# Bind("ID_SERVICIO") %>' Visible="true"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Identificador" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="LblIdentificador" runat="server" Text='<%# Bind("IDENTIFICADOR") %>' Visible="true"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Nombre">
                                                <ItemTemplate>
                                                    <asp:Label ID="LblNombre" runat="server" Text='<%# Bind("NOMBRE") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Email">
                                                <ItemTemplate>
                                                    <asp:Label ID="LblEmail" runat="server" Text='<%# Bind("EMAIL") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Teléfono">
                                                <ItemTemplate>
                                                    <asp:Label ID="LblTelefono" runat="server" Text='<%# Bind("TELEFONO") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Width="7%">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="btnEliminar" ToolTip="Eliminar" OnClick="btnEliminar_Click" OnClientClick="return confirm('¿Desea eliminar el registro?');" ForeColor="White" runat="server" CssClass="btn btn-xs btn-danger"><span class="fa fa-eraser"></span></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
                <div class="card-footer">
                    <asp:Button ID="btnGuardar" Text="Guardar" CssClass="btn btn-success" OnClick="btnGuardar_Click" runat="server" />
                    <asp:Button ID="btnCancelar" Text="Cancelar" CssClass="btn btn-default" OnClick="btnCancelar_Click" runat="server" />
                </div>
            </div>

        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="server">
</asp:Content>
