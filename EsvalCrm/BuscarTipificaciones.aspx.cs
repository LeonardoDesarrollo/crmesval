﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;

namespace EsvalCrm
{
    public partial class BuscarTipificaciones : System.Web.UI.Page
    {
        Datos dal = new Datos();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.Page.IsPostBack)
                {
                    string perfil = Session["variableIdPerfil"].ToString();
                    string idUsuario = Session["variableIdUsuario"].ToString();
                    string verTodos = Session["variableVerTodos"].ToString();

                    canal();
                    empresa();

                    string _idEstatus = Convert.ToString(Request.QueryString["e"]);
                    if (_idEstatus != null)
                    {
                        if (verTodos == "1")
                        {
                            buscarTipificacionPorIdEstatus(_idEstatus, null);
                        }
                        else
                        {
                            buscarTipificacionPorIdEstatus(_idEstatus, idUsuario);
                        }
                    }

                    if (Session["strTituloBuscadorTipificacion"] == null)
                    {
                        lblTituloBuscadorTipificaciones.Text = "Buscador Tipificacion";
                    }
                    else
                    {
                        lblTituloBuscadorTipificaciones.Text = Session["strTituloBuscadorTipificacion"].ToString();
                    }

                    Session["strTituloBuscadorTipificacion"] = null;
                }
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Visible = true;
            }
        }

        protected void ddlCanal_DataBound(object sender, EventArgs e)
        {
            ddlCanal.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        }

        protected void ddlServicio_DataBound(object sender, EventArgs e)
        {
            ddlServicio.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        }

        void canal()
        {
            ddlCanal.DataSource = dal.getBuscarCanal(null);
            ddlCanal.DataValueField = "ID_CANAL";
            ddlCanal.DataTextField = "CANAL";
            ddlCanal.DataBind();
        }

        void empresa()
        {
            ddlServicio.DataSource = dal.getBuscarEmpresa(null);
            ddlServicio.DataTextField = "EMPRESA";
            ddlServicio.DataValueField = "ID_EMPRESA";
            ddlServicio.DataBind();
        }


        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                string buscar = txtBuscar.Text.Trim();
                if (buscar.Length < 1)
                {
                    lblInfo.Text = "Texto de busqueda debe tener al menos 1 caracteres";
                    divAlerta.Attributes["class"] = "alert alert-warning";
                    divAlerta.Visible = true;
                    return;
                }

                divGrilla.Visible = true;
                //string idUsuario = buscarIdUsuario();


                DataTable dt = new DataTable();
                string verTodos = Session["variableVerTodos"].ToString();
                string idUsuario = Session["variableIdUsuario"].ToString();

                if (verTodos == "1")
                {
                    dt = dal.getBuscarGestionesBuscador(null, txtBuscar.Text.Trim(), null, "", null, null, null, null).Tables[0];
                }
                else
                {
                    dt = dal.getBuscarGestionesBuscador(idUsuario, txtBuscar.Text.Trim(), null, "", null, null, null, null).Tables[0];
                }


                Session["sessionDtTipificacion"] = dt;
                grvTickets.DataSource = dt;
                grvTickets.DataBind();

                //buscarGr();
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void btnBuscarPorRut_Click(object sender, EventArgs e)
        {
            try
            {
                string buscar = txtBuscarPorRut.Text.Trim();
                if (buscar.Length < 1)
                {
                    lblInfo.Text = "Texto de busqueda debe tener al menos 1 caracteres";
                    divAlerta.Attributes["class"] = "alert alert-warning";
                    divAlerta.Visible = true;
                    return;
                }

                divGrilla.Visible = true;

                //string idUsuario = buscarIdUsuario();
                DataTable dt = new DataTable();


                string verTodos = Session["variableVerTodos"].ToString();
                string idUsuario = Session["variableIdUsuario"].ToString();

                if (verTodos == "1")
                {
                    dt = dal.getBuscarGestionesBuscador(null, null, txtBuscarPorRut.Text.Trim(), "", null, null, null, null).Tables[0];

                }
                else
                {
                    dt = dal.getBuscarGestionesBuscador(idUsuario, null, txtBuscarPorRut.Text.Trim(), "", null, null, null, null).Tables[0];

                }


                Session["sessionDtTipificacion"] = dt;
                grvTickets.DataSource = dt;
                grvTickets.DataBind();

                //buscarGr();
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void btnBuscarPorCliente_Click(object sender, EventArgs e)
        {
            try
            {
                string buscar = txtBuscarPorNombreCliente.Text.Trim();
                if (buscar.Length < 1)
                {
                    lblInfo.Text = "Texto de busqueda debe tener al menos 1 caracteres";
                    divAlerta.Attributes["class"] = "alert alert-warning";
                    divAlerta.Visible = true;
                    return;
                }

                divGrilla.Visible = true;
                //string idUsuario = buscarIdUsuario();


                DataTable dt = new DataTable();


                string verTodos = Session["variableVerTodos"].ToString();
                string idUsuario = Session["variableIdUsuario"].ToString();

                if (verTodos == "1")
                {
                    dt = dal.getBuscarGestionesBuscador(null, null, null, txtBuscarPorNombreCliente.Text.Trim(), txtFechaDesde.Text, txtFechaHasta.Text, null, null).Tables[0];


                }
                else
                {
                    dt = dal.getBuscarGestionesBuscador(idUsuario, null, null, txtBuscarPorNombreCliente.Text.Trim(), txtFechaDesde.Text, txtFechaHasta.Text, null, null).Tables[0];

                }



                Session["sessionDtTipificacion"] = dt;
                grvTickets.DataSource = dt;
                grvTickets.DataBind();
                //buscarGr();
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void btnBuscarTicket_Click(object sender, EventArgs e)
        {
            try
            {
                //divResumen.Visible = false;
                divGrilla.Visible = true;
                //buscarTicket(ddlEstado.SelectedValue, null);


                DataTable dt = new DataTable();


                string verTodos = Session["variableVerTodos"].ToString();
                string idUsuario = Session["variableIdUsuario"].ToString();

                if (verTodos == "1")
                {
                    dt = dal.getBuscarGestionesBuscador(null, null, null, "", txtFechaDesde.Text.Trim(), txtFechaHasta.Text.Trim(), ddlServicio.SelectedValue, ddlCanal.SelectedValue).Tables[0];

                }
                else
                {
                    dt = dal.getBuscarGestionesBuscador(idUsuario, null, null, "", txtFechaDesde.Text.Trim(), txtFechaHasta.Text.Trim(), ddlServicio.SelectedValue, ddlCanal.SelectedValue).Tables[0];

                }


                Session["sessionDtTipificacion"] = dt;
                grvTickets.DataSource = dt;
                grvTickets.DataBind();
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void lbtnIdTicket_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtn = sender as LinkButton;
                GridViewRow row = (GridViewRow)lbtn.NamingContainer;
                Label _lblIdTicket = (Label)grvTickets.Rows[row.RowIndex].FindControl("lblIdTicket");
                Response.Redirect("SeguimientoTicket.aspx?t=" + _lblIdTicket.Text);
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void ibtnObservacion_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton btn = (ImageButton)sender;
                GridViewRow row = (GridViewRow)btn.NamingContainer;
                Label _lblObservacion = (Label)grvTickets.Rows[row.RowIndex].FindControl("lblObservacion");

                if (string.IsNullOrEmpty(_lblObservacion.Text))
                {
                    lblObservacionTicket.Text = string.Empty;
                }
                else
                {
                    lblObservacionTicket.Text = _lblObservacion.Text;
                }

                ScriptManager.RegisterStartupScript(UpdatePanel2, UpdatePanel2.GetType(), "show", "$(function () { $('#" + Panel1.ClientID + "').modal('show'); });", true);
                UpdatePanel2.Update();
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void btnArchivo_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                GridViewRow row = (GridViewRow)btn.NamingContainer;
                Label _lblArchivo = (Label)grvTickets.Rows[row.RowIndex].FindControl("lblArchivo");
                ScriptManager.RegisterStartupScript(this, this.GetType(), UniqueID, "window.open('" + _lblArchivo.Text + "','_blank');", true);
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        void buscarTipificacionPorIdEstatus(string idEstatus, string idUsuario)
        {
            DataTable dt = new DataTable();
            dt = dal.getBuscarTipificacionesPorIdEstatus(idEstatus, idUsuario).Tables[0];
            Session["sessionDtTipificacion"] = dt;
            grvTickets.DataSource = dt;
            grvTickets.DataBind();

            //divResumen.Visible = false;
            divGrilla.Visible = true;
        }



        protected void imgFirst_Click(object sender, EventArgs e)
        {
            //buscar();
            if (Session["sessionDtTipificacion"] != null)
            {
                grvTickets.DataSource = Session["sessionDtTipificacion"];
                grvTickets.DataBind();
            }
            else
            {
                //buscar();
            }
            grvTickets.PageIndex = 0;
            grvTickets.DataBind();
        }

        protected void imgPrev_Click(object sender, EventArgs e)
        {
            //buscar();
            if (Session["sessionDtTipificacion"] != null)
            {
                grvTickets.DataSource = Session["sessionDtTipificacion"];
                grvTickets.DataBind();
            }
            else
            {
                //buscar();
            }
            if (grvTickets.PageIndex != 0)
                grvTickets.PageIndex--;
            grvTickets.DataBind();
        }

        protected void imgNext_Click(object sender, EventArgs e)
        {
            //buscar();
            if (Session["sessionDtTipificacion"] != null)
            {
                grvTickets.DataSource = Session["sessionDtTipificacion"];
                grvTickets.DataBind();
            }
            else
            {
                //buscar();
            }

            if (grvTickets.PageIndex != (grvTickets.PageCount - 1))
                grvTickets.PageIndex++;
            grvTickets.DataBind();
        }

        protected void imgLast_Click(object sender, EventArgs e)
        {
            //buscar();
            if (Session["sessionDtTipificacion"] != null)
            {
                grvTickets.DataSource = Session["sessionDtTipificacion"];
                grvTickets.DataBind();
            }
            else
            {
                //buscar();
            }

            grvTickets.PageIndex = grvTickets.PageCount - 1;
            grvTickets.DataBind();
        }

        protected void paginacion_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label _lblArchivo = (Label)e.Row.FindControl("lblArchivo");

                LinkButton _btnArchivo = (LinkButton)e.Row.FindControl("btnArchivo");

                if (string.IsNullOrEmpty(_lblArchivo.Text) == true)
                {
                    _btnArchivo.Visible = false;
                }
                else
                {
                    _btnArchivo.Visible = true;
                }
            }

            if (e.Row.RowType == DataControlRowType.Pager)
            {
                Label _lblPagina = (Label)e.Row.FindControl("lblPagina");
                Label _lblTotal = (Label)e.Row.FindControl("lblTotal");
                Label _lblTotalRegistros = (Label)e.Row.FindControl("lblTotalRegistros");
                _lblPagina.Text = Convert.ToString(grvTickets.PageIndex + 1);
                _lblTotal.Text = Convert.ToString(grvTickets.PageCount);

                DataTable dt = new DataTable();
                dt = Session["sessionDtTipificacion"] as DataTable;
                _lblTotalRegistros.Text = dt.Rows.Count.ToString();
            }
        }
    }
}