﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DocumentoIn.aspx.cs" Inherits="EsvalCrm.DocumentoIn" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Documento</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Administración</a></li>
                        <li class="breadcrumb-item active">Documento</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <!-- Alertas -->
            <div id="divAlerta" runat="server" visible="false" class="alert alert-danger">
                <strong>Atención!: </strong>
                <asp:Label Text="" ID="lblInfo" runat="server" />
            </div>

            <asp:HiddenField ID="HdId" runat="server" />
            <div class="card card-danger card-outline">
                <div class="card-header">
                    Documento 
                <asp:Label ID="LblId" runat="server" CssClass="label label-primary" />
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group input-group-sm mb-3">
                                <div class="input-group-prepend">
                                    <label class="input-group-text">Nombre:</label>
                                </div>
                                <asp:TextBox ID="TxtNombre" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="input-group  input-group-sm mb-3">
                                <div class="input-group-prepend">
                                    <label class="input-group-text">Descripción:</label>
                                </div>
                                <asp:TextBox ID="TxtDescripcion" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>

                        </div>
                        <div class="col-md-2">
                            <div class="input-group input-group-sm mb-3">
                                <div class="input-group-prepend">
                                    <label class="input-group-text">Estado:</label>
                                </div>
                                <asp:DropDownList ID="ddlEstado" runat="server" CssClass="form-control">
                                    <asp:ListItem Text="Vigente" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="No Vigente" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                            </div>

                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group input-group-sm mb-3">
                                <div class="input-group-prepend">
                                    <label class="input-group-text">Categoría:</label>
                                </div>
                                <asp:DropDownList ID="DdlCategoria" runat="server" CssClass="form-control " OnDataBound="DdlCategoria_DataBound"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group input-group-sm mb-3">
                                <div class="input-group-prepend">
                                    <label class="input-group-text">Archivo:</label>
                                </div>
                                <asp:FileUpload ID="FuArchivo" runat="server" CssClass="form-control" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <asp:HyperLink ID="HlArchivo" runat="server" CssClass="label"></asp:HyperLink>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group input-group-sm mb-3">
                                <div class="input-group-prepend">
                                    <label class="input-group-text">Palabra Clave:</label>
                                </div>
                                <asp:TextBox ID="TxtPalabraClave" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>

                        </div>
                    </div>

                </div>
                <div class="card-footer">
                    <asp:Button ID="BtnGrabar" runat="server" OnClick="BtnGrabar_Click" ForeColor="White" Text="Grabar" CssClass="btn btn-success" />
                    <asp:Button ID="BtnCancelar" runat="server" OnClick="BtnCancelar_Click"  Text="Cancelar" CssClass=" btn btn-default" />
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="server">
</asp:Content>
