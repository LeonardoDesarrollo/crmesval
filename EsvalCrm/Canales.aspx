﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Canales.aspx.cs" Inherits="EsvalCrm.Canales" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">



    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Canales</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                       <li class="breadcrumb-item"><a href="#">Administración</a></li>
                        <li class="breadcrumb-item"><a href="#">Mantenedores</a></li>
                        <li class="breadcrumb-item active">Canales</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <!-- Alertas -->
            <div id="divAlerta" runat="server" visible="false" class="alert alert-danger">
                <strong>Atención!: </strong>
                <asp:Label Text="" ID="lblInfo" runat="server" />
            </div>


            <div runat="server" id="divCanales" class="card card-danger card-outline">
                <div class="card-header">
                    <asp:LinkButton ID="btnNuevo" OnClick="btnNuevo_Click" runat="server" CssClass="btn btn-xs btn-success" ForeColor="White"><span class="fa fa-plus"></span></asp:LinkButton>
                </div>
                <div class="card-body">
                    <asp:GridView ID="grvCanales" runat="server" ClientIDMode="Static" CssClass="table table-sm table-hover text-sm table-bordered"
                        AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
                        <Columns>
                            <asp:TemplateField HeaderText="ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblIdCanal" runat="server" Text='<%# Bind("ID_CANAL") %>' Visible="true"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Canal">
                                <ItemTemplate>
                                    <asp:Label ID="lblCanal" runat="server" Text='<%# Bind("CANAL") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Adicional 1">
                                <ItemTemplate>
                                    <asp:Label ID="lblAdicional1" runat="server" Text='<%# Bind("ADICIONAL_1") %>'></asp:Label>
                                    <br />
                                    <asp:CheckBox ID="chkAdicional1" Text="-Requerido" Enabled="false" runat="server" Checked='<%# Convert.ToBoolean(Eval("OBL_ADIC_1")) %>' />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Adicional 2">
                                <ItemTemplate>
                                    <asp:Label ID="lblAdicional2" runat="server" Visible="true" Text='<%# Bind("ADICIONAL_2") %>'></asp:Label>
                                    <br />

                                    <asp:CheckBox ID="chkAdicional2" Text="-Requerido" Enabled="false" runat="server" Checked='<%# Convert.ToBoolean(Eval("OBL_ADIC_2")) %>' />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Adicional 3">
                                <ItemTemplate>
                                    <asp:Label ID="lblAdicional3" runat="server" Text='<%# Bind("ADICIONAL_3") %>'></asp:Label>
                                    <br />

                                    <asp:CheckBox ID="chkAdicional3" Text="-Requerido" Enabled="false" runat="server" Checked='<%# Convert.ToBoolean(Eval("OBL_ADIC_3")) %>' />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Adicional 4">
                                <ItemTemplate>
                                    <asp:Label ID="lblAdicional4" runat="server" Text='<%# Bind("ADICIONAL_4") %>'></asp:Label>
                                    <br />

                                    <asp:CheckBox ID="chkAdicional4" Text="-Requerido" Enabled="false" runat="server" Checked='<%# Convert.ToBoolean(Eval("OBL_ADIC_4")) %>' />
                                </ItemTemplate>
                            </asp:TemplateField>



                            <asp:TemplateField HeaderText="Activo">
                                <ItemTemplate>
                                    <asp:Label ID="lblActivo" runat="server" Visible="false" Text='<%# Bind("ACTIVO") %>'></asp:Label>
                                    <asp:Label ID="lblActivo2" runat="server" Text='<%# Bind("ACTIVO2") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderStyle-Width="7%">
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnEditar" OnClick="btnEditar_Click" runat="server" ToolTip="Editar" CssClass="btn btn-xs btn-primary" ForeColor="White"><span class="fa fa-edit"></span></asp:LinkButton>
                                    <asp:LinkButton ID="btnEliminar" OnClick="btnEliminar_Click" OnClientClick="return confirm('¿Desea eliminar el registro?');" ForeColor="White" runat="server" ToolTip="Eliminar" CssClass="btn btn-xs btn-danger"><span class="fa fa-eraser"></span></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>


                    </asp:GridView>
                </div>
            </div>

            <div runat="server" id="divAddEditCanales" class="card card-danger card-outline" visible="false">
                <div class="card-header">
                </div>
                <div class="card-body">

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <asp:HiddenField ID="hfIdCanal" runat="server" />
                                <label for="txtCanal">Canal:</label>
                                <asp:TextBox ID="txtCanal" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label for="txtAdicional1">Adicional 1:</label>
                                <asp:TextBox ID="txtAdicional1" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                <asp:CheckBox Text="Requerido" ID="chkAdicional1" runat="server" />
                            </div>
                            <div class="form-group">
                                <label for="txtAdicional2">Adicional 2:</label>
                                <asp:TextBox ID="txtAdicional2" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                <asp:CheckBox Text="Requerido" ID="chkAdicional2" runat="server" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="txtAdicional3">Adicional 3:</label>
                                <asp:TextBox ID="txtAdicional3" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                <asp:CheckBox Text="Requerido" ID="chkAdicional3" runat="server" />
                            </div>

                            <div class="form-group">
                                <label for="txtAdicional4">Adicional 4:</label>
                                <asp:TextBox ID="txtAdicional4" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                <asp:CheckBox Text="Requerido" ID="chkAdicional4" runat="server" />
                            </div>


                            <div class="form-group">
                                <label for="ddlActivo">Activo:</label>
                                <asp:DropDownList ID="ddlActivo" runat="server" Width="150px" CssClass="form-control input-sm">
                                    <asp:ListItem Text="Si" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="card-footer">
                    <asp:Button ID="btnGuardar" Text="Guardar" CssClass="btn btn-success" OnClick="btnGuardar_Click" runat="server" />
                    <asp:Button ID="btnCancelar" Text="Cancelar" CssClass="btn btn-default" OnClick="btnCancelar_Click" runat="server" />
                </div>
            </div>


        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="server">
</asp:Content>
