﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="IngresarTicket.aspx.cs" Inherits="EsvalCrm.IngresarTicket" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Ingreso de Ticket</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Gestiones</a></li>
                        <li class="breadcrumb-item active">Ingreso de Ticket</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">

            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <!-- Alertas -->
                    <div id="divAlerta" runat="server" visible="false" class="alert alert-danger">
                        <strong>Atención!: </strong>
                        <asp:Label Text="" ID="lblInfo" runat="server" />
                    </div>

                    <asp:HiddenField ID="hfCero" runat="server" />
                    <asp:HiddenField ID="hfUno" runat="server" />
                    <asp:HiddenField ID="hfDos" runat="server" />
                    <asp:HiddenField ID="hfTres" runat="server" />
                    <asp:HiddenField ID="hfCuatro" runat="server" />
                    <asp:HiddenField ID="hfIdGestion" runat="server" />
                    <asp:HiddenField ID="hfIdLlamada" runat="server" />
                    <asp:HiddenField ID="hfTelefonoIVR" runat="server" />
                    <asp:HiddenField ID="hfOpcionIVR" runat="server" />
                    <asp:HiddenField ID="hfServicioIVR" runat="server" />

                    <div class="row">
                        <div class="col-4">
                            <div class="card card-primary">
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <%--<asp:ImageButton ID="ibtnObservacion" runat="server" OnClick="ibtnObservacion_Click" ImageUrl="~/img/page.png" ToolTip=' + lblScript.text + '/>--%>
                                            <%--<asp:ImageButton ID="ibtnObservacion" runat="server" ImageUrl="~/img/page.png" ToolTip="'" + lblScript.text + "'"/>--%>
                                            <asp:Label ID="lblScript" runat="server" Text="" Visible="false"></asp:Label>
                                            <asp:Label ID="lblRutaArch1" runat="server" Text="" Visible="false"></asp:Label>
                                            <asp:LinkButton ID="btnArchivo1" runat="server" Visible="false" OnClick="btnArchivo1_Click" Text="text"><i class="fa fa-files-o"></i></asp:LinkButton>
                                            <div class="input-group">
                                                <span class="input-group-addon"><strong>Servicio:</strong></span>
                                                <asp:DropDownList ID="ddlServicio" runat="server" CssClass="custom-select custom-select-sm select2" Width="100%" OnDataBound="ddlServicio_DataBound" OnDataBinding="ddlServicio_DataBinding" AutoPostBack="true" OnSelectedIndexChanged="ddlServicio_SelectedIndexChanged"></asp:DropDownList>
                                                <%--<asp:Label ID="lblRutaArch1" runat="server" Text="" Visible="false"></asp:Label>
                                            <asp:LinkButton ID="btnArchivo1" runat="server" Visible="false" OnClick="btnArchivo1_Click" Text="text"><i class="fa fa-files-o"></i></asp:LinkButton>--%>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <br />
                                        <div class="col-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><strong>Emisor:</strong></span>
                                                <asp:DropDownList ID="DdlEmisor" runat="server" CssClass="custom-select custom-select-sm select2" Width="100%" OnDataBound="DdlEmisor_DataBound" OnSelectedIndexChanged="DdlEmisor_SelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <br />
                                        <div class="col-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><strong>Canal:</strong></span>
                                                <asp:DropDownList ID="ddlCanal" runat="server" CssClass="custom-select custom-select-sm select2" Width="100%" OnDataBound="ddlCanal_DataBound" OnSelectedIndexChanged="ddlCanal_SelectedIndexChanged1" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <br />
                                        <div class="col-12">
                                            <strong>Id Cliente:</strong>
                                            <asp:TextBox ID="txtIdCliente" runat="server" MaxLength="7" onkeypress="return soloNumeros(event);" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <br />
                                        <div class="col-12">
                                            <strong>N° Atención:</strong>
                                            <asp:TextBox ID="txtNroAtencion" runat="server" MaxLength="7" onkeypress="return soloNumeros(event);" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                            <div class="card card-primary card-" runat="server" id="divTipificacion">
                                <div class="card-header with-border">
                                    <h3 class="card-title">Tipificación</h3>
                                    <div class="text-right">
                                        <asp:Button ID="BtnVerScript" runat="server" Text="Ver Ayuda" CssClass="btn btn-warning btn-xs" OnClick="BtnVerScript_Click" />
                                    </div>

                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="input-group input-group-sm mb-3 ">
                                                    <div class="input-group-prepend">
                                                        <label class="input-group-text" for="inputGroupSelect01">Nivel 1:</label>
                                                    </div>
                                                    <asp:DropDownList ID="ddlNivel1" runat="server" CssClass="custom-select custom-select-sm" OnSelectedIndexChanged="ddlNivel1_SelectedIndexChanged" OnDataBound="ddlNivel1_DataBound" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="input-group input-group-sm mb-3 ">
                                                    <div class="input-group-prepend">
                                                        <label class="input-group-text" for="inputGroupSelect01">Nivel 2:</label>
                                                    </div>
                                                    <asp:DropDownList ID="ddlNivel2" runat="server" CssClass="custom-select custom-select-sm" OnSelectedIndexChanged="ddlNivel2_SelectedIndexChanged" OnDataBound="ddlNivel2_DataBound" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="input-group input-group-sm mb-3 ">
                                                    <div class="input-group-prepend">
                                                        <label class="input-group-text" for="inputGroupSelect01">Nivel 3:</label>
                                                    </div>
                                                    <asp:DropDownList ID="ddlNivel3" runat="server" CssClass="custom-select custom-select-sm" OnSelectedIndexChanged="ddlNivel3_SelectedIndexChanged" OnDataBound="ddlNivel3_DataBound" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="input-group input-group-sm mb-3 ">
                                                    <div class="input-group-prepend">
                                                        <label class="input-group-text" for="inputGroupSelect01">Nivel 4:</label>
                                                    </div>
                                                    <asp:DropDownList ID="ddlNivel4" runat="server" CssClass="custom-select custom-select-sm" OnSelectedIndexChanged="ddlNivel4_SelectedIndexChanged" OnDataBound="ddlNivel4_DataBound" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <br />
                                        <div class="col-12">
                                            <div class="form-group">
                                                <div class="input-group" runat="server" visible="false" id="divTipo">
                                                    <span class="input-group-addon"><strong>Tipo:</strong></span>
                                                    <asp:DropDownList ID="ddlTipo" Style="width: 100%;" runat="server" CssClass="custom-select custom-select-sm select2">
                                                        <asp:ListItem Text="Derivar" Value="D"></asp:ListItem>
                                                        <asp:ListItem Text="Cerrar" Value="P"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group" runat="server" visible="false" id="divTicketSeleccionar">
                                                    <label>Ticket</label>
                                                    <asp:DropDownList ID="ddlTicketSeleccionar" Style="width: 100%;" runat="server" CssClass="custom-select custom-select-sm" OnDataBound="ddlTicketSeleccionar_DataBound"></asp:DropDownList>
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                </div>
                            </div>

                        </div>
                        <div class="col-8">
                            <div class="card card-success">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-4">
                                            <asp:RadioButtonList ID="RadioButtonList1" Visible="false" runat="server" AutoPostBack="true" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged">
                                                <asp:ListItem Selected="True">Titular</asp:ListItem>
                                                <asp:ListItem>No Titular</asp:ListItem>
                                            </asp:RadioButtonList>

                                            <div class="input-group input-group-sm">
                                                <asp:TextBox ID="txtBuscarCliente" runat="server" OnTextChanged="txtBuscarCliente_TextChanged" AutoPostBack="true" CssClass="form-control" placeholder="Buscar Cliente"></asp:TextBox>
                                                <div class=" input-group-append">
                                                    <asp:Button ID="btnBuscarCliente" runat="server" CssClass="btn btn-primary" Text="Buscar" OnClick="btnBuscarCliente_Click" />
                                                </div>
                                            </div>
                                            <div class="p-2 mb-1 bg-warning text-dark" role="alert">
                                                <strong><i class="fa fa-info-circle"></i></strong>Poner 99999999-9 si no se tiene info del Rut.
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <asp:Button ID="btnAgregarCliente" OnClick="btnAgregarCliente_Click" Text="+" CssClass="btn btn-success" runat="server" Visible="false" />
                                            <asp:Button ID="btnNuevoCliente" OnClick="btnNuevoCliente_Click" Text="Nuevo" CssClass="btn btn-success btn-sm" runat="server" />
                                            <asp:Button ID="btnModificarCliente" OnClick="btnModificarCliente_Click" Text="Editar" CssClass="btn btn-success btn-sm" runat="server" />
                                            <asp:Button ID="btnVerCaseMaker" OnClick="btnVerCaseMaker_Click" Text="Historial CRM anterior" CssClass="btn btn-success d-none" runat="server" />
                                            <asp:Button ID="btnVerHistorialSernac" OnClick="btnVerHistorialSernac_Click" Text="Historial Sernac" CssClass="btn btn-success btn-sm d-none" runat="server" />
                                            <asp:LinkButton ID="lbtnIrHistorialCliente" OnClick="lbtnIrHistorialCliente_Click" ToolTip="Ver Historial Cliente" CssClass="btn btn-success btn-sm" runat="server" Text="Historial" ForeColor="White"></asp:LinkButton>

                                            <div class="input-group input-group-sm mt-2 " id="divIngreseTitular" visible="false" runat="server">
                                                <div class="input-group-append">
                                                    <label class="input-group-text">Ingrese Titular:</label>
                                                </div>

                                                <asp:TextBox ID="txtIngreseTitular" runat="server" CssClass="form-control" OnTextChanged="txtIngreseTitular_TextChanged" AutoPostBack="true" placeholder="Ej:15239481-4"></asp:TextBox>
                                                <asp:Label ID="lblNombreCliTitular" Text="" runat="server" Visible="false" />
                                            </div>
                                        </div>


                                    </div>
                                    <div class="table-responsive" runat="server" id="divResultadoBusqueda">
                                        <asp:Label ID="lblIdContacto" runat="server" Visible="false" Text='<%# Bind("ID_CONTACTO") %>'></asp:Label>
                                        <asp:Label ID="lblRutCliente" runat="server" Visible="false" Text='<%# Bind("RUT_CLIENTE") %>'></asp:Label>
                                        <asp:GridView ID="grvClientes" HeaderStyle-CssClass="active" runat="server" CssClass="table table-bordered table-hover table-condensed small" AutoGenerateColumns="false" EmptyDataText="Cliente no encontrado" EmptyDataRowStyle-CssClass="active h3">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Rut">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRutCliente" runat="server" Text='<%# Bind("RUT_CLIENTE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Nombre">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblNombre" runat="server" Text='<%# Bind("NOMBRE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Dirección">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDireccion" runat="server" Text='<%# Bind("DIRECCION") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Comuna">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblComuna" runat="server" Text='<%# Bind("COMUNA") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Telefono">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTelefono" runat="server" Text='<%# Bind("TELEFONO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Celular">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCelular" runat="server" Text='<%# Bind("CELULAR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Email">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEmail" runat="server" Text='<%# Bind("EMAIL") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="N° Tarjeta">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTarjeta" runat="server" Visible="true" Text='<%# Bind("TARJETA") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Activo">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblActivo" runat="server" Visible="true" Text='<%# Bind("ACTIVO2") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Seleccionar">
                                                    <ItemTemplate>
                                                        <asp:LinkButton Text="" runat="server" ToolTip="Seleccionar Cliente" ID="btnSeleccionarCliente" CssClass="btn btn-success btn-xs" OnClick="btnSeleccionarCliente_Click"><span class="glyphicon glyphicon-ok"></span></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="9%" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>

                                    <div class="table-responsive" id="divClienteSeleccionado" runat="server" visible="false">
                                        <asp:HiddenField ID="hfNombres" runat="server" />
                                        <asp:HiddenField ID="hfPaterno" runat="server" />
                                        <asp:HiddenField ID="hfMaterno" runat="server" />
                                        <asp:HiddenField ID="hfSegmentoCliente" runat="server" />
                                        <asp:HiddenField ID="hfEmail" runat="server" />
                                        <asp:HiddenField ID="hfFechaCompra" runat="server" />
                                        <asp:HiddenField ID="hfSernac" runat="server" />
                                        <asp:HiddenField ID="hfCelular" runat="server" />
                                        <asp:HiddenField ID="hfFijo" runat="server" />
                                        <asp:HiddenField ID="hfLocal" runat="server" />
                                        <asp:HiddenField ID="hfCalle" runat="server" />
                                        <asp:HiddenField ID="hfNumero" runat="server" />
                                        <asp:HiddenField ID="hfResto" runat="server" />
                                        <asp:HiddenField ID="hfComuna" runat="server" />

                                        <table class="table table-condensed table-hover small">
                                            <tr class="success">
                                                <%--<th colspan="13" style="text-align: center"><strong>:: Cliente Seleccionado :: </strong></th>--%>
                                                <th colspan="13" style="text-align: center"><strong>
                                                    <asp:Label ID="lblCliTitularNoTitular" Text="" runat="server" /></td></strong></th>
                                            </tr>
                                            <tr>
                                                <th>Rut</th>
                                                <th>Nombre</th>
                                                <th>Email</th>
                                                <th>Celular</th>
                                                <th>Teléfono</th>
                                                <th></th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblRutCliente2" Text="" runat="server" /></td>
                                                <td>
                                                    <asp:Label ID="lblNombreCliente" Text="" runat="server" /></td>
                                                <td>
                                                    <asp:Label ID="lblEmailCliente" Text="" runat="server" /></td>
                                                <td>
                                                    <asp:Label ID="lblCelular" Text="" runat="server" /></td>
                                                <td>
                                                    <asp:Label ID="lblTelefonoFijo" Text="" runat="server" /></td>
                                                <td>
                                                    <asp:LinkButton ID="bntEliminaClienteSeleccionado" Visible="false" OnClick="bntEliminaClienteSeleccionado_Click" ToolTip="Eliminar selección" Text="text" CssClass="btn btn-xs btn-danger" runat="server"><span class="glyphicon glyphicon-remove"></span></asp:LinkButton></td>
                                            </tr>
                                            <tr>
                                                <th>Calle</th>
                                                <th>Número</th>
                                                <th>Resto</th>
                                                <th>Comuna</th>
                                                <th></th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblCalle" Text="" runat="server" /></td>
                                                <td>
                                                    <asp:Label ID="lblNumero" Text="" runat="server" /></td>
                                                <td>
                                                    <asp:Label ID="lblResto" Text="" runat="server" /></td>
                                                <td>
                                                    <asp:Label ID="lblComuna" Text="" runat="server" /></td>
                                                <th></th>
                                            </tr>
                                        </table>
                                    </div>

                                    <div id="divEnriquecimientoContactabilidad" runat="server" visible="false">
                                        <div class="row">
                                            <div class="col-4 d-none ">
                                                <label>Tipo</label>
                                                <asp:DropDownList ID="ddlRutoEmail" runat="server" CssClass="custom-select custom-select-sm input-sm">
                                                    <asp:ListItem Text="Rut Cliente" Value="RUT"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-4">
                                                <label>Rut</label>
                                                <%--<asp:TextBox ID="txtRutClienteExt" runat="server" MaxLength="10" CssClass="form-control input-sm" placeholder="Rut" onchange="Changed(this)"></asp:TextBox>--%>
                                                <asp:TextBox ID="txtRutClienteExt" runat="server" MaxLength="10" onkeypress="return numbersonlyRut(event);" oninput="checkRut(this)" CssClass="form-control input-sm" placeholder="Rut" OnTextChanged="txtRutClienteExt_TextChanged" AutoPostBack="true"></asp:TextBox>
                                            </div>
                                            <div class="col-4">
                                                <label>Email</label>
                                                <asp:TextBox ID="txtEmailClient" runat="server" CssClass="form-control input-sm" placeholder="Email"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" Display="Dynamic" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtEmailClient" ErrorMessage="formato del email no válido"></asp:RegularExpressionValidator>
                                            </div>
                                            <div class="col-4">
                                                <label>Nombre</label>
                                                <asp:TextBox ID="txtNombreClienteExt" runat="server" onkeypress="return esLetra(event)" CssClass="form-control input-sm" placeholder="Nombre"></asp:TextBox>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-4">
                                                <label>Apellido Paterno</label>
                                                <asp:TextBox ID="txtApellidoParternoClienteExt" runat="server" onkeypress="return esLetra(event)" CssClass="form-control input-sm" placeholder="Apellido Paterno"></asp:TextBox>
                                            </div>
                                            <div class="col-4">
                                                <label>Apellido Materno</label>
                                                <asp:TextBox ID="txtApellidoMaterno" runat="server" onkeypress="return esLetra(event)" CssClass="form-control input-sm" placeholder="Apellido Materno"></asp:TextBox>
                                            </div>
                                            <div class="col-4">
                                                <label>Teléfono</label>
                                                <asp:TextBox ID="txtPhone" runat="server" MaxLength="9" CssClass="form-control input-sm" placeholder="Telefono"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-4">
                                                <label>Celular</label>
                                                <asp:TextBox ID="txtCellPhone" runat="server" MaxLength="9" CssClass="form-control input-sm" placeholder="Celular"></asp:TextBox>
                                            </div>
                                            <div class="col-4">
                                                <label>Calle</label>
                                                <asp:TextBox ID="txtCalle" runat="server" CssClass="form-control input-sm" placeholder="Calle"></asp:TextBox>
                                            </div>
                                            <div class="col-4">
                                                <label>Número</label>
                                                <asp:TextBox ID="txtNumero" runat="server" CssClass="form-control input-sm" onkeypress="return soloNumeros(event);" placeholder="Número"></asp:TextBox>
                                            </div>
                                            <div class="col-4 d-none">
                                                <label>Sernac</label>
                                                <asp:DropDownList ID="ddlSernac" runat="server" Enabled="false" CssClass="custom-select custom-select-sm">
                                                    <asp:ListItem Text="No" Value="N"></asp:ListItem>
                                                    <asp:ListItem Text="Si" Value="S"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-4 d-none">
                                                <label>Cod Local</label>
                                                <asp:TextBox ID="txtLocal" runat="server" Enabled="false" MaxLength="3" CssClass="form-control input-sm" placeholder="Local"></asp:TextBox>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-4">
                                                <label>Resto</label>
                                                <asp:TextBox ID="txtResto" runat="server" CssClass="form-control input-sm" placeholder="Resto"></asp:TextBox>
                                            </div>
                                            <div class="col-4">
                                                <label>Comuna</label>
                                                <%--<asp:TextBox ID="txtComuna" runat="server" CssClass="form-control input-sm"></asp:TextBox>--%>
                                                <asp:DropDownList ID="ddlComuna" runat="server" CssClass="custom-select custom-select-sm" Width="100%" OnDataBound="ddlComuna_DataBound"></asp:DropDownList>
                                            </div>
                                            <div class="col-xs-4">
                                                <br />
                                                <asp:LinkButton ID="lbtnGrabarClienteExt" runat="server" CssClass="btn btn-danger btn-sm" OnClick="lbtnGrabarClienteExt_Click"><span class="glyphicon glyphicon-floppy-disk"></span>Grabar cliente</asp:LinkButton>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>

                            <div class="card card-danger" runat="server" id="divIngresarGestion">
                                <%--<div class="card-header with-border">
                            <h3 class="card-title">Ingresar Gestión</h3>
                        </div>--%>
                                <div class="card-body">
                                    <asp:HiddenField ID="hfIdEmpleado" runat="server" />
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="row">
                                                <div class="col-12 text-sm">
                                                    <label for="txtComentario">Observación</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <asp:TextBox ID="txtComentario" TextMode="MultiLine" Style="resize: none" CssClass="form-control" ClientIDMode="Static" runat="server"
                                                        Width="100%" Height="100px" MaxLength="1000" />
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-6">
                                            <div class="row">
                                                <div class="col-8 text-left text-sm">
                                                    <strong>Texto a responder al cliente</strong>
                                                </div>
                                                <div class="col-4 right text-sm">
                                                    <asp:CheckBox ID="chkEnviarObsCliente" AutoPostBack="true" OnCheckedChanged="chkEnviarObsCliente_CheckedChanged" runat="server" Text="Enviar correo" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <asp:TextBox ID="txtObservacionCliente" TextMode="MultiLine" Style="resize: none" CssClass="form-control" ClientIDMode="Static" runat="server"
                                                        Width="100%" Height="100px" MaxLength="1000" />
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-6 text-sm">
                                            <strong>Archivo Adjunto 1</strong>
                                            <%--                                            <div class="input-group input-group-sm">
                                                <div class="custom-file">
                                                    <asp:FileUpload ID="fuArchivo" CssClass="custom-file-input" runat="server" ClientIDMode="Static" />                                                    
                                                    <label class="custom-file-label" for="fuArchivo">Seleccione el archivo</label>
                                                </div>
                                            </div>--%>
                                            <asp:FileUpload ID="fuArchivo" CssClass="form-control input-sm" runat="server" Width="100%" />
                                        </div>
                                        <div class="col-6  text-sm">
                                            <strong>Archivo Adjunto 2</strong>
                                            <%--                                            <div class="input-group input-group-sm">
                                                <div class="custom-file">
                                                    <asp:FileUpload ID="fuArchivo2" CssClass="custom-file-input" runat="server" ClientIDMode="Static" />                                                   
                                                    <label class="custom-file-label" for="fuArchivo2">Seleccione el archivo</label>
                                                </div>
                                            </div>--%>
                                            <asp:FileUpload ID="fuArchivo2" CssClass="form-control input-sm" runat="server" Width="100%" />
                                        </div>
                                        <div class="col-12  text-sm">
                                            <div class="p-2 mb-1 bg-warning text-dark" role="alert">
                                                <strong>Archivos Permitidos!</strong> PDF, DOC, DOCX, XLSX, XLS, TXT, JPG, GIF, PNG, ZIP y RAR
                                            </div>
                                        </div>

                                        <div class="col-12 d-none">
                                            <asp:CheckBox ID="chkHabilitarEmail" runat="server" Text=" Ingresar email" AutoPostBack="True" OnCheckedChanged="chkHabilitarEmail_CheckedChanged" />
                                            <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control input-sm" Enabled="false"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtEmail" ErrorMessage="formato del email no válido"></asp:RegularExpressionValidator>
                                        </div>
                                        <div class="col-12">
                                            <div runat="server" id="divArchivoCliente" visible="false">
                                                <strong>Archivo Adjunto para Cliente:</strong>
                                                <asp:FileUpload ID="fuArchivo3" CssClass="form-control input-sm" Enabled="false" runat="server" Width="100%" />
                                                <div class="alert-info alert-dismissible" role="alert">
                                                    <strong>Archivos Permitidos!</strong> PDF, DOC, DOCX, XLSX, XLS, TXT, JPG, GIF, PNG, ZIP y RAR
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div id="divUsuarioFirma" runat="server" visible="false">
                                                <strong>Usuario</strong>
                                                <asp:TextBox ID="txtUsuarioFirma" ClientIDMode="Static" runat="server" CssClass="form-control input-sm" Width="100%" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="pull-right">
                                        <%--<asp:Button Text="Enviar" Width="100px" OnClientClick="javascript:return ValidarIngreso()" OnClick="btnGrabar_Click" CssClass="btn btn-success btn-lg" runat="server" ID="btnGrabar" />--%>
                                        <asp:Button ID="btnGrabar" runat="server" Width="120px" Text="Enviar" OnClientClick="this.disabled=true;this.value = 'Enviando...'" UseSubmitBehavior="false" CssClass="btn btn-success btn-lg" OnClick="btnGrabar_Click" />
                                    </div>

                                </div>
                            </div>

                        </div>

                    </div>
                    <div>
                        <div class="col-12">
                            <div id="accordion" class="card card-primary card-outline d-none">
                                <div class="card-header">
                                    <h3 class="card-title">
                                        <i class="fas fa-edit"></i>
                                        Adicionales
                                    </h3>
                                </div>
                                <div class="card-body">

                                    <div class="row">
                                        <div class="col-4 col-sm-4">
                                            <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
                                                <a class="nav-link active" id="vert-tabs-Producto-tab" role="tab" aria-selected="true" aria-controls="vert-tabs-Producto" href="#vert-tabs-Producto" data-toggle="pill">Producto</a>
                                                <a class="nav-link" id="vert-tabs-Beneficiario-tab" role="tab" aria-selected="false" aria-controls="vert-tabs-Beneficiario" href="#vert-tabs-Beneficiario" data-toggle="pill">Beneficiario</a>
                                                <a class="nav-link" id="vert-tabs-Proveedor-tab" role="tab" aria-selected="false" aria-controls="vert-tabs-Proveedor" href="#vert-tabs-Proveedor" data-toggle="pill">Proveedor</a>
                                                <a class="nav-link" id="vert-tabs-Compra-tab" role="tab" aria-selected="false" aria-controls="vert-tabs-Compra" href="#vert-tabs-Compra" data-toggle="pill">Datos de la Compra</a>
                                                <a class="nav-link" id="vert-tabs-NumeroRegistroConsulta-tab" role="tab" aria-selected="false" aria-controls="vert-tabs-NumeroRegistroConsulta" href="#vert-tabs-NumeroRegistroConsulta" data-toggle="pill">Número Registro Consulta</a>
                                                <a class="nav-link" id="vert-tabs-Liquidador-tab" role="tab" aria-selected="false" aria-controls="vert-tabs-Liquidador" href="#vert-tabs-Liquidador" data-toggle="pill">Liquidador</a>
                                                <a class="nav-link" id="vert-tabs-Siniestro-tab" role="tab" aria-selected="false" aria-controls="vert-tabs-Siniestro" href="#vert-tabs-Siniestro" data-toggle="pill">Siniestro</a>
                                                <a class="nav-link" id="vert-tabs-Taller-tab" role="tab" aria-selected="false" aria-controls="vert-tabs-Taller" href="#vert-tabs-Taller" data-toggle="pill">Taller</a>
                                            </div>
                                        </div>
                                        <div class="col-8 col-sm-8">
                                            <div class="tab-content" id="vert-tabs-tabContent">
                                                <div class="tab-pane text-left fade active show" id="vert-tabs-Producto" role="tabpanel" aria-labelledby="vert-tabs-Producto-tab">
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Nombre:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtNombreProducto" runat="server" CssClass="form-control" placeholder="Nombre Producto"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Socio:</label>
                                                                </div>
                                                                <asp:DropDownList ID="DdlSocio" runat="server" CssClass="custom-select custom-select-sm" ClientIDMode="Static" OnDataBound="DdlSocio_DataBound">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">

                                                        <div class="col-6">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Poliza:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtPoliza" runat="server" CssClass="form-control" placeholder="Poliza"></asp:TextBox>
                                                            </div>

                                                        </div>
                                                        <div class="col-6">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Canal Ventas:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtCanalVentas" runat="server" CssClass="form-control" placeholder="Canal de ventas"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Cobertura:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtCobertura" runat="server" CssClass="form-control" placeholder="Cobertura"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Estado Poliza:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtEstadoPoliza" runat="server" CssClass="form-control" placeholder="Estado Poliza"></asp:TextBox>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="row">

                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Asistencia:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtAsistencia" runat="server" CssClass="form-control" placeholder="Asistencia"></asp:TextBox>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="vert-tabs-Beneficiario" role="tabpanel" aria-labelledby="vert-tabs-Beneficiario-tab">
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">RUT:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtRutBeneficiario" runat="server" CssClass="form-control" placeholder="RUT"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-8">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Nombre:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtNombreBeneficiario" runat="server" CssClass="form-control" placeholder="Nombre Beneficiario"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-6">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Relación:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtRelacion" runat="server" CssClass="form-control" placeholder="Relación"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Celular:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtCelularBeneficiario" runat="server" CssClass="form-control" placeholder="Celular"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">

                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Email:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtEmailBeneficiario" runat="server" CssClass="form-control" placeholder="Email"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Domicilio:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtDomicilioBeneficiario" runat="server" CssClass="form-control" placeholder="Domicilio"></asp:TextBox>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Comuna:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtComunaBeneficiario" runat="server" CssClass="form-control" placeholder="Comuna"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="vert-tabs-Proveedor" role="tabpanel" aria-labelledby="vert-tabs-Proveedor-tab">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Nombre:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtNombreProveedor" runat="server" CssClass="form-control" placeholder="Nombre Proveedor"></asp:TextBox>
                                                            </div>

                                                        </div>
                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Teléfono:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtTelefonoProveedor" runat="server" CssClass="form-control" placeholder="Teléfono"></asp:TextBox>
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Casa Comercial:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtCasaComercial" runat="server" CssClass="form-control" placeholder="Casa Comercial"></asp:TextBox>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="vert-tabs-Compra" role="tabpanel" aria-labelledby="vert-tabs-Compra-tab">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">N° Boleta Compra:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtNumBoletaCompra" runat="server" CssClass="form-control" placeholder="N° Boleta Compra"></asp:TextBox>
                                                            </div>

                                                        </div>
                                                        <%--                                                <div class="col-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><strong>Fecha Compra:</strong></span>
                                                        <asp:TextBox ID="TxtFechaCompra" runat="server" CssClass="form-control" placeholder="Fecha Compra"></asp:TextBox>
                                                    </div>
                                                </div>--%>

                                                        <div class="col-12">

                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend ">
                                                                    <label class="input-group-text">Fecha Compra:</label>

                                                                </div>
                                                                <asp:TextBox ID="TxtFechaCompra" runat="server" CssClass="form-control class-date" AutoCompleteType="Disabled"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Valor Compra:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtValorCompra" runat="server" CssClass="form-control" placeholder="Valor Compra"></asp:TextBox>
                                                            </div>

                                                        </div>
                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Marca Producto:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtMarcaProducto" runat="server" CssClass="form-control" placeholder="Marca Producto"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Emp. Donde Realizó Compra:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtEmpDondeRealizoCompra" runat="server" CssClass="form-control" placeholder="Emp. Donde Realizó Compra"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Forma Pago:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtFormaPago" runat="server" CssClass="form-control" placeholder="Forma de Pago"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="vert-tabs-NumeroRegistroConsulta" role="tabpanel" aria-labelledby="vert-tabs-NumeroRegistroConsulta-tab">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">N° Registro Consulta:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtNumRegConsulta" runat="server" CssClass="form-control" placeholder="N° Registro Consulta"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="vert-tabs-Liquidador" role="tabpanel" aria-labelledby="vert-tabs-Liquidador-tab">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Nombre:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtNombreLiquidador" runat="server" CssClass="form-control" placeholder="Nombre Liquidador"></asp:TextBox>
                                                            </div>

                                                        </div>

                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Teléfono:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtTelefonoLiquidador" runat="server" CssClass="form-control" placeholder="Teléfono"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Nombre Coordinador:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtNombreCoordinador" runat="server" CssClass="form-control" placeholder="Nombre Coordinador"></asp:TextBox>
                                                            </div>

                                                        </div>

                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Teléfono Coordinador:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtTelefonoCoordinador" runat="server" CssClass="form-control" placeholder="Teléfono Coordinador"></asp:TextBox>
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Últ. Com. con Liquidador:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtUltComLiquidador" runat="server" CssClass="form-control" placeholder="Últ. Com. con Liquidador"></asp:TextBox>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="vert-tabs-Siniestro" role="tabpanel" aria-labelledby="vert-tabs-Siniestro-tab">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">N° Siniestro:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtNumSiniestro" runat="server" CssClass="form-control" placeholder="Número Siniestro"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Estado Siniestro:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtEstadoSiniestro" runat="server" CssClass="form-control" placeholder="Estado Siniestro"></asp:TextBox>
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Ingreso Taller:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtIngresoTaller" runat="server" CssClass="form-control" placeholder="Ingreso Taller"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <%--                                                <div class="col-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><strong>Fecha Aprobación:</strong></span>
                                                        <asp:TextBox ID="TxtFechaAprobacion" runat="server" CssClass="form-control" placeholder="Fecha Aprobación"></asp:TextBox>
                                                    </div>
                                                </div>--%>

                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Fecha Aprobación:</label>

                                                                </div>
                                                                <asp:TextBox ID="TxtFechaAprobacion" runat="server" ClientIDMode="Static" CssClass="form-control class-date" AutoCompleteType="Disabled"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <%--                                                <div class="col-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><strong>Fecha Entrega:</strong></span>
                                                        <asp:TextBox ID="TxtFechaEntrega" runat="server" CssClass="form-control" placeholder="Fecha Entrega"></asp:TextBox>
                                                    </div>
                                                </div>--%>

                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Fecha Entrega:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtFechaEntrega" runat="server" ClientIDMode="Static" CssClass="form-control class-date" AutoCompleteType="Disabled"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Suma Liquidación:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtSumaLiquidacion" runat="server" CssClass="form-control" placeholder="Suma Liquidación"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">N° Caso:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtNumeroCaso" runat="server" CssClass="form-control" placeholder="Número de Caso"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="vert-tabs-Taller" role="tabpanel" aria-labelledby="vert-tabs-Taller-tab">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Nombre:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtNombreTaller" runat="server" CssClass="form-control" placeholder="Nombre Taller"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Dirección:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtDireccionTaller" runat="server" CssClass="form-control" placeholder="Dirección"></asp:TextBox>
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Teléfono:</label>
                                                                </div>
                                                                <asp:TextBox ID="TxtTelefonoTaller" runat="server" CssClass="form-control" placeholder="Teléfono"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!-- /.card -->
                            </div>
                        </div>
                    </div>

                    <div class="card card-warning" runat="server" id="divAdicioanl" visible="false">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-3">
                                    <asp:Label Visible="false" Text="" ID="lblAdicional1" runat="server" />
                                    <asp:TextBox Visible="false" ID="txtAdicional1" runat="server" CssClass="form-control input-sm" placeholder=""></asp:TextBox>
                                </div>
                                <div class="col-sm-3">
                                    <asp:Label Visible="false" Text="" ID="lblAdicional2" runat="server" />
                                    <asp:TextBox Visible="false" ID="txtAdicional2" runat="server" CssClass="form-control input-sm" placeholder=""></asp:TextBox>
                                </div>
                                <div class="col-sm-3">
                                    <asp:Label Visible="false" Text="" ID="lblAdicional3" runat="server" />
                                    <asp:TextBox Visible="false" ID="txtAdicional3" runat="server" CssClass="form-control input-sm" placeholder=""></asp:TextBox>
                                </div>
                                <div class="col-sm-3">
                                    <asp:Label Visible="false" Text="" ID="lblAdicional4" runat="server" />
                                    <asp:TextBox Visible="false" ID="txtAdicional4" runat="server" CssClass="form-control input-sm" placeholder=""></asp:TextBox>
                                </div>
                            </div>

                            <div class="row clearfix"></div>
                        </div>
                    </div>

                    <asp:Panel ID="Panel1" runat="server" CssClass="modal fade bs-example-modal-lg" TabIndex="-1" role="dialog" aria-labelledby="myLabel">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id=" myLabel ">Historial de Tickets</h4>
                                        </div>
                                        <div class="modal-body">
                                            <asp:GridView ID="grvTickets" runat="server" CssClass="table table-bordered table-hover table-condensed small" HeaderStyle-CssClass="active" AutoGenerateColumns="false" OnRowDataBound="paginacion_RowDataBound">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Id Ticket">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblIdTicket" runat="server" Visible="false" Text='<%# Bind("ID_ATENCION") %>'></asp:Label>
                                                            <asp:LinkButton ID="lbtnIdTicket" runat="server" Visible="true" Text='<%# Bind("ID_ATENCION") %>' OnClick="lbtnIdTicket_Click"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Rut">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRutCliente3" runat="server" Visible="true" Text='<%# Bind("RUT_CLIENTE") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Nombre">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblNombreCliente" runat="server" Visible="true" Text='<%# Bind("NOMBRE_CLIENTE") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="U.Creación">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblUsuarioCreacion" runat="server" Text='<%# Bind("USUARIO") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="F.Creación">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblFechaCreacion" runat="server" Text='<%# Bind("FECHA") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="U.Asignado">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblUsuarioAsignado" runat="server" Text='<%# Bind("USUARIO_ASIG") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Estado">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblEstado" runat="server" Text='<%# Bind("ESTADO_ATENCION") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Obs">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblObservacion" runat="server" Text='<%# Bind("OBSERVACION") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                        <div class="modal-footer">
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </asp:Panel>

                    <asp:Panel ID="pnlCaseMaker" runat="server" CssClass="modal fade bs-example-modal-lg" TabIndex="-1" role="dialog" aria-labelledby="myLabel">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myLabel2">Gestiones Crm anterior</h4>
                                        </div>
                                        <div class="modal-body">
                                            <asp:GridView ID="grvCaseMaker" runat="server" CssClass="table table-bordered table-hover table-condensed small" HeaderStyle-CssClass="active" AutoGenerateColumns="false">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Id">
                                                        <ItemTemplate>
                                                            <asp:Label ID="caso_id" runat="server" Visible="true" Text='<%# Bind("caso_id") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Fecha Creación">
                                                        <ItemTemplate>
                                                            <asp:Label ID="fecha_creacion" runat="server" Visible="true" Text='<%# Bind("fecha_creacion") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Nombre">
                                                        <ItemTemplate>
                                                            <asp:Label ID="descripcion" runat="server" Visible="true" Text='<%# Bind("descripcion") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Telefono 1">
                                                        <ItemTemplate>
                                                            <asp:Label ID="telefono_1" runat="server" Text='<%# Bind("telefono_1_Cliente") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Telefono 2">
                                                        <ItemTemplate>
                                                            <asp:Label ID="telefono_2" runat="server" Text='<%# Bind("telefono_2_Cliente") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Email">
                                                        <ItemTemplate>
                                                            <asp:Label ID="email" runat="server" Text='<%# Bind("email_cliente") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Llamado Tipo Desc">
                                                        <ItemTemplate>
                                                            <asp:Label ID="llamado_tipo_desc" runat="server" Text='<%# Bind("llamado_tipo_desc") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Categoria Desc">
                                                        <ItemTemplate>
                                                            <asp:Label ID="categoria_desc" runat="server" Text='<%# Bind("categoria_desc") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Componente Desc">
                                                        <ItemTemplate>
                                                            <asp:Label ID="componente_desc" runat="server" Text='<%# Bind("componente_desc") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Motivo Desc">
                                                        <ItemTemplate>
                                                            <asp:Label ID="motivo_desc" runat="server" Text='<%# Bind("motivo_desc") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Tipo Desc">
                                                        <ItemTemplate>
                                                            <asp:Label ID="tipo_desc" runat="server" Text='<%# Bind("tipo_desc") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="estado_caso_desc">
                                                        <ItemTemplate>
                                                            <asp:Label ID="estado_caso_desc" runat="server" Text='<%# Bind("estado_caso_desc") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Nombre Creador">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Nombre_Creador" runat="server" Text='<%# Bind("Nombre_Creador") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Ubicación Desc">
                                                        <ItemTemplate>
                                                            <asp:Label ID="ubicacion_desc" runat="server" Text='<%# Bind("ubicacion_desc") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                        <div class="modal-footer">
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </asp:Panel>

                    <!-- Modal bounceIn animated-->
                    <div class="modal" id="myModalScript" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg ">
                            <div class="modal-content ">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalScriptLabel">Ayuda</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                                </div>
                                <div class="modal-body">
                                    <div class="table-responsive">
                                        <asp:GridView ID="GrvScript" runat="server" CssClass="table table-bordered table-hover table-sm text-sm" AutoGenerateColumns="false" OnRowDataBound="paginacionScript_RowDataBound" BorderColor="Transparent" OnPreRender="GrvScript_PreRender">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Id">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblIdRespuesta" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="right" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Pregunta" HeaderText="Pregunta" />
                                                <asp:BoundField DataField="Respuesta" HeaderText="Respuesta" />
                                                <asp:TemplateField HeaderText="Documentos">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblVerDocumentos" runat="server" Visible="false" Text='<%# Bind("Documentos") %>'></asp:Label>
                                                        <asp:Button ID="BtnVerDocumentos" runat="server" Text='<%# Bind("Documentos") %>' OnClientClick="EsconderModalScript();" OnClick="BtnVerDocumentos_Click" CssClass="btn btn-xs btn-primary" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="right" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                </div>
                            </div>
                            <!-- modal-content -->
                        </div>
                        <!-- modal-dialog -->
                    </div>
                    <!-- modal -->

                    <!-- Modal bounceIn animated-->
                    <div class="modal" id="myModalDocumentos" tabindex="-1" role="dialog" aria-labelledby="myModalDocumentosLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg ">
                            <div class="modal-content ">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalDocumentosLabel">Documentos</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                                </div>
                                <div class="modal-body">
                                    <div class="table-responsive">
                                        <asp:GridView ID="GrvDocumentos" runat="server" CssClass="table table-sm table-bordered" AutoGenerateColumns="false" OnPreRender="GrvDocumentos_PreRender" EmptyDataText="documento no encontrado" PageSize="50" AllowPaging="true">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Id">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblIdDocumento" runat="server" Visible="true" Text='<%# Bind("ID_DOCUMENTO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Nombre">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblNombre" runat="server" Visible="true" Text='<%# Bind("NOMBRE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Descripción">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblDescripcion" runat="server" Visible="true" Text='<%# Bind("DESCRIPCION") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Categoría">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblIdCategoria" runat="server" Visible="false" Text='<%# Bind("ID_CATEGORIA_DOCUMENTO") %>'></asp:Label>
                                                        <asp:Label ID="LblCategoria" runat="server" Text='<%# Bind("NOM_CATEGORIA_DOCUMENTO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Archivo">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblArchivo" runat="server" Visible="false" Text='<%# Bind("ARCHIVO") %>'></asp:Label>
                                                        <asp:LinkButton ID="btnArchivo" runat="server" OnClick="btnArchivo_Click" OnClientClick="EsconderModalDocumentos();" CssClass="btn btn-default btn-sm"><i class="fas fa-file-archive"></i></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>

                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                </div>
                            </div>
                            <!-- modal-content -->
                        </div>
                        <!-- modal-dialog -->
                    </div>
                    <!-- modal -->
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="server">
    <script>
        function soloNumeros(e) {
            var unicode = e.charCode ? e.charCode : e.keyCode;
            if (unicode != 8) {
                if (unicode < 48 || unicode > 57) //if not a number
                { return false } //disable key press    
            }
        }
        function MostrarModalDocumentos() {
          //  $('#myModalScript').modal('hide');
            $('#myModalDocumentos').modal('show');
        }
        function EsconderModalDocumentos() {
            //  $('#myModalScript').modal('hide');
            $('#myModalDocumentos').modal('hide');
        }

        function MostrarModalScript() {
            $('#myModalScript').modal('show');
        }

        function EsconderModalScript() {
            $('#myModalScript').modal('hide');
        }
    </script>
</asp:Content>
