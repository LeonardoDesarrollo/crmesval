﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="EsvalCrm.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>CRM Esval | Login</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="Assets/AdminLTE/plugins/fontawesome-free/css/all.min.css"/>
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"/>
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="Assets/AdminLTE/plugins/icheck-bootstrap/icheck-bootstrap.min.css"/>
    <!-- Theme style -->
    <link rel="stylesheet" href="Assets/AdminLTE/dist/css/adminlte.min.css"/>
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet"/>
</head>
<body class="hold-transition login-page">
    <form id="form1" runat="server">
        <div class="login-box">
            <div class="login-logo">
              

            </div>
            <!-- /.login-logo -->
            <div class="card">
                <div class="card-body login-card-body">
                      <img src="img/Logo_Esval.png" class="img-rounded" style="width: 100%; max-height: 100%" />
                    <p class="login-box-msg">Ingrese sus datos para iniciar la sesión</p>

                    <div>
                        <div class="input-group mb-3">
                            <input id="inpUsuario" type="text" runat="server" class="form-control" placeholder="Login" required="required" autofocus="autofocus" />
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-envelope"></span>
                                </div>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input id="inpPassword" type="password" runat="server" class="form-control" placeholder="Password" required="required" />
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-lock"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-8">
                            </div>
                            <!-- /.col -->
                            <div class="col-4">
                                <asp:Button ID="btnIngresar" OnClick="btnIngresar_Click" runat="server" Text="Ingresar" CssClass="btn btn-primary btn-block" />
                            </div>
                            <!-- /.col -->
                        </div>
                    </div>

                    <div>
                        <img src="img/logoUpcom.png" class="img-rounded" style="width: 100%; height: 100%;"/>
                    </div>
                    <!-- Alertas -->
                    <div id="divAlerta" runat="server" visible="false" class="alert alert-danger">
                        <strong>Error!</strong>
                        <asp:Label Text="" ID="lblError" runat="server" />
                    </div>
                </div>
                <!-- /.login-card-body -->
            </div>
        </div>
    </form>
    <!-- jQuery -->
    <script src="Assets/AdminLTE/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="Assets/AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="Assets/AdminLTE/dist/js/adminlte.min.js"></script>
</body>
</html>
