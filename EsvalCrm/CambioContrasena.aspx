﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CambioContrasena.aspx.cs" Inherits="EsvalCrm.CambioContrasena" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Cambio de contraseña</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Cambio de Constraseña</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">

            <!-- Alertas -->
            <div id="divAlerta" runat="server" visible="false" class="alert alert-danger">
                <strong>Atención!: </strong>
                <asp:Label Text="" ID="lblInfo" runat="server" />
            </div>



            <asp:HiddenField ID="hfIdUsuario" runat="server" />
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h3 class="card-title">Cambiar Contraseña</h3>
                </div>
                <div class="card-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Nombre</label>
                            <div class="col-sm-10">
                                <asp:Label ID="lblNombre" runat="server" Text="Label" CssClass="btn btn-link btn-sm"></asp:Label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Usuario</label>
                            <div class="col-sm-10">
                                <asp:Label ID="lblUsuario" runat="server" Text="Label" CssClass="btn btn-link btn-sm"></asp:Label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="txtContrasena" class="col-sm-2 control-label">Nueva Contraseña</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtContrasena" ToolTip="Recuerde que la Contraseña debe tener al menos 5 caracteres" Width="20%" runat="server" CssClass="form-control input-sm" TextMode="Password"></asp:TextBox>
                            </div>
                        </div>

                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                            ControlToValidate="txtContrasena"
                            Display="Dynamic"
                            ErrorMessage="Debe ingresar 5 caracteres como minimo para su contraseña"
                            ForeColor="Red">
                        </asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="card-footer">
                      <asp:LinkButton ID="lbtnGrabar" CssClass="btn btn-danger" runat="server" ForeColor="White"
                    OnClick="lbtnGrabar_Click"><i aria-hidden="true" class="fas fa-save"></i> Grabar</asp:LinkButton>
                </div>
              
            </div>

        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="server">
</asp:Content>
