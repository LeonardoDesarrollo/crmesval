﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;

namespace EsvalCrm
{
    public partial class ConfiguracionIn : System.Web.UI.Page
    {
        Datos dal = new Datos();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.Page.IsPostBack)
                {

                    Servicio();
                    usuario1();
                    usuario2();
                    usuario3();
                    usuario4();
                    area();

                    tipoMotivoCierre();
                    ddlTipo.SelectedValue = "B";
                }
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Visible = true;
            }
        }


        void Servicio()
        {
            ddlServicio.DataSource = dal.GetBuscarServicio(0, 1);
            ddlServicio.DataTextField = "SERVICIO";
            ddlServicio.DataValueField = "ID_SERVICIO";
            ddlServicio.DataBind();
        }

        protected void ddlNivel1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string idPerfil = Session["variableIdPerfil"].ToString();
                string perfilAtento = string.Empty;
                if (idPerfil == "2")
                {
                    perfilAtento = "1";
                }
                else
                {
                    perfilAtento = null;
                }
                nivel2();
                ddlNivel3.Items.Clear();
                ddlNivel4.Items.Clear();

                txtNivel1.Text = ddlNivel1.SelectedItem.ToString();


            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Visible = true;

            }
        }

        protected void ddlNivel2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string idPerfil = Session["variableIdPerfil"].ToString();
                string perfilAtento = string.Empty;
                if (idPerfil == "2")
                {
                    perfilAtento = "1";
                }
                else
                {
                    perfilAtento = null;
                }
                nivel3();
                ddlNivel4.Items.Clear();

                txtNivel2.Text = ddlNivel2.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Visible = true;

            }
        }

        protected void ddlNivel3_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string idPerfil = Session["variableIdPerfil"].ToString();
                string perfilAtento = string.Empty;
                if (idPerfil == "2")
                {
                    perfilAtento = "1";
                }
                else
                {
                    perfilAtento = null;
                }
                nivel4();

                txtNivel3.Text = ddlNivel3.SelectedItem.ToString();

            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }


        protected void ddlNivel4_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                txtNivel4.Text = ddlNivel4.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }



        void nivel1()
        {


            ddlNivel1.DataSource = dal.getBuscarNivel1(null, null, ddlServicio.SelectedValue);
            ddlNivel1.DataValueField = "NIVEL_1";
            ddlNivel1.DataTextField = "NIVEL_1";
            ddlNivel1.DataBind();

            ddlNivel2.Items.Clear();
            ddlNivel3.Items.Clear();
            ddlNivel4.Items.Clear();
        }

        void nivel2()
        {

            //ddlNivel2.DataSource = dal.getBuscarSubEstatus(ddlNivel1.SelectedValue);
            ddlNivel2.DataSource = dal.getBuscarNivel2(null, null, ddlServicio.SelectedValue, ddlNivel1.SelectedValue);
            ddlNivel2.DataValueField = "NIVEL_2";
            ddlNivel2.DataTextField = "NIVEL_2";
            ddlNivel2.DataBind();

            ddlNivel3.Items.Clear();
            ddlNivel4.Items.Clear();
        }

        void nivel3()
        {
            ddlNivel3.DataSource = dal.getBuscarNivel3(null, null, ddlServicio.SelectedValue, ddlNivel1.SelectedValue, ddlNivel2.SelectedValue);
            ddlNivel3.DataValueField = "NIVEL_3";
            ddlNivel3.DataTextField = "NIVEL_3";
            ddlNivel3.DataBind();

            ddlNivel4.Items.Clear();
        }

        void nivel4()
        {
            ddlNivel4.DataSource = dal.getBuscarNivel4(null, null, ddlServicio.SelectedValue, ddlNivel1.SelectedValue, ddlNivel2.SelectedValue, ddlNivel3.SelectedValue);
            ddlNivel4.DataValueField = "NIVEL_4";
            ddlNivel4.DataTextField = "NIVEL_4";
            ddlNivel4.DataBind();
        }

        protected void ddlServicio_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string idPerfil = Session["variableIdPerfil"].ToString();
                string perfilAtento = string.Empty;
                if (idPerfil == "2")
                {
                    perfilAtento = "1";
                }
                else
                {
                    perfilAtento = null;
                }
                nivel1();
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Visible = true;

            }
        }

        protected void ddlServicio_DataBound(object sender, EventArgs e)
        {
            ddlServicio.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        }

        protected void ddlNivel1_DataBound(object sender, EventArgs e)
        {
            ddlNivel1.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        }

        protected void ddlNivel2_DataBound(object sender, EventArgs e)
        {
            ddlNivel2.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        }

        protected void ddlNivel3_DataBound(object sender, EventArgs e)
        {
            ddlNivel3.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        }

        protected void ddlNivel4_DataBound(object sender, EventArgs e)
        {
            ddlNivel4.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        }


        protected void ddlUsuario1_DataBound(object sender, EventArgs e)
        {
            ddlUsuario1.Items.Insert(0, new ListItem("Seleccione", "0"));
        }
        protected void ddlUsuario2_DataBound(object sender, EventArgs e)
        {
            ddlUsuario2.Items.Insert(0, new ListItem("Seleccione", "0"));
        }
        protected void ddlUsuario3_DataBound(object sender, EventArgs e)
        {
            ddlUsuario3.Items.Insert(0, new ListItem("Seleccione", "0"));
        }
        protected void ddlUsuario4_DataBound(object sender, EventArgs e)
        {
            ddlUsuario4.Items.Insert(0, new ListItem("Seleccione", "0"));
        }

        protected void ddlTipoMotivoCierre_DataBound(object sender, EventArgs e)
        {
            ddlTipoMotivoCierre.Items.Insert(0, new ListItem("Seleccione", "0"));
        }

        void usuario1()
        {
            ddlUsuario1.DataSource = dal.getBuscarUsuario(null, null);
            ddlUsuario1.DataValueField = "ID_USUARIO";
            ddlUsuario1.DataTextField = "USUARIO";
            ddlUsuario1.DataBind();
        }
        void usuario2()
        {
            ddlUsuario2.DataSource = dal.getBuscarUsuario(null, null);
            ddlUsuario2.DataValueField = "ID_USUARIO";
            ddlUsuario2.DataTextField = "USUARIO";
            ddlUsuario2.DataBind();
        }
        void usuario3()
        {
            ddlUsuario3.DataSource = dal.getBuscarUsuario(null, null);
            ddlUsuario3.DataValueField = "ID_USUARIO";
            ddlUsuario3.DataTextField = "USUARIO";
            ddlUsuario3.DataBind();
        }
        void usuario4()
        {
            ddlUsuario4.DataSource = dal.getBuscarUsuario(null, null);
            ddlUsuario4.DataValueField = "ID_USUARIO";
            ddlUsuario4.DataTextField = "USUARIO";
            ddlUsuario4.DataBind();
        }
        void tipoMotivoCierre()
        {
            ddlTipoMotivoCierre.DataSource = dal.getBuscarTipoMotivoCierre();
            ddlTipoMotivoCierre.DataTextField = "TIPO_MOTIVO_CIERRE";
            ddlTipoMotivoCierre.DataValueField = "ID_TIPO_MOTIVO_CIERRE";
            ddlTipoMotivoCierre.DataBind();
        }


        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                string valor = string.Empty;

                if (string.IsNullOrEmpty(txtSLA1.Text.Trim()))
                {
                    txtSLA1.Text = "0";
                }

                if (ddlTipo.SelectedValue == "P")
                {
                    //2021-08-11 Comentado
                    //if (ddlUsuario1.SelectedValue != "0" && txtSLA1.Text != string.Empty)
                    //{
                    valor = dal.setIngresarTipificacion(ddlUsuario1.SelectedValue, ddlUsuario2.SelectedValue, ddlUsuario3.SelectedValue, ddlUsuario4.SelectedValue,
                            txtSLA1.Text, txtSLA2.Text, txtSLA3.Text, txtSLA4.Text, ddlTipo.SelectedValue, ddlTipoMotivoCierre.SelectedValue, ddlVisibleAtento.SelectedValue,
                          "1", "1", ddlDetenido.SelectedValue, ddlServicio.SelectedValue, txtNivel1.Text.Trim(),
                          txtNivel2.Text.Trim(), txtNivel3.Text.Trim(), txtNivel4.Text.Trim(), ddlClase.SelectedItem.ToString(), Convert.ToInt32(DdlArea.SelectedValue), ddlActivo.SelectedValue);

                    if (valor == "0")
                    {
                        lblInfo.Text = "La Tipificación ya existe. Favor intentar con otro nombre.";
                        divAlerta.Attributes["class"] = "alert alert-warning";
                        divAlerta.Visible = true;
                        return;
                    }

                    lblInfo.Text = "Tipificación ingresada exitosamente";
                        divAlerta.Attributes["class"] = "alert alert-success";
                        divAlerta.Visible = true;
                    //}
                    //else
                    //{
                    //    lblInfo.Text = "Debe por lo menos un usuario y SLA correspondiente.";
                    //    divAlerta.Attributes["class"] = "alert alert-danger";
                    //    divAlerta.Visible = true;
                    //}
                }
                else
                {
                    valor = dal.setIngresarTipificacion(ddlUsuario1.SelectedValue, ddlUsuario2.SelectedValue, ddlUsuario3.SelectedValue, ddlUsuario4.SelectedValue,
                            txtSLA1.Text, txtSLA2.Text, txtSLA3.Text, txtSLA4.Text, ddlTipo.SelectedValue, ddlTipoMotivoCierre.SelectedValue, ddlVisibleAtento.SelectedValue,
                          "1", "1", ddlDetenido.SelectedValue, ddlServicio.SelectedValue, txtNivel1.Text.Trim(),
                          txtNivel2.Text.Trim(), txtNivel3.Text.Trim(), txtNivel4.Text.Trim(), ddlClase.SelectedItem.ToString(), Convert.ToInt32(DdlArea.SelectedValue), ddlActivo.SelectedValue);

                    if (valor == "0")
                    {
                        lblInfo.Text = "La Tipificación ya existe. Favor intentar con otro nombre.";
                        divAlerta.Attributes["class"] = "alert alert-warning";
                        divAlerta.Visible = true;
                        return;
                    }

                    lblInfo.Text = "Tipificación ingresada exitosamente";
                    divAlerta.Attributes["class"] = "alert alert-success";
                    divAlerta.Visible = true;
                }
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Response.Redirect("Configuracion.aspx");
        }

        void area()
        {
            DdlArea.DataSource = dal.getBuscarArea();
            DdlArea.DataValueField = "ID_AREA";
            DdlArea.DataTextField = "AREA";
            DdlArea.DataBind();
        }
        protected void DdlArea_DataBound(object sender, EventArgs e)
        {
            DdlArea.Items.Insert(0, new ListItem("Seleccione", "0"));
        }
    }
}