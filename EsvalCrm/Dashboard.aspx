﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="EsvalCrm.Dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Dashboard</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <!-- Alertas -->
            <div id="divAlerta" runat="server" visible="false" class="alert alert-danger">
                <strong>¡Atención!: </strong>
                <asp:Label Text="" ID="lblInfo" runat="server" />
            </div>

            <!-- Small boxes (Stat box) -->
            <div class="row mb-2">
                <div class="col-lg-3 col-xs-6">
                    <div class="well well-sm">
                        <strong>Área</strong>
                        <asp:DropDownList ID="ddlArea" runat="server" OnDataBound="ddlArea_DataBound" CssClass="form-control input-sm" OnSelectedIndexChanged="ddlArea_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                    </div>
                </div>
                <div class="col-lg-3 col-xs-6" runat="server" id="divUsuario" visible="false">
                    <div class="well well-sm">
                        <strong>Usuario</strong>
                        <asp:DropDownList ID="ddlUsuario" runat="server" CssClass="form-control input-sm" OnDataBound="ddlUsuario_DataBound" OnSelectedIndexChanged="ddlUsuario_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->

                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3>
                                <asp:Label ID="lblTotalCasosAbiertos" runat="server" />
                            </h3>
                            <h4>Ticket Pendientes</h4>
                        </div>
                        <div class="icon">
                            <i class="ion ion-android-alert fa fa-exclamation"></i>
                        </div>
                        <asp:LinkButton ID="lbtnTicketAbiertos" runat="server" CssClass="small-box-footer" OnClick="lbtnTicketAbiertos_Click"><i class="fa fa-arrow-circle-right"></i> Mas Info</asp:LinkButton>
                        <%--<a href="#" class="small-box-footer">Más info <i class="fa fa-arrow-circle-right"></i>
                         </a>--%>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3>
                                <asp:Label ID="lblTotalCasosCerrados" Text="" runat="server" />
                            </h3>
                            <h4>Ticket Cerrados</h4>
                        </div>
                        <div class="icon">
                            <i class="ion ion-android-checkmark-circle fa fa-check"></i>
                        </div>
                        <asp:LinkButton ID="lbtnTicketCerrados" runat="server" CssClass="small-box-footer" OnClick="lbtnTicketCerrados_Click"><i class="fa fa-arrow-circle-right"></i> Mas Info</asp:LinkButton>
                        <%--<a href="#" class="small-box-footer">Más info <i class="fa fa-arrow-circle-right"></i>
                         </a>--%>
                    </div>
                </div>

                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3>
                                <asp:Label ID="lblTotalCasosEnProceso" Text="" runat="server" />
                            </h3>
                            <h4>Ticket En Proceso</h4>
                        </div>
                        <div class="icon">
                            <%--<i class="ion ion-android-checkmark-circle"></i>--%>
                            <i class="ion ion-gear-b fa fa-spinner"></i>
                        </div>
                        <asp:LinkButton ID="lbtnTicketEnProceso" runat="server" CssClass="small-box-footer" OnClick="lbtnTicketEnProceso_Click"><i class="fa fa-arrow-circle-right"></i> Mas Info</asp:LinkButton>
                    </div>
                </div>

                <!-- ./col -->
                <div class="col-lg-3 col-xs-6" runat="server" visible="false">
                    <!-- small box -->
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3>
                                <asp:Label ID="lblTotalTicketDerivados" Text="" runat="server" />
                            </h3>
                            <h4>Ticket Derivados</h4>
                        </div>
                        <div class="icon">
                            <i class="ion ion-android-checkmark-circle"></i>
                        </div>
                        <asp:LinkButton ID="lbtnTicketDerivados" runat="server" CssClass="small-box-footer" OnClick="lbtnTicketDerivados_Click"><i class="fa fa-arrow-circle-right"></i> Mas Info</asp:LinkButton>
                        <%--<a href="#" class="small-box-footer">Más info <i class="fa fa-arrow-circle-right"></i>
                         </a>--%>
                    </div>
                </div>

                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-red" runat="server" visible="false">
                        <div class="inner">
                            <h3>
                                <asp:Label ID="lblTotalDetenidos" runat="server" /></h3>
                            <h4>Ticket Detenidos</h4>
                        </div>
                        <div class="icon">
                            <i class="ion ion-android-checkmark-circle"></i>
                        </div>
                        <%--<a href="#" class="small-box-footer">Más info <i class="fa fa-arrow-circle-right"></i>
                             </a>--%>
                        <asp:LinkButton ID="lbtnTicketDetenidos" runat="server" CssClass="small-box-footer" OnClick="lbtnTicketDetenidos_Click"><i class="fa fa-arrow-circle-right"></i> Mas Info</asp:LinkButton>

                    </div>
                </div>
                <!-- ./col -->

                <div runat="server" id="divUsuariosRegistrados" visible="false">
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-yellow">
                            <div class="inner">
                                <h3>
                                    <asp:Label ID="lblTotalUsuarios" Text="" runat="server" />
                                </h3>
                                <h4>Usuarios Registrados</h4>
                            </div>
                            <div class="icon">
                                <i class="ion ion-person-add"></i>
                            </div>
                            <a href="Usuarios.aspx" class="small-box-footer">Más info <i class="fa fa-arrow-circle-right"></i>
                            </a>
                        </div>
                    </div>
                    <!-- ./col -->
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-3 col-xs-6" runat="server" visible="false">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3>
                                <asp:Label ID="LblTotalEmailTipificados" runat="server" />
                            </h3>
                            <h4>Email tipificados</h4>
                        </div>
                        <div class="icon">
                            <i class="ion ion-android-checkmark-circle"></i>
                        </div>
                        <asp:LinkButton ID="LbtnEmailTipificados" runat="server" Visible="true" CssClass="small-box-footer" OnClick="LbtnEmailTipificados_Click"><i class="fa fa-arrow-circle-right"></i> Mas Info</asp:LinkButton>
                        <%--<a href="#" class="small-box-footer">Más info <i class="fa fa-arrow-circle-right"></i>
                         </a>--%>
                    </div>


                </div>
                <div class="col-lg-3 col-xs-6" runat="server" visible="false">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3>
                                <asp:Label ID="LblTotalEmailNoTipificados" runat="server" /></h3>
                            <h4>Email no tipificados</h4>
                        </div>
                        <div class="icon">
                            <i class="ion ion-android-checkbox-outline-blank"></i>
                        </div>
                        <asp:LinkButton ID="LbtnEmailNoTipificados" runat="server" Visible="true" CssClass="small-box-footer" OnClick="LbtnEmailNoTipificados_Click"><i class="fa fa-arrow-circle-right"></i> Mas Info</asp:LinkButton>
                        <%--<a href="#" class="small-box-footer">Más info <i class="fa fa-arrow-circle-right"></i>
                         </a>--%>
                    </div>
                </div>
            </div>


            <div class="row" runat="server" id="divFilaAdmin" visible="false">
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3>
                                <asp:Label ID="lblCasosEscaladosPorDemora" runat="server" Text="0" /></h3>
                            <h4>Casos escalados por demora</h4>
                        </div>
                        <div class="icon">
                            <i class="ion ion-android-checkbox-outline-blank"></i>
                        </div>
                        <asp:LinkButton ID="lbtnCasosEscaladosPorDemora" runat="server" Visible="false" CssClass="small-box-footer" OnClick="lbtnCasosEscaladosPorDemora_Click"><i class="fa fa-arrow-circle-right"></i> Mas Info</asp:LinkButton>
                        <%--<a href="#" class="small-box-footer">Más info <i class="fa fa-arrow-circle-right"></i>
                         </a>--%>
                    </div>
                </div>
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3>
                                <asp:Label ID="lblTiempoResolucionPromedio" runat="server" Text="0" /></h3>
                            <h4>Tiempo de Resolución Promedio</h4>

                        </div>
                        <div class="icon">
                            <i class="ion ion-android-checkbox-outline-blank"></i>
                        </div>
                        <asp:LinkButton ID="lbtnTiempoResolucionPromedio" runat="server" Visible="true" CssClass="small-box-footer" OnClick="lbtnTiempoResolucionPromedio_Click"><i class="fa fa-arrow-circle-right"></i> Mas Info</asp:LinkButton>
                        <%--<a href="#" class="small-box-footer">Más info <i class="fa fa-arrow-circle-right"></i>
                         </a>--%>
                    </div>
                </div>

                <div class="col-lg-3 col-xs-6" runat="server" visible="false">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3>
                                <asp:Label ID="lblTicketConInsistencias" runat="server" Text="0" />
                            </h3>
                            <h4>Tickets con insistencias</h4>
                        </div>
                        <div class="icon">
                            <i class="ion ion-android-checkbox-outline-blank"></i>
                        </div>
                        <asp:LinkButton ID="lbtnTicketConInsistencia" runat="server" CssClass="small-box-footer" OnClick="lbtnTicketConInsistencia_Click"><i class="fa fa-arrow-circle-right"></i> Mas Info</asp:LinkButton>
                        <%--<a href="#" class="small-box-footer">Más info <i class="fa fa-arrow-circle-right"></i>
                         </a>--%>
                    </div>
                </div>

            </div>

            <div class="row d-none">

                <div class="col-lg-3 col-xs-6" runat="server" visible="true">
                    <!-- small box -->
                    <div class="small-box bg-blue">
                        <div class="inner">
                            <h3>
                                <asp:Label ID="lblTipificacionesConsulta" runat="server" Text="0" />
                            </h3>
                            <h4>Tipificaciones Deriva</h4>
                        </div>
                        <div class="icon">
                            <i class="ion ion-android-create"></i>
                        </div>
                        <asp:LinkButton ID="lbtnTipificacionesConsulta" runat="server" CssClass="small-box-footer" OnClick="lbtnTipificacionesConsulta_Click"><i class="fa fa-arrow-circle-right"></i> Mas Info</asp:LinkButton>
                        <%--<a href="#" class="small-box-footer">Más info <i class="fa fa-arrow-circle-right"></i>
                         </a>--%>
                    </div>
                </div>

                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-blue">
                        <div class="inner">
                            <h3>
                                <asp:Label ID="lblTipificacionesReclamo" runat="server" Text="0" /></h3>
                            <h4>Tipificaciones Primera Linea</h4>

                        </div>
                        <div class="icon">
                            <i class="ion ion-android-create"></i>
                        </div>
                        <asp:LinkButton ID="lbtnTipificacionesReclamo" runat="server" CssClass="small-box-footer" OnClick="lbtnTipificacionesReclamo_Click"><i class="fa fa-arrow-circle-right"></i> Mas Info</asp:LinkButton>
                        <%--<a href="#" class="small-box-footer">Más info <i class="fa fa-arrow-circle-right"></i>
                         </a>--%>
                    </div>
                </div>

                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-blue">
                        <div class="inner">
                            <h3>
                                <asp:Label ID="lblTipificacionesSolicitud" runat="server" Text="0" /></h3>
                            <h4>Tipificaciones Dual</h4>

                        </div>
                        <div class="icon">
                            <i class="ion ion-android-create"></i>
                        </div>
                        <asp:LinkButton ID="lbtnTipificacionesSolicitud" runat="server" CssClass="small-box-footer" OnClick="lbtnTipificacionesSolicitud_Click"><i class="fa fa-arrow-circle-right"></i> Mas Info</asp:LinkButton>
                        <%--<a href="#" class="small-box-footer">Más info <i class="fa fa-arrow-circle-right"></i>
                         </a>--%>
                    </div>
                </div>

                <div class="col-lg-3 col-xs-6" runat="server" visible="false">
                    <!-- small box -->
                    <div class="small-box bg-blue">
                        <div class="inner">
                            <h3>
                                <asp:Label ID="lblTipificacionesIncistencias" runat="server" Text="0" /></h3>
                            <h4>Tipificaciones Reporte Incidencia</h4>
                        </div>
                        <div class="icon">
                            <i class="ion ion-android-create"></i>
                        </div>
                        <asp:LinkButton ID="lbtnTipificacionIncidencia" runat="server" CssClass="small-box-footer" OnClick="lbtnTipificacionIncidencia_Click"><i class="fa fa-arrow-circle-right"></i> Mas Info</asp:LinkButton>
                        <%--<a href="#" class="small-box-footer">Más info <i class="fa fa-arrow-circle-right"></i>
                         </a>--%>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="server">
</asp:Content>
