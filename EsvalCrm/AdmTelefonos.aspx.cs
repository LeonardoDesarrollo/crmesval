﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using DataTableToExcel;

namespace EsvalCrm
{
    public partial class AdmTelefonos : System.Web.UI.Page
    {
        Datos dal = new Datos();
        TelefonoAccess DalTel = new TelefonoAccess();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.Page.IsPostBack)
                {
                    Buscar();
                }
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        void Buscar()
        {
            Grv.DataSource = DalTel.GetTelefonos();
            Grv.DataBind();
        }

        protected void Grv_PreRender(object sender, EventArgs e)
        {
            if (Grv.Rows.Count > 0)
            {
                Grv.UseAccessibleHeader = true;
                Grv.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void btnEditar_Click(object sender, EventArgs e)
        {

            try
            {
                LinkButton btn = (LinkButton)sender;
                GridViewRow row = (GridViewRow)btn.NamingContainer;
                Label _lblId = (Label)Grv.Rows[row.RowIndex].FindControl("lblId");
                Response.Redirect("AdmTelefonosIn.aspx?Id=" + _lblId.Text);
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                GridViewRow row = (GridViewRow)btn.NamingContainer;
                Label _lblId = (Label)Grv.Rows[row.RowIndex].FindControl("lblId");

                DalTel.SetDelTelefono(Convert.ToInt32(_lblId.Text));

                Buscar();
                lblInfo.Text = "Teléfono eliminado!";
                divAlerta.Attributes["class"] = "alert alert-success";
                divAlerta.Visible = true;
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void LbtnNuevoTelefono_Click(object sender, EventArgs e)
        {
            Response.Redirect("AdmTelefonosIn?Id=0");
        }

        protected void ibtnExportarExcel_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = dal.getBuscarTelefonoExporte().Tables[0];
                //Utilidad.ExportDataTableToExcel(dt, "exporte.xls", "", "", "", "");
                //Utilidad.ExportDataTableToExcel(dt, "exporte.xls", "", "", "", "");
                Response.ContentType = "Application/x-msexcel";
                Response.AddHeader("content-disposition", "attachment;filename=" + "exporte_telefono" + ".csv");
                Response.ContentEncoding = Encoding.Unicode;
                Response.Write(Utilidad.ExportToCSVFile(dt));
                Response.End();
                //Utilidad.ExportDataTableToExcel(dt, "Exporte.xls", "", "", "", "");
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }
    }
}