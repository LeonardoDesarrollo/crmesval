﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DAL;

namespace EsvalCrm
{
    public partial class Login : System.Web.UI.Page
    {
        Datos dal = new Datos();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.Page.IsPostBack)
                {

                }
            }
            catch (Exception ex)
            {
                divAlerta.Visible = true;
                lblError.Text = ex.Message;
            }
        }

        protected void btnIngresar_Click(object sender, EventArgs e)
        {
            try
            {
                string valor = dal.getValUsuario(inpUsuario.Value, inpPassword.Value);
                DataTable dt = dal.getBuscarUsuarioPorLogin(inpUsuario.Value).Tables[0];
                string idUsuario = "";
                string nomUsuario = "";
                string idPerfil = "";
                string perfil = "";
                string apeUsuario = "";
                string usuario = "";
                string imagen = "";
                string verTodos = "";
                string idEmpresa = string.Empty;
                string empresa = string.Empty;
                string activo = string.Empty;
                string idArea = string.Empty;
                string nomArea = string.Empty;
                //nomUsuario = row["NOMBRES"].ToString();
                //perfil = row["PERFIL"].ToString();



                if (valor == "1")
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        idUsuario = item["ID_USUARIO"].ToString();
                        usuario = item["USUARIO"].ToString();
                        idPerfil = item["ID_PERFIL"].ToString();
                        perfil = item["NOM_PERFIL"].ToString();
                        nomUsuario = item["NOMBRE"].ToString();
                        imagen = item["RUTA_FOTO"].ToString();
                        verTodos = item["VER_TODOS"].ToString();
                        idEmpresa = item["ID_EMPRESA"].ToString();
                        empresa = item["EMPRESA"].ToString();
                        activo = item["ACTIVO"].ToString();
                        idArea = item["ID_AREA"].ToString();
                        nomArea = item["AREA"].ToString();
                    }


                    Session["variableUsuario"] = usuario;
                    //JRL 2018-09-27 comentado
                    //if (imagen == string.Empty)
                    //{
                    //    imagen = "img/user-blank.jpg";
                    //}

                    //JRL 2018-09-27
                    try
                    {
                        System.Drawing.Image img = System.Drawing.Image.FromFile(Server.MapPath(imagen));
                    }
                    catch
                    {
                        imagen = "img/user-blank.jpg";
                    }

                    Session["variableImagenUsuario"] = imagen;
                    Session["variablePerfil"] = perfil;
                    Session["variableIdPerfil"] = idPerfil;
                    Session["variableIdUsuario"] = idUsuario;
                    Session["variableFechaSession"] = DateTime.Now.ToString("G");
                    Session["variableNomUsuario"] = nomUsuario;
                    Session["variableApeUsuario"] = apeUsuario;
                    Session["variableVerTodos"] = verTodos;
                    Session["variableIdEmpresa"] = idEmpresa;
                    Session["variableEmpresa"] = empresa;
                    Session["variableIdArea"] = idArea;
                    Session["variableArea"] = nomArea;
                    if (activo == "True")
                    {
                        Response.Redirect("Dashboard.aspx");
                    }
                    else
                    {
                        divAlerta.Visible = true;
                        lblError.Text = "El usuario no se encuentra activo. Favor comunicarse con el administrador.";
                        return;
                    }
                }
                else
                {
                    //error de usuario
                    divAlerta.Visible = true;
                    lblError.Text = "Nombre de usuario y/o contraseña no valida";
                }
            }
            catch (Exception ex)
            {
                divAlerta.Visible = true;
                lblError.Text = ex.Message;
            }


        }
    }
}