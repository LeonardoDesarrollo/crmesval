﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Globalization;
using System.ComponentModel;
using AegisImplicitMail;

namespace EsvalCrm
{
    public class Comun
    {
        public static bool esHoraValida(string hora)
        {
            Regex r = new Regex(@"([0-1][0-9]|2[0-3]):[0-5][0-9]");
            Match m = r.Match(hora);
            return m.Success;
        }

        public bool IsNumeric(string s)
        {
            float output;
            return float.TryParse(s, out output);
        }

        public bool EsNumerico(string sCaracteres)
        {
            foreach (char ch in sCaracteres)
            {
                if (!Char.IsNumber(ch))
                {
                    return false;
                }
            }
            return true;
        }

        public static string Right(string param, int length)
        {
            int value = param.Length - length;
            string result = param.Substring(value, length);
            return result;
        }

        public static string Left(string param, int length)
        {
            string result = param.Substring(0, length);
            return result;
        }

        public void separaRut(string rutConDv,out string rut, out string digito)
        {
            string rutCliente = rutConDv.Trim();
            string dv = string.Empty;
            
            String[] arRut = rutCliente.Split('-');
            for (int i = 0; i < arRut.Length; i++)
            {
                rutCliente = arRut[0];
                if (arRut.Length > 0)
                {
                    dv = arRut[1];
                }
            }
            rut = rutCliente;
            digito = dv;
        }

        public String formatearRutSinPuntos(String rut)
        {
            int cont = 0;
            String format;
            if (rut.Length == 0)
            {
                return "";
            }
            else
            {
                rut = rut.Replace(".", "");
                rut = rut.Replace("-", "");

                format = "-" + rut.Substring(rut.Length - 1);
                for (int i = rut.Length - 2; i >= 0; i--)
                {
                    format = rut.Substring(i, 1) + format;

                    cont++;
                    if (cont == 3 && i != 0)
                    {
                        format = "" + format;
                        cont = 0;
                    }
                }
                return format;
            }
        }

        public String formatearRutConPuntos(String rut)
        {
            int cont = 0;
            String format;
            if (rut.Length == 0)
            {
                return "";
            }
            else
            {
                rut = rut.Replace(".", "");
                rut = rut.Replace("-", "");
                format = "-" + rut.Substring(rut.Length - 1);
                for (int i = rut.Length - 2; i >= 0; i--)
                {
                    format = rut.Substring(i, 1) + format;

                    cont++;
                    if (cont == 3 && i != 0)
                    {
                        format = "." + format;
                        cont = 0;
                    }
                }
                return format;
            }
        }

        public bool verificaRut(int rut, string dv)
        {
            int Digito;
            int Contador;
            int Multiplo;
            int Acumulador;
            string RutDigito;

            Contador = 2;
            Acumulador = 0;

            while (rut != 0)
            {
                Multiplo = (rut % 10) * Contador;
                Acumulador = Acumulador + Multiplo;
                rut = rut / 10;
                Contador = Contador + 1;

                if (Contador == 8)
                {
                    Contador = 2;
                }
            }

            Digito = 11 - (Acumulador % 11);
            RutDigito = Digito.ToString().Trim();
            if (Digito == 10)
            {
                RutDigito = "K";
            }
            if (Digito == 11)
            {
                RutDigito = "0";
            }

            if (RutDigito.ToString() == dv.ToString())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void enviarEmail(string usuario, string email, string asunto, string texto)
        {
            System.Net.Mail.MailMessage correo = new System.Net.Mail.MailMessage();

            SmtpClient client = new SmtpClient();

            correo.From = new MailAddress("<alertas@dydcomchile.cl>");
            correo.To.Add(new MailAddress(email));

            correo.Subject = asunto;
            correo.IsBodyHtml = true;
            correo.Body = texto;

            client.Send(correo);
        }

        public bool validarRut(string rut)
        {
            bool validacion = false;
            try
            {
               int index = rut.IndexOf("-");
               if (index == -1)
               {
                   validacion = false;
                   return validacion;
               }
                rut = rut.ToUpper();
                rut = rut.Replace(".", "");
                rut = rut.Replace("-", "");
                int rutAux = int.Parse(rut.Substring(0, rut.Length - 1));

                char dv = char.Parse(rut.Substring(rut.Length - 1, 1));

                int m = 0, s = 1;
                for (; rutAux != 0; rutAux /= 10)
                {
                    s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
                }
                if (dv == (char)(s != 0 ? s + 47 : 75))
                {
                    validacion = true;
                }
            }
            catch (Exception)
            {
            }
            return validacion;
        }

        bool invalid = false;
        public bool IsValidEmail(string strIn)
        {
            invalid = false;
            if (String.IsNullOrEmpty(strIn))
                return false;

            // Use IdnMapping class to convert Unicode domain names.
            try
            {
                strIn = Regex.Replace(strIn, @"(@)(.+)$", this.DomainMapper,
                                      RegexOptions.None, TimeSpan.FromMilliseconds(200));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }

            if (invalid)
                return false;

            // Return true if strIn is in valid e-mail format.
            try
            {
                return Regex.IsMatch(strIn,
                      @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                      @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                      RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        private string DomainMapper(Match match)
        {
            // IdnMapping class with default property values.
            IdnMapping idn = new IdnMapping();

            string domainName = match.Groups[2].Value;
            try
            {
                domainName = idn.GetAscii(domainName);
            }
            catch (ArgumentException)
            {
                invalid = true;
            }
            return match.Groups[1].Value + domainName;
        }


        public void EnviarEmailDeriva(string usuarioAsignado, string ticket, string canal, string sistema, string servicio, string motivoLlamado, string NombreCompletoFuncionario, string comunaTrabajo, string telFijo, string cel, string email, string emailSendUsuarioAsignado, string emailCopia = null)
        {
            string subject = "Notificación de Escalamiento:";

            string body = "Estimado(a) <b>" + usuarioAsignado + "</b>,<br><br>";
            body += "Se ha generado el siguiente caso para su gestión:<br><br>";
            body += "Nro. de ticket: " + ticket + "<br>";
            body += "Canal: " + canal + "<br>";
            body += "Tipo de Solicitud: " + sistema + "<br>";
            body += "Categoría de la solicitud: " + servicio + "<br>";
            body += "Motivo del llamado: " + motivoLlamado + "<br><br>";
            body += "<b>DATOS USUARIO:</b><br>";
            body += "Nombre: " + NombreCompletoFuncionario + "<br>";
            body += "Comuna de trabajo: " + comunaTrabajo + "<br>";
            body += "Tel. fijo: " + telFijo + "<br>";
            body += "Celular: " + cel + "<br>";
            body += "Correo: " + email + "<br><br>";

            body += "Para gestionar el caso pinchar http://190.96.2.126/crmJUNJI/SeguimientoTicket.aspx?t=" + ticket + "<br>";
            body += "Atte.<br><br>";
            body += "CRM JUNJI.<br>";

            if (!string.IsNullOrEmpty(emailCopia))
            {
                emailSendUsuarioAsignado = emailSendUsuarioAsignado + ";" + emailCopia;
            }

            EnviarEmailSSLImplicito(emailSendUsuarioAsignado, body.Replace("\r\n", "<br>"), subject, null);
        }

        public void EnviarEmailCierre(string usuarioAsignado, string ticket, string canal, string sistema, string servicio, string motivoLlamado, string NombreCompletoFuncionario, string comunaTrabajo, string telFijo, string cel, string email, string emailSendUsuarioAsignado, string motivoCierre)
        {
            string subject = "NOTIFICACION DE CIERRE:";

            string body = "Estimado(a) <b>" + usuarioAsignado + ",</b><br><br>";
            body += "Se ha cerrado el siguiente caso:<br><br>";
            body += "Nro. de ticket: " + ticket + "<br>";
            body += "Canal: " + canal + "<br>";
            body += "Tipo de Solicitud: " + sistema + "<br>";
            body += "Categoría de la solicitud: " + servicio + "<br>";
            body += "Motivo del llamado: " + motivoLlamado + "<br>";
            body += "Motivo de cierre: " + motivoCierre + "<br><br>";
            body += "<b>DATOS USUARIO:</b><br>";
            body += "Nombre: " + NombreCompletoFuncionario + "<br>";
            body += "Comuna de trabajo: " + comunaTrabajo + "<br>";
            body += "Tel. fijo: " + telFijo + "<br>";
            body += "Celular: " + cel + "<br>";
            body += "Correo: " + email + "<br><br>";

            body += "Para ver el caso pinchar http://190.96.2.126/crmJUNJI/SeguimientoTicket.aspx?t=" + ticket + "<br>";
            body += "Atte.<br><br>";
            body += "CRM JUNJI.<br>";

            EnviarEmailSSLImplicito(emailSendUsuarioAsignado, body.Replace("\r\n", "<br>"), subject, null);
        }


        public void EnviarEmailSSLImplicito(string email, string body, string sub, string rutaAdjunto, string emailCopia = null)
        {
            var from = "notificacionescrmesval@upcom.cl";
            var host = "mail.upcom.cl";
            //var host = "smtp.upcomdts.cl";
            var user = "notificacionescrmesval@upcom.cl";
            var pass = "UpC0m1026..2803";
            //< network host = "smtp.upcomdts.cl" port = "25" userName = "crmjunji@upcomdts.cl" password = "UpC0m2019..3174" />
            //Generate Message
            var mymessage = new MimeMailMessage();
            mymessage.From = new MimeMailAddress(from);

            String[] AMailto = email.Split(';');
            foreach (String mail in AMailto)
            {
                mymessage.To.Add(new MailAddress(mail));
            }

            if (!string.IsNullOrEmpty(emailCopia))
            {
                String[] AMailtoCopia = emailCopia.Split(';');
                foreach (String mailCC in AMailtoCopia)
                {
                    mymessage.CC.Add(new MailAddress(mailCC));
                }
            }

            mymessage.Subject = sub;
            mymessage.Body = body;
            mymessage.SubjectEncoding = System.Text.Encoding.UTF8;
            mymessage.HeadersEncoding = System.Text.Encoding.UTF8;
            mymessage.IsBodyHtml = true;
            mymessage.Priority = MailPriority.High;

            //if (!string.IsNullOrEmpty(rutaAdjunto))
            //{
            //    MimeAttachment adj = new MimeAttachment(System.Web.HttpContext.Current.Server.MapPath(rutaAdjunto));
            //    mymessage.Attachments.Add(adj);
            //}

            //Create Smtp Client
            var mailer = new MimeMailer(host, 25);
            mailer.User = user;
            mailer.Password = pass;
            //mailer.SslType = SslMode.Ssl;
            //mailer.AuthenticationMode = AuthenticationType.Base64;

            //Set a delegate function for call back
            mailer.SendCompleted += compEvent;
            mailer.SendMailAsync(mymessage);
        }


        //Call back function
        private void compEvent(object sender, AsyncCompletedEventArgs e)
        {
            if (e.UserState != null)
                Console.Out.WriteLine(e.UserState.ToString());

            Console.Out.WriteLine("is it canceled? " + e.Cancelled);

            if (e.Error != null)
                Console.Out.WriteLine("Error : " + e.Error.Message);
        }

    }
}