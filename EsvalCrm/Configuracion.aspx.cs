﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using DataTableToExcel;

namespace EsvalCrm
{
    public partial class Configuracion : System.Web.UI.Page
    {
        Datos dal = new Datos();

        protected void Page_Load(object sender, EventArgs e)
        {
            lblInfo.Text = "";
            divAlerta.Visible = false;

            try
            {
                ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(this.ibtnExportarExcel);
                scriptManager.RegisterPostBackControl(this.bntNuevo);

                if (!this.Page.IsPostBack)
                {
                    buscarGrilla();
                    usuario1();
                    usuario2();
                    usuario3();
                    usuario4();
                    area();
                    tipoMotivoCierre();
                }
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }
        void area()
        {
            DdlArea.DataSource = dal.getBuscarArea();
            DdlArea.DataValueField = "ID_AREA";
            DdlArea.DataTextField = "AREA";
            DdlArea.DataBind();
        }
        protected void ibtnExportarExcel_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = dal.getBuscarConfiguracionExporte().Tables[0];
                //Utilidad.ExportDataTableToExcel(dt, "exporte.xls", "", "", "", "");
                //Utilidad.ExportDataTableToExcel(dt, "exporte.xls", "", "", "", "");
                Response.ContentType = "Application/x-msexcel";
                Response.AddHeader("content-disposition", "attachment;filename=" + "exporte_escalamiento" + ".csv");
                Response.ContentEncoding = Encoding.Unicode;
                Response.Write(Utilidad.ExportToCSVFile(dt));
                Response.End();
                //Utilidad.ExportDataTableToExcel(dt, "Exporte.xls", "", "", "", "");
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        void usuario1()
        {
            ddlUsuario1.DataSource = dal.getBuscarUsuario(null, null);
            ddlUsuario1.DataValueField = "ID_USUARIO";
            ddlUsuario1.DataTextField = "USUARIO";
            ddlUsuario1.DataBind();
        }
        void usuario2()
        {
            ddlUsuario2.DataSource = dal.getBuscarUsuario(null, null);
            ddlUsuario2.DataValueField = "ID_USUARIO";
            ddlUsuario2.DataTextField = "USUARIO";
            ddlUsuario2.DataBind();
        }
        void usuario3()
        {
            ddlUsuario3.DataSource = dal.getBuscarUsuario(null, null);
            ddlUsuario3.DataValueField = "ID_USUARIO";
            ddlUsuario3.DataTextField = "USUARIO";
            ddlUsuario3.DataBind();
        }
        void usuario4()
        {
            ddlUsuario4.DataSource = dal.getBuscarUsuario(null, null);
            ddlUsuario4.DataValueField = "ID_USUARIO";
            ddlUsuario4.DataTextField = "USUARIO";
            ddlUsuario4.DataBind();
        }
        void tipoMotivoCierre()
        {
            ddlTipoMotivoCierre.DataSource = dal.getBuscarTipoMotivoCierre();
            ddlTipoMotivoCierre.DataTextField = "TIPO_MOTIVO_CIERRE";
            ddlTipoMotivoCierre.DataValueField = "ID_TIPO_MOTIVO_CIERRE";
            ddlTipoMotivoCierre.DataBind();
        }

        protected void ddlUsuario1_DataBound(object sender, EventArgs e)
        {
            ddlUsuario1.Items.Insert(0, new ListItem("Seleccione", "0"));
        }
        protected void ddlUsuario2_DataBound(object sender, EventArgs e)
        {
            ddlUsuario2.Items.Insert(0, new ListItem("Seleccione", "0"));
        }
        protected void ddlUsuario3_DataBound(object sender, EventArgs e)
        {
            ddlUsuario3.Items.Insert(0, new ListItem("Seleccione", "0"));
        }
        protected void ddlUsuario4_DataBound(object sender, EventArgs e)
        {
            ddlUsuario4.Items.Insert(0, new ListItem("Seleccione", "0"));
        }
        protected void ddlTipoMotivoCierre_DataBound(object sender, EventArgs e)
        {
            ddlTipoMotivoCierre.Items.Insert(0, new ListItem("Seleccione", "0"));
        }

        void buscarGrilla()
        {
            divGrilla.Visible = true;
            grvConfiguracion.DataSource = dal.getBuscarConfiguracion();
            grvConfiguracion.DataBind();
            grvConfiguracion.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        //protected void grvConfiguracion_RowCreated(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowIndex == 0)
        //    {
        //        //e.Row.RowType = DataControlRowType.Header;

        //    }
        //}

        protected void btnEditar_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                GridViewRow row = (GridViewRow)btn.NamingContainer;

                Label _lblId = (Label)grvConfiguracion.Rows[row.RowIndex].FindControl("lblId");
                Label _lblEmpresa = (Label)grvConfiguracion.Rows[row.RowIndex].FindControl("lblEmpresa");
                Label _lblNivel1 = (Label)grvConfiguracion.Rows[row.RowIndex].FindControl("lblNivel1");
                Label _lblNivel2 = (Label)grvConfiguracion.Rows[row.RowIndex].FindControl("lblNivel2");
                Label _lblNivel3 = (Label)grvConfiguracion.Rows[row.RowIndex].FindControl("lblNivel3");
                Label _lblNivel4 = (Label)grvConfiguracion.Rows[row.RowIndex].FindControl("lblNivel4");

                Label _lblUsuario1 = (Label)grvConfiguracion.Rows[row.RowIndex].FindControl("lblIdUsuario1");
                Label _lblUsuario2 = (Label)grvConfiguracion.Rows[row.RowIndex].FindControl("lblIdUsuario2");
                Label _lblUsuario3 = (Label)grvConfiguracion.Rows[row.RowIndex].FindControl("lblIdUsuario3");
                Label _lblUsuario4 = (Label)grvConfiguracion.Rows[row.RowIndex].FindControl("lblIdUsuario4");

                Label _lblSla1 = (Label)grvConfiguracion.Rows[row.RowIndex].FindControl("lblSla1");
                Label _lblSla2 = (Label)grvConfiguracion.Rows[row.RowIndex].FindControl("lblSla2");
                Label _lblSla3 = (Label)grvConfiguracion.Rows[row.RowIndex].FindControl("lblSla3");
                Label _lblSla4 = (Label)grvConfiguracion.Rows[row.RowIndex].FindControl("lblSla4");

                Label _LblIdArea = (Label)grvConfiguracion.Rows[row.RowIndex].FindControl("LblIdArea");
                Label _lblIdTpo = (Label)grvConfiguracion.Rows[row.RowIndex].FindControl("lblIdTpo");
                //JRL
                Label _lblClase = (Label)grvConfiguracion.Rows[row.RowIndex].FindControl("lblClase");

                Label _lblIdTipoMotivoCierre = (Label)grvConfiguracion.Rows[row.RowIndex].FindControl("lblIdTipoMotivoCierre");

                //Label _lblVisibleAtento = (Label)grvConfiguracion.Rows[row.RowIndex].FindControl("lblVisibleAtento");
                Label _lblVisibleClienteInterno = (Label)grvConfiguracion.Rows[row.RowIndex].FindControl("lblVisibleClienteInterno");
                Label _lblVisibleClienteExterno = (Label)grvConfiguracion.Rows[row.RowIndex].FindControl("lblVisibleClienteExterno");

                Label _lblidDetenido = (Label)grvConfiguracion.Rows[row.RowIndex].FindControl("lblidDetenido");
                Label _lblidActivo = (Label)grvConfiguracion.Rows[row.RowIndex].FindControl("lblidActivo");

                limpiar();
                txtEmpresa.Text = _lblEmpresa.Text;

                if (_lblUsuario1.Text != string.Empty)
                {
                    ddlUsuario1.SelectedValue = _lblUsuario1.Text;

                }
                if (_lblUsuario2.Text != string.Empty)
                {
                    ddlUsuario2.SelectedValue = _lblUsuario2.Text;
                }
                if (_lblUsuario3.Text != string.Empty)
                {
                    ddlUsuario3.SelectedValue = _lblUsuario3.Text;
                }
                if (_lblUsuario4.Text != string.Empty)
                {
                    ddlUsuario4.SelectedValue = _lblUsuario4.Text;
                }

                if (_lblIdTpo.Text == string.Empty)
                {
                    ddlTipo.SelectedValue = "0";
                }
                else if (_lblIdTpo.Text != string.Empty)
                {
                    ddlTipo.SelectedValue = _lblIdTpo.Text;
                }

                if (!string.IsNullOrEmpty(_lblidDetenido.Text))
                {
                    ddlDetenido.SelectedValue = _lblidDetenido.Text;
                }
                else
                {
                    ddlDetenido.SelectedValue = "0";
                }


                ddlActivo.SelectedValue = _lblidActivo.Text;

                //JRL
                if (_lblClase.Text == "RECLAMO")
                {
                    ddlClase.SelectedValue = "1";
                }
                else
                {
                    ddlClase.SelectedValue = "0";
                }

                if (_lblIdTipoMotivoCierre.Text != string.Empty)
                {
                    ddlTipoMotivoCierre.SelectedValue = _lblIdTipoMotivoCierre.Text;
                }

                if (_lblVisibleClienteExterno.Text == "0")
                {
                    ddlVisibleClienteExterno.SelectedValue = "0";
                }
                else
                {
                    ddlVisibleClienteExterno.SelectedValue = "1";
                }

                if (_lblVisibleClienteInterno.Text == "0")
                {
                    ddlVisibleClienteInterno.SelectedValue = "0";
                }
                else
                {
                    ddlVisibleClienteInterno.SelectedValue = "1";
                }

                if (_LblIdArea.Text != "0")
                {
                    DdlArea.SelectedValue = _LblIdArea.Text;
                }

                txtSLA1.Text = _lblSla1.Text;
                txtSLA2.Text = _lblSla2.Text;
                txtSLA3.Text = _lblSla3.Text;
                txtSLA4.Text = _lblSla4.Text;

                hfNivel1.Value = _lblNivel1.Text;
                hfNivel2.Value = _lblNivel2.Text;
                hfNivel3.Value = _lblNivel3.Text;
                hfNivel4.Value = _lblNivel4.Text;

                txtNivel1.Text = hfNivel1.Value;
                txtNivel2.Text = hfNivel2.Value;
                txtNivel3.Text = hfNivel3.Value;
                txtNivel4.Text = hfNivel4.Value;

                hfIdTipificacion.Value = _lblId.Text;

                divGrilla.Visible = false;
                divEditarConfiguracion.Visible = true;
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if(string.IsNullOrEmpty(txtSLA1.Text.Trim()))
                {
                    txtSLA1.Text = "0";
                }

                if (ddlTipo.SelectedValue == "D")
                {
                    //2021-08-11 Comentado
                    //if (ddlUsuario1.SelectedValue != "0" && txtSLA1.Text != string.Empty)
                    //{
                        dal.setEditarConfiguracion(ddlUsuario1.SelectedValue, ddlUsuario2.SelectedValue, ddlUsuario3.SelectedValue, ddlUsuario4.SelectedValue,
                          txtSLA1.Text, txtSLA2.Text, txtSLA3.Text, txtSLA4.Text, hfIdTipificacion.Value,
                          ddlTipo.SelectedValue, ddlTipoMotivoCierre.SelectedValue, null,
                          "1", "1", ddlDetenido.SelectedValue,
                          txtNivel1.Text, txtNivel2.Text, txtNivel3.Text, txtNivel4.Text, ddlClase.SelectedItem.ToString(), Convert.ToInt32(DdlArea.SelectedValue), ddlActivo.SelectedValue);

                        buscarGrilla();
                        divGrilla.Visible = true;
                        divEditarConfiguracion.Visible = false;
                        lblInfo.Text = "Tipificación Editada exitosamente";
                        divAlerta.Attributes["class"] = "alert alert-success";
                        divAlerta.Visible = true;
                    //}
                    //else
                    //{
                    //    lblInfo.Text = "Debe ingresar por lo menos un usuario y SLA correspondiente.";
                    //    divAlerta.Attributes["class"] = "alert alert-danger";
                    //    divAlerta.Visible = true;
                    //}
                }
                else
                {
                    dal.setEditarConfiguracion(ddlUsuario1.SelectedValue, ddlUsuario2.SelectedValue, ddlUsuario3.SelectedValue, ddlUsuario4.SelectedValue,
                          txtSLA1.Text, txtSLA2.Text, txtSLA3.Text, txtSLA4.Text, hfIdTipificacion.Value,
                          ddlTipo.SelectedValue, ddlTipoMotivoCierre.SelectedValue, null,
                          "1", "1", ddlDetenido.SelectedValue,
                          txtNivel1.Text, txtNivel2.Text, txtNivel3.Text, txtNivel4.Text, ddlClase.SelectedItem.ToString(), Convert.ToInt32(DdlArea.SelectedValue), ddlActivo.SelectedValue);

                    buscarGrilla();
                    divGrilla.Visible = true;
                    divEditarConfiguracion.Visible = false;
                    lblInfo.Text = "Tipificación Editada exitosamente";
                    divAlerta.Attributes["class"] = "alert alert-success";
                    divAlerta.Visible = true;
                }
            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                divAlerta.Attributes["class"] = "alert alert-danger";
                divAlerta.Visible = true;
            }
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            buscarGrilla();
            divGrilla.Visible = true;
            divEditarConfiguracion.Visible = false;
        }

        void limpiar()
        {
            txtSLA1.Text = string.Empty;
            txtSLA2.Text = string.Empty;
            txtSLA3.Text = string.Empty;
            txtSLA4.Text = string.Empty;
            ddlUsuario1.ClearSelection();
            ddlUsuario2.ClearSelection();
            ddlUsuario3.ClearSelection();
            ddlUsuario4.ClearSelection();

            ddlTipo.ClearSelection();
            ddlTipoMotivoCierre.ClearSelection();

            ddlVisibleClienteExterno.ClearSelection();
            ddlVisibleClienteInterno.ClearSelection();
        }

        protected void bntNuevo_Click(object sender, EventArgs e)
        {
            Response.Redirect("ConfiguracionIn.aspx");
        }

        protected void btnCancelar_Click1(object sender, EventArgs e)
        {
            Response.Redirect("Configuracion.aspx");

        }

        protected void DdlArea_DataBound(object sender, EventArgs e)
        {
            DdlArea.Items.Insert(0, new ListItem("Seleccione", "0"));
        }
    }
}