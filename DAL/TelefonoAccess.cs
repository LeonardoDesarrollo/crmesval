﻿using System;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;
using System.Data.Common;

namespace DAL
{
    public class TelefonoAccess
    {
        Database db = DatabaseFactory.CreateDatabase();

        public string SetInTelefono(string id, string nombre, string telefono)
        {
           
            DbCommand cmd = db.GetStoredProcCommand("usp_GM_TELEFONOInsert");
            db.AddInParameter(cmd, "@ID", DbType.String, id);
            db.AddInParameter(cmd, "@NOMBRE", DbType.String, nombre);
            db.AddInParameter(cmd, "@TELEFONO", DbType.String, telefono);
            try
            {
                string val = db.ExecuteScalar(cmd).ToString();
                return id = val;
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el teléfono, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el teléfono, " + ex.Message, ex);
            }

            //return idUsAsig;
        }

        public void SetUpTelefono(int Id, string nombre, string telefono)
        {
            DbCommand cmd = db.GetStoredProcCommand("usp_GM_RESPUESTASUpdate");


            db.AddInParameter(cmd, "@ID", DbType.String, Id);
            db.AddInParameter(cmd, "@NOMBRE", DbType.String, nombre);
            db.AddInParameter(cmd, "@TELEFONO", DbType.String, telefono);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo editar, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo editar, " + ex.Message, ex);
            }
        }

        public DataSet GetTelefonos(int Id=0)
        {
            DbCommand cmd = db.GetStoredProcCommand("usp_GM_TELEFONOSelect");
            if (Id != 0)
            {
                db.AddInParameter(cmd, "@ID", DbType.String, Id);
            }

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar, " + ex.Message, ex);
            }
        }

        public void SetDelTelefono(int Id)
        {
            DbCommand cmd = db.GetStoredProcCommand("usp_GM_TELEFONODelete");

            db.AddInParameter(cmd, "@ID", DbType.String, Id);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo eliminar, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo eliminar, " + ex.Message, ex);
            }
        }
    }

  
}
