﻿using System;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;
using System.Data.Common;

namespace DAL
{
    public class Datos
    {
        Database db = DatabaseFactory.CreateDatabase();

        //JRL
        public string obtenerIdUsuarioAsig(string nombreUsuarioAsig)
        {
            string idUsAsig = "";

            DbCommand cmd = db.GetStoredProcCommand("stp_ObtieneIdUsuarioAsig");
            db.AddInParameter(cmd, "@nomUsuarioA", DbType.String, nombreUsuarioAsig);

            try
            {
                string val = db.ExecuteScalar(cmd).ToString();
                return idUsAsig = val;
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el Id del usuario, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el Id del usuario, " + ex.Message, ex);
            }

            //return idUsAsig;
        }

        public int setInRespuesta(string Tipologia, string Tema, string SubTema, string PalabrasClaves, string pregunta, string respuesta, string DocumentoAsociado, string DocumentoAsociado2)
        {
            DbCommand cmd = db.GetStoredProcCommand("usp_GM_RESPUESTASInsert");

            //@TIPOLOGIA varchar(100) = NULL,
            //@TEMA varchar(100) = NULL,
            //@SUBTEMA varchar(100) = NULL,
            //@PALABRAS_CLAVES varchar(100) = NULL,
            //@PREGUNTA varchar(4000) = NULL,
            //@RESPUESTA varchar(500) = NULL,
            //@DOCUMENTO_ASOCIADO varchar(50) = NULL,
            //@DOCUMENTO_ASOCIADO_2 varchar(50) = NULL

            db.AddInParameter(cmd, "@TIPOLOGIA", DbType.String, Tipologia);
            db.AddInParameter(cmd, "@TEMA", DbType.String, Tema);
            db.AddInParameter(cmd, "@SUBTEMA", DbType.String, SubTema);
            db.AddInParameter(cmd, "@PALABRAS_CLAVES", DbType.String, PalabrasClaves);
            db.AddInParameter(cmd, "@PREGUNTA", DbType.String, pregunta);
            db.AddInParameter(cmd, "@RESPUESTA", DbType.String, respuesta);
            //db.AddInParameter(cmd, "@DOCUMENTO_ASOCIADO", DbType.String, DocumentoAsociado);
            //db.AddInParameter(cmd, "@DOCUMENTO_ASOCIADO_2", DbType.String, DocumentoAsociado2);

            try
            {
                int valor = Convert.ToInt32(db.ExecuteScalar(cmd));
                return valor;
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo ingresar, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo ingresar, " + ex.Message, ex);
            }
        }
        public void SetEditarRespuesta(int Id, string Tipologia, string Tema, string SubTema, string PalabrasClaves, string pregunta, string respuesta, string DocumentoAsociado, string DocumentoAsociado2)
        {
            DbCommand cmd = db.GetStoredProcCommand("usp_GM_RESPUESTASUpdate");


            db.AddInParameter(cmd, "@ID", DbType.String, Id);
            db.AddInParameter(cmd, "@TIPOLOGIA", DbType.String, Tipologia);
            db.AddInParameter(cmd, "@TEMA", DbType.String, Tema);
            db.AddInParameter(cmd, "@SUBTEMA", DbType.String, SubTema);
            db.AddInParameter(cmd, "@PALABRAS_CLAVES", DbType.String, PalabrasClaves);
            db.AddInParameter(cmd, "@PREGUNTA", DbType.String, pregunta);
            db.AddInParameter(cmd, "@RESPUESTA", DbType.String, respuesta);
            //db.AddInParameter(cmd, "@DOCUMENTO_ASOCIADO", DbType.String, DocumentoAsociado);
            //db.AddInParameter(cmd, "@DOCUMENTO_ASOCIADO_2", DbType.String, DocumentoAsociado2);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo editar, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo editar, " + ex.Message, ex);
            }
        }

        public void setEliminarRespuesta(int id)
        {
            DbCommand cmd = db.GetStoredProcCommand("usp_GM_RESPUESTASDelete");

            db.AddInParameter(cmd, "@id", DbType.String, id);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo eliminar, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo eliminar, " + ex.Message, ex);
            }
        }

        public DataSet getBuscarCategoriaDocumento(int Id)
        {
            DbCommand cmd = db.GetStoredProcCommand("usp_GM_CATEGORIA_DOCUMENTOSelect");
            if (Id != 0)
            {
                db.AddInParameter(cmd, "@ID_CATEGORIA_DOCUMENTO", DbType.String, Id);
            }

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar, " + ex.Message, ex);
            }
        }

        public DataSet getBuscarDocumento(int Id, string nombre, int idCategoria, string palabraClave, int? activo = null)
        {
            DbCommand cmd = db.GetStoredProcCommand("usp_GM_DOCUMENTOSelect");
            if (Id != 0)
            {
                db.AddInParameter(cmd, "@ID_DOCUMENTO", DbType.String, Id);
            }

            if (!string.IsNullOrEmpty(nombre))
            {
                db.AddInParameter(cmd, "@NOMBRE", DbType.String, nombre);
            }

            if (idCategoria != 0)
            {
                db.AddInParameter(cmd, "@ID_CATEGORIA_DOCUMENTO", DbType.String, idCategoria);
            }

            if (!string.IsNullOrEmpty(palabraClave))
            {
                db.AddInParameter(cmd, "@PALABRA_CLAVE", DbType.String, palabraClave);
            }

            db.AddInParameter(cmd, "@ACTIVO", DbType.Int16, activo);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar, " + ex.Message, ex);
            }
        }

        public void SetEditarDocumento(int Id, string nombre, string descripcion, int IdCategoriaDocumento, string archivo, int IdUsuarioIngreso, bool? Banner, int Activo, string palabraClave)
        {
            DbCommand cmd = db.GetStoredProcCommand("usp_GM_DOCUMENTOUpdate");

            db.AddInParameter(cmd, "@ID_DOCUMENTO", DbType.String, Id);
            db.AddInParameter(cmd, "@NOMBRE", DbType.String, nombre);
            db.AddInParameter(cmd, "@DESCRIPCION", DbType.String, descripcion);
            if (IdCategoriaDocumento != 0)
            {
                db.AddInParameter(cmd, "@ID_CATEGORIA_DOCUMENTO", DbType.String, IdCategoriaDocumento);
            }
            db.AddInParameter(cmd, "@ARCHIVO", DbType.String, archivo);
            db.AddInParameter(cmd, "@ID_USUARIO_INGRESO", DbType.String, IdUsuarioIngreso);
            if (Banner != null)
            {
                db.AddInParameter(cmd, "@BANNER", DbType.String, Banner);
            }
            db.AddInParameter(cmd, "@ACTIVO", DbType.String, Activo);
            db.AddInParameter(cmd, "@PALABRA_CLAVE", DbType.String, palabraClave);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo editar, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo editar, " + ex.Message, ex);
            }
        }

        public void SetEliminarDocumento(int Id)
        {
            DbCommand cmd = db.GetStoredProcCommand("usp_GM_DOCUMENTODelete");

            db.AddInParameter(cmd, "@ID_DOCUMENTO", DbType.String, Id);
          
            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo eliminar, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo eliminar, " + ex.Message, ex);
            }
        }

        public int setInDocumento(string nombre, string descripcion, int IdCategoriaDocumento, string archivo, int IdUsuarioIngreso, bool? Banner, int Activo, string palabraClave)
        {
            DbCommand cmd = db.GetStoredProcCommand("usp_GM_DOCUMENTOInsert");



            db.AddInParameter(cmd, "@NOMBRE", DbType.String, nombre);
            db.AddInParameter(cmd, "@DESCRIPCION", DbType.String, descripcion);

            if (IdCategoriaDocumento != 0)
            {
                db.AddInParameter(cmd, "@ID_CATEGORIA_DOCUMENTO", DbType.String, IdCategoriaDocumento);
            }

            db.AddInParameter(cmd, "@ARCHIVO", DbType.String, archivo);
            db.AddInParameter(cmd, "@ID_USUARIO_INGRESO", DbType.String, IdUsuarioIngreso);
            if (Banner != null)
            {
                db.AddInParameter(cmd, "@BANNER", DbType.String, Banner);
            }

            db.AddInParameter(cmd, "@ACTIVO", DbType.String, Activo);
            db.AddInParameter(cmd, "@PALABRA_CLAVE", DbType.String, palabraClave);

            try
            {
                int valor = Convert.ToInt32(db.ExecuteScalar(cmd));
                return valor;
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo ingresar, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo ingresar, " + ex.Message, ex);
            }
        }

        public DataSet getBuscarRespuestasId(int Id)
        {
            DbCommand cmd = db.GetStoredProcCommand("usp_GM_RESPUESTASSelect");
            if (Id != 0)
            {
                db.AddInParameter(cmd, "@ID", DbType.Int16, Id);
            }


            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar las respuestas, sp [usp_GM_RESPUESTASSelect] " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar las respuestas, " + ex.Message, ex);
            }
        }

        //public void setEliminarRespuesta(int id)
        //{
        //    DbCommand cmd = db.GetStoredProcCommand("usp_GM_RESPUESTASDelete");

        //    db.AddInParameter(cmd, "@id", DbType.String, id);

        //    try
        //    {
        //        db.ExecuteNonQuery(cmd);
        //    }
        //    catch (SqlException ex)
        //    {
        //        throw new Exception("No se pudo eliminar, " + ex.Message, ex);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception("No se pudo eliminar, " + ex.Message, ex);
        //    }
        //}

        public void setIngresarDocumentoRespuesta(int IdRespuesta, int IdDocumento)
        {
            DbCommand cmd = db.GetStoredProcCommand("usp_RELA_DOC_RESPInsert");

            db.AddInParameter(cmd, "@ID_RESPUESTA", DbType.String, IdRespuesta);
            db.AddInParameter(cmd, "@ID_DOCUMENTO", DbType.String, IdDocumento);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("error al ingresar, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("error al ingresar, " + ex.Message, ex);
            }
        }

        public DataSet getBuscarDocumentosRespuesta(int Id)
        {
            DbCommand cmd = db.GetStoredProcCommand("usp_RELA_DOC_RESPSelect");
            if (Id != 0)
            {
                db.AddInParameter(cmd, "@ID_RESPUESTA", DbType.String, Id);
            }

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar, " + ex.Message, ex);
            }
        }

        public void setEliminarDocumentoRespuesta(int idDocumento, int IdRespuesta)
        {
            DbCommand cmd = db.GetStoredProcCommand("usp_RELA_DOC_RESPDelete");

            db.AddInParameter(cmd, "@ID_RESPUESTA", DbType.String, IdRespuesta);
            db.AddInParameter(cmd, "@ID_DOCUMENTO", DbType.String, idDocumento);
            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo eliminar, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo eliminar, " + ex.Message, ex);
            }
        }


        public string getValUsuario(string usuario, string contrasena)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_validaUsuario");
            db.AddInParameter(cmd, "@usuario", DbType.String, usuario);
            db.AddInParameter(cmd, "@contrasena", DbType.String, contrasena);

            try
            {
                string val = db.ExecuteScalar(cmd).ToString();
                return val;
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el usuario, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el usuario, " + ex.Message, ex);
            }
        }

        public DataSet getBuscarComuna(string nombre)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarComuna");

            if (nombre == string.Empty)
            {
                db.AddInParameter(cmd, "@nombre", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@nombre", DbType.String, nombre);
            }

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar la comuna, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar la comuna, " + ex.Message, ex);
            }
        }

        public DataSet getBuscarUsuarioPorLogin(string nombre)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarUsuarioLogin");

            if (nombre == string.Empty)
            {
                db.AddInParameter(cmd, "@nombre", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@nombre", DbType.String, nombre);
            }

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el usuario, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el usuario, " + ex.Message, ex);
            }
        }

        //Usuarios
        public DataSet getBuscarUsuarioArea(string area, string nombre)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarUsuarioArea");


            if (nombre == string.Empty)
            {
                db.AddInParameter(cmd, "@nombre", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@nombre", DbType.String, "%" + nombre + "%");
            }



            if (area == string.Empty)
            {
                db.AddInParameter(cmd, "@area", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@area", DbType.String, area);
            }


            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el  area usuario, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el area usuario, " + ex.Message, ex);
            }
        }

        //Usuarios
        public DataSet getBuscarUsuario(string login, string nombre)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarUsuario");


            if (nombre == string.Empty)
            {
                db.AddInParameter(cmd, "@nombre", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@nombre", DbType.String, "%" + nombre + "%");
            }



            if (login == string.Empty)
            {
                db.AddInParameter(cmd, "@login", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@login", DbType.String, "%" + login + "%");
            }


            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el usuario, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el usuario, " + ex.Message, ex);
            }
        }

        public DataSet getBuscarUsuarioExporte()
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarUsuarioExporte");

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el usuario exporte, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el usuario exporte, " + ex.Message, ex);
            }
        }

        public DataSet getBuscarRespuestaExporte()
        {
            DbCommand cmd = db.GetStoredProcCommand("usp_GM_RESPUESTASSelectExporte");

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar la Respuesta exporte, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar la Respuesta exporte, " + ex.Message, ex);
            }
        }

        public DataSet getBuscarTelefonoExporte()
        {
            DbCommand cmd = db.GetStoredProcCommand("usp_GM_TELEFONOSelectExporte");

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el Teléfono exporte, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el Teléfono exporte, " + ex.Message, ex);
            }
        }


        public void setEliminarUsuario(string idUsuario)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_EliminarUsuario");
            db.AddInParameter(cmd, "@idUsuario", DbType.String, idUsuario);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo eliminar el usuario, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo eliminar el usuario, " + ex.Message, ex);
            }
        }
        public void setEliminarCanal(int IdCanal)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_EliminarCanal");
            db.AddInParameter(cmd, "@IdCanal", DbType.Int32, IdCanal);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo eliminar el Canal, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo eliminar el Canal, " + ex.Message, ex);
            }
        }


        public void setEditarContrasena(string idUsuario, string contrasena)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_EditarContrasena");
            db.AddInParameter(cmd, "@idUsuario", DbType.String, idUsuario);
            db.AddInParameter(cmd, "@contrasena", DbType.String, contrasena);
            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo editar el usuario, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo editar el usuario, " + ex.Message, ex);
            }
        }


        public void setEditarEmail(int IdEmail, int IdAtencion)
        {

            DbCommand cmd = db.GetStoredProcCommand("stp_EditarEmail");
            db.AddInParameter(cmd, "@IdEmail", DbType.String, IdEmail);
            db.AddInParameter(cmd, "@IdAtencion", DbType.String, IdAtencion);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo editar el usuario, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo editar el usuario, " + ex.Message, ex);
            }
        }
        public void setEditarServicio(string idServicio, string servicio, string idScript, string activo, string idSoftCall)
        {
            DbCommand cmd = db.GetStoredProcCommand("usp_GM_SERVICIOUpdate");

            db.AddInParameter(cmd, "@ID_SERVICIO", DbType.String, idServicio);
            db.AddInParameter(cmd, "@SERVICIO", DbType.String, servicio);

            if (idScript != "0")
            {
                db.AddInParameter(cmd, "@ID_SCRIPT", DbType.String, idScript);
            }

            db.AddInParameter(cmd, "@ACTIVO", DbType.String, activo);

            if (!string.IsNullOrEmpty(idSoftCall))
            {
                db.AddInParameter(cmd, "@ID_SOFTCALL", DbType.String, idSoftCall);
            }

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo editar el usuario, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo editar el usuario, " + ex.Message, ex);
            }
        }


        public void setEditarScript(string idScript, string script, string activo, string ruta1)
        {

            DbCommand cmd = db.GetStoredProcCommand("usp_GM_SCRIPTUpdate");
            db.AddInParameter(cmd, "@ID_SCRIPT", DbType.String, idScript);
            db.AddInParameter(cmd, "@NOMBRE_SCRIPT", DbType.String, script);
            db.AddInParameter(cmd, "@ACTIVO", DbType.String, activo);
            db.AddInParameter(cmd, "@RUTA_ARCHIVO_1", DbType.String, ruta1);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo editar el script, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo editar el script, " + ex.Message, ex);
            }
        }

        public void setEditarCanal(int IdCanal, string Canal, string Adicional1, string Adicional2,
           string Adicional3, string Adicional4, string Activo, bool rAdicional1,
            bool rAdicional2, bool rAdicional3, bool rAdicional4)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_EditarCanal");

            db.AddInParameter(cmd, "@IdCanal", DbType.Int32, IdCanal);
            db.AddInParameter(cmd, "@Canal", DbType.String, Canal);
            db.AddInParameter(cmd, "@Adicional1", DbType.String, Adicional1);
            db.AddInParameter(cmd, "@Adicional2", DbType.String, Adicional2);
            db.AddInParameter(cmd, "@Adicional3", DbType.String, Adicional3);
            db.AddInParameter(cmd, "@Adicional4", DbType.String, Adicional4);

            db.AddInParameter(cmd, "@rAdicional1", DbType.Boolean, rAdicional1);
            db.AddInParameter(cmd, "@rAdicional2", DbType.Boolean, rAdicional2);
            db.AddInParameter(cmd, "@rAdicional3", DbType.Boolean, rAdicional3);
            db.AddInParameter(cmd, "@rAdicional4", DbType.Boolean, rAdicional4);


            db.AddInParameter(cmd, "@Activo", DbType.Byte, Activo);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo editar el canal, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo editar el canal, " + ex.Message, ex);
            }
        }

        public void setIngresarCanal(string Canal, string Adicional1, string Adicional2,
         string Adicional3, string Adicional4, string Activo, bool rAdicional1,
            bool rAdicional2, bool rAdicional3, bool rAdicional4)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_IngresarCanal");

            db.AddInParameter(cmd, "@Canal", DbType.String, Canal);
            db.AddInParameter(cmd, "@Adicional1", DbType.String, Adicional1);
            db.AddInParameter(cmd, "@Adicional2", DbType.String, Adicional2);
            db.AddInParameter(cmd, "@Adicional3", DbType.String, Adicional3);
            db.AddInParameter(cmd, "@Adicional4", DbType.String, Adicional4);

            db.AddInParameter(cmd, "@rAdicional1", DbType.Boolean, rAdicional1);
            db.AddInParameter(cmd, "@rAdicional2", DbType.Boolean, rAdicional2);
            db.AddInParameter(cmd, "@rAdicional3", DbType.Boolean, rAdicional3);
            db.AddInParameter(cmd, "@rAdicional4", DbType.Boolean, rAdicional4);

            db.AddInParameter(cmd, "@Activo", DbType.Byte, Activo);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo editar el canal, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo editar el canal, " + ex.Message, ex);
            }
        }


        public void setEditarUsuario(string idUsuario, string usuario, string contrasena, string email,
            string perfil, string nombre, string activo, string idEmpresa, string idArea, int idServicio, int IdCargo, string ID_ZONA)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_EditarUsuario15112016");

            db.AddInParameter(cmd, "@idUsuario", DbType.String, idUsuario);
            db.AddInParameter(cmd, "@usuario", DbType.String, usuario);
            db.AddInParameter(cmd, "@contrasena", DbType.String, contrasena);
            db.AddInParameter(cmd, "@email", DbType.String, email);
            db.AddInParameter(cmd, "@perfil", DbType.String, perfil);
            db.AddInParameter(cmd, "@nombre", DbType.String, nombre);
            db.AddInParameter(cmd, "@activo", DbType.String, activo);
            db.AddInParameter(cmd, "@idServicio", DbType.String, idServicio);
            db.AddInParameter(cmd, "@IdCargo", DbType.String, IdCargo);

            if (idEmpresa == "0")
            {
                db.AddInParameter(cmd, "@idEmpresa", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idEmpresa", DbType.String, idEmpresa);
            }

            db.AddInParameter(cmd, "@idArea", DbType.String, idArea);

            if (!string.IsNullOrEmpty(ID_ZONA) && ID_ZONA != "0")
            {
                db.AddInParameter(cmd, "@ID_ZONA", DbType.String, ID_ZONA);
            }

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo editar el usuario, sp [stp_EditarUsuario15112016] " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo editar el usuario, " + ex.Message, ex);
            }
        }

        public string setIngresarUsuario(string usuario, string contrasena, string email, string perfil, string nombre, 
            string activo, string idEmpresa, string idArea, int idServicio, int IdCargo, string ID_ZONA)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_IngresarUsuario15112016");

            db.AddInParameter(cmd, "@usuario", DbType.String, usuario);
            db.AddInParameter(cmd, "@contrasena", DbType.String, contrasena);
            db.AddInParameter(cmd, "@email", DbType.String, email);
            db.AddInParameter(cmd, "@perfil", DbType.String, perfil);
            db.AddInParameter(cmd, "@nombre", DbType.String, nombre);

            if (idServicio == 0)
            {
                db.AddInParameter(cmd, "@idServicio", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idServicio", DbType.String, idServicio);
            }

            if (IdCargo == 0)
            {
                db.AddInParameter(cmd, "@IdCargo", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@IdCargo", DbType.String, IdCargo);
            }

            if (idEmpresa == "0")
            {
                db.AddInParameter(cmd, "@idEmpresa", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idEmpresa", DbType.String, idEmpresa);
            }

            db.AddInParameter(cmd, "@idArea", DbType.String, idArea);
            db.AddInParameter(cmd, "@activo", DbType.String, activo);
            //db.AddInParameter(cmd, "@idGrupo", DbType.String, idGrupo);
            //db.AddInParameter(cmd, "@idLocalidad", DbType.String, idLocalidad);

            if (!string.IsNullOrEmpty(ID_ZONA) && ID_ZONA != "0")
            {
                db.AddInParameter(cmd, "@ID_ZONA", DbType.String, ID_ZONA);
            }

            try
            {
                string valor;
                valor = db.ExecuteScalar(cmd).ToString();

                //db.ExecuteNonQuery(cmd);

                return valor;
                //db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo ingresar el usuario, sp [stp_IngresarUsuario15112016] " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo ingresar el usuario, " + ex.Message, ex);
            }
        }

        public DataSet getBuscarParametro(string idParametro)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarParametro");
            db.AddInParameter(cmd, "@idParametro", DbType.String, idParametro);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el parámetro, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el parámetro, " + ex.Message, ex);
            }
        }


        public void setEditarParametro(string idParametro, string emailCC, string nombreCampo1, string nombreCampo2,
            string nombreCampo3, string nombreCampo4, string nombreCampo5, string gestionAdjunto)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_EditarParametro");

            db.AddInParameter(cmd, "@idParametro", DbType.String, idParametro);
            db.AddInParameter(cmd, "@emailCC", DbType.String, emailCC);
            db.AddInParameter(cmd, "@nombreCampo1", DbType.String, nombreCampo1);
            db.AddInParameter(cmd, "@nombreCampo2", DbType.String, nombreCampo2);
            db.AddInParameter(cmd, "@nombreCampo3", DbType.String, nombreCampo3);
            db.AddInParameter(cmd, "@nombreCampo4", DbType.String, nombreCampo4);
            db.AddInParameter(cmd, "@nombreCampo5", DbType.String, nombreCampo5);
            db.AddInParameter(cmd, "@gestionAdjunto", DbType.String, gestionAdjunto);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo editar el parámetro, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo editar el parámetro, " + ex.Message, ex);
            }
        }



        //tabla 1

        public DataSet getBuscarTabla1(string nombre)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarTabla1");
            db.AddInParameter(cmd, "@nombre", DbType.String, nombre);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar la tabla 1, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar la tabla 1, " + ex.Message, ex);
            }
        }



        public void setEditarTabla1(string idCampo, string nombre, string activo)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_EditarTabla1");

            db.AddInParameter(cmd, "@idCampo", DbType.String, idCampo);
            db.AddInParameter(cmd, "@nombre", DbType.String, nombre);
            db.AddInParameter(cmd, "@activo", DbType.String, activo);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo editar el parámetro, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo editar el parámetro, " + ex.Message, ex);
            }
        }


        public void setIngresarTabla1(string nombre, string activo)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_IngresarTabla1");

            db.AddInParameter(cmd, "@nombre", DbType.String, nombre);
            db.AddInParameter(cmd, "@activo", DbType.String, activo);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo ingresar la tabla, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo ingresar la tabla, " + ex.Message, ex);
            }
        }

        public void setEliminarTabla1(string idCampo)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_EliminarTabla1");

            db.AddInParameter(cmd, "@idCampo", DbType.String, idCampo);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo eliminar la tabla, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo eliminar la tabla, " + ex.Message, ex);
            }
        }


        //tabla 2

        public DataSet getBuscarTabla2(string nombre)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarTabla2");
            db.AddInParameter(cmd, "@nombre", DbType.String, nombre);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar la tabla 2, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar la tabla 2, " + ex.Message, ex);
            }
        }



        public void setEditarTabla2(string idCampo, string nombre, string activo)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_EditarTabla2");

            db.AddInParameter(cmd, "@idCampo", DbType.String, idCampo);
            db.AddInParameter(cmd, "@nombre", DbType.String, nombre);
            db.AddInParameter(cmd, "@activo", DbType.String, activo);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo editar la tabla, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo editar la tabla, " + ex.Message, ex);
            }
        }


        public void setIngresarTabla2(string nombre, string activo)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_IngresarTabla2");

            db.AddInParameter(cmd, "@nombre", DbType.String, nombre);
            db.AddInParameter(cmd, "@activo", DbType.String, activo);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo ingresar la tabla, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo ingresar la tabla, " + ex.Message, ex);
            }
        }

        public void setEliminarTabla2(string idCampo)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_EliminarTabla2");

            db.AddInParameter(cmd, "@idCampo", DbType.String, idCampo);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo eliminar la tabla, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo eliminar la tabla, " + ex.Message, ex);
            }
        }


        //INICIO tabla 3
        public DataSet getBuscarTabla3(string nombre)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarTabla3");
            db.AddInParameter(cmd, "@nombre", DbType.String, nombre);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar la tabla, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar la tabla, " + ex.Message, ex);
            }
        }

        public DataSet getBuscarMotivoCierrePorIdTipo(string idTipo)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarMotivoCierrePorIdTipo");
            db.AddInParameter(cmd, "@idTipoCierre", DbType.String, idTipo);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar motivo cierre, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar motivo cierre, " + ex.Message, ex);
            }
        }

        public void setEditarTabla3(string idCampo, string nombre, string activo)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_EditarTabla3");

            db.AddInParameter(cmd, "@idCampo", DbType.String, idCampo);
            db.AddInParameter(cmd, "@nombre", DbType.String, nombre);
            db.AddInParameter(cmd, "@activo", DbType.String, activo);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo editar el parámetro, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo editar el parámetro, " + ex.Message, ex);
            }
        }


        public void setIngresarTabla3(string nombre, string activo)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_IngresarTabla3");

            db.AddInParameter(cmd, "@nombre", DbType.String, nombre);
            db.AddInParameter(cmd, "@activo", DbType.String, activo);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo ingresar la tabla, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo ingresar la tabla, " + ex.Message, ex);
            }
        }

        public void setEliminarTabla3(string idCampo)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_EliminarTabla3");

            db.AddInParameter(cmd, "@idCampo", DbType.String, idCampo);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo eliminar la tabla, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo eliminar la tabla, " + ex.Message, ex);
            }
        }
        //FIN TABLA 3




        //INICIO tabla 4
        public DataSet getBuscarTabla4(string nombre)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarTabla4");
            db.AddInParameter(cmd, "@nombre", DbType.String, nombre);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar la tabla, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar la tabla, " + ex.Message, ex);
            }
        }



        public void setEditarTabla4(string idCampo, string nombre, string activo)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_EditarTabla4");

            db.AddInParameter(cmd, "@idCampo", DbType.String, idCampo);
            db.AddInParameter(cmd, "@nombre", DbType.String, nombre);
            db.AddInParameter(cmd, "@activo", DbType.String, activo);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo editar el parámetro, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo editar el parámetro, " + ex.Message, ex);
            }
        }


        public void setIngresarTabla4(string nombre, string activo)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_IngresarTabla4");

            db.AddInParameter(cmd, "@nombre", DbType.String, nombre);
            db.AddInParameter(cmd, "@activo", DbType.String, activo);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo ingresar la tabla, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo ingresar la tabla, " + ex.Message, ex);
            }
        }

        public void setEliminarTabla4(string idCampo)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_EliminarTabla4");

            db.AddInParameter(cmd, "@idCampo", DbType.String, idCampo);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo eliminar la tabla, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo eliminar la tabla, " + ex.Message, ex);
            }
        }
        //FIN TABLA 4




        //INICIO tabla 5
        public DataSet getBuscarTabla5(string nombre)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarTabla5");
            db.AddInParameter(cmd, "@nombre", DbType.String, nombre);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar la tabla, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar la tabla, " + ex.Message, ex);
            }
        }



        public void setEditarTabla5(string idCampo, string nombre, string activo)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_EditarTabla5");

            db.AddInParameter(cmd, "@idCampo", DbType.String, idCampo);
            db.AddInParameter(cmd, "@nombre", DbType.String, nombre);
            db.AddInParameter(cmd, "@activo", DbType.String, activo);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo editar el parámetro, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo editar el parámetro, " + ex.Message, ex);
            }
        }


        public void setIngresarTabla5(string nombre, string activo)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_IngresarTabla5");

            db.AddInParameter(cmd, "@nombre", DbType.String, nombre);
            db.AddInParameter(cmd, "@activo", DbType.String, activo);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo ingresar la tabla, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo ingresar la tabla, " + ex.Message, ex);
            }
        }

        public void setEliminarTabla5(string idCampo)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_EliminarTabla5");

            db.AddInParameter(cmd, "@idCampo", DbType.String, idCampo);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo eliminar la tabla, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo eliminar la tabla, " + ex.Message, ex);
            }
        }
        //FIN TABLA 5

        //CENTRAL
        public DataSet getBuscarCentral(string nombre)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarCentral");
            db.AddInParameter(cmd, "@nombre", DbType.String, nombre);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el canal, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el canal, " + ex.Message, ex);
            }
        }

        //CANAL


        public DataSet getBuscarCanal(string nombre)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarCanal");
            db.AddInParameter(cmd, "@nombre", DbType.String, nombre);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el canal, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el canal, " + ex.Message, ex);
            }
        }

        public DataSet getBuscarCanalId(int @IdCanal)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarCanalId");
            db.AddInParameter(cmd, "@IdCanal", DbType.Int32, IdCanal);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el canal, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el canal, " + ex.Message, ex);
            }
        }


        public DataSet getGenerarDashboard(string idUsuario, int idArea)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_GenerarDashboard");

            if (idUsuario == "0")
            {
                db.AddInParameter(cmd, "@idUsuario", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idUsuario", DbType.String, idUsuario);
            }

            if (idArea == 0)
            {
                db.AddInParameter(cmd, "@idArea", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idArea", DbType.String, idArea);
            }


            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el estatus, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el estatus, " + ex.Message, ex);
            }
        }



        public DataSet getBuscarEstatus(string idEmpresa)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarTipo");
            db.AddInParameter(cmd, "@idEmpresa", DbType.String, idEmpresa);
            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el estatus, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el estatus, " + ex.Message, ex);
            }
        }


        public DataSet getBuscarNivel1(string clienteInterno, string clienteExterno, string idEmpresa)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarNivel1");

            if (clienteInterno == string.Empty)
            {
                db.AddInParameter(cmd, "@clienteInterno", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@clienteInterno", DbType.String, clienteInterno);
            }

            if (clienteExterno == string.Empty)
            {
                db.AddInParameter(cmd, "@clienteExterno", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@clienteExterno", DbType.String, clienteExterno);
            }

            db.AddInParameter(cmd, "@idEmpresa", DbType.String, idEmpresa);
            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el nivel 1, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el nivel 1, " + ex.Message, ex);
            }
        }


        public DataSet getBuscarNivel2(string clienteInterno, string clienteExterno, string idEmpresa, string nivel1)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarNivel2");
            if (clienteInterno == string.Empty)
            {
                db.AddInParameter(cmd, "@clienteInterno", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@clienteInterno", DbType.String, clienteInterno);
            }

            if (clienteExterno == string.Empty)
            {
                db.AddInParameter(cmd, "@clienteExterno", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@clienteExterno", DbType.String, clienteExterno);
            }

            db.AddInParameter(cmd, "@idEmpresa", DbType.String, idEmpresa);
            db.AddInParameter(cmd, "@nivel1", DbType.String, nivel1);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el nivel 2, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el nivel 2, " + ex.Message, ex);
            }
        }

        public DataSet getBuscarNivel3Respuesta(string clienteInterno, string clienteExterno, string idEmpresa, string nivel1, string nivel2)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarNivel3Respuesta");
            if (clienteInterno == string.Empty)
            {
                db.AddInParameter(cmd, "@clienteInterno", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@clienteInterno", DbType.String, clienteInterno);
            }

            if (clienteExterno == string.Empty)
            {
                db.AddInParameter(cmd, "@clienteExterno", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@clienteExterno", DbType.String, clienteExterno);
            }

            db.AddInParameter(cmd, "@idEmpresa", DbType.String, idEmpresa);

            db.AddInParameter(cmd, "@nivel1", DbType.String, nivel1);
            db.AddInParameter(cmd, "@nivel2", DbType.String, nivel2);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el nivel 1, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el nivel 1, " + ex.Message, ex);
            }
        }

        //public DataSet getBuscarNivel3(string clienteInterno, string clienteExterno, string idEmpresa, string nivel1, string nivel2)
        //{
        //    DbCommand cmd = db.GetStoredProcCommand("stp_BuscarNivel3Respuesta");
        //    if (clienteInterno == string.Empty)
        //    {
        //        db.AddInParameter(cmd, "@clienteInterno", DbType.String, null);
        //    }
        //    else
        //    {
        //        db.AddInParameter(cmd, "@clienteInterno", DbType.String, clienteInterno);
        //    }

        //    if (clienteExterno == string.Empty)
        //    {
        //        db.AddInParameter(cmd, "@clienteExterno", DbType.String, null);
        //    }
        //    else
        //    {
        //        db.AddInParameter(cmd, "@clienteExterno", DbType.String, clienteExterno);
        //    }

        //    db.AddInParameter(cmd, "@idEmpresa", DbType.String, idEmpresa);

        //    db.AddInParameter(cmd, "@nivel1", DbType.String, nivel1);
        //    db.AddInParameter(cmd, "@nivel2", DbType.String, nivel2);

        //    try
        //    {
        //        return db.ExecuteDataSet(cmd);
        //    }
        //    catch (SqlException ex)
        //    {
        //        throw new Exception("No se pudo buscar el nivel 1, " + ex.Message, ex);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception("No se pudo buscar el nivel 1, " + ex.Message, ex);
        //    }
        //}


        public DataSet getBuscarNivel3(string clienteInterno, string clienteExterno, string idEmpresa, string nivel1, string nivel2)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarNivel3");
            if (clienteInterno == string.Empty)
            {
                db.AddInParameter(cmd, "@clienteInterno", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@clienteInterno", DbType.String, clienteInterno);
            }

            if (clienteExterno == string.Empty)
            {
                db.AddInParameter(cmd, "@clienteExterno", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@clienteExterno", DbType.String, clienteExterno);
            }

            db.AddInParameter(cmd, "@idEmpresa", DbType.String, idEmpresa);

            db.AddInParameter(cmd, "@nivel1", DbType.String, nivel1);
            db.AddInParameter(cmd, "@nivel2", DbType.String, nivel2);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el nivel 1, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el nivel 1, " + ex.Message, ex);
            }
        }


        public DataSet getBuscarNivel4(string clienteInterno, string clienteExterno, string idEmpresa, string nivel1, string nivel2, string nivel3)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarNivel4");

            if (clienteInterno == string.Empty)
            {
                db.AddInParameter(cmd, "@clienteInterno", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@clienteInterno", DbType.String, clienteInterno);
            }

            if (clienteExterno == string.Empty)
            {
                db.AddInParameter(cmd, "@clienteExterno", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@clienteExterno", DbType.String, clienteExterno);
            }

            db.AddInParameter(cmd, "@idEmpresa", DbType.String, idEmpresa);

            db.AddInParameter(cmd, "@nivel1", DbType.String, nivel1);
            db.AddInParameter(cmd, "@nivel2", DbType.String, nivel2);
            db.AddInParameter(cmd, "@nivel3", DbType.String, nivel3);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el nivel 4, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el nivel 4, " + ex.Message, ex);
            }
        }

        public DataSet getBuscarNivel5(string nivel3, string nivel2, string nivel4)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarPalabrasClaves");

            db.AddInParameter(cmd, "@nivel4", DbType.String, nivel4);
            db.AddInParameter(cmd, "@nivel2", DbType.String, nivel2);
            db.AddInParameter(cmd, "@nivel3", DbType.String, nivel3);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el nivel 4, " + ex.Message, ex);
            }
        }

        public DataSet getBuscarSubEstatus(string nivel1)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarSubEstatus");
            db.AddInParameter(cmd, "@nivel1", DbType.String, nivel1);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el sub estatus, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el sub estatus, " + ex.Message, ex);
            }
        }

        public DataSet getNivel3(string nivel1, string nivel2)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarEstatusSeguimiento");

            db.AddInParameter(cmd, "@nivel1", DbType.String, nivel1);
            db.AddInParameter(cmd, "@nivel2", DbType.String, nivel2);
            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el estatus seguimiento, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el estatus seguimiento, " + ex.Message, ex);
            }
        }


        public DataSet getNivel4(string nivel1, string nivel2, string nivel3)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarNivel4");

            db.AddInParameter(cmd, "@nivel1", DbType.String, nivel1);
            db.AddInParameter(cmd, "@nivel2", DbType.String, nivel2);
            db.AddInParameter(cmd, "@nivel3", DbType.String, nivel3);
            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el nivel 4, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el nivel 4, " + ex.Message, ex);
            }
        }


        public DataSet getContarGestiones(string idUsuario, string fechaDesde, string fechaHasta,
            string campo1, string idEstado, string tipo, string idCanal, int idArea)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_ContarGestiones");



            if (idUsuario == "0")
            {
                db.AddInParameter(cmd, "@idUsuario", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idUsuario", DbType.String, idUsuario);
            }

            if (fechaDesde == string.Empty)
            {
                db.AddInParameter(cmd, "@fechaDesde", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@fechaDesde", DbType.String, fechaDesde);
            }

            if (fechaHasta == string.Empty)
            {
                db.AddInParameter(cmd, "@fechaHasta", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@fechaHasta", DbType.String, fechaHasta);
            }


            if (campo1 == "0")
            {
                db.AddInParameter(cmd, "@campo1", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@campo1", DbType.String, campo1);
            }

            if (idEstado == "0")
            {
                db.AddInParameter(cmd, "@idEstado", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idEstado", DbType.String, idEstado);
            }



            if (tipo == "0")
            {
                db.AddInParameter(cmd, "@tipo", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@tipo", DbType.String, tipo);
            }


            if (idCanal == "0")
            {
                db.AddInParameter(cmd, "@idCanal", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idCanal", DbType.String, idCanal);
            }

            if (idArea == 0)
            {
                db.AddInParameter(cmd, "@idArea", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idArea", DbType.String, idArea);
            }

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el estatus seguimiento, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el estatus seguimiento, " + ex.Message, ex);
            }
        }

        public DataSet getReporteTickets(string idUsuario, string fechaDesde,
            string fechaHasta, string insistencia, int idArea)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_ReporteTickets");

            if (idUsuario == "0")
            {
                db.AddInParameter(cmd, "@idUsuario", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idUsuario", DbType.String, idUsuario);
            }

            if (idArea == 0)
            {
                db.AddInParameter(cmd, "@idArea", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idArea", DbType.String, idArea);
            }

            if (fechaDesde == string.Empty)
            {
                db.AddInParameter(cmd, "@fechaDesde", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@fechaDesde", DbType.String, fechaDesde);
            }

            if (fechaHasta == string.Empty)
            {
                db.AddInParameter(cmd, "@fechaHasta", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@fechaHasta", DbType.String, fechaHasta);
            }

            if (insistencia == "0")
            {
                db.AddInParameter(cmd, "@insistencia", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@insistencia", DbType.String, insistencia);
            }

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el reporte de tickets, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el reporte de tickets, " + ex.Message, ex);
            }
        }

        public DataSet getBuscarGestionesExporte(string idUsuario, string fechaDesde, string fechaHasta,
            string campo1, string idEstado, int idArea)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarGestionesExporte");

            if (idUsuario == "0")
            {
                db.AddInParameter(cmd, "@idUsuario", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idUsuario", DbType.String, idUsuario);
            }

            if (fechaDesde == string.Empty)
            {
                db.AddInParameter(cmd, "@fechaDesde", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@fechaDesde", DbType.String, fechaDesde);
            }

            if (fechaHasta == string.Empty)
            {
                db.AddInParameter(cmd, "@fechaHasta", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@fechaHasta", DbType.String, fechaHasta);
            }


            if (campo1 == "0")
            {
                db.AddInParameter(cmd, "@campo1", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@campo1", DbType.String, campo1);
            }


            if (idEstado == "0")
            {
                db.AddInParameter(cmd, "@idEstado", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idEstado", DbType.String, idEstado);
            }

            if (idArea == 0)
            {
                db.AddInParameter(cmd, "@idArea", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idArea", DbType.String, idArea);
            }

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo exportar las gestiones, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo exportar las gestiones, " + ex.Message, ex);
            }
        }



        public DataSet getGraficoGestiones(string idUsuario, string fechaDesde, string fechaHasta,
             string idEstado, int idArea)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_GraficoContarGestiones");

            if (idArea == 0)
            {
                db.AddInParameter(cmd, "@idArea", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idArea", DbType.String, idArea);
            }

            if (idUsuario == "0")
            {
                db.AddInParameter(cmd, "@idUsuario", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idUsuario", DbType.String, idUsuario);
            }

            if (fechaDesde == string.Empty)
            {
                db.AddInParameter(cmd, "@fechaDesde", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@fechaDesde", DbType.String, fechaDesde);
            }

            if (fechaHasta == string.Empty)
            {
                db.AddInParameter(cmd, "@fechaHasta", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@fechaHasta", DbType.String, fechaHasta);
            }


            if (idEstado == "0")
            {
                db.AddInParameter(cmd, "@idEstado", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idEstado", DbType.String, idEstado);
            }

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo exportar las gestiones, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo exportar las gestiones, " + ex.Message, ex);
            }
        }


        public DataSet getBuscarGestionesDeriva(string idEstatus, string idSubEstatus, string idEstatusSeguimiento)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_buscarDerivaPorGestion");

            db.AddInParameter(cmd, "@idEstatus", DbType.String, idEstatus);
            db.AddInParameter(cmd, "@idSubEstatus", DbType.String, idSubEstatus);
            db.AddInParameter(cmd, "@idEstatusSeguimiento", DbType.String, idEstatusSeguimiento);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo exportar las gestiones, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo exportar las gestiones, " + ex.Message, ex);
            }
        }


        public DataSet getBuscarDetalleGestiones(string idTipificacion, string idUsuario, string tipo, string idCanal,
            string fechaDesde, string fechaHasta)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarGestionesPorIdTipificacion");
            db.AddInParameter(cmd, "@idTipificacion", DbType.String, idTipificacion);

            if (fechaDesde == string.Empty)
            {
                db.AddInParameter(cmd, "@fechaDesde", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@fechaDesde", DbType.String, fechaDesde);
            }

            if (fechaHasta == string.Empty)
            {
                db.AddInParameter(cmd, "@fechaHasta", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@fechaHasta", DbType.String, fechaHasta);
            }

            if (idUsuario == "0")
            {
                db.AddInParameter(cmd, "@idUsuario", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idUsuario", DbType.String, idUsuario);
            }

            if (tipo == "0")
            {
                db.AddInParameter(cmd, "@tipo", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@tipo", DbType.String, tipo);
            }

            if (idCanal == "0")
            {
                db.AddInParameter(cmd, "@idCanal", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idCanal", DbType.String, idCanal);
            }

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el detalle de las gestiones, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el detalle de las gestiones, " + ex.Message, ex);
            }
        }

        //gestion
        public string setIngresarGestion(string idUsuario, string idEstatus, string idSubEstatus,
            string idEstatusSeguimiento, string idCampo1, string idCampo2, string idCampo3, string idCampo4,
            string idCampo5, string textoCampo1, string textoCampo2, string textoCampo3,
            string textoCampo4, string textoCampo5, string observacion,
            string telefonoAsociado, string idCanal, string ruta, string ruta2, string ruta3,
            string fechaAgend, string tipo, string llamadoID, string llamadoRut)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_IngresarGestion");
            db.AddInParameter(cmd, "@idUsuario", DbType.String, idUsuario);
            db.AddInParameter(cmd, "@idEstatus", DbType.String, idEstatus);
            db.AddInParameter(cmd, "@idSubEstatus", DbType.String, idSubEstatus);
            db.AddInParameter(cmd, "@idEstatusSeguimiento", DbType.String, idEstatusSeguimiento);
            db.AddInParameter(cmd, "@idCampo1", DbType.String, idCampo1);
            db.AddInParameter(cmd, "@idCampo2", DbType.String, idCampo2);
            db.AddInParameter(cmd, "@idCampo3", DbType.String, idCampo3);
            db.AddInParameter(cmd, "@idCampo4", DbType.String, idCampo4);
            db.AddInParameter(cmd, "@idCampo5", DbType.String, idCampo5);
            db.AddInParameter(cmd, "@textoCampo1", DbType.String, textoCampo1);
            db.AddInParameter(cmd, "@textoCampo2", DbType.String, textoCampo2);
            db.AddInParameter(cmd, "@textoCampo3", DbType.String, textoCampo3);
            db.AddInParameter(cmd, "@textoCampo4", DbType.String, textoCampo4);
            db.AddInParameter(cmd, "@textoCampo5", DbType.String, textoCampo5);

            db.AddInParameter(cmd, "@observacion", DbType.String, observacion);
            db.AddInParameter(cmd, "@telefonoAsociado", DbType.String, telefonoAsociado);
            db.AddInParameter(cmd, "@idCanal", DbType.String, idCanal);
            db.AddInParameter(cmd, "@ruta", DbType.String, ruta);
            db.AddInParameter(cmd, "@ruta2", DbType.String, ruta2);
            db.AddInParameter(cmd, "@ruta3", DbType.String, ruta3);
            db.AddInParameter(cmd, "@fechaAgend", DbType.String, fechaAgend);
            db.AddInParameter(cmd, "@tipo", DbType.String, tipo);

            db.AddInParameter(cmd, "@llamadoID", DbType.String, llamadoID);
            db.AddInParameter(cmd, "@llamadoRut", DbType.String, llamadoRut);
            try
            {
                string valor;
                valor = db.ExecuteScalar(cmd).ToString();
                return valor;
                //db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo ingresar la gestión, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo ingresar la gestión, " + ex.Message, ex);
            }
        }


        //gestion
        public string setIngresarGestionInicial(string idUsuario, string telefonoAsociado,
            string llamadoId, string llamadorRut, string llamadoParam1, string llamadoParam2)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_IngresoGestionInicial");
            db.AddInParameter(cmd, "@idUsuario", DbType.String, idUsuario);
            db.AddInParameter(cmd, "@telefonoAsociado", DbType.String, telefonoAsociado);
            db.AddInParameter(cmd, "@llamadoId", DbType.String, llamadoId);
            db.AddInParameter(cmd, "@llamadorRut", DbType.String, llamadorRut);
            db.AddInParameter(cmd, "@llamadoParam1", DbType.String, llamadoParam1);
            db.AddInParameter(cmd, "@llamadoParam2", DbType.String, llamadoParam2);

            try
            {
                string valor;
                valor = db.ExecuteScalar(cmd).ToString();
                return valor;
                //db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo ingresar la gestión, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo ingresar la gestión, " + ex.Message, ex);
            }
        }


        public void setEditarGestion(string idGestion, string idUsuario, string idCampo1, string idCampo2, string idCampo3, string idCampo4,
            string idCampo5, string textoCampo1, string textoCampo2, string textoCampo3,
            string textoCampo4, string textoCampo5, string observacion,
            string telefonoAsociado, string idCanal, string ruta,
            string fechaAgend, string tipo, string llamadoID, string llamadoRut, string idTicketInsistencia,
            string idEmpresa, string usuarioFirma, string rutCliente, string idTipificacion, string nivel1, string tipoCliente)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_EditarGestion");

            db.AddInParameter(cmd, "@idGestion", DbType.String, idGestion);
            db.AddInParameter(cmd, "@idUsuario", DbType.String, idUsuario);
            db.AddInParameter(cmd, "@idCampo1", DbType.String, idCampo1);
            db.AddInParameter(cmd, "@idCampo2", DbType.String, idCampo2);
            db.AddInParameter(cmd, "@idCampo3", DbType.String, idCampo3);
            db.AddInParameter(cmd, "@idCampo4", DbType.String, idCampo4);
            db.AddInParameter(cmd, "@idCampo5", DbType.String, idCampo5);
            db.AddInParameter(cmd, "@textoCampo1", DbType.String, textoCampo1);
            db.AddInParameter(cmd, "@textoCampo2", DbType.String, textoCampo2);
            db.AddInParameter(cmd, "@textoCampo3", DbType.String, textoCampo3);
            db.AddInParameter(cmd, "@textoCampo4", DbType.String, textoCampo4);
            db.AddInParameter(cmd, "@textoCampo5", DbType.String, textoCampo5);

            db.AddInParameter(cmd, "@observacion", DbType.String, observacion);
            db.AddInParameter(cmd, "@telefonoAsociado", DbType.String, telefonoAsociado);
            db.AddInParameter(cmd, "@idCanal", DbType.String, idCanal);
            db.AddInParameter(cmd, "@ruta", DbType.String, ruta);
            db.AddInParameter(cmd, "@fechaAgend", DbType.String, fechaAgend);
            db.AddInParameter(cmd, "@tipo", DbType.String, tipo);

            db.AddInParameter(cmd, "@llamadoID", DbType.String, llamadoID);
            db.AddInParameter(cmd, "@llamadoRut", DbType.String, llamadoRut);
            db.AddInParameter(cmd, "@idTicketInsistencia", DbType.String, idTicketInsistencia);
            db.AddInParameter(cmd, "@idEmpresa", DbType.String, idEmpresa);
            db.AddInParameter(cmd, "@usuarioFirma", DbType.String, usuarioFirma);
            db.AddInParameter(cmd, "@rutCliente", DbType.String, rutCliente);
            db.AddInParameter(cmd, "@idTipificacion", DbType.String, idTipificacion);
            db.AddInParameter(cmd, "@nivel1", DbType.String, nivel1);

            db.AddInParameter(cmd, "@tipoCliente", DbType.String, tipoCliente);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo ingresar la gestión, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo ingresar la gestión, " + ex.Message, ex);
            }
        }


        public void setEditarGestionCliente(int idGestion, string nombre, string email, string telefono, string ip)
        {
            //@idGestion int, @nombre varchar(50), @email varchar(50), @telefono varchar(12)
            DbCommand cmd = db.GetStoredProcCommand("stp_EditarGestionCliente");

            db.AddInParameter(cmd, "@idGestion", DbType.String, idGestion);
            db.AddInParameter(cmd, "@nombre", DbType.String, nombre);
            db.AddInParameter(cmd, "@email", DbType.String, email);
            db.AddInParameter(cmd, "@telefono", DbType.String, telefono);
            db.AddInParameter(cmd, "@ip", DbType.String, ip);
            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo ingresar la gestión, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo ingresar la gestión, " + ex.Message, ex);
            }
        }

        public void setEditarRutaFotoUsuario(int idUsuario, string ruta)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_EditarRutaFotoUsuario");

            db.AddInParameter(cmd, "@idUsuario", DbType.Int16, idUsuario);
            db.AddInParameter(cmd, "@ruta", DbType.String, ruta);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo editar la gestion, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo editar la gestión, " + ex.Message, ex);
            }
        }


        public void setEditarRutaArchivoGestion(int idGestion, string ruta)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_EditarRutaArchivoGestion");

            db.AddInParameter(cmd, "@idGestion", DbType.Int32, idGestion);//JRL Cambio de Int16 a Int32
            db.AddInParameter(cmd, "@ruta", DbType.String, ruta);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo editar la gestion, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo editar la gestión, " + ex.Message, ex);
            }
        }

        public void setEditarRutaArchivoGestion2(int idGestion, string ruta)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_EditarRutaArchivoGestion2");

            db.AddInParameter(cmd, "@idGestion", DbType.Int32, idGestion);//JRL Cambio de Int16 a Int32
            db.AddInParameter(cmd, "@ruta", DbType.String, ruta);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo editar la gestion, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo editar la gestión, " + ex.Message, ex);
            }
        }

        //JRL 2018-05-03
        public void setEditarRutaArchivoGestion3(int idGestion, string ruta)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_EditarRutaArchivoGestion3");

            db.AddInParameter(cmd, "@idGestion", DbType.Int32, idGestion);//JRL Cambio de Int16 a Int32
            db.AddInParameter(cmd, "@ruta", DbType.String, ruta);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo editar la gestion, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo editar la gestión, " + ex.Message, ex);
            }
        }

        public void setEditarRutaArchivoAtencionHistorico(int idAtencion, int Correlativo, string ruta1, string ruta2)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_EditarAtencionHistoricoRutaArchivo");

            db.AddInParameter(cmd, "@idAtencion", DbType.Int32, idAtencion);//JRL Cambio de Int16 a Int32
            db.AddInParameter(cmd, "@correlativo", DbType.Int32, Correlativo);//JRL Cambio de Int16 a Int32
            db.AddInParameter(cmd, "@ruta", DbType.String, ruta1);
            db.AddInParameter(cmd, "@ruta2", DbType.String, ruta2);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo editar la ruta del histórico, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo editar la ruta del histórico, " + ex.Message, ex);
            }
        }

        //JRL 2018-09-25
        public string setIngresarTicket(string rutCliente, string idUsuarioAsignado, string idUsuarioCreacion,
            string idEstadoAtencion, string observacion, string tipo, string nivel1, string idEmpresa,
            string idTipificacion, string obsCliente, string nombreCliente, string idCanal, string telefonoAsociado,
            string tipoCliente, string local, string idLlamada, int? idVendedor, string idCentral,
            string Adicional1, string Adicional2, string Adicional3, string Adicional4, string idEMail, int IdEmisor,
            int idArea, string rutCliNoTitular, string telefonoIVR, string opcionIVR, string servicioIVR, string idCliente,
            string NRO_ATENCION)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_IngresarTicket_20092017");

            db.AddInParameter(cmd, "@rutCliente", DbType.String, rutCliente);
            db.AddInParameter(cmd, "@idUsuarioAsignado", DbType.String, idUsuarioAsignado);
            db.AddInParameter(cmd, "@idUsuarioCreacion", DbType.String, idUsuarioCreacion);
            db.AddInParameter(cmd, "@idEstadoAtencion", DbType.String, idEstadoAtencion);
            db.AddInParameter(cmd, "@observacion", DbType.String, observacion);
            db.AddInParameter(cmd, "@tipo", DbType.String, tipo);
            db.AddInParameter(cmd, "@nivel1", DbType.String, nivel1);
            db.AddInParameter(cmd, "@idEmpresa", DbType.String, idEmpresa);
            db.AddInParameter(cmd, "@idTipificacion", DbType.String, idTipificacion);
            db.AddInParameter(cmd, "@obsCliente", DbType.String, obsCliente);
            db.AddInParameter(cmd, "@nombreCliente", DbType.String, nombreCliente);
            db.AddInParameter(cmd, "@idCanal", DbType.String, idCanal);
            db.AddInParameter(cmd, "@idArea", DbType.String, idArea);
            db.AddInParameter(cmd, "@telefonoAsociado", DbType.String, telefonoAsociado);
            db.AddInParameter(cmd, "@tipoCliente", DbType.String, tipoCliente);
            db.AddInParameter(cmd, "@local", DbType.String, local);

            if (!string.IsNullOrEmpty(idLlamada))
            {
                db.AddInParameter(cmd, "@idLlamada", DbType.String, idLlamada);
            }

            db.AddInParameter(cmd, "@idVendedor", DbType.String, idVendedor);
            if (idCentral == "0")
            {
                db.AddInParameter(cmd, "@idCentral", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idCentral", DbType.String, idCentral);
            }

            if (IdEmisor != 0)
            {
                db.AddInParameter(cmd, "@IdEmisor", DbType.String, IdEmisor);
            }

            //JRL 2018-09-26
            if (string.IsNullOrEmpty(idEMail))
                //if (idEMail == string.Empty)
                db.AddInParameter(cmd, "@idEMail", DbType.String, null);
            else
                db.AddInParameter(cmd, "@idEMail", DbType.Int32, Convert.ToInt32(idEMail));

            if (Adicional1 == string.Empty)
                db.AddInParameter(cmd, "@Adicional1", DbType.String, null);
            else
                db.AddInParameter(cmd, "@Adicional1", DbType.String, Adicional1);

            if (Adicional1 == string.Empty)
                db.AddInParameter(cmd, "@Adicional2", DbType.String, null);
            else
                db.AddInParameter(cmd, "@Adicional2", DbType.String, Adicional2);

            if (Adicional1 == string.Empty)
                db.AddInParameter(cmd, "@Adicional3", DbType.String, null);
            else
                db.AddInParameter(cmd, "@Adicional3", DbType.String, Adicional3);

            if (Adicional1 == string.Empty)
                db.AddInParameter(cmd, "@Adicional4", DbType.String, null);
            else
                db.AddInParameter(cmd, "@Adicional4", DbType.String, Adicional4);

            if (string.IsNullOrEmpty(rutCliNoTitular))
                db.AddInParameter(cmd, "@rutCliNoTitular", DbType.String, null);
            else
                db.AddInParameter(cmd, "@rutCliNoTitular", DbType.String, rutCliNoTitular);

            if (!string.IsNullOrEmpty(telefonoIVR))
            {
                db.AddInParameter(cmd, "@telefonoIVR", DbType.String, telefonoIVR);
            }

            if (!string.IsNullOrEmpty(opcionIVR))
            {
                db.AddInParameter(cmd, "@opcionIVR", DbType.String, opcionIVR);
            }

            if (!string.IsNullOrEmpty(servicioIVR))
            {
                db.AddInParameter(cmd, "@servicioIVR", DbType.String, servicioIVR);
            }

            if (!string.IsNullOrEmpty(idCliente))
            {
                db.AddInParameter(cmd, "@idCliente", DbType.String, idCliente);
            }

            if (!string.IsNullOrEmpty(NRO_ATENCION))
            {
                db.AddInParameter(cmd, "@NRO_ATENCION", DbType.String, NRO_ATENCION);
            }

            //@nombreCliente,@idCanal,@telefonoAsociado,@tipoCliente
            try
            {
                string val = db.ExecuteScalar(cmd).ToString();
                return val;
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo ingresar el ticket, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo ingresar el ticket, " + ex.Message, ex);
            }
        }


        public DataSet getBuscarClientePrincipal(string razonSocial, string rut)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_buscarClientePrincipal");

            if (razonSocial == "")
            {
                db.AddInParameter(cmd, "@razonSocial", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@razonSocial", DbType.String, "%" + razonSocial + "%");
            }

            if (rut == "")
            {
                db.AddInParameter(cmd, "@rut", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@rut", DbType.String, "%" + rut + "%");
            }


            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el cliente, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el cliente, " + ex.Message, ex);
            }
        }


        public void setIngresarContacto(string nomContacto, string rutCliente, string email1,
            string email2, string celular, string telefono1, string telefono2)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_IngresarContacto");
            db.AddInParameter(cmd, "@nomContacto", DbType.String, nomContacto);
            db.AddInParameter(cmd, "@rutCliente", DbType.String, rutCliente);
            db.AddInParameter(cmd, "@email1", DbType.String, email1);
            db.AddInParameter(cmd, "@email2", DbType.String, email2);
            db.AddInParameter(cmd, "@celular", DbType.String, celular);
            db.AddInParameter(cmd, "@telefono1", DbType.String, telefono1);
            db.AddInParameter(cmd, "@telefono2", DbType.String, telefono2);
            //db.AddInParameter(cmd, "@cargo", DbType.String, cargo);
            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo ingresar el contacto, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo ingresar el contacto, " + ex.Message, ex);
            }
        }


        public DataSet getBuscarContacto(string rutCliente)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarContacto");
            db.AddInParameter(cmd, "@rutCliente", DbType.String, rutCliente);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar contacto, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar contacto, " + ex.Message, ex);
            }
        }


        public void setIngresarCliente(string rutCliente, string nombre, string idUsuarioCreacion, string fono, string celular, string email, string activo, string comuna, string direccion)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_IngresarCliente");
            db.AddInParameter(cmd, "@rutCliente", DbType.String, rutCliente);
            db.AddInParameter(cmd, "@nombre", DbType.String, nombre);
            db.AddInParameter(cmd, "@idUsuarioCreacion", DbType.String, idUsuarioCreacion);
            db.AddInParameter(cmd, "@fono", DbType.String, fono);
            db.AddInParameter(cmd, "@celular", DbType.String, celular);
            db.AddInParameter(cmd, "@email", DbType.String, email);
            db.AddInParameter(cmd, "@activo", DbType.String, activo);

            db.AddInParameter(cmd, "@comuna", DbType.String, comuna);
            db.AddInParameter(cmd, "@direccion", DbType.String, direccion);

            try
            {
                db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo ingresar el cliente, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo ingresar el cliente, " + ex.Message, ex);
            }
        }

        public DataSet getBuscarClientePorRut(string rut)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarClientePorRut");
            db.AddInParameter(cmd, "@rut", DbType.String, rut);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el cliente, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el cliente, " + ex.Message, ex);
            }
        }



        public DataSet getBuscarTicketBuscador(string idAtencion, string nombre, string idUsuario)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_buscarTicketBuscador");
            db.AddInParameter(cmd, "@idAtencion", DbType.String, "%" + idAtencion + "%");

            if (nombre == string.Empty)
            {
                db.AddInParameter(cmd, "@nombre", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@nombre", DbType.String, "%" + nombre + "%");
            }

            if (idUsuario == string.Empty)
            {
                db.AddInParameter(cmd, "@idUsuario", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idUsuario", DbType.String, idUsuario);
            }

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el ticket, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el ticket, " + ex.Message, ex);
            }
        }


        public DataSet getBuscarTicketBuscadorPorIdTicketUsuario(string idAtencion, string idUsuario)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_buscarTicketBuscadorPorIdTicketIdUsuario");
            db.AddInParameter(cmd, "@idAtencion", DbType.String, idAtencion);
            db.AddInParameter(cmd, "@idUsuario", DbType.String, idUsuario);


            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el ticket, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el ticket, " + ex.Message, ex);
            }
        }






        public DataSet getBuscarTicketBuscadorPorIdTicket(string idAtencion)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_buscarTicketBuscadorPorIdTicket");
            db.AddInParameter(cmd, "@idAtencion", DbType.String, idAtencion);


            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el ticket, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el ticket, " + ex.Message, ex);
            }
        }

        public DataSet getBuscarTicketBuscadorPorCliente(string nombre)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_buscarTicketBuscadorPorCliente");

            if (nombre == string.Empty)
            {
                db.AddInParameter(cmd, "@nombre", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@nombre", DbType.String, "%" + nombre + "%");
            }

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el ticket, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el ticket, " + ex.Message, ex);
            }
        }

        public DataSet getBuscarTicketBuscadorPorRut(string rut)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_buscarTicketBuscadorPorRut");

            if (rut == string.Empty)
            {
                db.AddInParameter(cmd, "@rutCliente", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@rutCliente", DbType.String, rut);
            }

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el ticket, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el ticket, " + ex.Message, ex);
            }
        }



        public DataSet getBuscarTicketBuscadorPorTelefonoCi(string telefonoCi)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_buscarTicketBuscadorPorTelefonoCi");

            if (telefonoCi == string.Empty)
            {
                db.AddInParameter(cmd, "@telefonoCI", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@telefonoCI", DbType.String, telefonoCi);
            }

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el ticket, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el ticket, " + ex.Message, ex);
            }
        }

        public DataSet getBuscarTicketBuscadorPorEmailCi(string emailCi)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_buscarTicketBuscadorPorEmailCi");

            if (emailCi == string.Empty)
            {
                db.AddInParameter(cmd, "@EmailCI", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@EmailCI", DbType.String, emailCi);
            }

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el ticket, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el ticket, " + ex.Message, ex);
            }
        }

        public DataSet getBuscarTicketBuscadorParametros(string usuarioAsig, string fechaDesde, string fechaHasta, string idEstado,
            string insistencia, string escalamiento, string nivel1, int idArea, string tipo, string derivado, int IdServicio, 
            int idTicket, string rutCliente, string verAtrasado, string NRO_ATENCION)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_buscarTicketBuscadorParametros");

            if (usuarioAsig == "0" || usuarioAsig == string.Empty)
            {
                db.AddInParameter(cmd, "@usuarioAsig", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@usuarioAsig", DbType.String, usuarioAsig);
            }

            if (idEstado == "0")
            {
                db.AddInParameter(cmd, "@idEstado", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idEstado", DbType.String, idEstado);
            }

            if (tipo == "0")
            {
                db.AddInParameter(cmd, "@tipo", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@tipo", DbType.String, tipo);
            }

            if (fechaDesde == string.Empty)
            {
                db.AddInParameter(cmd, "@fechaDesde", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@fechaDesde", DbType.String, fechaDesde);
            }

            if (fechaHasta == string.Empty)
            {
                db.AddInParameter(cmd, "@fechaHasta", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@fechaHasta", DbType.String, fechaHasta);
            }

            if (insistencia == "0")
            {
                db.AddInParameter(cmd, "@insistencia", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@insistencia", DbType.String, insistencia);
            }

            if (escalamiento == "999999")
            {
                db.AddInParameter(cmd, "@escalamiento", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@escalamiento", DbType.String, escalamiento);
            }

            if (nivel1 == "0")
            {
                db.AddInParameter(cmd, "@nivel1", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@nivel1", DbType.String, nivel1);
            }

            if (idArea == 0)
            {
                db.AddInParameter(cmd, "@idArea", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idArea", DbType.String, idArea);
            }

            if (derivado == string.Empty)
            {
                db.AddInParameter(cmd, "@derivado", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@derivado", DbType.String, derivado);
            }

            if (IdServicio != 0)
            {
                db.AddInParameter(cmd, "@IdServicio", DbType.String, IdServicio);
            }
            if (idTicket != 0)
            {
                db.AddInParameter(cmd, "@idTicket", DbType.String, idTicket);
            }
            db.AddInParameter(cmd, "@rutCliente", DbType.String, rutCliente);

            if (verAtrasado != "Todos")
            {
                db.AddInParameter(cmd, "@intVerAtrasado", DbType.String, verAtrasado);
            }

            if(!string.IsNullOrEmpty(NRO_ATENCION))
            {
                db.AddInParameter(cmd, "@NRO_ATENCION", DbType.String, NRO_ATENCION);
            }

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el ticket, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el ticket, " + ex.Message, ex);
            }
        }

        public DataSet getBuscarTicketBuscadorParametrosExpGeneral(string usuarioAsig, string fechaDesde, string fechaHasta, string idEstado,
        string insistencia, string escalamiento, string nivel1, int idArea, string tipo, string derivado, int IdServicio, int idTicket, 
        string rutCliente, string verAtrasado, string NRO_ATENCION)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_buscarTicketBuscadorParametrosExpGeneral");

            if (!string.IsNullOrEmpty(usuarioAsig) && usuarioAsig != "0")
            {
                db.AddInParameter(cmd, "@usuarioAsig", DbType.String, usuarioAsig);
            }

            if (idEstado == "0")
            {
                db.AddInParameter(cmd, "@idEstado", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idEstado", DbType.String, idEstado);
            }

            if (tipo == "0")
            {
                db.AddInParameter(cmd, "@tipo", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@tipo", DbType.String, tipo);
            }

            if (!string.IsNullOrEmpty(fechaDesde))
            {
                db.AddInParameter(cmd, "@fechaDesde", DbType.String, fechaDesde);
            }

            if (!string.IsNullOrEmpty(fechaHasta))
            {
                db.AddInParameter(cmd, "@fechaHasta", DbType.String, fechaHasta);
            }

            if (insistencia == "0")
            {
                db.AddInParameter(cmd, "@insistencia", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@insistencia", DbType.String, insistencia);
            }

            if (escalamiento == "999999")
            {
                db.AddInParameter(cmd, "@escalamiento", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@escalamiento", DbType.String, escalamiento);
            }

            if (nivel1 == "0")
            {
                db.AddInParameter(cmd, "@nivel1", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@nivel1", DbType.String, nivel1);
            }

            if (idArea == 0)
            {
                db.AddInParameter(cmd, "@idArea", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idArea", DbType.String, idArea);
            }

            if (!string.IsNullOrEmpty(derivado))
            {
                db.AddInParameter(cmd, "@derivado", DbType.String, derivado);
            }

            if (IdServicio != 0)
            {
                db.AddInParameter(cmd, "@IdServicio", DbType.String, IdServicio);
            }
            if (idTicket != 0)
            {
                db.AddInParameter(cmd, "@idTicket", DbType.String, idTicket);
            }

            if (!string.IsNullOrEmpty(rutCliente))
            {
                db.AddInParameter(cmd, "@rutCliente", DbType.String, rutCliente);
            }

            if (verAtrasado != "Todos")
            {
                db.AddInParameter(cmd, "@intVerAtrasado", DbType.String, verAtrasado);
            }

            if (!string.IsNullOrEmpty(NRO_ATENCION))
            {
                db.AddInParameter(cmd, "@NRO_ATENCION", DbType.String, NRO_ATENCION);
            }

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el exporte de ticket, sp [stp_buscarTicketBuscadorParametrosExpGeneral] " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el exporte de ticket, " + ex.Message, ex);
            }
        }









        //        (@idGestion varchar(10),@rutCliente varchar(20),@nombreCliente varchar(100),
        //@fechaDesde varchar(10),@fechaHasta varchar(10),@idEmpresa varchar(10),@idCanal varchar(10))

        public DataSet getBuscarGestionesBuscador(string idUsuario, string idGestion, string rutCliente, string nombreCliente, string fechaDesde, string fechaHasta, string idEmpresa, string idCanal)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarGestionesUsuario");

            if (idUsuario == string.Empty)
            {
                db.AddInParameter(cmd, "@idUsuario", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idUsuario", DbType.String, idUsuario);
            }

            if (fechaDesde == string.Empty)
            {
                db.AddInParameter(cmd, "@fechaDesde", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@fechaDesde", DbType.String, fechaDesde);
            }

            if (fechaHasta == string.Empty)
            {
                db.AddInParameter(cmd, "@fechaHasta", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@fechaHasta", DbType.String, fechaHasta);
            }

            if (idGestion == string.Empty)
            {
                db.AddInParameter(cmd, "@idGestion", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idGestion", DbType.String, idGestion);
            }

            if (rutCliente == string.Empty)
            {
                db.AddInParameter(cmd, "@rutCliente", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@rutCliente", DbType.String, rutCliente);
            }


            if (nombreCliente == string.Empty)
            {
                db.AddInParameter(cmd, "@nombreCliente", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@nombreCliente", DbType.String, "%" + nombreCliente + "%");
            }

            if (idEmpresa == "0")
            {
                db.AddInParameter(cmd, "@idEmpresa", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idEmpresa", DbType.String, idEmpresa);
            }

            if (idCanal == "0")
            {
                db.AddInParameter(cmd, "@idCanal", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idCanal", DbType.String, idCanal);
            }

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el ticket, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el ticket, " + ex.Message, ex);
            }
        }

        public DataSet getBuscarTipificacionesPorIdEstatus(string idEstatus, string idUsuario)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarTipificacionPorIdEstatus");


            if (string.IsNullOrEmpty(idEstatus))
            {
                db.AddInParameter(cmd, "@nivel1", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@nivel1", DbType.String, idEstatus);
            }

            if (string.IsNullOrEmpty(idUsuario))
            {
                db.AddInParameter(cmd, "@idUsuario", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idUsuario", DbType.String, idUsuario);
            }


            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar la tipificación, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar la tipificación, " + ex.Message, ex);
            }
        }












        public DataSet getBuscarEstadoAtencion()
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_buscarEstadoAtencion");

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el estado, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el estado, " + ex.Message, ex);
            }
        }



        public DataSet getBuscarTicketBuscadorParametrosExporte(string usuarioAsig, string fechaDesde, string fechaHasta, string idEstado)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_buscarTicketBuscadorParametrosExporte");

            if (usuarioAsig == "0")
            {
                db.AddInParameter(cmd, "@usuarioAsig", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@usuarioAsig", DbType.String, usuarioAsig);
            }

            if (idEstado == "0")
            {
                db.AddInParameter(cmd, "@idEstado", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idEstado", DbType.String, idEstado);
            }

            if (fechaDesde == string.Empty)
            {
                db.AddInParameter(cmd, "@fechaDesde", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@fechaDesde", DbType.String, fechaDesde);
            }

            if (fechaHasta == string.Empty)
            {
                db.AddInParameter(cmd, "@fechaHasta", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@fechaHasta", DbType.String, fechaHasta);
            }

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el ticket, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el ticket, " + ex.Message, ex);
            }
        }



        public DataSet getBuscarTicketResumen(string fechaDesde, string fechaHasta)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_buscarTicketResumen");

            if (fechaDesde == string.Empty)
            {
                db.AddInParameter(cmd, "@fechaDesde", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@fechaDesde", DbType.String, fechaDesde);
            }

            if (fechaHasta == string.Empty)
            {
                db.AddInParameter(cmd, "@fechaHasta", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@fechaHasta", DbType.String, fechaHasta);
            }

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el ticket, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el ticket, " + ex.Message, ex);
            }
        }

        public DataSet getBuscarPerfil(string nombre)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarPerfil");
            db.AddInParameter(cmd, "@nombre", DbType.String, nombre);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el perfil, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el perfil, " + ex.Message, ex);
            }
        }


        public DataSet getBuscarArea()
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarArea");

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el area, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el area, " + ex.Message, ex);
            }
        }

        public DataSet getBuscarTicket(string idAtencion)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_buscarTicket");
            db.AddInParameter(cmd, "@idAtencion", DbType.String, idAtencion);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el ticket, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el ticket, " + ex.Message, ex);
            }
        }

        public DataSet getBuscarTicketHistorico(string idAtencion)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_buscarTicketHistorico");
            db.AddInParameter(cmd, "@idAtencion", DbType.String, idAtencion);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el ticket, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el ticket, " + ex.Message, ex);
            }
        }

        //JRL 2018-05-03
        public DataSet getBuscarTicketHistoricoUltimo(string idAtencion)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_buscarTicketHistoricoUltimo");
            db.AddInParameter(cmd, "@idAtencion", DbType.String, idAtencion);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el ticket, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el ticket, " + ex.Message, ex);
            }
        }

        public DataSet getBuscarUsuarioPorIdPerfil(string idPerfil)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_buscarUsuarioPorIdPerfil");
            db.AddInParameter(cmd, "@idPerfil", DbType.String, idPerfil);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar usuario, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar usuario, " + ex.Message, ex);
            }
        }

        public DataSet getBuscarAtencionControl(string idAtencion)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarAtencionControlPorId");
            db.AddInParameter(cmd, "@idAtencion", DbType.String, idAtencion);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar la Atención Control, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar la Atención Control, " + ex.Message, ex);
            }
        }

        public DataSet getBuscarGestionPorIdTicket(string idTicket)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarGestionPorIdTicket");
            db.AddInParameter(cmd, "@idTicket", DbType.String, idTicket);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el ticket, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el ticket, " + ex.Message, ex);
            }
        }



        public DataSet getBuscarUsuarioPorId(string id)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_buscarUsuarioPorId");
            db.AddInParameter(cmd, "@idUsuario", DbType.String, id);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el ticket, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el ticket, " + ex.Message, ex);
            }
        }





        public DataSet getBuscarConfiguracionExporte()
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarConfiguracionExporte");

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar la configuracion, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar la configuracion, " + ex.Message, ex);
            }
        }

        //


        public DataSet getBuscarTipificacionPorIdEstatusIdSubEstatusIdEstatusGestion(string nivel1, string nivel2, string nivel3, string nivel4, int idEmpresa)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarTipificacionPorNivel1Nivel2Nivel3Nivel4");
            db.AddInParameter(cmd, "@nivel1", DbType.String, nivel1);
            db.AddInParameter(cmd, "@nivel2", DbType.String, nivel2);
            db.AddInParameter(cmd, "@nivel3", DbType.String, nivel3);
            db.AddInParameter(cmd, "@nivel4", DbType.String, nivel4);
            db.AddInParameter(cmd, "@idEmpresa", DbType.String, idEmpresa);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar la tipificación, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar la tipificación, " + ex.Message, ex);
            }
        }

        //
        public DataSet getBuscarTipificacionPorId(string idTipificacion)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarTipificacionPorId");
            db.AddInParameter(cmd, "@idTipificacion", DbType.String, idTipificacion);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar la tipificación, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar la tipificación, " + ex.Message, ex);
            }
        }

        public string setIngresarTicketHistorico(string idAtencion, string idUsuarioCreacion, string idUsuarioAsignado,
            string idEstadoAtencion, string observacion, string fechaAgend, string obsCliente, int IdArea)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_IngresarTicketHistorico_13102016");
            db.AddInParameter(cmd, "@idAtencion", DbType.String, idAtencion);
            db.AddInParameter(cmd, "@idUsuarioCreacion", DbType.String, idUsuarioCreacion);
            db.AddInParameter(cmd, "@idUsuarioAsignado", DbType.String, idUsuarioAsignado);
            db.AddInParameter(cmd, "@idEstadoAtencion", DbType.String, idEstadoAtencion);
            db.AddInParameter(cmd, "@observacion", DbType.String, observacion);
            db.AddInParameter(cmd, "@obsCliente", DbType.String, obsCliente);
            db.AddInParameter(cmd, "@IdArea", DbType.String, IdArea);
            if (fechaAgend == string.Empty)
            {
                db.AddInParameter(cmd, "@fechaAgend", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@fechaAgend", DbType.String, fechaAgend);
            }


            try
            {
                //db.ExecuteNonQuery(cmd);
                string val = db.ExecuteScalar(cmd).ToString();
                return val;
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo ingresar el ticket histórico, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo ingresar el ticket histórico, " + ex.Message, ex);
            }
        }

        public void setIngresarAtencionControl(string idAtencion, string idUsuario, string idTomado)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_IngresarAtencionControl");

            if (!string.IsNullOrEmpty(idAtencion))
            {
                db.AddInParameter(cmd, "@idAtencion", DbType.String, idAtencion);
            }

            if (!string.IsNullOrEmpty(idUsuario))
            {
                db.AddInParameter(cmd, "@idUsuario", DbType.String, idUsuario);
            }

            if (!string.IsNullOrEmpty(idTomado))
            {
                db.AddInParameter(cmd, "@idTomado", DbType.String, idTomado);
            }

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo ingresar la Atención Control, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo ingresar la Atención Control, " + ex.Message, ex);
            }
        }

        public void setEliminarAtencionControl(string idAtencion)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_EliminarAtencionControl");

            if (!string.IsNullOrEmpty(idAtencion))
            {
                db.AddInParameter(cmd, "@idAtencion", DbType.String, idAtencion);
            }

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo eliminar la Atención Control, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo eliminar la Atención Control, " + ex.Message, ex);
            }
        }

        //JRL 2019-08-07 Ingreso en tabla GM_ATENCION_FORMULARIO
        public string setIngresarAtencionFormulario(string idAtencion, string rut, string nomProducto,
            string socio, string poliza, string canalVentas, string cobertura, string estadoPoliza, string asistencia, string nomBeneficiario,
            string rutBeneficiario, string relacion, string celularBeneficiario, string mailBeneficiario, string domicilioBeneficiario,
            string comunaBen, string nomProveedor, string telefonoProveedor, string casaComercial, string numBoletaCompra, string fechaCompra,
            string valorCompra, string marcaProducto, string empRealizoCompra, string formaPago, string numRegConsulta, string nomLiquidador,
            string telefonoLiquidador, string nomCoordinador, string telefonoCoordinador, string ultComLiquidador, string numSiniestro,
            string estadoSiniestro, string ingresoTaller, string fechaAprobacion, string fechaEntrega, string sumaLiquidacion,
            string numCaso, string nomTaller, string direccionTaller, string telefonoTaller)
        {
            DbCommand cmd = db.GetStoredProcCommand("usp_GM_ATENCION_FORMULARIOInsert");
            db.AddInParameter(cmd, "@ID_ATENCION", DbType.Int32, Convert.ToInt32(idAtencion));
            db.AddInParameter(cmd, "@RUT", DbType.String, rut);
            db.AddInParameter(cmd, "@PRO_NOMBRE_PRODUCTO", DbType.String, nomProducto);
            if (!socio.Equals("Seleccione"))
            {
                db.AddInParameter(cmd, "@PRO_SOCIO", DbType.String, socio);
            }
            db.AddInParameter(cmd, "@PRO_POLIZA", DbType.String, poliza);
            db.AddInParameter(cmd, "@PRO_CANAL_VENTAS", DbType.String, canalVentas);
            db.AddInParameter(cmd, "@PRO_COBERTURA", DbType.String, cobertura);
            db.AddInParameter(cmd, "@PRO_ESTADO_POLIZA", DbType.String, estadoPoliza);
            db.AddInParameter(cmd, "@PRO_ASISTENCIA", DbType.String, asistencia);
            db.AddInParameter(cmd, "@BEN_NOMBRE_BENEFICIARIO", DbType.String, nomBeneficiario);
            db.AddInParameter(cmd, "@BEN_RUT_BENEFICIARIO", DbType.String, rutBeneficiario);
            db.AddInParameter(cmd, "@BEN_RELACION", DbType.String, relacion);
            db.AddInParameter(cmd, "@BEN_CELULAR_BENEFICIARIO", DbType.String, celularBeneficiario);
            db.AddInParameter(cmd, "@BEN_MAIL_BENEFICIARIO", DbType.String, mailBeneficiario);
            db.AddInParameter(cmd, "@BEN_DOMICILIO_BENEFICIARIO", DbType.String, domicilioBeneficiario);
            db.AddInParameter(cmd, "@BEN_COMUNA", DbType.String, comunaBen);
            db.AddInParameter(cmd, "@PROV_NOMBRE_PROVEEDOR", DbType.String, nomProveedor);
            db.AddInParameter(cmd, "@PROV_TELEFONO_PROVEEDOR", DbType.String, telefonoProveedor);
            db.AddInParameter(cmd, "@PROV_CASA_COMERCIAL", DbType.String, casaComercial);
            db.AddInParameter(cmd, "@COM_NUMERO_BOLETA_COMPRA", DbType.String, numBoletaCompra);
            db.AddInParameter(cmd, "@COM_FECHA_COMPRA", DbType.String, fechaCompra);
            db.AddInParameter(cmd, "@COM_VALOR_COMPRA", DbType.String, valorCompra);
            db.AddInParameter(cmd, "@COM_MARCA_PRODUCTO", DbType.String, marcaProducto);
            db.AddInParameter(cmd, "@COM_EMP_REALIZO_COMPRA", DbType.String, empRealizoCompra);
            db.AddInParameter(cmd, "@COM_FORMA_PAGO", DbType.String, formaPago);
            db.AddInParameter(cmd, "@OTR_NUM_REGISTRO_CONSULTA", DbType.String, numRegConsulta);
            db.AddInParameter(cmd, "@LIQ_NOMBRE_LIQUIDADOR", DbType.String, nomLiquidador);
            db.AddInParameter(cmd, "@LIQ_TELEFONO_LIQUIDADOR", DbType.String, telefonoLiquidador);
            db.AddInParameter(cmd, "@LIQ_NOMBRE_COORDINADOR", DbType.String, nomCoordinador);
            db.AddInParameter(cmd, "@LIQ_TELEFONO_COORDINADOR", DbType.String, telefonoCoordinador);
            db.AddInParameter(cmd, "@LIQ_ULT_COM_LIQUIDADOR", DbType.String, ultComLiquidador);
            db.AddInParameter(cmd, "@SIN_NUM_SINIESTRO", DbType.String, numSiniestro);
            db.AddInParameter(cmd, "@SIN_ESTADO_SINIESTRO", DbType.String, estadoSiniestro);
            db.AddInParameter(cmd, "@SIN_INGRESO_TALLER", DbType.String, ingresoTaller);
            db.AddInParameter(cmd, "@SIN_FECHA_APROBACION", DbType.String, fechaAprobacion);
            db.AddInParameter(cmd, "@SIN_FECHA_ENTREGA", DbType.String, fechaEntrega);
            db.AddInParameter(cmd, "@SIN_SUMA_LIQUIDACION", DbType.String, sumaLiquidacion);
            db.AddInParameter(cmd, "@SIN_NUMERO_CASO", DbType.String, numCaso);
            db.AddInParameter(cmd, "@TALL_NOMBRE_TALLER", DbType.String, nomTaller);
            db.AddInParameter(cmd, "@TALL_DIRECCION_TALLER", DbType.String, direccionTaller);
            db.AddInParameter(cmd, "@TALL_TELEFONO_TALLER", DbType.String, telefonoTaller);

            //if (nomProducto == string.Empty)
            //{
            //    db.AddInParameter(cmd, "@PRO_NOMBRE_PRODUCTO", DbType.String, null);
            //}
            //else
            //{
            //    db.AddInParameter(cmd, "@PRO_NOMBRE_PRODUCTO", DbType.String, nomProducto);
            //}

            try
            {
                //db.ExecuteNonQuery(cmd);
                string val = db.ExecuteScalar(cmd).ToString();
                return val;
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo ingresar la Atención Formulario, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo ingresar la Atención Formulario, " + ex.Message, ex);
            }
        }




        public void setEditarTicketInsistencia(string idTicket)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_EditarTicketInsistencia");
            db.AddInParameter(cmd, "@idAtencion", DbType.String, idTicket);

            try
            {
                db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo ingresar el ticket, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo ingresar el ticket, " + ex.Message, ex);
            }
        }


        public void setEditarTicket(string idTicket, string idEstado,
            string idEmpleado, string fechaAgend, string idMotivoCierre)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_EditarTicket");
            db.AddInParameter(cmd, "@idTicket", DbType.String, idTicket);
            db.AddInParameter(cmd, "@idEstado", DbType.String, idEstado);
            db.AddInParameter(cmd, "@idEmpleado", DbType.String, idEmpleado);

            if (idMotivoCierre == string.Empty)
            {
                db.AddInParameter(cmd, "@idMotivoCierre", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idMotivoCierre", DbType.String, idMotivoCierre);
            }

            if (fechaAgend == string.Empty)
            {
                db.AddInParameter(cmd, "@fechaAgend", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@fechaAgend", DbType.String, fechaAgend);
            }

            try
            {
                db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo ingresar el ticket, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo ingresar el ticket, " + ex.Message, ex);
            }
        }


        public void setEditarGestionPorId(string idTicket, string observacion)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_EditarGestionPorId");
            db.AddInParameter(cmd, "@id", DbType.String, idTicket);
            db.AddInParameter(cmd, "@observacion", DbType.String, observacion);


            try
            {
                db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo editar el ticket, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo editar el ticket, " + ex.Message, ex);
            }
        }

        public void setEditarGestion(string idGestion, string ruta)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_EditarGestionRutaArchivo");
            db.AddInParameter(cmd, "@idGestion", DbType.String, idGestion);
            db.AddInParameter(cmd, "@ruta", DbType.String, ruta);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo editar la gestión, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo editar la gestión, " + ex.Message, ex);
            }
        }


        public DataSet getBuscarTipoMotivoCierre()
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarTipoMotivoCierre");
            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("Error al buscar el tipo motivo cierre, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar el tipo motivo cierre, " + ex.Message, ex);
            }
        }

        //buscar configuración - sjara

        public DataSet getBuscarConfiguracion()
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_buscarConfiguracion");
            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("Error al buscar la configuración, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar la configuración, " + ex.Message, ex);
            }
        }

        public void setEditarConfiguracion(string idUsuario1, string idUsuario2, string idUsuario3, string idUsuario4, string sla1, string sla2, string sla3,
           string sla4, string idTipificacion, string tipo, string idTipoMotivoCierre, string visibleAtento,
           string visibleCliInterno, string visibleCliExterno, string detenido, string nivel1, string nivel2, string nivel3, string nivel4, string clase, int idArea, string activo)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_EditarConfiguracion");

            if (idUsuario1 == string.Empty)
            {
                db.AddInParameter(cmd, "@idUsuario1", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idUsuario1", DbType.String, idUsuario1);
            }

            if (idUsuario2 == "0")
            {
                db.AddInParameter(cmd, "@idUsuario2", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idUsuario2", DbType.String, idUsuario2);
            }

            if (idUsuario3 == "0")
            {
                db.AddInParameter(cmd, "@idUsuario3", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idUsuario3", DbType.String, idUsuario3);
            }

            if (idUsuario4 == "0")
            {
                db.AddInParameter(cmd, "@idUsuario4", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idUsuario4", DbType.String, idUsuario4);
            }

            if (idTipoMotivoCierre == "0")
            {
                db.AddInParameter(cmd, "@idTipoMotivoCierre", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idTipoMotivoCierre", DbType.String, idTipoMotivoCierre);
            }

            if (sla1 != string.Empty)
            {
                db.AddInParameter(cmd, "@sla1", DbType.String, sla1);
            }
            else
            {
                db.AddInParameter(cmd, "@sla1", DbType.String, null);
            }
            if (sla2 != string.Empty)
            {
                db.AddInParameter(cmd, "@sla2", DbType.String, sla2);
            }
            else
            {
                db.AddInParameter(cmd, "@sla2", DbType.String, null);
            }
            if (sla3 != string.Empty)
            {
                db.AddInParameter(cmd, "@sla3", DbType.String, sla3);
            }
            else
            {
                db.AddInParameter(cmd, "@sla3", DbType.String, null);
            }
            if (sla4 != string.Empty)
            {
                db.AddInParameter(cmd, "@sla4", DbType.String, sla4);
            }
            else
            {
                db.AddInParameter(cmd, "@sla4", DbType.String, null);
            }


            if (tipo == "0")
            {
                db.AddInParameter(cmd, "@tipo", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@tipo", DbType.String, tipo);
            }

            if (idArea != 0)
            {
                db.AddInParameter(cmd, "@idArea", DbType.String, idArea);
            }

            db.AddInParameter(cmd, "@idTipificacion", DbType.String, idTipificacion);

            //db.AddInParameter(cmd, "@visibleAtento", DbType.String, visibleAtento);
            db.AddInParameter(cmd, "@visibleCliInterno", DbType.String, visibleCliInterno);
            db.AddInParameter(cmd, "@visibleCliExterno", DbType.String, visibleCliExterno);

            db.AddInParameter(cmd, "@detenido", DbType.String, detenido);

            db.AddInParameter(cmd, "@nivel1", DbType.String, nivel1);
            db.AddInParameter(cmd, "@nivel2", DbType.String, nivel2);
            db.AddInParameter(cmd, "@nivel3", DbType.String, nivel3);
            db.AddInParameter(cmd, "@nivel4", DbType.String, nivel4);

            db.AddInParameter(cmd, "@clase", DbType.String, clase);
            db.AddInParameter(cmd, "@activo", DbType.String, activo);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo editar la configuración, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo editar la configuración, " + ex.Message, ex);
            }
        }




        public DataSet getBuscarGrupo()
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarGrupo");



            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar la empresa, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar la empresa, " + ex.Message, ex);
            }
        }


        //EMPRESA


        public DataSet getBuscarEmpresa(string idEmpresa)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarEmpresa");


            db.AddInParameter(cmd, "@idEmpresa", DbType.String, idEmpresa);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar la empresa, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar la empresa, " + ex.Message, ex);
            }
        }

        public DataSet getBuscarUsuarioPorIdArea(string idArea)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarUsuarioPorIdArea");
            if (idArea == "0")
            {
                db.AddInParameter(cmd, "@idArea", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idArea", DbType.String, idArea);
            }

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el usuario, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el usuario, " + ex.Message, ex);
            }
        }

        public DataSet getBuscarTicketExporte(string idUsuario, string fechaDesde, string fechaHasta, string insistencia)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarTicketExporte");

            if (idUsuario == "0")
            {
                db.AddInParameter(cmd, "@idUsuario", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idUsuario", DbType.String, idUsuario);
            }

            if (fechaDesde == string.Empty)
            {
                db.AddInParameter(cmd, "@fechaDesde", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@fechaDesde", DbType.String, fechaDesde);
            }

            if (fechaHasta == string.Empty)
            {
                db.AddInParameter(cmd, "@fechaHasta", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@fechaHasta", DbType.String, fechaHasta);
            }

            if (insistencia == "0")
            {
                db.AddInParameter(cmd, "@insistencia", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@insistencia", DbType.String, insistencia);
            }

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el reporte de tickets, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el reporte de tickets, " + ex.Message, ex);
            }
        }

        public void setEliminarGestion(string idGestion)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_EliminarGestion");

            db.AddInParameter(cmd, "@idGestion", DbType.String, idGestion);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo eliminar la gestión, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo eliminar la gestión, " + ex.Message, ex);
            }
        }

        public void setEliminarTicket(string idTicket)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_EliminarTicket");

            db.AddInParameter(cmd, "@idTicket", DbType.String, idTicket);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo eliminar el ticket, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo eliminar el ticket, " + ex.Message, ex);
            }
        }


        //JRL
        public string setIngresarTipificacion(string idUsuario1, string idUsuario2,
            string idUsuario3, string idUsuario4, string sla1, string sla2,
            string sla3, string sla4, string tipo, string idTipoMotivoCierre, string visibleAtento,
            string visibleCliInterno, string visibleCliExterno, string detenido, string idEmpresa, string nivel1, string nivel2,
            string nivel3, string nivel4, string clase, int IdArea, string activo)
        {
            //            @idUsuario1 varchar(10), @idUsuario2 varchar(10), @idUsuario3 varchar(10), @idUsuario4 varchar(10),
            //          @sla1  varchar(50), @sla2 varchar(50), @sla3 varchar(50),
            //           @sla4  varchar(50), @tipo char(1), @idTipoMotivoCierre varchar(10),
            //           @visibleAtento varchar(5), @visibleCliInterno varchar(5), @visibleCliExterno varchar(5), @detenido varchar(1),
            //           @idEmpresa int, @nivel1 varchar(100), @nivel2 varchar(100), @nivel3 varchar(100), @nivel4 varchar(100)
            DbCommand cmd = db.GetStoredProcCommand("stp_IngresarTipificacion");

            if (idUsuario1 == "0")
            {
                db.AddInParameter(cmd, "@idUsuario1", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idUsuario1", DbType.String, idUsuario1);
            }
            
            if (idUsuario2 == "0")
            {
                db.AddInParameter(cmd, "@idUsuario2", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idUsuario2", DbType.String, idUsuario2);
            }

            if (idUsuario3 == "0")
            {
                db.AddInParameter(cmd, "@idUsuario3", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idUsuario3", DbType.String, idUsuario3);
            }

            if (idUsuario4 == "0")
            {
                db.AddInParameter(cmd, "@idUsuario4", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idUsuario4", DbType.String, idUsuario4);
            }

            db.AddInParameter(cmd, "@sla1", DbType.String, sla1);
            db.AddInParameter(cmd, "@sla2", DbType.String, sla2);
            db.AddInParameter(cmd, "@sla3", DbType.String, sla3);
            db.AddInParameter(cmd, "@sla4", DbType.String, sla4);
            db.AddInParameter(cmd, "@tipo", DbType.String, tipo);

            if (idTipoMotivoCierre == "0")
            {
                db.AddInParameter(cmd, "@idTipoMotivoCierre", DbType.String, null);
            }
            else
            {
                db.AddInParameter(cmd, "@idTipoMotivoCierre", DbType.String, idTipoMotivoCierre);
            }

            db.AddInParameter(cmd, "@visibleAtento", DbType.String, visibleAtento);
            db.AddInParameter(cmd, "@visibleCliInterno", DbType.String, visibleCliInterno);
            db.AddInParameter(cmd, "@visibleCliExterno", DbType.String, visibleCliExterno);
            db.AddInParameter(cmd, "@detenido", DbType.String, detenido);
            db.AddInParameter(cmd, "@idEmpresa", DbType.String, idEmpresa);
            db.AddInParameter(cmd, "@nivel1", DbType.String, nivel1);
            db.AddInParameter(cmd, "@nivel2", DbType.String, nivel2);
            db.AddInParameter(cmd, "@nivel3", DbType.String, nivel3);
            db.AddInParameter(cmd, "@nivel4", DbType.String, nivel4);
            if (IdArea != 0)
            {
                db.AddInParameter(cmd, "@idArea", DbType.String, IdArea);
            }
            //JRL
            db.AddInParameter(cmd, "@clase", DbType.String, clase);

            db.AddInParameter(cmd, "@activo", DbType.String, activo);

            try
            {
                //db.ExecuteDataSet(cmd);
                string val = Convert.ToString(db.ExecuteScalar(cmd));
                return val;
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo ingresar, sp [stp_IngresarTipificacion]" + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo ingresar, " + ex.Message, ex);
            }
        }


        public DataSet getBuscarFeriado(int activo)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarFeriado");

            db.AddInParameter(cmd, "@activo", DbType.String, activo);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar los feriados, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar los feriados, " + ex.Message, ex);
            }
        }


        public void setEliminarFeriado(int idFeriado)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_EliminarFeriado");

            db.AddInParameter(cmd, "@idFeriado", DbType.String, idFeriado);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo eliminar el feriado, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo eliminar el feriado, " + ex.Message, ex);
            }
        }


        public void setIngresarFeriado(string fecha, int activo)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_IngresarFeriado");

            db.AddInParameter(cmd, "@feriadoFecha", DbType.String, fecha);
            db.AddInParameter(cmd, "@activo", DbType.String, activo);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo eliminar el feriado, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo eliminar el feriado, " + ex.Message, ex);
            }
        }

        public void setUpFeriado(int idFeriado, string fecha, int activo)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_UpFeriado");
            db.AddInParameter(cmd, "@idFeriado", DbType.String, idFeriado);
            db.AddInParameter(cmd, "@fecha", DbType.String, fecha);
            db.AddInParameter(cmd, "@activo", DbType.String, activo);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo editar el feriado, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo editar el feriado, " + ex.Message, ex);
            }
        }



        public void setEliminarLocalidad(int id)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_DelLocalidad");

            db.AddInParameter(cmd, "@id", DbType.String, id);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo eliminar la localidad, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo eliminar la localidad, " + ex.Message, ex);
            }
        }

        public void setUpLocalidad(int id, string nombre, int activo)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_UpLocalidad");
            db.AddInParameter(cmd, "@id", DbType.String, id);
            db.AddInParameter(cmd, "@nombre", DbType.String, nombre);
            db.AddInParameter(cmd, "@activo", DbType.String, activo);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo editar la localidad, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo editar la localidad, " + ex.Message, ex);
            }
        }

        public string setIngresarLocalidad(string nombre, string activo)
        {
            //@nombre varchar(100),@activo int
            DbCommand cmd = db.GetStoredProcCommand("stp_ingresarLocalidad");
            db.AddInParameter(cmd, "@nombre", DbType.String, nombre);
            db.AddInParameter(cmd, "@activo", DbType.String, activo);

            try
            {
                string val = db.ExecuteScalar(cmd).ToString();
                return val;
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el usuario, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el usuario, " + ex.Message, ex);
            }
        }



        //JRL 2018-09-06
        public enum enum_OpF_Usuario
        {
            PorFiltros = 0,
            ObtenerEmails = 1

        }

        //JRL 2018-09-06
        public DataTable OpF_Usuario(enum_OpF_Usuario vSql, int ID_USUARIO, int ID_AREA, int ID_ATENCION)
        {

            DbCommand cmd = db.GetStoredProcCommand("stp_OpF_Usuario");

            db.AddInParameter(cmd, "@vSql", DbType.Int32, vSql);

            if (ID_USUARIO == 0)
                db.AddInParameter(cmd, "@ID_USUARIO", DbType.Int32, null);
            else
                db.AddInParameter(cmd, "@ID_USUARIO", DbType.Int32, ID_USUARIO);

            if (ID_AREA == 0)
                db.AddInParameter(cmd, "@ID_AREA", DbType.Int32, null);
            else
                db.AddInParameter(cmd, "@ID_AREA", DbType.Int32, ID_AREA);

            if (ID_ATENCION == 0)
                db.AddInParameter(cmd, "@ID_ATENCION", DbType.Int32, null);
            else
                db.AddInParameter(cmd, "@ID_ATENCION", DbType.Int32, ID_ATENCION);

            //if (id_servicio == 0)
            //    db.AddInParameter(cmd, "@id_servicio", DbType.Int32, null);
            //else
            //    db.AddInParameter(cmd, "@id_servicio", DbType.Int32, id_servicio);


            //if (id_campana == 0)
            //    db.AddInParameter(cmd, "@id_campana", DbType.Int32, null);
            //else
            //    db.AddInParameter(cmd, "@id_campana", DbType.Int32, id_campana);

            try
            {
                return db.ExecuteDataSet(cmd).Tables[0];
            }
            catch (Exception ex)
            {
                throw new Exception("Error en la operacion de Base de Datos Usuarios, " + ex.Message, ex);
            }
        }

        //JRL 2018-09-06
        public DataTable OpF_EMail(int TIPIFICADO, int SPAM, string FECHA_DESDE, string FECHA_HASTA, string TO, string cuenta)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_OpF_EMail");
            if (TIPIFICADO == -1)
                db.AddInParameter(cmd, "@TIPIFICADO", DbType.Int32, null);
            else
                db.AddInParameter(cmd, "@TIPIFICADO", DbType.Int32, TIPIFICADO);

            if (SPAM == -1)
                db.AddInParameter(cmd, "@SPAM", DbType.Int32, null);
            else
                db.AddInParameter(cmd, "@SPAM", DbType.Int32, SPAM);

            if (FECHA_DESDE == null || FECHA_DESDE.Length != 10)
                db.AddInParameter(cmd, "@FECHA_DESDE", DbType.String, null);
            else
                db.AddInParameter(cmd, "@FECHA_DESDE", DbType.String, FECHA_DESDE);

            if (FECHA_HASTA == null || FECHA_HASTA.Length != 10)
                db.AddInParameter(cmd, "@FECHA_HASTA", DbType.String, null);
            else
                db.AddInParameter(cmd, "@FECHA_HASTA", DbType.String, FECHA_HASTA);

            if (TO == null || TO.Length == 0)
                db.AddInParameter(cmd, "@TO", DbType.String, null);
            else
                db.AddInParameter(cmd, "@TO", DbType.String, TO);

            if (!string.IsNullOrEmpty(cuenta))
            {
                db.AddInParameter(cmd, "@CUENTA", DbType.String, cuenta);
            }

            //@ID_SERVICIO INT, @PERIODO INT ,@RUT_CLIENTE VARCHAR(12), @ID_CASO INT
            try
            {
                return db.ExecuteDataSet(cmd).Tables[0];
            }
            catch (Exception ex)
            {
                throw new Exception("Error en la operacion de Base de Datos EMail, " + ex.Message, ex);
            }
        }

        //JRL 2018-09-06
        public enum enum_Op_Email
        {
            OnlySPAM = 1,
            OnlyATENCION = 2,
            NewEmail = 3
        }

        //JRL 2018-09-06
        public void Op_Email(enum_Op_Email vsql, int Id_Atencion, int Id_Email, string ID_GMAIL, string FROM, string SUBJECT, string BODY, string TO, DateTime? DATE)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_Op_Email");
            db.AddInParameter(cmd, "@vSql", DbType.Int32, vsql);
            db.AddInParameter(cmd, "@ID_ATENCION", DbType.Int32, Id_Atencion);
            db.AddInParameter(cmd, "@ID_EMAIL", DbType.Int32, Id_Email);
            db.AddInParameter(cmd, "@ID_GMAIL", DbType.String, ID_GMAIL);
            db.AddInParameter(cmd, "@FROM", DbType.String, FROM);
            db.AddInParameter(cmd, "@SUBJECT", DbType.String, SUBJECT);
            db.AddInParameter(cmd, "@BODY", DbType.String, BODY);
            db.AddInParameter(cmd, "@TO", DbType.String, TO);
            db.AddInParameter(cmd, "@DATE", DbType.DateTime, DATE);

            try
            {
                db.ExecuteDataSet(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception("Error en la operacion de Base de Emails, " + ex.Message, ex);
            }

        }




        public DataSet getBuscarLocalidad(int activo)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarLocalidad");

            db.AddInParameter(cmd, "@activo", DbType.String, activo);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar localidad, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar localidad, " + ex.Message, ex);
            }
        }


        public DataSet getBuscarCaseMaker(string rutCliente)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarCaseMaker");

            db.AddInParameter(cmd, "@rutCliente", DbType.String, rutCliente);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar case maker, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar case maker, " + ex.Message, ex);
            }
        }

        public DataSet getBuscarTipificacionPorIdIvr(string idIvr)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarTipificacionPorIdIvr");

            db.AddInParameter(cmd, "@idIvr", DbType.String, idIvr);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("Error buscar tipificacion, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("Error buscar tipificacion, " + ex.Message, ex);
            }
        }


        public DataSet getBuscarHistorialSernac(string rut)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarHistorialSernac");

            db.AddInParameter(cmd, "@rut", DbType.String, rut);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("Error buscar tipificacion, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("Error buscar tipificacion, " + ex.Message, ex);
            }
        }


        public void setIngresarCliente(string rut, string email, string nombre, string paterno,
                string materno, string telefono, string celular, int idUsuarioCreacion, string calle, string numero,
                string resto, string idComuna)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_IngresarCliente");


            db.AddInParameter(cmd, "@rut", DbType.String, rut);
            db.AddInParameter(cmd, "@email", DbType.String, email);
            db.AddInParameter(cmd, "@nombre", DbType.String, nombre);
            db.AddInParameter(cmd, "@paterno", DbType.String, paterno);
            db.AddInParameter(cmd, "@materno", DbType.String, materno);
            db.AddInParameter(cmd, "@telefono", DbType.String, telefono);
            db.AddInParameter(cmd, "@celular", DbType.String, celular);
            db.AddInParameter(cmd, "@idUsuarioCreacion", DbType.String, idUsuarioCreacion);

            if (!string.IsNullOrEmpty(calle))
            {
                db.AddInParameter(cmd, "@calle", DbType.String, calle);
            }

            if (!string.IsNullOrEmpty(numero))
            {
                db.AddInParameter(cmd, "@numero", DbType.String, numero);
            }

            if (!string.IsNullOrEmpty(resto))
            {
                db.AddInParameter(cmd, "@resto", DbType.String, resto);
            }

            if (idComuna != "0")
            {
                db.AddInParameter(cmd, "@idComuna", DbType.String, idComuna);
            }

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo ingresar el cliente, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo ingresar el cliente, " + ex.Message, ex);
            }
        }

        public void setIngresarServicio(string servicio, string idScript, string activo, string idSoftCall)
        {
            DbCommand cmd = db.GetStoredProcCommand("usp_GM_SERVICIOInsert");

            db.AddInParameter(cmd, "@SERVICIO", DbType.String, servicio);

            if (idScript != "0")
            {
                db.AddInParameter(cmd, "@ID_SCRIPT", DbType.String, idScript);
            }
         
            db.AddInParameter(cmd, "@ACTIVO", DbType.String, activo);

            if (!string.IsNullOrEmpty(idSoftCall))
            {
                db.AddInParameter(cmd, "@ID_SOFTCALL", DbType.String, idSoftCall);
            }
            
            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo ingresar el servicio, sp [usp_GM_SERVICIOInsert] " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo ingresar el servicio, " + ex.Message, ex);
            }
        }

        public void setIngresarScript(string script, string activo, string ruta1)
        {
            DbCommand cmd = db.GetStoredProcCommand("usp_GM_SCRIPTInsert");

            db.AddInParameter(cmd, "@NOMBRE_SCRIPT", DbType.String, script);
            db.AddInParameter(cmd, "@ACTIVO", DbType.String, activo);
            db.AddInParameter(cmd, "@RUTA_ARCHIVO_1", DbType.String, ruta1);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo ingresar el script, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo ingresar el script, " + ex.Message, ex);
            }
        }


        public DataSet getBuscarClientePorEmail(string email)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarClientePorEmail");

            db.AddInParameter(cmd, "@email", DbType.String, email);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("Error buscar el cliente, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("Error buscar el cliente, " + ex.Message, ex);
            }
        }


        public DataSet getBuscarFechaIngresoTicket(string ticket)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarFechaIngresoTicket");

            db.AddInParameter(cmd, "@ticket", DbType.String, ticket);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("Error al buscar la fecha de ingreso del ticket, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar la fecha de ingreso del ticket, " + ex.Message, ex);
            }
        }

        public DataSet GetBuscarServicio(int IdServicio, int? Activo = null)
        {
            //DbCommand cmd = db.GetStoredProcCommand("usp_GM_SERVICIOSelectNO");
            DbCommand cmd = db.GetStoredProcCommand("usp_GM_SERVICIOSelect");


            if (IdServicio != 0)
            {
                db.AddInParameter(cmd, "@ID_SERVICIO", DbType.String, IdServicio);
            }
            if (Activo != null)
            {
                db.AddInParameter(cmd, "@Activo", DbType.String, Activo);
            }

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("Error al buscar el Servicio, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar el Servicio, " + ex.Message, ex);
            }
        }

        public DataSet GetBuscarZona(string ID_ZONA, int? ACTIVO = null)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarZona");


            if (!string.IsNullOrEmpty(ID_ZONA))
            {
                db.AddInParameter(cmd, "@ID_ZONA", DbType.String, ID_ZONA);
            }

            if (ACTIVO != null)
            {
                db.AddInParameter(cmd, "@ACTIVO", DbType.String, ACTIVO);
            }

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("Error al buscar la Zona, sp [stp_BuscarZona] " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar la Zona, " + ex.Message, ex);
            }
        }

        public DataSet GetBuscarServicioPorSoftCall(string softCall, int? Activo = null)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarServicioPorSoftCall");

            if (!string.IsNullOrEmpty(softCall))
            {
                db.AddInParameter(cmd, "@ID_SOFTCALL", DbType.String, softCall);
            }

            if (Activo != null)
            {
                db.AddInParameter(cmd, "@activo", DbType.String, Activo);
            }

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("Error al buscar servicio por softCall, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar servicio softCall, " + ex.Message, ex);
            }
        }


        public DataSet GetBuscarServicioConId(int IdServicio, int? Activo)
        {
            DbCommand cmd = db.GetStoredProcCommand("usp_GM_SERVICIOSelectConId");
            //DbCommand cmd = db.GetStoredProcCommand("usp_GM_SERVICIOSelect");

            if (IdServicio != 0)
            {
                db.AddInParameter(cmd, "@ID_SERVICIO", DbType.String, IdServicio);
            }
            if (Activo != null)
            {
                db.AddInParameter(cmd, "@Activo", DbType.String, Activo);
            }

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("Error al buscar servicio Ingreso ticket, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar servicio Ingreso ticket, " + ex.Message, ex);
            }
        }

        public DataSet GetBuscarServicioNotiticacion(int ID_SERVICIO, int IDENTIFICADOR)
        {
            DbCommand cmd = db.GetStoredProcCommand("usp_GM_SERVICIO_NOTIFICACIONSelect");


            if (ID_SERVICIO != 0)
            {
                db.AddInParameter(cmd, "@ID_SERVICIO", DbType.String, ID_SERVICIO);
            }

            if (IDENTIFICADOR != 0)
            {
                db.AddInParameter(cmd, "@IDENTIFICADOR", DbType.String, IDENTIFICADOR);
            }

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("Error al buscar Servicio Notificación, sp [usp_GM_SERVICIO_NOTIFICACIONSelect] " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar Servicio Notificación, " + ex.Message, ex);
            }
        }

        public string setIngresarServicioNotiticacion(int ID_SERVICIO, string NOMBRE, string EMAIL, string TELEFONO)
        {
            DbCommand cmd = db.GetStoredProcCommand("usp_GM_SERVICIO_NOTIFICACIONInsert");

            db.AddInParameter(cmd, "@ID_SERVICIO", DbType.String, ID_SERVICIO);

            if (!string.IsNullOrEmpty(NOMBRE))
            {
                db.AddInParameter(cmd, "@NOMBRE", DbType.String, NOMBRE);
            }

            if (!string.IsNullOrEmpty(EMAIL))
            {
                db.AddInParameter(cmd, "@EMAIL", DbType.String, EMAIL);
            }

            if (!string.IsNullOrEmpty(TELEFONO))
            {
                db.AddInParameter(cmd, "@TELEFONO", DbType.String, TELEFONO);
            }

            try
            {
                //db.ExecuteNonQuery(cmd);
                string val = db.ExecuteScalar(cmd).ToString();
                return val;
            }
            catch (SqlException ex)
            {
                throw new Exception("error al ingresar Servicio Notificación, sp [usp_GM_SERVICIO_NOTIFICACIONInsert] " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("error al ingresar Servicio Notificación, " + ex.Message, ex);
            }
        }

        public void setEliminarServicioNotiticacion(int ID_SERVICIO, int IDENTIFICADOR)
        {
            DbCommand cmd = db.GetStoredProcCommand("usp_GM_SERVICIO_NOTIFICACIONDelete");

            db.AddInParameter(cmd, "@ID_SERVICIO", DbType.String, ID_SERVICIO);
            db.AddInParameter(cmd, "@IDENTIFICADOR", DbType.String, IDENTIFICADOR);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo eliminar el Servicio Notificación, sp [usp_GM_SERVICIO_NOTIFICACIONDelete] " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo eliminar el Servicio Notificación, " + ex.Message, ex);
            }
        }

        public DataSet GetBuscarCargo(int IdServicio)
        {
            DbCommand cmd = db.GetStoredProcCommand("usp_GM_CARGOSelect");

            if (IdServicio != 0)
            {
                db.AddInParameter(cmd, "@IdServicio", DbType.String, IdServicio);
            }

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("Error al buscar cargo, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar cargo, " + ex.Message, ex);
            }
        }
        public DataSet GetBuscarScript(int IdScript)
        {
            DbCommand cmd = db.GetStoredProcCommand("usp_GM_SCRIPTSelect");

            if (IdScript != 0)
            {
                db.AddInParameter(cmd, "@Id_Script", DbType.String, IdScript);
            }

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("Error al buscar Script, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar Script, " + ex.Message, ex);
            }
        }

        public DataSet GetBuscarServicioScript(int IdServicio)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarServicioScript");

            db.AddInParameter(cmd, "@ID_SERVICIO", DbType.Int16, IdServicio);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("Error al buscar Servicio Script, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar Servicio Script, " + ex.Message, ex);
            }
        }

        public DataSet GetBuscarEmisor(int activo)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarEmisor");

            if (activo != 0)
            {
                db.AddInParameter(cmd, "@activo", DbType.String, activo);
            }

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("Error al buscar el Emisor, [stp_BuscarEmisor] " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar el Emisor, " + ex.Message, ex);
            }
        }

        public DataSet GetBuscarSocio(int activo)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarSocio");

            if (activo != 0)
            {
                db.AddInParameter(cmd, "@activo", DbType.String, activo);
            }

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("Error al buscar el Socio, [stp_BuscarSocio] " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar el Socio, " + ex.Message, ex);
            }
        }

        public void setEliminarServicio(int IdServicio)
        {
            DbCommand cmd = db.GetStoredProcCommand("usp_GM_SERVICIODelete");
            db.AddInParameter(cmd, "@ID_SERVICIO", DbType.String, IdServicio);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo eliminar el Servicio, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo eliminar el Servicio, " + ex.Message, ex);
            }
        }

        public void setEliminarScript(int IdScript)
        {
            DbCommand cmd = db.GetStoredProcCommand("usp_GM_SCRIPTDelete");
            db.AddInParameter(cmd, "@ID_SCRIPT", DbType.String, IdScript);

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo eliminar el SCRIPT, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo eliminar el SCRIPT, " + ex.Message, ex);
            }
        }

        public DataSet getBuscarAtencionFormularioPorId(string Ticket)
        {
            DbCommand cmd = db.GetStoredProcCommand("[usp_GM_ATENCION_FORMULARIOSelect]");

            if (Ticket != "")
            {
                db.AddInParameter(cmd, "@ID_ATENCION", DbType.String, Ticket);
            }

            try
            {
                //wait Task.Run(() =>  
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar el Formulario Atención, sp [usp_GM_ATENCION_FORMULARIOSelect] " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar el Formulario Atención, " + ex.Message, ex);
            }
        }
        public DataSet getBuscarScriptRespuesta(int idTipificacion)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarScriptRespuesta");

            db.AddInParameter(cmd, "@tipificacion", DbType.String, idTipificacion);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("Error al buscar , " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar, " + ex.Message, ex);
            }
        }

        public DataSet getBuscarEmails(int IdEmail)
        {
            DbCommand cmd = db.GetStoredProcCommand("usp_GM_CONF_EMAILSelect");

            if (IdEmail != 0)
            {
                db.AddInParameter(cmd, "@ID", DbType.String, IdEmail);
            }

            try
            {
                //wait Task.Run(() =>  
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar sp [usp_GM_CONF_EMAILSelect] " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar, " + ex.Message, ex);
            }
        }

        public DataSet getBuscarEmail(int IdEmail)
        {
            DbCommand cmd = db.GetStoredProcCommand("usp_GM_EMAILSelect");

            if (IdEmail != 0)
            {
                db.AddInParameter(cmd, "@ID_EMAIL", DbType.String, IdEmail);
            }

            try
            {
                //wait Task.Run(() =>  
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar sp [usp_GM_EMAILSelect] " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar, " + ex.Message, ex);
            }
        }

        public DataSet getBuscarEmailCasilla(string idEmail)
        {
            DbCommand cmd = db.GetStoredProcCommand("stp_BuscarEmailCasilla");

            db.AddInParameter(cmd, "@idEmail", DbType.String, idEmail);

            try
            {
                return db.ExecuteDataSet(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo buscar la casilla, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo buscar la casilla, " + ex.Message, ex);
            }
        }

        //@EMAIL_UPCOM varchar(50) = NULL,
        //@ID_GMAIL varchar(250) = NULL,
        //@DE varchar(200) = NULL,
        //@SUBJECT varchar(500) = NULL,
        //@BODY varchar(MAX) = NULL,
        //@DATE datetime = NULL,
        //@TIPIFICADO tinyint = NULL,
        //@FECHA datetime = NULL,
        //@PARA varchar(200) = NULL,
        //@SPAM tinyint = NULL,
        //@ID_ATENCION int = NULL

        public void setIngresarEmailUpcom(string EmailUpcom, string IdGmail, string de, string subject, string body, string date, int tipificado, string para, int spam, int IdAtencion)
        {
            DbCommand cmd = db.GetStoredProcCommand("usp_GM_EMAILInsert");

            if (!string.IsNullOrEmpty(EmailUpcom))
            {
                db.AddInParameter(cmd, "@EMAIL_UPCOM", DbType.String, EmailUpcom);
            }

            db.AddInParameter(cmd, "@ID_GMAIL", DbType.String, IdGmail);
            db.AddInParameter(cmd, "@DE", DbType.String, de);
            db.AddInParameter(cmd, "@SUBJECT", DbType.String, subject);
            db.AddInParameter(cmd, "@BODY", DbType.String, body);
            db.AddInParameter(cmd, "@DATE", DbType.String, date);

            db.AddInParameter(cmd, "@TIPIFICADO", DbType.String, tipificado);

            db.AddInParameter(cmd, "@PARA", DbType.String, para);

            if (spam != 0)
            {
                db.AddInParameter(cmd, "@SPAM", DbType.String, spam);
            }
            if (IdAtencion != 0)
            {
                db.AddInParameter(cmd, "@ID_ATENCION", DbType.String, IdAtencion);
            }

            try
            {
                db.ExecuteNonQuery(cmd);
            }
            catch (SqlException ex)
            {
                throw new Exception("No se pudo ingresar el script, " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo ingresar el script, " + ex.Message, ex);
            }
        }
    }

}
